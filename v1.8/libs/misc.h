﻿// -*- coding:utf-8-with-signature -*-
// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

// 細々した関数群

#ifndef QSNICOLA_MISC
#define QSNICOLA_MISC

#include <vector>
//#include <list>
//#include <set>
#include <string>
#include <ctype.h>
#include <map>          // for CFont
#include <X11/Xlib.h>
#include <string.h>

#ifdef DEBUG
extern void q_trace(const char* format, ...);
extern void q_trace_set_file(const char* fname);
#define TRACE q_trace
#define TRACE_SET_FILE q_trace_set_file
#define VERIFY(f) assert(f)
#else // DEBUG
inline void q_trace(const char*, ...) { }
inline void q_trace_set_file(const char*) { }
#define TRACE true ? (void) 0 : q_trace
#define TRACE_SET_FILE true ? (void) 0 : q_trace_set_file
#define VERIFY(f) ((void)(f))
#endif

////////////////////////////////////////////////////////////////////////

template <typename C__>
C__* skip_space(C__* p)
{
    while (*p && strchr(" \t\r\n", *p))
        p++;
    return p;
}

extern bool createFolder(const char* path);
extern int char_length(const char* b, const char* e);
extern int byte_length(const char* s, int char_len);
extern void error(const char* format, ...);
extern std::string getHomeDir();
extern bool isYesString(const char* s);


////////////////////////////////////////////////////////////////////////
// SizeList

template <typename Ty>
class SizeList: public std::vector<Ty>
{
public:
    typedef std::vector<Ty> super;
    typedef typename super::const_iterator const_iterator;

    Ty sum(int first, int last) const
        // [first, last)の合計
    {
        Ty r = 0;
        const_iterator i;
        for (i = super::begin() + first;
             i != super::end() && i != super::begin() + last; i++) {
            r += *i;
        }
        return r;
    }
    void replace(int pos, int n, const Ty& val)
        // posからn要素を一つのvalで置き換える
    {
        erase(super::begin() + pos, super::begin() + pos + n - 1);
        super::operator [](pos) = val;
    }
};

////////////////////////////////////////////////////////////////////////
// X11

extern int getColor(Display* disp, const char* name);
extern bool isToplevelWindow(Display* disp, Window w);
extern Window getToplevelWindow(Display* disp, Window w);

////////////////////////////////////////////////////////////////////////
// CPoint

class CPoint: public XPoint
{
public:
    CPoint();
    CPoint(int x, int y);
    CPoint(const XPoint& );
    virtual ~CPoint();

    CPoint operator + (const XPoint& a) const;
    CPoint operator - (const XPoint& a) const;
    CPoint& operator = (const XPoint& a);
};

////////////////////////////////////////////////////////////////////////
// CRect

class CRect: public XRectangle
{
public:
    CRect();
    CRect(int x, int y, int width, int height);
    CRect(const XRectangle& );
    virtual ~CRect();

    CRect& operator = (const XRectangle& a);
};

////////////////////////////////////////////////////////////////////////
// CDimension

class CDimension
{
public:
    int width, height;
    CDimension(): width(0), height(0) { }
};

///////////////////////////////////////////////////////////////////////
// CFont

extern XFontSet createFont(
    Display* disp,
    const char* name,
    const char* default_font = "-alias-fixed-medium-r-normal--16-*-*-*-*-*-*");

class CFont
{
    struct ref {
        XFontSet font;
        int count;
        ref(): font(0), count(0) { }
    };
    typedef std::map<std::string, ref*> FontMap;

    Display* disp;
    std::string cur_name;
    static FontMap fm;
    
public:
    CFont();
    virtual ~CFont();
    bool create(Display* disp, const char* name);
    void destroy();
    XFontSet xfont() const;
    
private:
    CFont(const CFont& );               // not implement
    CFont& operator = (const CFont& );  // not implement
};

#endif  // QSNICOLA_MISC
