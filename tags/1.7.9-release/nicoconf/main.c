// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

// Q's Nicolatter�������

#include "../config.h"

#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "interface.h"
#include "support.h"

#include "../keymap.h"
#include "../global.h"

GtkWindow* main_window;

int
main (int argc, char *argv[])
{
    setenv("LC_CTYPE", JA_EUCJP_LOCALE_NAME, 1);
    gtk_set_locale();
    gtk_init (&argc, &argv);

    top_display = GDK_DISPLAY();
    global_prop.load();

    main_window = GTK_WINDOW(create_nicoconf());
    gtk_widget_show(GTK_WIDGET(main_window));

  gtk_main ();
  return 0;
}

