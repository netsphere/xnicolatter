// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// このファイル
//     IIIMクライアントとの通信パケット

#include "../config.h"

#include <cstdio>
#include <string>
#include <cassert>
#include <cerrno>
#include <unistd.h>

#include "XimProto.h"
#include "preedit.h"
#include "status.h"
#include "client.h"
#include "iiimtrans.h"
#include "iiimdisp.h"

#include <EncString.h>
#include <misc.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// IIIMPWriter

IIIMPWriter::IIIMPWriter(bool endian, int opcode): SendBuffer(endian)
{
    buf[0] = opcode;
    cur = buf + 4; // TODO: 8バイトヘッダの考慮
}

IIIMPWriter::~IIIMPWriter()
{
}

int IIIMPWriter::getData(unsigned char* out)
{
    assert(reserved.size() == 0);

    pad();
    ssize_t len = cur - buf;
    assert(!(len % 4));
    if (!out)
        return len;

    // specにはThe length includs the length of header.とあるが，
    // どの実装も含んでいない
    int siz = (len - 4) / 4; // TODO: 8バイトヘッダの考慮
    buf[1] = (siz >> 16) & 0xff;
    buf[2] = (siz >> 8) & 0xff;
    buf[3] = siz & 0xff;
    debug_out();

    memcpy(out, buf, len);
    cur = buf + 4;
    return len;
}

void IIIMPWriter::uniString(const char* s)
{
    EncString str(s, strlen(s), "EUC-JP");
    str.convert("UTF-16");
    reserve16(); 
    const unsigned char* p = str.data();
    int i = 0;
    if (p[0] == 0xff && p[1] == 0xfe && bigEndian
        || p[0] == 0xfe && p[1] == 0xff && !bigEndian) {
        // バイトを入れ替え
        i += 2;
        for ( ; i < str.length(); i += 2) {
            *cur++ = p[i + 1];
            *cur++ = p[i];
        }
    }
    else if (p[0] == 0xff && p[1] == 0xfe && !bigEndian
             || p[0] == 0xfe && p[1] == 0xff && bigEndian) {
        i += 2;
        for ( ; i < str.length(); i++)
            *cur++ = p[i];
    }
    else {
        // 環境のエンディアンと（たぶん）同じ
        for ( ; i < str.length(); i += 2) {
            if (bigEndian) {
                *cur++ = ((*(uint16_t*)(p + i)) >> 8) & 0xff;
                *cur++ = (*(uint16_t*)(p + i + 1)) & 0xff;
            }
            else {
                *cur++ = (*(uint16_t*)(p + i)) & 0xff;
                *cur++ = ((*(uint16_t*)(p + i + 1)) >> 8) & 0xff;
            }
        }
    }
    store_back(len_from_reserved()); // バイト単位
    pad();
}

////////////////////////////////////////////////////////////////////////
// IIIMPReader

IIIMPReader::IIIMPReader(bool endian): RecvBuffer(endian)
{
}

bool IIIMPReader::input(int fd)
{
    assert(filled == false);

    ssize_t r;
    int len; // ヘッダを含むバイト数

    // 4-Byte Length Packet Header
    if (!buf)
        buf = (unsigned char*) malloc(4);
    while (count_ < 4) {
        r = read(fd, buf + count_, 4 - count_);
        if (r == -1) {
            if (errno == EAGAIN)
                return true;
            else if (errno == EINTR)
                continue;
            else {
                assert(0); // TODO: error handle
            }
        }
        if (r == 0)
            goto closed;
        count_ += r;
    }
    
    // 8-Byte Length Packet Header
    if (count_ == 4 && (buf[0] & 0x80) != 0) {
        buf = (unsigned char*) realloc(buf, 8);
        assert(buf);
        while (count_ < 8) {
            r = read(fd, buf + count_, 8 - count_);
            if (r == -1) {
                if (errno == EAGAIN)
                    return true;
                else if (errno == EINTR)
                    continue;
                else {
                    assert(0); // TODO: error handle
                }
            }
            if (r == 0)
                goto closed;
            count_ += r;
        }
    }
    
    if ((buf[0] & 0x80) == 0) {
        len = 4 + ((buf[1] << 16) + (buf[2] << 8) + buf[3]) * 4;
        assert(len > 0);
    }
    else {
        assert(0); // 64ビットintが必要？
    }

    buf = (unsigned char*) realloc(buf, len);
    assert(buf);
    while (count_ < len) {
        r = read(fd, buf + count_, len - count_);
        if (r == -1) {
            if (errno == EAGAIN)
                return true;
            else if (errno == EINTR)
                continue;
            else {
                assert(0); // TODO: error handle
            }
        }
        if (r == 0)
            goto closed;
        count_ += r;
    }
    debug_out();

    if (buf[0] == IM_CONNECT) {
        if (buf[4] == 0x42)
            bigEndian = true;
        else if (buf[4] == 0x6c)
            bigEndian = false;
        else {
            error("IIIMP transport endian error.\n");
            assert(0);
        }
    }
    
    cur = buf + 4;
    filled = true;
    return true;

closed:
    TRACE("IIIMP recv: client closed\n");
    cur = buf + 4;
    count_ = 0;
    filled = false;
    return false;
}

IIIMPReader::~IIIMPReader()
{
    if (buf) {
        free(buf);
        buf = NULL;
    }
}

int IIIMPReader::opcode() const
{
    return buf[0] & 0x7f;
}

uint16_t IIIMPReader::card8()
{
    return *cur++;
}

KeyEvent IIIMPReader::keyevent()
{
    KeyEvent e;
    e.keycode = card32();
    e.keychar = card32();
    e.modifier = card32();
    e.time = card32();
    return e;
}
