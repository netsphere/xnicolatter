
#define CANNA_NEW_WCHAR_AWARE
//#define CANNA_WCHAR16

// Canna 3.7p3 は、32bit widechar が正常に動作しないのでは??
// => 64bit UNIX (LP64環境) では、long の長さが 64bit になるため、か? (未確認)

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
//#include <curses.h> // X/Open Curses, getch()
#include <locale.h>
#include <canna/jrkanji.h>
#include "common.h"

int main()
{
    unsigned char** warning = NULL;
    char utf_buf[1000];

    setlocale(LC_ALL, "");
    //initscr();
    //noecho();
    
    // 初期化。Context は 0 固定.
    int r;
    if ( (r = wcKanjiControl(0, KC_INITIALIZE, (char*) &warning)) == -1) {
        printf("Cannaライブラリの初期化に失敗: %s\n", jrKanjiError);
        return 1;
    }
    printf("ret = %d\n", r);
    if (warning) {
      for (unsigned char** p = warning; *p; p++) {
        euc_to_utf8(utf_buf, *p, 1000);  // メッセージはEUC-JP!
        printf("Warning: %s\n", utf_buf);
      }
      return 1;
    }

    int context = rand();

    // モード設定
    wcKanjiStatus status;
    constexpr int BUF_SIZE = 1000;
    cannawc work_buf[BUF_SIZE];

    wcKanjiStatusWithValue ksv;
    ksv.ks = &status;
    ksv.buffer = work_buf;
    ksv.n_buffer = BUF_SIZE;
    ksv.val = CANNA_MODE_HenkanMode;
    wcKanjiControl(context, KC_CHANGEMODE, (char*) &ksv);

    // 入力を渡していく
    int ch ;
    while ( (ch = getchar()) != EOF ) {
        //move(1, 1);
        printf("input: ch=%d ", ch);
        int result = wcKanjiString(context, ch,
                                   (cannawc*) work_buf,
                               BUF_SIZE, // バイト数ではなく要素数
                               &status);
        // 確定文字列あり
        if (result > 0) {
            work_buf[result] = '\0';
            cannawcs_to_utf8(utf_buf, work_buf, 1000);
            //euc_to_utf8(utf_buf, work_buf, 1000);
            //move(3, 1);
            printf("%d %s %s ",
                   result, dump((unsigned char*) work_buf, result * sizeof(cannawc)),
                   utf_buf);
        }
    }
  
    wcKanjiControl(0, KC_FINALIZE, 0);
    //endwin();
    return 0;
}
