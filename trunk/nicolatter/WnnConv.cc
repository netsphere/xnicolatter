// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"
#ifdef USE_WNN

#include <clocale>
#include <cstdlib>
#include <fstream>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "../jllib.h"

#include "WnnConv.h"
#include "WnnWindow.h"
#include "../global.h"
#include "client.h"
#include "LevelSelector.h"
#include "../qWnn.h"

using namespace std;

///////////////////////////////////////////////////////////////////////
// RomaKanaDef

unsigned char SEP = 0xff;

class RomaKanaDef
{
public:
    string roma, kana;

    RomaKanaDef(const string& r, const string& k);
    virtual ~RomaKanaDef();
    int getReplaceLen() const;
        // replace bytes, if 'x/y' -> x bytes
};

RomaKanaDef::RomaKanaDef(const string& r, const string& k)
                                                : roma(r), kana(k)
{
}

RomaKanaDef::~RomaKanaDef()
{
}

int RomaKanaDef::getReplaceLen() const
{
    const char* s = roma.c_str();
    const char* p = s;
    while (*p && *(unsigned char*) p != SEP)
        p += mblen(p, MB_CUR_MAX);
    return p - s;
}

///////////////////////////////////////////////////////////////////////
// WnnConv

WnnConv::RomaKanaDefs WnnConv::romaDefs;

WnnConv::WnnConv(InputContext* ic): super(ic), candWindow(NULL),
                    kana_mode(MODE_OFF), last_mode(MODE_OFF)
{
    wnn = new qWnn();
    assert(wnn);
    clear();
}

WnnConv::~WnnConv()
{
    delete wnn;
    delete candWindow;
}

void WnnConv::convertRoma()
    // 入力中文字列 = 'jkjkjkjkjkjk'のとき，
    //      'u'を入れて，'jkjkjkじゅkjkjk'になる。
{
    if (!global_prop.use_roma || kana_mode == MODE_ALPHA)
        return;

    for (int pos = 0; pos < charSizes.size(); pos++) {
        // ローマ字の後ろに仮名文字が続いていることがある
        int romalen;    // ローマ字のバイト数
        for (romalen = 0; romalen < pos + charSizes.size()
                    && statusList[pos + romalen] == CHAR_ROMA
                    && pos + romalen != caret; romalen++) {
            // caretと比較するのは，
            //      'j<caret>k'で'k' -> 'jkk'
            //      'jk<caret>'で'k' -> 'jっk'
        }
        if (romalen == 0)
            continue;
        bool last_caret = pos + romalen == caret;
            // キャレットで終わる場合はbuf = 'n'でdef = 'n', 'nn'を解決できない
        romalen = charSizes.sum(pos, pos + romalen);

        // 最長一致によってローマ字定義を調べる
        RomaKanaDefs::const_iterator i;
        int sellen = 0;
        RomaKanaDefs::const_iterator sel;
        int count = 0;
        for (i = romaDefs.begin(); i != romaDefs.end(); i++) {
            int mlen = matchRoma(preconv.c_str() + charSizes.sum(0, pos),
                            romalen, last_caret, (*i)->roma.c_str());
                // 入力文字列'n<caret>'に対し，ローマ字定義'n'と'nn'はいずれも1を返す
                // 入力文字列'n<null>'に対し，ローマ字定義'n'は1，'nn'は0を返す
            if (mlen > sellen) {
                sel = i;
                sellen = mlen;
                count = 1;
            }
            else if (mlen == sellen)
                count++;
        }

        // 定義'd/d'のとき，sellen > getReplaceLen()
        //  sellen < getReplaceLen()は，buf = 'sh', def = 'shi'の
        //  時だから，変換しない
        if (count == 1 && sellen >= (*sel)->getReplaceLen()) {
            int repl_len = (*sel)->getReplaceLen(); // 置き換えるバイト長
            preconv.replace(charSizes.sum(0, pos), repl_len, (*sel)->kana);
            // ローマ字の文字区切りと採用したローマ字定義の区切りが一致してい
            // るとは限らない。
            // 入力ローマ字での区切りを求める
            romalen = 0;
            for (count = 0; romalen < repl_len; count++)
                romalen += charSizes[pos + count];

            if (caret > pos)
                caret -= count - 1;
            charSizes.replace(pos, count, (*sel)->kana.length());
            statusList.replace(pos, count, CHAR_READ);
            if (romalen > repl_len) {
                charSizes.insert(charSizes.begin() + pos + 1, romalen - repl_len);
                statusList.insert(statusList.begin() + pos + 1, CHAR_ROMA);
            }
        }
    }
}

int WnnConv::matchRoma(
        const char* buf,        // 入力文字
        int len,                // 入力ローマ字の最大バイト長
        bool last_caret,        // 入力文字列の後ろにキャレットがあるか
        const char* def) const  // 調べるローマ字定義
    // 戻り値
    //      マッチしたとき，そのバイト長
    //          'd/d'定義の時 = 2（'/'を除いたバイト長）
    //      マッチしなかったとき，0
    // 例
    //      buf = 'nd', def = 'n'    -> ret = 1
    //      buf = 'shh', def = 'shi'  -> ret = 0
    //      buf = 'sh<caret>', def = 'shi' -> ret = 2
    //      buf = 'sh<null>', def = 'shi' -> ret = 0    // 文末の'n'を変換するのに重要
{
    if (!last_caret && strlen(def) > len)
        return 0;

    const char* p = buf;
    int retval = 0;
    while (*p && *def && len > 0) {
        if (*(unsigned char*) def == SEP)
            def++;
        else {
            if (global_prop.roma_strict) {
                if (*p != *def)
                    return 0;
            }
            else {
                if (tolower(*p) != tolower(*def))
                    return 0;
            }

            p++; def++;
            retval++;
            len--;
        }
    }
    return retval;
}

bool WnnConv::loadRomaDef(
        const char* filename)   // ローマ字定義ファイルのファイル名
    // 機能
    //      定義を読み込む
{
    romaDefs.clear();

    ifstream ifs;
    if (!filename || (ifs.open(filename), !ifs.is_open()))
        return false;

    char buf[1000];
    while (!ifs.eof()) {
        ifs.getline(buf, sizeof(buf));
        const char* p = buf;
        string roma, kana;

        while (*p && (*p == ' ' || *p == '\t'))
            p++;
        if (!*p)
            continue;
        while (*p && *p != ' ' && *p != '\t') {
            if (*p == '/') {
                p++;
                roma += SEP;
                continue;
            }
            else if (*p == '\\')
                p++;

            roma += *p++;
        }
        if (!*p)
            continue;
        while (*p && (*p == ' ' || *p == '\t'))
            p++;
        if (!*p)
            continue;
        while (*p && *p != ' ' && *p != '\t')
            kana += *(p++);

        romaDefs.push_back(new RomaKanaDef(roma, kana));
    }

    return true;
}

void WnnConv::through()
{
    // TRACE("THROUGH\n");
    if (im_state != MI_NYURYOKU) {
        all_determine();
        im_state = MI_NYURYOKU;
    }
    result_stat = THROUGH;
}

void WnnConv::na()
{
    // TRACE("NONE\n");
    result_stat = NONE;
}

void WnnConv::kanji()
{
    // TRACE("mode to alpha\n");
    if (im_state != MI_NYURYOKU) {
        all_determine();
        im_state = MI_NYURYOKU;
    }
    result_stat = CONTROL_FUNC; // 仮名モード -> 英字モード
    conv_count = 0;
}

struct NormalizePair {
    const char* s1;
    const char* s2;
};

static const NormalizePair norm_pair[] = {
    { "か゛", "が" }, { "き゛", "ぎ" }, { "く゛", "ぐ" }, { "け゛", "げ" }, { "こ゛", "ご" },
    { "さ゛", "ざ" }, { "し゛", "じ" }, { "す゛", "ず" }, { "せ゛", "ぜ" }, { "そ゛", "ぞ" },
    { "た゛", "だ" }, { "ち゛", "ぢ" }, { "つ゛", "づ" }, { "て゛", "で" }, { "と゛", "ど" },
    { "は゛", "ば" }, { "ひ゛", "び" }, { "ふ゛", "ぶ" }, { "へ゛", "べ" }, { "ほ゛", "ぼ" },
    { "カ゛", "ガ" }, { "キ゛", "ギ" }, { "ク゛", "グ" }, { "ケ゛", "ゲ" }, { "コ゛", "ゴ" },
    { "サ゛", "ザ" }, { "シ゛", "ジ" }, { "ス゛", "ズ" }, { "セ゛", "ゼ" }, { "ソ゛", "ゾ" },
    { "タ゛", "ダ" }, { "チ゛", "ヂ" }, { "ツ゛", "ヅ" }, { "テ゛", "デ" }, { "ト゛", "ド" },
    { "ハ゛", "バ" }, { "ヒ゛", "ビ" }, { "フ゛", "ブ" }, { "ヘ゛", "ベ" }, { "ホ゛", "ボ" },
    { "は゜", "ぱ" }, { "ひ゜", "ぴ" }, { "ふ゜", "ぷ" }, { "へ゜", "ぺ" }, { "ほ゜", "ぽ" },
    { "ハ゜", "パ" }, { "ヒ゜", "ピ" }, { "フ゜", "プ" }, { "ヘ゜", "ペ" }, { "ホ゜", "ポ" },
    { "ウ゛", "ヴ" },
    { NULL, NULL }
};

void WnnConv::addChar(const char* str)
    // 機能
    //      編集バッファに追加する
    // 解説
    //      strは1文字でなければならない
{
redo:
    switch (im_state)
    {
    case MI_NYURYOKU:
    case HENKAN_MAE:
        if (!strcmp(str, "゛") || !strcmp(str, "゜")) {
            int pos = charSizes.sum(0, caret) - 2;
            if (pos >= 0) {
                for (int i = 0; norm_pair[i].s1; i++) {
                    if (norm_pair[i].s1[0] == preconv[pos]
                            && norm_pair[i].s1[1] == preconv[pos + 1]
                            && !strcmp(norm_pair[i].s1 + 2, str)) {
                        preconv[pos] = norm_pair[i].s2[0];
                        preconv[pos + 1] = norm_pair[i].s2[1];
                        return;
                    }
                }
            }
        }
        preconv.insert(charSizes.sum(0, caret), str);
        charSizes.insert(charSizes.begin() + caret, strlen(str));
        statusList.insert(statusList.begin() + caret,
                    kana_mode != MODE_ALPHA ? CHAR_ROMA : CHAR_READ);
        caret++;
        im_state = HENKAN_MAE;
        convertRoma();
        break;
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        all_determine();
        im_state = MI_NYURYOKU;
        goto redo;
        break;
    default:
        assert(0);
    }
}

void WnnConv::space()
    // 空白入力
    // 一つのキーで制御機能と図形文字の両方を生成できないので，
    // [Space]のために用意した。
{
    addChar("　");  // TODO: 半角固定の時
}

void WnnConv::query()
{
    converted = "";
    int pos = 0;
    conv_i.clear();
    read_i.clear();
    conv_i.push_back(0);

    for (int i = 0; i < wnn->countClause(); i++) {
        converted += wnn->getComposition(i);
        conv_i.push_back(converted.length());
        read_i.push_back(pos);
        int read_len = wnn->getReadLength(i);
        // TRACE("read_len = %d\n", read_len);
        int count;
        for (count = 0; charSizes.sum(pos, pos + count) < read_len; count++)
            ;
        if (charSizes.sum(pos, pos + count) > read_len) {
            // 文字区切りと変換後の読みの区切りが違う場合
            charSizes.insert(charSizes.begin() + pos + count,
                        charSizes.sum(pos, pos + count)- read_len);
            statusList.insert(statusList.begin() + pos + count,
                        statusList[pos + count - 1]);
            charSizes[pos + count - 1] = read_len;
        }
        pos += count;
    }
    read_i.push_back(pos);
#if DEBUG > 1
    IndexList::const_iterator ii;
    TRACE("read_i = ");
    for (ii = read_i.begin(); ii != read_i.end(); ii++)
        TRACE("%d ", *ii);
    TRACE("\n");
#endif
}

void WnnConv::convert()
{
    caret = -1;
    convertRoma();  // 末尾の'n'をローマ字変換する

    wnn->convert(preconv.c_str());
    query();
    cur_clause = 0;
    im_state = ZEN_HENKAN;
    conv_count = 0;
}

void WnnConv::next_cand()
    // 次候補
{
    // TRACE("next_cand()\n");
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);
    switch (im_state)
    {
    case ZEN_HENKAN:
        if (++conv_count >= 2) {
            if (!candWindow)
                candWindow = new WnnCandidate(this, ic->preeditWindow);
            candWindow->clear();
            candWindow->setCandidates(wnn->getCandidateList(cur_clause));
            candWindow->select(wnn->nextCandidate(cur_clause, 1));
            im_state = KOUHO_ICHIRAN;
        }
        else {
            wnn->nextCandidate(cur_clause, 1);
        }
        query();
        break;
    case KOUHO_ICHIRAN:
        candWindow->select(wnn->nextCandidate(cur_clause, 1));
        query();
        break;
    default:
        assert(0);
    }
}

void WnnConv::prev_cand()
{
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);
    switch (im_state)
    {
    case ZEN_HENKAN:
        wnn->nextCandidate(cur_clause, -1);
        query();
        break;
    case KOUHO_ICHIRAN:
        candWindow->select(wnn->nextCandidate(cur_clause, -1));
        query();
        break;
    default:
        assert(0);
    }
}

void WnnConv::caret_left()
    // キャレット左
{
    assert(im_state == HENKAN_MAE);
    if (caret > 0)
        caret--;
    convertRoma();
}

void WnnConv::caret_right()
    // キャレット右
{
    assert(im_state == HENKAN_MAE);
    if (caret < charSizes.size())
        caret++;
    convertRoma();
}

void WnnConv::shrink()
{
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);

    int first = charSizes.sum(0, read_i[cur_clause]);
    int last = charSizes.sum(0, read_i[cur_clause + 1] - 1);
    // TRACE("first = %d, last = %d\n", first, last);
    if (first < last) {
        const char* p = preconv.c_str();
        int len = char_length(p + first, p + last);
        wnn->set_clause_len(cur_clause, len);
        query();
        conv_count = 0;
        im_state = ZEN_HENKAN;
    }
}

void WnnConv::expand()
{
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);

    if (cur_clause < wnn->countClause() - 1) {
        int first = charSizes.sum(0, read_i[cur_clause]);
        int last = charSizes.sum(0, read_i[cur_clause + 1] + 1);

        const char* p = preconv.c_str();
        int len = char_length(p + first, p + last);
        wnn->set_clause_len(cur_clause, len);
        query();
        conv_count = 0;
        im_state = ZEN_HENKAN;
    }
}

void WnnConv::clause_left()
{
    switch (im_state)
    {
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        im_state = ZEN_HENKAN;
        if (cur_clause > 0)
            cur_clause--;
        conv_count = 0;
        break;
    default:
        assert(0);
    }
}

void WnnConv::clause_right()
{
    switch (im_state)
    {
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        im_state = ZEN_HENKAN;
        if (cur_clause < wnn->countClause() - 1)
            cur_clause++;
        conv_count = 0;
        break;
    default:
        assert(0);
    }
}

void WnnConv::clause_det()
{
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);
    int clause_count = wnn->countClause();
    if (clause_count < 1)
        return;

    int len = wnn->getReadLength(0);
    preconv.erase(0, len);
    int i = 0;
    while (i < len) {
        i += charSizes[0];
        charSizes.erase(charSizes.begin());
        statusList.erase(statusList.begin());
    }

    determined = wnn->determineFirstClause();
    query();
    im_state = clause_count == 1 ? MI_NYURYOKU : ZEN_HENKAN;
    cur_clause = 0;
    conv_count = 0;
}

void WnnConv::all_determine()
    // 全確定
{
    switch (im_state)
    {
    case MI_NYURYOKU:
        determined = "";
        break;
    case HENKAN_MAE:
        determined = preconv;
        break;
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        determined = converted;
        wnn->updateLearning();
        break;
    default:
        assert(0);
    }
    conv_count = 0;
    clear();
}

void WnnConv::clear()
{
    preconv = "";
    charSizes.clear();
    statusList.clear();
    caret = 0;
    im_state = MI_NYURYOKU;
    conv_count = 0;
}

void WnnConv::left_erase()
{
    assert(im_state == HENKAN_MAE);
    if (caret > 0) {
        preconv.erase(charSizes.sum(0, caret - 1), charSizes[caret - 1]);
        charSizes.erase(charSizes.begin() + caret - 1);
        statusList.erase(statusList.begin() + caret - 1);
        caret--;
        im_state = preconv == "" ? MI_NYURYOKU : HENKAN_MAE;
            // この時点ではローマ字変換は入れない
            //      'kjk'で'j'を削除しても'っ'へ変換しない
    }
}

void WnnConv::right_erase()
{
    assert(im_state == HENKAN_MAE);
    if (caret < charSizes.size()) {
        preconv.erase(charSizes.sum(0, caret), charSizes[caret]);
        charSizes.erase(charSizes.begin() + caret);
        statusList.erase(statusList.begin() + caret);
        im_state = preconv == "" ? MI_NYURYOKU : HENKAN_MAE;
    }
}

void WnnConv::revert()
    // 全戻し
{
    assert(im_state == ZEN_HENKAN || im_state == KOUHO_ICHIRAN);
    caret = charSizes.size();
    im_state = HENKAN_MAE;
    conv_count = 0;
}

void WnnConv::head()
{
    if (im_state == HENKAN_MAE)
        caret = 0;
    else if (im_state == ZEN_HENKAN)
        cur_clause = 0;
}

void WnnConv::tail()
{
    if (im_state == HENKAN_MAE)
        caret = charSizes.size();
    else if (im_state == ZEN_HENKAN)
        cur_clause = wnn->countClause() - 1;
}

void WnnConv::fixed_alnum()
{
    if (kana_mode != MODE_ALPHA) {
        last_mode = kana_mode;
        kana_mode = MODE_ALPHA;
    }
    else
        kana_mode = last_mode;
}

void WnnConv::execGraphChar(const char* str, KanaKanjiStatus* charType)
{
    assert(str);
    assert(charType);

    determined = "";

    char buf[10];
    while (*str) {
        int len = mblen(str, MB_CUR_MAX);
        if (len == -1) {
            TRACE("str = '%s'\n", str);
            perror("mblen");
            assert(0);
        }
        strncpy(buf, str, len);
        buf[len] = 0;
        addChar(buf);
        str += len;
    }

    if (determined != "")
        *charType = GRAPHIC_CHAR;
    else
        *charType = NONE;

    if (im_state != KOUHO_ICHIRAN && candWindow)
        candWindow->clear();

    if (im_state == MI_NYURYOKU)
        caret = 0;      // convert()でcaret = -1にしてる
}

void WnnConv::execCtrlFunc(const CtrlFunc* func, KanaKanjiStatus* charType)
{
    assert(func);
    determined = "";
    result_stat = NONE;
    FuncPieceId piece = func->piece[im_state];

    for (int i = 0; conv_funcs[i].id; i++) {
        if (conv_funcs[i].id == piece) {
            (this->*(conv_funcs[i].exec))();
            break;
        }
    }
    *charType = result_stat;

    if (im_state != KOUHO_ICHIRAN && candWindow)
        candWindow->clear();

    if (im_state == MI_NYURYOKU)
        caret = 0;
}

const PieceIdExecPair WnnConv::conv_funcs[] = {
    { CF_THROUGH,       &WnnConv::through },
    { CF_NA,            &WnnConv::na },
    { CF_KANJI,         &WnnConv::kanji },
    { CF_SPACE,         &WnnConv::space },
    { CF_CONVERT,       &WnnConv::convert },
    { CF_NEXT_CAND,     &WnnConv::next_cand },
    { CF_PREV_CAND,     &WnnConv::prev_cand },
    { CF_CARET_LEFT,    &WnnConv::caret_left },
    { CF_SHRINK,        &WnnConv::shrink },
    { CF_CARET_RIGHT,   &WnnConv::caret_right },
    { CF_EXPAND,        &WnnConv::expand },
    { CF_CLAUSE_LEFT,   &WnnConv::clause_left },
    { CF_CLAUSE_RIGHT,  &WnnConv::clause_right },
    { CF_ALL_DETERMINE, &WnnConv::all_determine },
    { CF_CLEAR,         &WnnConv::clear },
    { CF_LEFT_ERASE,    &WnnConv::left_erase },
    { CF_RIGHT_ERASE,   &WnnConv::right_erase },
    { CF_REVERT,        &WnnConv::revert },   // 全戻し
    { CF_HEAD,          &WnnConv::head },
    { CF_TAIL,          &WnnConv::tail },
    { CF_CLAUSE_DET,    &WnnConv::clause_det },
    { CF_FIXED_ALNUM,   &WnnConv::fixed_alnum },
    { CF_ERROR, 0 }
};

KanaKanjiStatus WnnConv::input(const KeyEvent& event, int level)
    // wnn版
/*
    制御キー    配列を変更する
    図形キー    仮名モードのとき配列を変更，それ以外のとき元の配列
    未定義キー  元の配列を使う
*/
{
    assert(level >= 0 && level <= 2);

    if (getKanaMode() == MODE_OFF) {
        TRACE("WnnConv::input: modif = %d, keycode = %d\n",
              event.modifier, event.keycode);
        if (getKeyName2(event.modifier, event.keycode)
                                    == global_prop.kana_key) {
            if (event.type == KeyPress)
                setKanaMode(MODE_KANA);
            return NONE;
        }
        else
            return THROUGH;
    }
    
    if ((event.modifier & Mod1Mask) != 0) { // [Alt]
        if (im_state == MI_NYURYOKU)
            return THROUGH;
        else
            return NONE;
    }

    const Key* key = keyMap->find_key(getKeyName(event.modifier, event.keycode));
    KanaKanjiStatus status = NONE;

    if (key && key->getType() == FUNCTION_KEY) {
        // 制御キー
        if (!level) {
            const CtrlFunc* func = dynamic_cast<const FunctionKey*>(key)->func[level];
            if (func)
                execCtrlFunc(func, &status);
            return status;
        }
        else {
            if (im_state == MI_NYURYOKU)
                return THROUGH;   // [Shift] + カーソルキー
            else
                return NONE;
        }
    }

    // 図形キーまたは未定義
    if (!global_prop.use_keymap || kana_mode == MODE_ALPHA) {
        // QWERTY or 無変換固定
        XKeyEvent tmp = *event.xk;
        if (level)
            tmp.state |= ShiftMask;
        else
            tmp.state &= ~ShiftMask;
        char buf[100];
        KeySym sym;
        int r = XLookupString(&tmp, buf, sizeof(buf), &sym, NULL);
        if (r > 0 && buf[0] >= 0x20 && buf[0] <= 0x7e) {
            buf[r] = '\0';
            execGraphChar(buf, &status);
        }
        else {
            // 未定義
            if (im_state == MI_NYURYOKU)
                status = THROUGH;
            else
                status = NONE;
        }
    }
    else {
        // 仮名
        if (key && key->getType() == GRAPHIC_KEY) {
            string graph = dynamic_cast<const GraphicKey*>(key)
                                             ->getGraphChar(level);
            execGraphChar(graph.c_str(), &status);
        }
        else {
            if (im_state == MI_NYURYOKU)
                status = THROUGH;
            else
                status = NONE;
        }
    }

    return status;
}

KanaMode WnnConv::getKanaMode() const
{
    return kana_mode;
}

void WnnConv::setKanaMode(KanaMode mode)
{
    kana_mode = mode;
}

PreeditWindow* WnnConv::createPreeditWindow()
{
    return new WnnPreedit(this, ic);
}

StatusWindow* WnnConv::createStatusWindow()
{
    return new WnnStatus(this, ic);
}

void WnnConv::setCandidateVisible(bool v)
{
    if (candWindow)
        candWindow->setVisible(v);
}

void WnnConv::updateCandidate()
{
    if (candWindow)
        candWindow->update();
}

#endif  // USE_WNN
