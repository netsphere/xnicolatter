// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// XIM X transport / XIM VJE拡張 / Unicode Input
// Xイベントを使うプロトコルの実装

#include "../config.h"

#include <X11/Xatom.h>
#ifdef USE_GTK
  #include <gtk/gtk.h>
  #include <gdk/gdkx.h>
#else
  #include <X11/Intrinsic.h>
#endif  // USE_GTK

#include "imext.h"
#include "client.h"
#include "Connection.h"
#include "conv.h"
#include "start.h"

///////////////////////////////////////////////////////////////////////

static InputContext* findIC(Window focus)
{
    Connections::const_iterator i;
    for (i = clients.begin(); i != clients.end(); i++) {
        Connection::InputMethods::const_iterator j;
        for (j = (*i)->methods.begin(); j != (*i)->methods.end(); j++) {
            InputMethod::InputContexts::const_iterator k;
            for (k = (*j)->contexts.begin(); k != (*j)->contexts.end();
                    k++) {
                if ((*k)->focusWindow == focus)
                    return *k;
            }
        }
    }
    return NULL;
}

void setIMExtValue(Window focus, IMExtCmd cmdid)
{
    InputContext* ic = findIC(focus);
    if (!ic)
        return;

    ic->disabled = cmdid == IMEXT_HIDE;

    switch (cmdid)
    {
    case IMEXT_ON:
        ic->kanaKanji->setKanaMode(MODE_KANA);
        break;
    case IMEXT_OFF:
        ic->kanaKanji->setKanaMode(MODE_OFF);
        break;
    default:
        break;
    }
    ic->updateView();
}

// only-CM & Property-with-CM
static const int SERVER_MAJOR_TRANSPORT_VERSION = 0;
static const int SERVER_MINOR_TRANSPORT_VERSION = 0;

static Atom XA_XIM_XCONNECT = 0;
/*extern*/ Atom XA_XIM_PROTOCOL = 0;
static Atom XA_UINPUT_IS_SERVER = 0;
static Atom XA_UINPUT_SET_SERVER = 0;

#ifdef USE_GTK
static void onConnWindowRealized(GtkWidget* widget, void* cli_window)
{
    gdk_window_add_filter(widget->window, filterXEvent, NULL);
    XSelectInput(top_display, (Window) cli_window, StructureNotifyMask);
    XSelectInput(top_display, GDK_WINDOW_XWINDOW(widget->window), NoEventMask);
}
#endif

static void createConnectionWindow(Connection::TransportType type,
                                   Window client_window)
    // クライアントを区別するため，通信用のウィンドウが必要
    // imTrX.c:_XimXConnect()
{
    // TRACE("createConnectionWindow()\n");
    assert(client_window);
#ifdef USE_GTK
    GtkWidget* widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    assert(widget);
    gtk_signal_connect(GTK_OBJECT(widget), "realize",
                       GTK_SIGNAL_FUNC(onConnWindowRealized),
                       (void*) client_window);
    gtk_widget_realize(widget);
    Window w = GDK_WINDOW_XWINDOW(widget->window);
    assert(w);
#else
    Window w = XCreateSimpleWindow(
        top_display, XDefaultRootWindow(top_display),
        0, 0, 1, 1, 1, 0, 0);
    assert(w);
#endif

    Connection* cli;
    if (type == Connection::XIM_X_TRANSPORT)
        cli = new Connection(type, w, client_window);
    else if (type == Connection::UNICODE_INPUT) {
        // Unicode Input
        cli = find_client_x_client(client_window);
        if (cli)
            return;
        TRACE("top = %x, client = %x\n", w, client_window);
        cli = new Connection(type, w, client_window);
    }
    clients.insert(cli);

    XClientMessageEvent r;
    memset(&r, 0, sizeof(r));
    r.type = ClientMessage;
    r.display = top_display;
    r.format = 32;
    if (type == Connection::XIM_X_TRANSPORT) {
        // XIM X transport
        r.window = cli->getClientWindow();
        r.message_type = XA_XIM_XCONNECT;
        r.data.l[0] = w;
        r.data.l[1] = SERVER_MAJOR_TRANSPORT_VERSION;
        r.data.l[2] = SERVER_MINOR_TRANSPORT_VERSION;
    }
    else {
        // Unicode Input
        r.window = w;
        r.message_type = XA_UINPUT_SET_SERVER;
    }

    XSendEvent(top_display, cli->getClientWindow(), False,
               NoEventMask, (XEvent*) &r);

    // 相手ウィンドウの方にマスクを付けないとDestroyNotifyがこない
    XSelectInput(top_display, cli->getClientWindow(), StructureNotifyMask);
    XSelectInput(top_display, w, KeyPressMask | KeyReleaseMask);
}

static bool onClientEvent(const XClientMessageEvent& ev)
    // XIM VJE Extension / XIM X transport / Unicode Input
{
    // TRACE("onClientEvent()\n");

    if (XA_XIM_XCONNECT == 0) {
        XA_XIM_XCONNECT = XInternAtom(top_display, "_XIM_XCONNECT", False);
        XA_XIM_PROTOCOL = XInternAtom(top_display, "_XIM_PROTOCOL", False);
        XA_UINPUT_IS_SERVER = XInternAtom(top_display, "IsUnicodeInputServer",
                                          False);
        XA_UINPUT_SET_SERVER = XInternAtom(top_display, "SetUnicodeInputServer",
                                           False);
    }
#ifdef USE_GTK
    Window top_window = GDK_WINDOW_XWINDOW(top_widget->window);
#else
    Window top_window = XtWindow(top_widget);
#endif
    
    if (ev.message_type == XA_XIM_XCONNECT) {
        // XIM X transport: connect
        if (ev.display != top_display || ev.window != top_window
                || ev.format != 32) {
            error("XIM X transport: connect error: ev.format = %d\n", ev.format);
        }
        else
            createConnectionWindow(Connection::XIM_X_TRANSPORT, ev.data.l[0]);
        return true;
    }
    else if (ev.message_type == XA_XIM_PROTOCOL) {
        if (ev.window == top_window) {
            // VJE Extension
            // ev.windowがサーバーのtopウィンドウでないときはXIM X transport
            setIMExtValue(ev.data.l[0], (IMExtCmd) ev.data.l[2]);
            return true;
        }
        Connection* cli = find_client_x_self(ev.window);
        if (cli) {
            cli->onXInput(ev);
            return true;
        }
    }
    else if (ev.message_type == XA_UINPUT_IS_SERVER) {
        // Unicode Input
        // ev.windowはクライアント・ウィンドウ
        if (ev.display != top_display || ev.format != 32)
            error("Unicode Input: connect error: ev.format = %d\n", ev.format);
        else 
            createConnectionWindow(Connection::UNICODE_INPUT, ev.window);
        return true;
    }

    return false; // unknown ClientMessage
}

#ifdef USE_GTK
GdkFilterReturn filterXEvent(GdkXEvent* xev_, GdkEvent* , void* )
{
    XEvent* xev = (XEvent*) xev_;
    
    if (xev->type == ClientMessage) {
        return onClientEvent(xev->xclient)
            ? GDK_FILTER_REMOVE : GDK_FILTER_CONTINUE;
    }
    else if (xev->type == DestroyNotify) {
        TRACE("filterXEvent: DestroyNotify\n");
        Connection* cli = find_client_x_client(xev->xdestroywindow.window);
        if (cli) {
            TRACE("client-window destroyed.\n");
            cli->close();
            clients.erase(cli);
            return GDK_FILTER_REMOVE;
        }
    }
#if DEBUG > 1
    else {
        Connection* cli = find_client_x_client(xev->xany.window);
        TRACE("client = %x, ", cli);
        if (!cli) {
            cli = find_client_x_self(xev->xany.window);
            TRACE("self = %d\n", cli);
        }
        else {
            TRACE("\n");
        }
    }
#endif
    return GDK_FILTER_CONTINUE;
}
#else // USE_GTK
bool filterXEvent(const XEvent& ev)
    // XIM VJE Extension / X transport / Unicode Input
{
    if (ev.type == ClientMessage)
        return onClientEvent(ev.xclient);
    else if (ev.type == DestroyNotify) {
        // TRACE("DestroyNotify: ev = %d, window = %d\n",
        //       ev.xdestroywindow.event, ev.xdestroywindow.window);
        Connection* cli = find_client_x_client(ev.xdestroywindow.window);
        if (cli) {
            TRACE("client-window destroyed.\n");
            cli->close();
            clients.erase(cli);
            return true;
        }
    }
    else if (ev.type == KeyPress || ev.type == KeyRelease) {
        // Unicode Input
        Connection* cli = find_client_x_client(ev.xkey.window);
        if (cli) {
            // TRACE("found: %x\n", cli->getClientWindow());
            InputMethod* im = *cli->methods.begin();
            InputContext* ic = *im->contexts.begin();
            ic->processEvent(KeyEvent(ev.xkey));
            ic->updateView();
            return true;
        }
    }

    return false; // unknown event
}
#endif // USE_GTK
