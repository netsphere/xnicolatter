// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.
//   email: <mailto:vzw00011@nifty.ne.jp>
//   website: <http://www2.airnet.ne.jp/pak04955/>
// -*- mode:c++ -*-

#include "../config.h"
 
#include <cstdio>
#include <cassert>
#include <X11/keysym.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include <FontDialog.h>
#include <misc.h>
#include <misc-gtk.h>

///////////////////////////////////////////////////////////////////////
// TablePage

static const char* CHAR_FONT_NAME = "-*-fixed-medium-r-normal--24-*-*-*-*-*-*-*";

TablePage::TablePage(): font(NULL)
{
}

TablePage::~TablePage()
{
}

void TablePage::on_selection_done(GtkMenuShell* menu_shell,
                                  TablePage* this_)
    // GtkMenuShell::selection_done
{
    int idx = getSelectedIndex(GTK_OPTION_MENU(this_->selector));
    if (idx >= 0) {
        this_->cur_ccs = idx;
        if (this_->drawing) {
            gtk_widget_queue_draw(this_->drawing);
            gtk_widget_queue_draw(this_->row_label);
            gtk_widget_queue_draw(this_->col_label);
        }
        
        this_->updateAdjustment();
        
        int code = this_->ccs[this_->cur_ccs].start
            + this_->ccs[this_->cur_ccs].pos;
        paletteWindow.setCode(this_->getText(code));
    }
}

void TablePage::updateCharsets()
{
    Display* disp = GDK_DISPLAY();
    assert(disp);
#if 0
    // 2000.08.10 フォント情報から文字情報を得るのは，美味くない
    //        iso8859-1は0-255を返すし，jisx0201も同様。
            
    XOM om = XOpenOM(disp, NULL, NULL, NULL);
    assert(om);
    TRACE("locale = %s\n", XLocaleOfOM(om));

    XOMCharSetList csList;
    const char* mes = XGetOMValues(om, XNRequiredCharSet, &csList, NULL);
    if (mes) {
        error("%s\n", mes);
        assert(0);
    }
    for (int i = 0; i < csList.charset_count; i++) {
        CharSet charset;
        charset.name = csList.charset_list[i];

        string f = string("-*-*-medium-r-normal--*-*-*-*-*-*-")
            + charset.name;
        XFontStruct* fs = XLoadQueryFont(disp, f.c_str());
        assert(fs);
        charset.siz[0].l = fs->min_byte1;
        charset.siz[0].h = fs->max_byte1;
        charset.siz[1].l = fs->min_char_or_byte2;
        charset.siz[1].h = fs->max_char_or_byte2;
        TRACE("siz: %d - %d, %d - %d\n",
              charset.siz[0].l, charset.siz[0].h,
              charset.siz[1].l, charset.siz[1].h);
        XFreeFont(disp, fs);

        cs.push_back(charset);
    }

    XCloseOM(om);
#else
    CharSet charset;

    charset.name = "iso8859-1";
    charset.siz[1] = Range(0x20, 0x7e);
    ccs.push_back(charset);

    charset.name = "jisx0208.1983-0";
    charset.siz[0] = Range(0x21, 0x7e);
    charset.siz[1] = Range(0x21, 0x7e);
    ccs.push_back(charset);

    charset.name = "jisx0201.1976-0";
    charset.siz[0] = Range();
    charset.siz[1] = Range(0xa1, 0xdf);
    ccs.push_back(charset);
#endif
}

void
on_table_page_realize                  (GtkWidget       *widget,
                                        gpointer         user_data)
{
    TRACE("on_table_page_realize()\n");
    TablePage* this_ = &tablePage;
    
    VERIFY(tablePage.selector = lookup_widget(widget, "charset_sel"));
    VERIFY(tablePage.drawing = lookup_widget(widget, "table_drawing"));
    VERIFY(tablePage.row_label = lookup_widget(widget, "table_row_label"));
    VERIFY(tablePage.col_label = lookup_widget(widget, "table_col_label"));
    VERIFY(tablePage.vscrollbar = lookup_widget(widget, "table_vscrollbar"));

    tablePage.updateCharsets();

    // 文字集合セレクターの初期化
    GtkWidget* menu = gtk_option_menu_get_menu(
                         GTK_OPTION_MENU(tablePage.selector));
    gtk_container_foreach(GTK_CONTAINER(menu),
                              (GtkCallback) gtk_widget_destroy, NULL);

    for (unsigned int i = 0; i < tablePage.ccs.size(); i++) {
        GtkWidget* item = gtk_menu_item_new_with_label(
                                   tablePage.ccs[i].name.c_str());
        gtk_menu_append(GTK_MENU(menu), item);
    }
    gtk_widget_show_all(menu);
    
    gtk_signal_connect(
            GTK_OBJECT(menu), "selection-done",
            GTK_SIGNAL_FUNC(TablePage::on_selection_done), &tablePage);

    this_->createFont();
}

void
on_table_drawing_size_allocate         (GtkWidget       *widget,
                                        GtkAllocation   *allocation,
                                        gpointer         user_data)
{
    tablePage.on_drawing_size_allocate(allocation);
}

void
on_table_drawing_draw_focus            (GtkWidget       *widget,
                                        gpointer         user_data)
{
    tablePage.on_drawing_draw_focus();
}

void TablePage::createFont()
{
    if (!font) {
        font = gdk_fontset_load(CHAR_FONT_NAME);
        labelFont = gdk_fontset_load(LABEL_FONT);
        if (font) {
            XFontSetExtents* fse = XExtentsOfFontSet(GDK_FONT_XFONTSET(font));
            chsiz = fse->max_logical_extent;
            chsiz.width += CHAR_BORDER * 2;
            chsiz.height += CHAR_BORDER * 2;
            TRACE("fs x = %d, y = %d, w = %d, h = %d\n",
                  chsiz.x, chsiz.y, chsiz.width, chsiz.height);
        }
        else {
            chsiz = CRect(0, -22, 28, 28);
        }
    }
}

void
on_table_drawing_realize               (GtkWidget       *widget,
                                        gpointer         user_data)
{
    TRACE("on_table_drawing_realize()\n");
    tablePage.on_drawing_realize();
}

gboolean
on_table_col_label_expose_event        (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    tablePage.on_col_label_expose_event(widget);
    return TRUE;
}

gboolean
on_table_row_label_expose_event        (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    tablePage.on_row_label_expose_event(widget);
    return TRUE;
}

string TablePage::getText(unsigned int code) const
    // 入力：現在の文字集合でのコード
{
    const CharSet& cs = ccs[cur_ccs];
    string r;
    switch (cur_ccs)
    {
    case 0:
        // IRV
        r = code + cs.siz[1].l;
        break;
    case 1:
        // 漢字
        r = (code / cs.siz[1].getSize() + cs.siz[0].l) | 0x80;
        r += (code % cs.siz[1].getSize() + cs.siz[1].l) | 0x80;
        break;
    case 2:
        // 半角カナ
        r = 0x8e;
        r += code + cs.siz[1].l;
        break;
    default:
        error("unknown ccs: %d\n", cur_ccs);
        assert(0);
        break;
    }
    return r;
}

gboolean
on_table_drawing_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    TablePage* this_ = &tablePage;
    
    gdk_window_clear(widget->window);
    this_->on_drawing_draw_focus();

    if (!this_->font)
        return TRUE;
    
    for (int y = 0; y <= this_->height; y++) {
        for (int x = 0; x < this_->width; x++) {
            int code = this_->ccs[this_->cur_ccs].start
                                  + x + y * this_->width;
            if (code < this_->ccs[this_->cur_ccs].getSize()) {
                string ch = this_->getText(code);
                gdk_draw_string(
                    widget->window, tablePage.font,
                    widget->style->black_gc,
                    TablePage::TABLE_BORDER + x * tablePage.chsiz.width
                    - tablePage.chsiz.x + TablePage::CHAR_BORDER,
                    TablePage::TABLE_BORDER + y * tablePage.chsiz.height
                    - tablePage.chsiz.y + TablePage::CHAR_BORDER,
                    ch.c_str());
            }
        }
    }

    tablePage.setCursorVisible(true);
    return TRUE;
}


gboolean
on_table_drawing_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
    tablePage.on_drawing_button_press_event(widget, event);
    return TRUE;
}


gboolean
on_table_drawing_key_press_event       (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
    return tablePage.on_drawing_key_press_event(widget, event);
}

gboolean
on_table_drawing_focus_in_event        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
    GTK_WIDGET_SET_FLAGS(tablePage.drawing, GTK_HAS_FOCUS);
    gtk_widget_draw_focus(tablePage.drawing);
    return TRUE;
}

gboolean
on_table_drawing_focus_out_event       (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
    tablePage.on_drawing_focus_out_event(widget);
    return TRUE;
}

void TablePage::updateSelector()
{
    /* empty */
}
