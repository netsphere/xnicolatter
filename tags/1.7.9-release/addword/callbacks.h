#include <gtk/gtk.h>

extern "C" {

void
on_ok_button_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_addword_window_realize              (GtkWidget       *widget,
                                        gpointer         user_data);

}
