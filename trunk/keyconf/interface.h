/*
 * 編集禁止! - このファイルはGladeによって生成されています.
 */

GtkWidget* create_map_window (void);
GtkWidget* create_funcs_window (void);
GtkWidget* create_key_prop (void);
GtkWidget* create_func_prop (void);
GtkWidget* create_k_popup (void);
GtkWidget* create_f_popup (void);
