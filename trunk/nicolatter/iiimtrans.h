// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_IIIMTRANS_H
#define QSNICOLA_IIIMTRANS_H

#include "transbase.h"
#include "LevelSelector.h"

////////////////////////////////////////////////////////////////////////
// IIIMPWriter

class IIIMPWriter: public SendBuffer
{
public:
    IIIMPWriter(bool endian, int opcode);
    virtual ~IIIMPWriter();
    virtual int getData(unsigned char* out);
    void uniString(const char* s);
};

////////////////////////////////////////////////////////////////////////
// IIIMPReader

class IIIMPReader: public RecvBuffer
{
public:
    IIIMPReader(bool endian);
    virtual ~IIIMPReader();

    virtual bool input(int fd);
    int opcode() const;
    uint16_t card8();
    virtual KeyEvent keyevent();
};

#endif // QSNICOLA_TRANSPORT_H
