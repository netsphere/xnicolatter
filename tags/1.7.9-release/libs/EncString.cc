// Q's C++ Library
// Copyright (c) 2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 文字列クラス
// ナル文字を含むことができる (UTF-16対策)

#include <config.h>

#include <iconv.h>
#include <stdlib.h>
#include "EncString.h"

////////////////////////////////////////////////////////////////////////
// EncString

EncString::EncString(): str(NULL), len(-1)
{
}

EncString::~EncString()
{
    free(str);
    str = NULL;
}

EncString::EncString(const EncString& s): str(NULL), len(-1)
{
    if (s.str) {
        str = (unsigned char*) malloc(s.len + 1);
        memcpy(str, s.str, s.len);
        len = s.len;
        str[len] = 0;
        encoding = s.encoding;
    }
}

EncString::EncString(const char* s, int len_, const std::string& enc)
    : str(NULL), len(len_)
{
    if (s && len > 0) {
        str = (unsigned char*) malloc(len + 1);
        memcpy(str, s, len);
        str[len] = 0;
        encoding = enc;
    }
}

EncString& EncString::operator = (const EncString& s)
{
    if (this != &s) {
        free(str);
        str = NULL;
        if (s.str) {
            str = (unsigned char*) malloc(s.len + 1);
            memcpy(str, s.str, s.len);
            len = s.len;
            str[len] = 0;
            encoding = s.encoding;
        }
    }
    return *this;
}

#if DEBUG > 1
#include <stdio.h>
static void d_(unsigned char* s, int len) {
    for (int i = 0; i < len; i++)
        printf("%02x ", s[i]);
    printf("\n");
}
#endif

int EncString::convert(const std::string& new_enc)
{
    if (str) {
        iconv_t cd = iconv_open(new_enc.c_str(), encoding.c_str());
        if (cd == (iconv_t) -1)
            return -1;
    
        size_t bufsiz = len * 6 + 1;
        char* buf = (char*) malloc(bufsiz);

        const char* inbuf = (char*) str;
        size_t inbytes = len;
        char* outbuf = buf;
#if DEBUG > 1
        d_(str, len);
#endif
#ifdef ICONV_CONST
        size_t r = iconv(cd, &inbuf, &inbytes, &outbuf, &bufsiz);
#else
        size_t r = iconv(cd, const_cast<char**>(&inbuf), &inbytes,
                         &outbuf, &bufsiz);
#endif
        if (r == -1 || inbytes != 0) {
            iconv_close(cd);
            free(buf);
            return -1;
        }

        iconv_close(cd);
        free(str);
        str = (unsigned char*) buf;
        len = outbuf - buf;
        str[len] = 0;
#if DEBUG > 1
        d_(str, len);
#endif
    }
    encoding = new_enc;
    return 0;
}

void EncString::regard(const std::string& new_enc)
{
    encoding = new_enc;
}

const unsigned char* EncString::data() const
{
    return str;
}

int EncString::length() const
{
    return len;
}

#ifdef ENCSTR_TEST
#include <stdio.h>
int main()
{
    EncString s("ABC日本語", 9, "EUC-JP");
    s.convert("UTF-16");
    
    const unsigned char* p = s.data();
    for (int i = 0; i < s.length(); i++)
        printf("%02x ", p[i]);
    printf("\n");
    return 0;
}
#endif
