// Q's C++ Library
// Copyright (c) 1996-2000 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// UTF-8, a transformation format of ISO 10646 (RFC 2279)

#ifndef Q_UTF8_H
#define Q_UTF8_H

#include <stdlib.h>
#include <inttypes.h>

#ifdef __cplusplus
extern "C" {
#endif

extern size_t ucstoutf8(char* dest, const uint32_t* src, size_t n);
extern size_t utf8toucs(uint32_t* pwcs, const char* s, size_t n);
extern int utf8len(const char* p, size_t n);

#ifdef __cplusplus
}
#endif

#endif // Q_UTF8_H
