// キー配列設定プログラム

#ifndef KEYCONF_CALLBACK_H
#define KEYCONF_CALLBACK_H

#include "../keymap.h"
#include <gtk/gtk.h>
#include <document.h>

////////////////////////////////////////////////////////////////////////
// MapEditorDoc

class MapEditorDoc: public Document
{
public:
    KeyMap map_data;

    MapEditorDoc();
    virtual ~MapEditorDoc();
    void setKey(Key* key);
    void setCtrlFunc(CtrlFunc* func);

    virtual bool save();
};

////////////////////////////////////////////////////////////////////////
// MapWindow

class MapWindow: public Controller
{
public:
    GtkWidget* window;
    GtkWidget* clist;
private:
    static const int COLUMN_COUNT;

public:
    MapWindow();
    virtual ~MapWindow();
    void update(const Key* key);
    virtual void updateTitle(const string& title);
    static void onViewMenuMap(GtkWidget* widget, MapWindow* );
private:
    void drawLine(int row, const Key* key);
};

void
on_map_window_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_map_select_row                      (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

gboolean
on_map_window_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_map_window_destroy                  (GtkObject       *object,
                                        gpointer         user_data);

void
on_popup_delete_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_map_clist_button_release_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

////////////////////////////////////////////////////////////////////////

bool loadFile(const string& filename);

void
on_file_new_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_file_open_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_file_save_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


void
on_file_saveas_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_close_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_funcs_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_view_funcprop_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);


void
on_view_keyprop_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

////////////////////////////////////////////////////////////////////////
// FuncsWindow

class FuncsWindow
{
public:
    GtkWidget* window;
    GtkWidget* clist;
private:
    static const int COLUMN_COUNT;

public:
    void update(const CtrlFunc* func);
private:
    void drawLine(int row, const CtrlFunc* func);
};

void
on_funcs_window_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_funcs_window_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_funcs_select_row                    (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

////////////////////////////////////////////////////////////////////////
// KeyProp

class KeyProp
{
public:
    GtkWidget* window;
    GtkWidget* key_ctrl;
    GtkWidget* key_entry;

    GtkWidget* type_graph;
    GtkWidget* graph_frame;
    GtkWidget* graph_entry[3];

    GtkWidget* type_func;
    GtkWidget* func_frame;
    GtkWidget* func_sel;
private:
    int cur_code;

public:
    void update(int mod, int keycode);
    void updateFuncs();
};

void
on_key_prop_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_keyprop_keychoose_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_keyprop_type_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_keyprop_ok                          (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_key_prop_delete_event               (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

////////////////////////////////////////////////////////////////////////
// FuncProp

struct FuncProp
{
    GtkWidget* window;
    GtkWidget* name_entry;
    GtkWidget* pie_sel[4];

    void update(const CtrlFunc* func);
};

void
on_funcprop_ok                         (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_func_prop_delete_event              (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_func_prop_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_funcs_clist_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_f_popup_delete_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

////////////////////////////////////////////////////////////////////////

#endif
