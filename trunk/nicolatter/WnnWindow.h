﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// Wnn用の編集ウィンドウ，ステータスウィンドウ，候補ウィンドウ

#include <vector>
#include <string>

#include "preedit.h"
#include "status.h"
#include "../qWnn.h"

using namespace std;

//////////////////////////////////////////////////////////////////////
// WnnPreedit

class WnnConv;
class WnnPreedit: public PreeditWindow
{
    typedef PreeditWindow super;
    WnnConv* conv;
public:
    WnnPreedit(WnnConv* conv, InputContext* ic);
    virtual ~WnnPreedit();
    virtual void update();
private:
    virtual void draw(const CRect& );
};

//////////////////////////////////////////////////////////////////////
// WnnStatus

class WnnConv;
class WnnStatus: public StatusWindow
{
    typedef StatusWindow super;
    WnnConv* conv;
    static const char* modes[];

public:
    WnnStatus(WnnConv* conv, InputContext* ic);
    virtual ~WnnStatus();
    virtual void update();
};

//////////////////////////////////////////////////////////////////////
// WnnCandidate

class WnnConv;
class WnnCandidate: public CandidateWindow
{
    typedef CandidateWindow super;

    int sel_top;
#ifdef USE_GTK
    GtkWidget* list_widget;
    GtkWidget* label;
    GtkWidget* scrolled;
    GtkAdjustment* adjustment;
#else
    Widget canvas;
    XFontSet font;
    int page_size;
    int cur_sel;
#endif
    WnnConv* conv;
    CandidateList cand_list;
    PreeditWindow* preeditWindow;
    static int CAND_SIZE;
    static const int BORDER;

public:
    WnnCandidate(WnnConv* conv, PreeditWindow* pre);
    virtual ~WnnCandidate();
    void setCandidates(const CandidateList& list);
    void select(int index);
    void clear();
    void update();
private:
    void create();
    void resizeWindow();
#ifdef USE_GTK
    static void onSelected(GtkCList* clist, gint row, gint column,
                                GdkEvent* event, WnnCandidate* this_);
#else
    static void onExposed(Widget w, void* closure, XEvent* event, Boolean* cont);
    void draw();
#endif
};
