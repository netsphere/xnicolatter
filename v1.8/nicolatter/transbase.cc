// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// このファイル
//     送信・受信パケット；XIM/IIIMで共通の部分

#include "../config.h"

#include <cstdio>
#include <cassert>
#include <string>
#include <cerrno>
#include <unistd.h>

#include "transbase.h"
#include "preedit.h"
#include "status.h"
#include "client.h"

using namespace std;

////////////////////////////////////////////////////////////////////////
// SendBuffer

SendBuffer::SendBuffer(bool endian): bigEndian(endian)
{
}

SendBuffer::~SendBuffer()
{
}

void SendBuffer::card32(unsigned char* pos, uint32_t val)
{
    if (bigEndian) {
        *pos++ = (val >> 24) & 0xff;
        *pos++ = (val >> 16) & 0xff;
        *pos++ = (val >> 8) & 0xff;
        *pos++ = val & 0xff;
    }
    else {
        *pos++ = val & 0xff;
        *pos++ = (val >> 8) & 0xff;
        *pos++ = (val >> 16) & 0xff;
        *pos++ = (val >> 24) & 0xff;
    }
}

void SendBuffer::card32(uint32_t val)
{
    card32(cur, val);
    cur += 4;
}

void SendBuffer::card16(unsigned char* pos, uint16_t val)
{
    if (bigEndian) {
        *pos++ = (val >> 8) & 0xff;
        *pos++ = val & 0xff;
    }
    else {
        *pos++ = val & 0xff;
        *pos++ = (val >> 8) & 0xff;
    }
}

void SendBuffer::card16(uint16_t val)
{
    card16(cur, val);
    cur += 2;
}

void SendBuffer::reserve16()
{
    card16(0);
    reserved.push_back(RPos(16, cur - buf));
}

void SendBuffer::reserve32()
{
    card32(0);
    reserved.push_back(RPos(32, cur - buf));
}

int SendBuffer::len_from_reserved() const
{
    assert(reserved.size() > 0);
    return (cur - buf) - reserved.back().pos;
}

void SendBuffer::store_back(int n)
{
    assert(reserved.size() > 0);
    RPos pos = reserved.back();
    if (pos.siz == 16)
        card16(buf + pos.pos - 2, n);
    else
        card32(buf + pos.pos - 4, n);
    reserved.pop_back();
}

void SendBuffer::pad()
{
    int len = cur - buf;
    for (int pad = (4 - (len % 4)) % 4; pad > 0; pad--)
        *(cur++) = 0;
}

#ifdef DEBUG
void SendBuffer::debug_out() const
{
#if DEBUG > 1
    int len = cur - buf;
    TRACE("send: ");
    for (int i = 0; i < len; i++)
        TRACE("%02x ", buf[i]);
    TRACE("\n");
#endif
}
#endif // DEBUG

////////////////////////////////////////////////////////////////////////
// RecvBuffer

RecvBuffer::RecvBuffer(bool endian): cur(NULL), filled(false), bigEndian(endian),
    buf(NULL), count_(0)
{
}

RecvBuffer::~RecvBuffer()
{
}

uint16_t RecvBuffer::card16()
{
    uint16_t r;
    if (bigEndian)
        r = (*cur << 8) + cur[1];
    else
        r = *cur + (cur[1] << 8);
    cur += 2;
    return r;
}

uint32_t RecvBuffer::card32()
{
    uint32_t r;
    if (bigEndian)
        r = (*cur << 24) + (cur[1] << 16) + (cur[2] << 8) + cur[3];
    else
        r = *cur + (cur[1] << 8) + (cur[2] << 16) + (cur[3] << 24);
    cur += 4;
    return r;
}

bool RecvBuffer::isBigEndian() const
{
    return bigEndian;
}

bool RecvBuffer::isFilled() const
{
    return filled;
}

int RecvBuffer::pad()
    // 進めたバイト数を返す
{
    assert(buf != NULL && cur != NULL);

    int len = cur - buf;
    int r = (4 - (len % 4)) % 4;
    cur += r;
    return r;
}

#ifdef DEBUG
void RecvBuffer::debug_out() const
{
#if DEBUG > 1
    TRACE("recv: ");
    for (int i = 0; i < count_; i++)
        TRACE("%02x ", buf[i]);
    TRACE("\n");
#endif
}
#endif // DEBUG
