// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../keymap.h"
#include "conv.h"

//////////////////////////////////////////////////////////////////////
// WnnConv

enum IM_State {
    MI_NYURYOKU,    // 未入力
    HENKAN_MAE,     // 入力中（キャレットあり）
    ZEN_HENKAN,     // 全変換（キャレットなし）
    KOUHO_ICHIRAN   // 候補一覧（キャレットなし）
};

class WnnConv;
struct PieceIdExecPair
{
    FuncPieceId id;
    void (WnnConv::*exec)();
};

class WnnCandidate;
class RomaKanaDef;
class WnnConv: public KanaKanjiConv
{
    typedef KanaKanjiConv super;
    typedef ptr_vector<RomaKanaDef*> RomaKanaDefs;
    typedef vector<int> IndexList;

public:
    IM_State im_state;
    string preconv;         // 読み
    string converted;
    SizeList<int> charSizes;     // 各文字のバイト長
    int caret;
    IndexList conv_i;   // 変換後文字列の各文節のバイト位置
    int cur_clause;
    CharStatusList statusList;
    class qWnn* wnn;

private:
    static RomaKanaDefs romaDefs;
    KanaKanjiStatus result_stat;

    IndexList read_i;   // 仮名の各文節の文字位置
    int conv_count;     // 候補一覧を出すまでの回数
    KanaMode kana_mode;     // 0 = 英字，1 = 漢字, 2 = 無変換固定
    KanaMode last_mode;     // 一つ前のモード。トグル機能が使う
    WnnCandidate* candWindow;

    static const PieceIdExecPair conv_funcs[];

public:
    WnnConv(InputContext* );
    virtual ~WnnConv();

    virtual KanaKanjiStatus input(const KeyEvent& event, int level);

    static bool loadRomaDef(const char* filename);
    void onCandidateSelected(int cand);

    virtual void all_determine();
    virtual void clear();
    virtual KanaMode getKanaMode() const;
    virtual void setKanaMode(KanaMode mode);
    virtual PreeditWindow* createPreeditWindow();
    virtual StatusWindow* createStatusWindow();
    virtual void setCandidateVisible(bool );
    virtual void updateCandidate();
private:
    void execGraphChar(const char* str, KanaKanjiStatus* charType);
    void execCtrlFunc(const CtrlFunc* func, KanaKanjiStatus* charType);

    int matchRoma(const char* buf, int len, bool last_caret,
                  const char* def) const;
    void convertRoma();
    void query();
    void addChar(const char* s);

    void through();
    void na();
    void kanji();
    void space();
    void convert();
    void next_cand();
    void prev_cand();
    void caret_left();
    void shrink();
    void caret_right();
    void expand();
    void clause_left();
    void clause_right();
    void left_erase();
    void right_erase();
    void revert();
    void head();
    void tail();
    void clause_det();
    void fixed_alnum();
};

