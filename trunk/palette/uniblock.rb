
  f = File.open("Blocks-3.txt", "r")
  w = File.open("uniblock.cc", "w")

  w.print <<'EOF'
// Unicode Blocks

#include <stdlib.h>
#include "uniblock.h"

const UniBlock uniBlock[] = {
EOF

  while line = f.gets
    line.chomp!
    next if line =~ /^#/
    d = line.split("; ")
    w.printf("  { 0x%s, \"%s\" },\n", d[0], d[2])
  end

  w.print "  { -1, NULL }\n"
  w.print "};\n"

  f.close
  w.close
