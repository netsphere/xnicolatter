// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_QWNN_H
#define QSNICOLA_QWNN_H

#include <string>
#include <vector>

#include "jllib.h"

using namespace std;

////////////////////////////////////////////////////////////////////////
// qWnn

typedef vector<string> CandidateList;

class qWnn
{
    struct wnn_buf* wnn;
public:
    qWnn();
    virtual ~qWnn();

    bool is_open() const;
    void convert(const char* str);
    bool set_clause_len(int clause, int len);
    
    string getComposition(int clause) const;
        // �Ѵ����ʸ����
    virtual int getReadLength(int clause) const;
        // �ɤ�
    CandidateList getCandidateList(int clause) const;
        // ����
    int countClause() const;
    virtual void close();
    virtual int nextCandidate(int clause, int next);
    string determineFirstClause();
    void updateLearning();
};

////////////////////////////////////////////////////////////////////////

extern size_t wnn_mbstowcs(w_char* pcode, const char* euc, size_t );
extern size_t wnn_wcstombs(char* euc, const w_char* pcode, size_t );
extern struct wnn_buf* wnn_connect();

#endif  // QSNICOLA_QWNN_H
