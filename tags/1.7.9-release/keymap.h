// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#ifndef QSNICOLA_KEYMAP_H
#define QSNICOLA_KEYMAP_H

#include <string>
#include <cstdio>
#include <cassert>
using namespace std;

#include <misc.h>

//////////////////////////////////////////////////////////////////////

extern Display* top_display;

extern string getKeyName(int mod, int keycode);
extern string getKeyName2(int mod, int keycode);
extern int getKeyMod(const string& key_name);
extern int getKeyCode(const string& key_name);

//////////////////////////////////////////////////////////////////////
// Key

enum KeyType {
    UNDEF_KEY = 0,
    GRAPHIC_KEY,    // 図形キー ISO/IEC 9995-1参照
    FUNCTION_KEY    // 制御キー
};

class Key
{
public:
    string name;    // キー名 [Ctrl]併用では，"ctrl-"が付く

    Key(const string& name_);
    virtual ~Key() { }
    virtual KeyType getType() const = 0;
};

//////////////////////////////////////////////////////////////////////
// GraphicKey

class GraphicKey: public Key
/*
    1999.01.17　図形キーと制御キーを区別する方法で実装してみる。以前このモデルは破棄したはずなん
    だが，理由が思い出せない。このモデルで破綻したら，図形キーと制御キーを区別せず，各キー位置で図
    形文字と制御機能を選ぶモデルに変更する。
*/
{
    friend class KeyMap;
    string chars[3];    // 単独打鍵，左シフト，右シフト
public:
    GraphicKey(const string& name, const string* s);
    virtual ~GraphicKey();
    virtual KeyType getType() const;
    virtual string getGraphChar(int level) const;
};

//////////////////////////////////////////////////////////////////////
// CtrlFunc

enum FuncPieceId {
    CF_ERROR = 0,   // 機能は割り当てない
    CF_THROUGH = 1,
    CF_NA,
    CF_KANJI,
    CF_SPACE,
    CF_CONVERT,
    CF_NEXT_CAND,
    CF_PREV_CAND,
    CF_CARET_LEFT,
    CF_SHRINK,
    CF_CARET_RIGHT,
    CF_EXPAND,
    CF_CLAUSE_LEFT,
    CF_CLAUSE_RIGHT,
    CF_ALL_DETERMINE,
    CF_CLEAR,
    CF_LEFT_ERASE,
    CF_RIGHT_ERASE,
    CF_REVERT,
    CF_HEAD,
    CF_TAIL,
    CF_CLAUSE_DET,
    CF_FIXED_ALNUM
};

const int COUNT_IME_STAT = 4;

struct FuncNameIdPair
{
    const char* name;   // "revert"など
    FuncPieceId id;     // CF_REVERT
    bool canUse[COUNT_IME_STAT];     // IMEの状態ごとに，使えるか使えないか
};

extern const FuncNameIdPair funcNameId[];
extern FuncPieceId find_piece_id(const string& piece_name, int ime);
extern const char* find_piece_name(FuncPieceId id);

struct CtrlFunc
    // 制御機能
{
    string name;        // 「確定」など。key.defでユーザーが指定
    FuncPieceId piece[COUNT_IME_STAT];   // IMEの状態ごとの機能片
                        // key.defでは"revert"など文字列だが，funcNameIdで引く
};

//////////////////////////////////////////////////////////////////////
// FunctionKey

class FunctionKey: public Key
    // 制御キー
{
public:
    const CtrlFunc* func[3];   // 単独，左，右
            // 複数のキーがある制御機能を生成できるから，制御キーは制御機能を所有しない

    FunctionKey(const string& name);
    virtual ~FunctionKey();
    virtual KeyType getType() const;
};

//////////////////////////////////////////////////////////////////////
// KeyMap

struct KeyComp {
    bool operator () (const Key* x, const Key* y) const {
        assert(x && y);
        return x->name < y->name;
}};

struct FuncComp {
    bool operator () (const CtrlFunc* x, const CtrlFunc* y) const {
        assert(x && y);
        return x->name < y->name;
}};

class KeyMap
    // キー配列クラス
    // 各キーについて
    //      図形キー，制御キーの別
    //      図形キーから文字
    //      制御キーなら制御機能
    // を持つ。
    // このクラスは配列表を格納するのみであって，
    // キー入力を文字に変換する役割を持たない。
{
public:
    typedef ptr_set<Key*, KeyComp> KeyList;
    typedef ptr_set<CtrlFunc*, FuncComp> FuncList;

    KeyList keyList;
    FuncList funcList;

    KeyMap();
    virtual ~KeyMap();

    virtual bool load(const string& filename);
    virtual bool save(const char* filename) const;
    void clear();

    const Key* find_key(const string& key_name) const;
    void remove_key(const string& key_name);

    FunctionKey* createFuncKey(const string& key_name, const string func[]);

private:
    void saveKeyList(FILE* fp) const;
    void saveFuncList(FILE* fp) const;
    void loadKey(FILE* fp, int& line);
    void loadFunc(FILE* fp, int& line);
    const CtrlFunc* find_func(const string& func_name) const;
};

extern KeyMap* keyMap;

#endif  // QSNICOLA_KEYMAP_H
