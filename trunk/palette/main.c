// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 漢字検索

#include "../config.h"
#include <gtk/gtk.h>

#include "interface.h"
#include "support.h"
#include "callbacks.h"
#include <misc.h>

extern PaletteWindow paletteWindow;

static const char* SYM_FILE = ".nicolatter/symbols.def";
static const char* SYS_SYM_FILE = SYS_RC_DIR "/symbols.def";

int main(int argc, char* argv[])
{
/*
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);
*/
    setenv("LC_CTYPE", JA_EUCJP_LOCALE_NAME, 1);
    gtk_set_locale ();
    gtk_init (&argc, &argv);
/*
  add_pixmap_directory (PACKAGE_DATA_DIR "/pixmaps");
  add_pixmap_directory (PACKAGE_SOURCE_DIR "/pixmaps");
*/

    string sym_file = getHomeDir() + "/" + SYM_FILE;
    if (!load_symdef(sym_file)) {
        sym_file = SYS_SYM_FILE;
        if (!load_symdef(sym_file)) {
            error("error: '%s'の読み込みに失敗\n", sym_file.c_str());
            return 1;
        }
    }

    paletteWindow.window = create_palette_window ();
    gtk_widget_show(paletteWindow.window);

  gtk_main ();
  return 0;
}

