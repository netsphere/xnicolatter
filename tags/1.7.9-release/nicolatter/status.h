// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// ステータスウィンドウ

#include <string>
#include <X11/Xlib.h>

#include <window.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// CandidateWindow

class CandidateWindow: public PopupWindow
{
};

////////////////////////////////////////////////////////////////////////
// StatusWindow

class InputContext;
class StatusWindow: public PopupWindow
{
    typedef PopupWindow super;
public:
    CRect area;
    CRect areaNeeded;
    int colormap;
    int stdColormap;
    int foreground;
    int background;
    int backgroundPixmap;
    string fontName;
    int lineSpace;
    int cursor;
    CDimension size;

protected:
#ifdef USE_GTK
    GtkWidget* label;
#else
    Widget label;
#endif

private:
    InputContext* ic;

public:
    virtual ~StatusWindow();
    virtual void update() = 0;

    StatusWindow& operator = (const StatusWindow& );

protected:
    StatusWindow(InputContext* );
    void adjustLocation();
    bool isStyleArea() const;

private:
    void create();
#ifdef USE_GTK
    GtkWidget* createSelLabel(GtkWidget*& label);
    static void onOptionClicked(GtkButton* button, StatusWindow* this_);
#endif
    StatusWindow(const StatusWindow& ); // not impl
};
