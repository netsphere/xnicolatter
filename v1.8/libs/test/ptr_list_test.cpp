
// gcc -Wall ptr_list_test.cpp -lstdc++

#include "../ptr_list.h"
#include <iostream>
#include <typeinfo>
using namespace std;

struct S {
  virtual ~S() { }
  virtual void foo() = 0; };
struct T: public S { 
  virtual void foo() { cout << "T" << endl; } };
struct U: public S { 
  virtual void foo() { cout << "U" << endl; } };

int main() {
  ptr_list<S*> lst;
  lst.push_back(new T);
  lst.push_back(new U);

  for (auto e: lst) {
    T* p = dynamic_cast<T*>(e);
    cout << typeid(e).name() << " " << typeid(p).name() << endl; //=> P1S P1T 変数の型が表示
    e->foo(); // RTTI で正しいメソッドが呼び出される
  }
}

    
