/*
 * 編集禁止! - このファイルはGladeによって生成されています.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

GtkWidget*
create_palette_window (void)
{
  GtkWidget *palette_window;
  GtkWidget *hbox1;
  GtkWidget *notebook;
  GtkWidget *symbol_page;
  GtkWidget *symbol_kind_clist;
  GtkWidget *label3;
  GtkWidget *scrolledwindow1;
  GtkWidget *symbol_char_clist;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *label6;
  GtkWidget *label1;
  GtkWidget *table_page;
  GtkWidget *hbox3;
  GtkWidget *label9;
  GtkWidget *charset_sel;
  GtkWidget *charset_sel_menu;
  GtkWidget *glade_menuitem;
  GtkWidget *table2;
  GtkWidget *table_row_label;
  GtkWidget *table_col_label;
  GtkWidget *table_viewport;
  GtkWidget *table_drawing;
  GtkWidget *table_vscrollbar;
  GtkWidget *label2;
  GtkWidget *uni_page;
  GtkWidget *hbox4;
  GtkWidget *label11;
  GtkWidget *block_sel;
  GtkWidget *block_sel_menu;
  GtkWidget *table4;
  GtkWidget *uni_row;
  GtkWidget *uni_col;
  GtkWidget *uni_viewport;
  GtkWidget *uni_drawing;
  GtkWidget *uni_vscrollbar;
  GtkWidget *label10;
  GtkWidget *vbox1;
  GtkWidget *font_button;
  GtkWidget *hbox2;
  GtkWidget *label7;
  GtkWidget *code_entry;
  GtkWidget *sel_button;
  GtkWidget *adopt_text;
  GtkWidget *copy_button;
  GtkWidget *cancel_button;

  palette_window = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_widget_set_name (palette_window, "palette_window");
  gtk_object_set_data (GTK_OBJECT (palette_window), "palette_window", palette_window);
  gtk_window_set_title (GTK_WINDOW (palette_window), "palette");

  hbox1 = gtk_hbox_new (FALSE, 5);
  gtk_widget_set_name (hbox1, "hbox1");
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_container_add (GTK_CONTAINER (palette_window), hbox1);
  gtk_container_set_border_width (GTK_CONTAINER (hbox1), 5);

  notebook = gtk_notebook_new ();
  gtk_widget_set_name (notebook, "notebook");
  gtk_widget_ref (notebook);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "notebook", notebook,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (notebook);
  gtk_box_pack_start (GTK_BOX (hbox1), notebook, TRUE, TRUE, 0);

  symbol_page = gtk_hpaned_new ();
  gtk_widget_set_name (symbol_page, "symbol_page");
  gtk_widget_ref (symbol_page);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "symbol_page", symbol_page,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (symbol_page);
  gtk_container_add (GTK_CONTAINER (notebook), symbol_page);
  gtk_container_set_border_width (GTK_CONTAINER (symbol_page), 5);

  symbol_kind_clist = gtk_clist_new (1);
  gtk_widget_set_name (symbol_kind_clist, "symbol_kind_clist");
  gtk_widget_ref (symbol_kind_clist);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "symbol_kind_clist", symbol_kind_clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (symbol_kind_clist);
  gtk_paned_pack1 (GTK_PANED (symbol_page), symbol_kind_clist, FALSE, TRUE);
  gtk_clist_set_column_width (GTK_CLIST (symbol_kind_clist), 0, 80);
  gtk_clist_column_titles_show (GTK_CLIST (symbol_kind_clist));

  label3 = gtk_label_new ("種類");
  gtk_widget_set_name (label3, "label3");
  gtk_widget_ref (label3);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label3", label3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label3);
  gtk_clist_set_column_widget (GTK_CLIST (symbol_kind_clist), 0, label3);

  scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_name (scrolledwindow1, "scrolledwindow1");
  gtk_widget_ref (scrolledwindow1);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "scrolledwindow1", scrolledwindow1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrolledwindow1);
  gtk_paned_pack2 (GTK_PANED (symbol_page), scrolledwindow1, TRUE, TRUE);
  gtk_widget_set_usize (scrolledwindow1, 270, -2);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow1), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  symbol_char_clist = gtk_clist_new (3);
  gtk_widget_set_name (symbol_char_clist, "symbol_char_clist");
  gtk_widget_ref (symbol_char_clist);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "symbol_char_clist", symbol_char_clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (symbol_char_clist);
  gtk_container_add (GTK_CONTAINER (scrolledwindow1), symbol_char_clist);
  gtk_clist_set_column_width (GTK_CLIST (symbol_char_clist), 0, 42);
  gtk_clist_set_column_width (GTK_CLIST (symbol_char_clist), 1, 48);
  gtk_clist_set_column_width (GTK_CLIST (symbol_char_clist), 2, 80);
  gtk_clist_column_titles_show (GTK_CLIST (symbol_char_clist));

  label4 = gtk_label_new ("文字");
  gtk_widget_set_name (label4, "label4");
  gtk_widget_ref (label4);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label4", label4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label4);
  gtk_clist_set_column_widget (GTK_CLIST (symbol_char_clist), 0, label4);

  label5 = gtk_label_new ("コード");
  gtk_widget_set_name (label5, "label5");
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label5", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_clist_set_column_widget (GTK_CLIST (symbol_char_clist), 1, label5);

  label6 = gtk_label_new ("名前");
  gtk_widget_set_name (label6, "label6");
  gtk_widget_ref (label6);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label6", label6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label6);
  gtk_clist_set_column_widget (GTK_CLIST (symbol_char_clist), 2, label6);

  label1 = gtk_label_new ("記号表");
  gtk_widget_set_name (label1, "label1");
  gtk_widget_ref (label1);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label1", label1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label1);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 0), label1);

  table_page = gtk_vbox_new (FALSE, 5);
  gtk_widget_set_name (table_page, "table_page");
  gtk_widget_ref (table_page);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_page", table_page,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_page);
  gtk_container_add (GTK_CONTAINER (notebook), table_page);
  gtk_container_set_border_width (GTK_CONTAINER (table_page), 5);

  hbox3 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox3, "hbox3");
  gtk_widget_ref (hbox3);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "hbox3", hbox3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox3);
  gtk_box_pack_start (GTK_BOX (table_page), hbox3, FALSE, FALSE, 0);

  label9 = gtk_label_new ("文字集合：");
  gtk_widget_set_name (label9, "label9");
  gtk_widget_ref (label9);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label9", label9,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label9);
  gtk_box_pack_start (GTK_BOX (hbox3), label9, FALSE, FALSE, 0);

  charset_sel = gtk_option_menu_new ();
  gtk_widget_set_name (charset_sel, "charset_sel");
  gtk_widget_ref (charset_sel);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "charset_sel", charset_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (charset_sel);
  gtk_box_pack_start (GTK_BOX (hbox3), charset_sel, TRUE, TRUE, 0);
  charset_sel_menu = gtk_menu_new ();
  glade_menuitem = gtk_menu_item_new_with_label ("iso8859-1");
  gtk_widget_show (glade_menuitem);
  gtk_menu_append (GTK_MENU (charset_sel_menu), glade_menuitem);
  glade_menuitem = gtk_menu_item_new_with_label ("jisx0208.1983-0");
  gtk_widget_show (glade_menuitem);
  gtk_menu_append (GTK_MENU (charset_sel_menu), glade_menuitem);
  gtk_option_menu_set_menu (GTK_OPTION_MENU (charset_sel), charset_sel_menu);

  table2 = gtk_table_new (2, 3, FALSE);
  gtk_widget_set_name (table2, "table2");
  gtk_widget_ref (table2);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table2", table2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table2);
  gtk_box_pack_start (GTK_BOX (table_page), table2, TRUE, TRUE, 0);

  table_row_label = gtk_drawing_area_new ();
  gtk_widget_set_name (table_row_label, "table_row_label");
  gtk_widget_ref (table_row_label);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_row_label", table_row_label,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_row_label);
  gtk_table_attach (GTK_TABLE (table2), table_row_label, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
  gtk_widget_set_usize (table_row_label, 40, -2);

  table_col_label = gtk_drawing_area_new ();
  gtk_widget_set_name (table_col_label, "table_col_label");
  gtk_widget_ref (table_col_label);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_col_label", table_col_label,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_col_label);
  gtk_table_attach (GTK_TABLE (table2), table_col_label, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_widget_set_usize (table_col_label, -2, 20);

  table_viewport = gtk_viewport_new (NULL, NULL);
  gtk_widget_set_name (table_viewport, "table_viewport");
  gtk_widget_ref (table_viewport);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_viewport", table_viewport,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_viewport);
  gtk_table_attach (GTK_TABLE (table2), table_viewport, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  table_drawing = gtk_drawing_area_new ();
  gtk_widget_set_name (table_drawing, "table_drawing");
  gtk_widget_ref (table_drawing);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_drawing", table_drawing,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_drawing);
  gtk_container_add (GTK_CONTAINER (table_viewport), table_drawing);
  GTK_WIDGET_SET_FLAGS (table_drawing, GTK_CAN_FOCUS);
  gtk_widget_set_events (table_drawing, GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_KEY_PRESS_MASK);

  table_vscrollbar = gtk_vscrollbar_new (GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 8836, 10, 1, 1)));
  gtk_widget_set_name (table_vscrollbar, "table_vscrollbar");
  gtk_widget_ref (table_vscrollbar);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table_vscrollbar", table_vscrollbar,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table_vscrollbar);
  gtk_table_attach (GTK_TABLE (table2), table_vscrollbar, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  label2 = gtk_label_new ("文字コード表");
  gtk_widget_set_name (label2, "label2");
  gtk_widget_ref (label2);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label2", label2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label2);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 1), label2);

  uni_page = gtk_vbox_new (FALSE, 5);
  gtk_widget_set_name (uni_page, "uni_page");
  gtk_widget_ref (uni_page);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_page", uni_page,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_page);
  gtk_container_add (GTK_CONTAINER (notebook), uni_page);
  gtk_container_set_border_width (GTK_CONTAINER (uni_page), 5);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox4, "hbox4");
  gtk_widget_ref (hbox4);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "hbox4", hbox4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox4);
  gtk_box_pack_start (GTK_BOX (uni_page), hbox4, FALSE, FALSE, 0);

  label11 = gtk_label_new ("ブロック：");
  gtk_widget_set_name (label11, "label11");
  gtk_widget_ref (label11);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label11", label11,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label11);
  gtk_box_pack_start (GTK_BOX (hbox4), label11, FALSE, FALSE, 0);

  block_sel = gtk_option_menu_new ();
  gtk_widget_set_name (block_sel, "block_sel");
  gtk_widget_ref (block_sel);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "block_sel", block_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (block_sel);
  gtk_box_pack_start (GTK_BOX (hbox4), block_sel, TRUE, TRUE, 0);
  block_sel_menu = gtk_menu_new ();
  glade_menuitem = gtk_menu_item_new_with_label ("Basic Latin");
  gtk_widget_show (glade_menuitem);
  gtk_menu_append (GTK_MENU (block_sel_menu), glade_menuitem);
  glade_menuitem = gtk_menu_item_new_with_label ("Latin-1 Supplement");
  gtk_widget_show (glade_menuitem);
  gtk_menu_append (GTK_MENU (block_sel_menu), glade_menuitem);
  gtk_option_menu_set_menu (GTK_OPTION_MENU (block_sel), block_sel_menu);

  table4 = gtk_table_new (2, 3, FALSE);
  gtk_widget_set_name (table4, "table4");
  gtk_widget_ref (table4);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "table4", table4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table4);
  gtk_box_pack_start (GTK_BOX (uni_page), table4, TRUE, TRUE, 0);

  uni_row = gtk_drawing_area_new ();
  gtk_widget_set_name (uni_row, "uni_row");
  gtk_widget_ref (uni_row);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_row", uni_row,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_row);
  gtk_table_attach (GTK_TABLE (table4), uni_row, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);
  gtk_widget_set_usize (uni_row, 40, -2);

  uni_col = gtk_drawing_area_new ();
  gtk_widget_set_name (uni_col, "uni_col");
  gtk_widget_ref (uni_col);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_col", uni_col,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_col);
  gtk_table_attach (GTK_TABLE (table4), uni_col, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);
  gtk_widget_set_usize (uni_col, -2, 20);

  uni_viewport = gtk_viewport_new (NULL, NULL);
  gtk_widget_set_name (uni_viewport, "uni_viewport");
  gtk_widget_ref (uni_viewport);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_viewport", uni_viewport,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_viewport);
  gtk_table_attach (GTK_TABLE (table4), uni_viewport, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  uni_drawing = gtk_drawing_area_new ();
  gtk_widget_set_name (uni_drawing, "uni_drawing");
  gtk_widget_ref (uni_drawing);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_drawing", uni_drawing,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_drawing);
  gtk_container_add (GTK_CONTAINER (uni_viewport), uni_drawing);
  GTK_WIDGET_SET_FLAGS (uni_drawing, GTK_CAN_FOCUS);
  gtk_widget_set_events (uni_drawing, GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | GDK_KEY_PRESS_MASK);

  uni_vscrollbar = gtk_vscrollbar_new (GTK_ADJUSTMENT (gtk_adjustment_new (0, 0, 8836, 10, 1, 1)));
  gtk_widget_set_name (uni_vscrollbar, "uni_vscrollbar");
  gtk_widget_ref (uni_vscrollbar);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "uni_vscrollbar", uni_vscrollbar,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (uni_vscrollbar);
  gtk_table_attach (GTK_TABLE (table4), uni_vscrollbar, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  label10 = gtk_label_new ("Unicode表");
  gtk_widget_set_name (label10, "label10");
  gtk_widget_ref (label10);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label10", label10,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label10);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 2), label10);

  vbox1 = gtk_vbox_new (FALSE, 5);
  gtk_widget_set_name (vbox1, "vbox1");
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_box_pack_start (GTK_BOX (hbox1), vbox1, FALSE, TRUE, 0);

  font_button = gtk_button_new_with_label ("フォント...");
  gtk_widget_set_name (font_button, "font_button");
  gtk_widget_ref (font_button);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "font_button", font_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (font_button);
  gtk_box_pack_start (GTK_BOX (vbox1), font_button, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox2, "hbox2");
  gtk_widget_ref (hbox2);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "hbox2", hbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox2, FALSE, FALSE, 0);

  label7 = gtk_label_new ("コード：");
  gtk_widget_set_name (label7, "label7");
  gtk_widget_ref (label7);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "label7", label7,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label7);
  gtk_box_pack_start (GTK_BOX (hbox2), label7, FALSE, FALSE, 0);

  code_entry = gtk_entry_new ();
  gtk_widget_set_name (code_entry, "code_entry");
  gtk_widget_ref (code_entry);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "code_entry", code_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (code_entry);
  gtk_box_pack_start (GTK_BOX (hbox2), code_entry, TRUE, TRUE, 0);
  gtk_widget_set_usize (code_entry, 65, -2);
  gtk_entry_set_editable (GTK_ENTRY (code_entry), FALSE);

  sel_button = gtk_button_new_with_label ("↓");
  gtk_widget_set_name (sel_button, "sel_button");
  gtk_widget_ref (sel_button);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "sel_button", sel_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (sel_button);
  gtk_box_pack_start (GTK_BOX (vbox1), sel_button, FALSE, FALSE, 0);

  adopt_text = gtk_text_new (NULL, NULL);
  gtk_widget_set_name (adopt_text, "adopt_text");
  gtk_widget_ref (adopt_text);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "adopt_text", adopt_text,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (adopt_text);
  gtk_box_pack_start (GTK_BOX (vbox1), adopt_text, TRUE, TRUE, 0);
  gtk_widget_set_usize (adopt_text, 80, -2);
  gtk_text_set_editable (GTK_TEXT (adopt_text), TRUE);

  copy_button = gtk_button_new_with_label ("コピー");
  gtk_widget_set_name (copy_button, "copy_button");
  gtk_widget_ref (copy_button);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "copy_button", copy_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (copy_button);
  gtk_box_pack_start (GTK_BOX (vbox1), copy_button, FALSE, FALSE, 0);

  cancel_button = gtk_button_new_with_label ("閉じる");
  gtk_widget_set_name (cancel_button, "cancel_button");
  gtk_widget_ref (cancel_button);
  gtk_object_set_data_full (GTK_OBJECT (palette_window), "cancel_button", cancel_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cancel_button);
  gtk_box_pack_start (GTK_BOX (vbox1), cancel_button, FALSE, FALSE, 0);

  gtk_signal_connect (GTK_OBJECT (palette_window), "realize",
                      GTK_SIGNAL_FUNC (on_palette_window_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (palette_window), "destroy",
                      GTK_SIGNAL_FUNC (on_palette_window_destroy),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (symbol_page), "realize",
                      GTK_SIGNAL_FUNC (on_symbol_page_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (symbol_kind_clist), "select_row",
                      GTK_SIGNAL_FUNC (on_symbol_kind_clist_select_row),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (symbol_kind_clist), "realize",
                      GTK_SIGNAL_FUNC (on_symbol_kind_clist_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (symbol_char_clist), "select_row",
                      GTK_SIGNAL_FUNC (on_symbol_char_clist_select_row),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_page), "realize",
                      GTK_SIGNAL_FUNC (on_table_page_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_row_label), "expose_event",
                      GTK_SIGNAL_FUNC (on_table_row_label_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_col_label), "expose_event",
                      GTK_SIGNAL_FUNC (on_table_col_label_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "expose_event",
                      GTK_SIGNAL_FUNC (on_table_drawing_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "button_press_event",
                      GTK_SIGNAL_FUNC (on_table_drawing_button_press_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "key_press_event",
                      GTK_SIGNAL_FUNC (on_table_drawing_key_press_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "focus_in_event",
                      GTK_SIGNAL_FUNC (on_table_drawing_focus_in_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "focus_out_event",
                      GTK_SIGNAL_FUNC (on_table_drawing_focus_out_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "realize",
                      GTK_SIGNAL_FUNC (on_table_drawing_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "size_allocate",
                      GTK_SIGNAL_FUNC (on_table_drawing_size_allocate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (table_drawing), "draw_focus",
                      GTK_SIGNAL_FUNC (on_table_drawing_draw_focus),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_page), "realize",
                      GTK_SIGNAL_FUNC (on_uni_page_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_row), "expose_event",
                      GTK_SIGNAL_FUNC (on_uni_row_label_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_col), "expose_event",
                      GTK_SIGNAL_FUNC (on_uni_col_label_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "expose_event",
                      GTK_SIGNAL_FUNC (on_uni_drawing_expose_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "button_press_event",
                      GTK_SIGNAL_FUNC (on_uni_drawing_button_press_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "key_press_event",
                      GTK_SIGNAL_FUNC (on_uni_drawing_key_press_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "focus_in_event",
                      GTK_SIGNAL_FUNC (on_uni_drawing_focus_in_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "focus_out_event",
                      GTK_SIGNAL_FUNC (on_uni_drawing_focus_out_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "realize",
                      GTK_SIGNAL_FUNC (on_uni_drawing_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "size_allocate",
                      GTK_SIGNAL_FUNC (on_uni_drawing_size_allocate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (uni_drawing), "draw_focus",
                      GTK_SIGNAL_FUNC (on_uni_drawing_draw_focus),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (font_button), "clicked",
                      GTK_SIGNAL_FUNC (on_font_button_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (sel_button), "clicked",
                      GTK_SIGNAL_FUNC (on_sel_button_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (copy_button), "clicked",
                      GTK_SIGNAL_FUNC (on_copy_button_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (cancel_button), "clicked",
                      GTK_SIGNAL_FUNC (on_cancel_button_clicked),
                      NULL);

  return palette_window;
}

