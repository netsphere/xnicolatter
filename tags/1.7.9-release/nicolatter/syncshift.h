// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifdef USE_GTK
  #include <gtk/gtk.h>
#else
  #include <X11/Xlib.h>
  #include <X11/Intrinsic.h>
#endif  // USE_GTK

#include "LevelSelector.h"

//////////////////////////////////////////////////////////////////////
// SyncSelector

class SyncSelector: public LevelSelector
    // 同時打鍵シフト動作
{
    typedef LevelSelector super;
    
    KeyEvent gr, sr;
    int state;
    int timeout_id;
public:
    SyncSelector();
    virtual ~SyncSelector();
    virtual void input(InputContext* ic, const KeyEvent& event);
    void setShiftKey(int left, int right);
    virtual string getStatus() const;
private:
#ifdef USE_GTK
    static gint onTimeout(void* this_);
#else
    static void onTimeout(void* closure, XtIntervalId* id);
#endif
};
