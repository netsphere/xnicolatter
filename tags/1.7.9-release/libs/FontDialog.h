// フォント選択クラス

#include <string>
#include <vector>

#include <dialog.h>

using namespace std;

///////////////////////////////////////////////////////////////////////
// FontChooser

class FontChooser
{
public:
    FontChooser();
    virtual ~FontChooser();
    string getFontName() const;
};

///////////////////////////////////////////////////////////////////////
// FontDialog

struct Charset
{
    string name;        // 'iso8859-1'など
    string cur_face[4];    // 書体 'b&h' 'lucidatypewriter' 'sans' 'm'
    string cur_style[2];    // スタイル 'black' 'i'
        // スタイルは文字集合間で共通にしたいが，jisx0208のスタイルが
        // 不足するため，当面，個別に指定することにする
    GtkWidget* face_sel;
    GtkWidget* style_sel;
};

class FontDialog: public Dialog
{
    typedef Dialog super;

    typedef vector<Charset> Charsets;
    Charsets charsets;
    int ptSize;
    GtkWidget* size_metrics[2];
    GtkWidget* preview_entry;
    bool isChanged;
    string cur_font;
    GtkWidget* scalable_only;
    GtkWidget* size_entry;
    GtkWidget* size_list;
    
public:
    FontDialog(GtkWindow* parent);
    virtual ~FontDialog();
    string getFontName() const;
private:
    void create();
    GtkWidget* create_sub();
    void getFaces();
    void updatePreview();
    void updateSizeSel();
    void updateStyleSel(Charset* cs);
    void setChanged(bool );
    
    static void onFaceSelected(GtkEditable* editable, FontDialog* );
    static void onStyleSelected(GtkEditable* editable, FontDialog* );
    static gint onSizeEntryChanged(GtkWidget* widget,
                             GdkEventFocus* event, FontDialog* this_);
    static void onSizeListChanged(GtkCList* clist,
                                   gint row, gint column,
                                   GdkEvent* event);
    static void onScalableOnlyChanged(GtkToggleButton* b,
                                       FontDialog* this_);

    static void onOK(GtkButton* , FontDialog* this_);
    static void onCancel(GtkButton* , FontDialog* this_);
    static void onApplyNow(GtkButton* , FontDialog* this_);

    static gint onIdle(void* );
};
