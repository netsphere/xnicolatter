// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <string>
#include <cstdio>
using namespace std;

//////////////////////////////////////////////////////////////////////
// GlobalProperty

class GlobalProperty
    // Q's Nicolatter大域情報
{
public:
    int conv_server;
    bool use_keymap;
    string keymap_file;
    string alnum_keymap;
    string kana_key;
    int shift_method;
    string shift_key[2];
    bool use_roma;
    string roma_file;
    bool roma_strict;

    string preedit_font;
    string status_font;

    // プロトコル
    bool xim;
    bool iiimp;

private:
    static const string RC_FILE;
    static const string SYS_RC_FILE;
    static const char* conv_servs[];
    static const char* shift_methods[];

public:
    GlobalProperty();
    virtual ~GlobalProperty();

    bool save() const;
    bool load();
    void initialize();
private:
    void save_sub(FILE* fp) const;
};

//////////////////////////////////////////////////////////////////////

extern GlobalProperty global_prop;
