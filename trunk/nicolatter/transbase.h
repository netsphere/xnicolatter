﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_TRANSBASE_H
#define QSNICOLA_TRANSBASE_H

#include <vector>
#include <inttypes.h> // uint16_t
#include <misc.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// SendBuffer

class SendBuffer
{
    struct RPos {
        int siz;
        int pos;
        RPos(int x, int y): siz(x), pos(y) {}
    };
protected:
    unsigned char buf[2000];
    unsigned char* cur;
    bool bigEndian;
    vector<RPos> reserved;
public:
    virtual ~SendBuffer();
    virtual int getData(unsigned char* out) = 0;
    void card16(uint16_t val);
    void card32(uint32_t val);
    void reserve16();
    void reserve32();
    int len_from_reserved() const;
    void store_back(int n);   // 予約した領域に値を入れる。
    void pad(); // 4バイト境界まで0を詰める
protected:
    SendBuffer(bool endian);

    void debug_out() const;

private:
    void card16(unsigned char* pos, uint16_t val);
    void card32(unsigned char* pos, uint32_t val);
};

////////////////////////////////////////////////////////////////////////
// RecvBuffer

class RecvBuffer
{
protected:
    unsigned char* buf;
    unsigned char* cur;
    int count_;
    bool bigEndian;
    bool filled;
public:
    virtual ~RecvBuffer();
    virtual bool input(int fd) = 0; // XIM TCP transport / IIIMP
    virtual bool isFilled() const; // XIM TCP transport / IIIMP
    virtual bool isBigEndian() const;
    uint16_t card16();
    uint32_t card32();
    int pad();  // 戻り値：何バイト進めたか
protected:
    RecvBuffer(bool endian);

    void debug_out() const;
};

#endif // QSNICOLA_TRANSPORT_H
