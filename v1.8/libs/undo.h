
////////////////////////////////////////////////////////////////////////
// UndoList

// このクラスはWindows/Linuxで共通

template <typename Ty>
class UndoList
    // ex: UndoList<TextChunk*> undo_list;
{
    typedef vector<Ty> ChunkList;
    ChunkList list;
    ChunkList::iterator current;
    
public:
    UndoList()
    {
        current = list.end();
    }

    virtual ~UndoList()
    {
        clear();
    }

    void add(const Ty& chunk)
    {
        if (current != list.end()) {
            ChunkList::iterator i;
            for (i = current; i != list.end(); i++)
                delete *i;
            list.erase(current, list.end());
        }
        list.push_back(chunk);
        current = list.end();
    }

    Ty undo()
        // オブジェクトはUndoListが所有するので，undo/redo()で得られたポインタを
        // deleteしてはならない。
    {
        if (canUndo())
            return *(--current);
        else
            return NULL;
    }

    Ty redo()
    {
        if (canRedo())
            return *(current++);
        else
            return NULL;
    }

    void clear()
    {
        ChunkList::iterator i;
        for (i = list.begin(); i != list.end(); i++)
            delete *i;
        list.clear();
        current = list.end();
    }

    bool canUndo() const
    {
        return current != list.begin();
    }
    
    bool canRedo() const
    {
        return current != list.end();
    }

    string getUndoText() const
    {
        if (canUndo())
            return (*(current - 1))->getText();
        else
            return "";
    }

    string getRedoText() const
    {
        if (canRedo())
            return (*current)->getText();
        else
            return "";
    }

private:
    UndoList(const UndoList& );
    UndoList& operator = (const UndoList& );
};

////////////////////////////////////////////////////////////////////////
