// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.
//   email: <mailto:vzw00011@nifty.ne.jp>
//   website: <http://www2.airnet.ne.jp/pak04955/>

// Canna用の編集ウィンドウ，ステータスウィンドウ，候補ウィンドウ

#include "../config.h"
#ifdef USE_CANNA

#ifdef USE_GTK
#include <gdk/gdkx.h>
#else
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Form.h>
#endif  // USE_GTK

#include "CannaWindow.h"
#include "CannaConv.h"
#include "client.h"
#include "LevelSelector.h"
#include "start.h"
#include "../global.h"

///////////////////////////////////////////////////////////////////////

#ifdef USE_GTK
void update_line(GtkWidget* widget, const CPoint& loc, XFontSet fs,
                 const EchoLine& echo, bool hasCaret)
#else
void update_line(Widget widget, const CPoint& loc, XFontSet fs,
                 const EchoLine& echo, bool hasCaret)
#endif
{
    static int blue = 0;
    static int white = 0;
    if (!blue && !white) {
        blue = getColor(top_display, "blue");
        white = getColor(top_display, "white");
    }

    if (echo.revLen != 0 && hasCaret)
        Caret::instance()->setVisible(false);

    int cax = 0;
#ifdef USE_GTK
    Window w = GDK_WINDOW_XWINDOW(widget->window);
    GC gc = XCreateGC(top_display, GDK_WINDOW_XWINDOW(widget->window),
                      0, 0);
#else
    Window w = XtWindow(widget);
    GC gc = XtGetGC(widget, 0, NULL);
#endif
    XClearWindow(top_display, w);

    XSetForeground(top_display, gc, blue);
    XSetBackground(top_display, gc, white);

    char mb_buf[1000];
    XRectangle logical;
    int x, y;
    c16_wcstombs(mb_buf, echo.line, sizeof(mb_buf));
    XmbTextExtents(fs, mb_buf, strlen(mb_buf), NULL, &logical);
    x = -logical.x + loc.x;
    y = -logical.y + loc.y;

    int i;
    for (i = 0; i < echo.length; i++) {
        int clen = c16_wctomb(mb_buf, echo.line[i]);

        if (echo.revLen != 0) {
            if (i == echo.revPos) {
                XSetForeground(top_display, gc, white);
                XSetBackground(top_display, gc, blue);
            }
            else if (i == echo.revPos + echo.revLen) {
                XSetForeground(top_display, gc, blue);
                XSetBackground(top_display, gc, white);
            }
        }
        else {
            if (i == echo.revPos)
                cax = x;
        }

        XmbDrawImageString(top_display, w, fs, gc, x, y, mb_buf, clen);
        XmbTextExtents(fs, mb_buf, clen, NULL, &logical);
        x += logical.width;
    }
#ifdef USE_GTK
    XFreeGC(top_display, gc);
#else
    XtReleaseGC(widget, gc);
    gc = 0;
#endif
    
    if (i == echo.revPos)
        cax = x;

    if (echo.revLen == 0 && hasCaret) {
        Caret::instance()->setPosition(CRect(cax, loc.y, 2, logical.height));
        Caret::instance()->setVisible(true);
    }
}

///////////////////////////////////////////////////////////////////////
// CannaPreedit

CannaPreedit::CannaPreedit(CannaConv* conv_, InputContext* ic):
                    super(ic), conv(conv_)
{
}

CannaPreedit::~CannaPreedit()
{
}

void CannaPreedit::draw(const CRect& )
{
    assert(conv);
    update_line(canvas, CPoint(PREEDIT_BORDER, PREEDIT_BORDER),
                font.xfont(), conv->echo, true);
}

void CannaPreedit::update()
{
    if (conv->echo.length <= 0) {
        Caret::instance()->setVisible(false);
        setVisible(false);
        return;
    }

    if (!font.xfont()) {
        updateFont();
        assert(font.xfont());
    }

    char mb_buf[1000];
    c16_wcstombs(mb_buf, conv->echo.line, sizeof(mb_buf));
    XRectangle logical;
    XmbTextExtents(font.xfont(), mb_buf, strlen(mb_buf), NULL, &logical);

    adjustLocation(logical);
    setVisible(true);

#ifdef USE_GTK
    gtk_widget_draw(GTK_WIDGET(window), 0);
#else
    Window w = XtWindow(canvas);
    if (w)
        XClearArea(top_display, w, 0, 0, 0, 0, True);
#endif
}

///////////////////////////////////////////////////////////////////////
// CannaStatus

CannaStatus::CannaStatus(CannaConv* conv_, InputContext* ic):
            super(ic), conv(conv_)
{
}

CannaStatus::~CannaStatus()
{
}

void CannaStatus::update()
{
    string line = conv->getModeLine();
/*
    // IME OFFでも現在のモードを知るためにステータスウィンドウを表示し
    // た方が便宜なので，このコードをコメントアウトした。

    // IME OFFでステータスウィンドウを消す
    bool allsp = true;
    for (int i = 0; i < line.length(); i++) {
        if (line[i] != ' ') {
            allsp = false;
            break;
        }
    }
    if (allsp) {
        setVisible(false);
        return;
    }
*/
    adjustLocation();
    setVisible(true);

    string ss = line;
    string ks = keyChar->getStatus();
    if (ks != "")
        ss += string(" ") + ks;
    
#ifdef USE_GTK
    gtk_label_set_text(GTK_LABEL(label), ss.c_str());
#else
    XtVaSetValues(label, XtNlabel, ss.c_str(), NULL);
#endif
}

///////////////////////////////////////////////////////////////////////
// CannaCandidate

CannaCandidate::CannaCandidate(CannaConv* conv_, PreeditWindow* pre)
                                : conv(conv_), preeditWindow(pre)
{
#ifdef USE_GTK
    window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_POPUP));

    canvas = gtk_drawing_area_new();
    gtk_widget_show(canvas);
    gtk_container_add(GTK_CONTAINER(window), canvas);

    gtk_signal_connect(GTK_OBJECT(canvas), "expose_event",
                    GTK_SIGNAL_FUNC(onExposed), this);
#else
    window = XtCreatePopupShell("cand-shell", overrideShellWidgetClass,
                top_widget,
                NULL, 0);
    canvas = window;
/*
    canvas = XtVaCreateManagedWidget("canvas", formWidgetClass,
                window,
                XtNwidth, 200, XtNheight, 20,
                NULL);
*/
    XtAddEventHandler(canvas, ExposureMask, False, onExposed, this);
#endif
    font = createFont(top_display, global_prop.preedit_font.c_str());
    assert(font);
}

CannaCandidate::~CannaCandidate()
{
    if (font) {
        XFreeFontSet(top_display, font);
        font = 0;
    }
    destroy();
}

static const int BORDER = 2;

void CannaCandidate::update()
{
    if (!conv->cand.line[0]) {
        setVisible(false);
        return;
    }

    XPoint pre_loc = preeditWindow->getLocation();
    pre_loc.y += preeditWindow->size.height;
    setLocation(pre_loc);

    char mb_buf[1000];
    XRectangle logical;
    c16_wcstombs(mb_buf, conv->cand.line, sizeof(mb_buf));
    XmbTextExtents(font, mb_buf, strlen(mb_buf), NULL, &logical);
    setSize(logical.width + BORDER * 2, logical.height + BORDER * 2);

    setVisible(true);
    update_line(canvas, CPoint(BORDER, BORDER), font, conv->cand, false);
}

#ifdef USE_GTK
gint CannaCandidate::onExposed(GtkWidget* w, GdkEventExpose* event,
                    CannaCandidate* this_)
{
    update_line(this_->canvas, CPoint(BORDER, BORDER), this_->font, this_->conv->cand, false);
    return TRUE;
}
#else
void CannaCandidate::onExposed(Widget w, void* closure, XEvent* event, Boolean* cont)
{
    CannaCandidate* this_ = (CannaCandidate*) closure;
    update_line(this_->canvas, CPoint(BORDER, BORDER), this_->font, this_->conv->cand, false);
}
#endif  // USE_GTK

#endif  // USE_CANNA
