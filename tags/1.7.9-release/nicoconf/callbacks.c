// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include "../config.h"

#include <stdio.h>  // popen(), pclose()
#include <signal.h> // kill()
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/keysym.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include "../global.h"
#include "../keymap.h"
#include "../DetectDlg.h"

#include <URI.h>
#include <FontDialog.h>

extern GlobalProperty global_prop;
extern GtkWindow* main_window;

// widgets

GtkToggleButton* wnn;
GtkToggleButton* canna;
GtkToggleButton* use_map;
GtkEntry* map_entry;
GtkWidget* map_choose;
GtkToggleButton* kana_ctrl;
GtkToggleButton* kana_shift;
GtkEntry* kana_entry;
GtkWidget* kana_choose;

GtkToggleButton* shift_method[3];
GtkEntry* oya_entry[2];
GtkWidget* oya_choose[2];
GtkToggleButton* use_roma;
GtkEntry* roma_entry;
GtkWidget* roma_choose;
GtkToggleButton* roma_strict;
GtkEntry* preedit_font = NULL;
GtkEntry* status_font = NULL;

void
onRealized                             (GtkWidget       *widget,
                                        gpointer         user_data)
{
    wnn = GTK_TOGGLE_BUTTON(lookup_widget(widget, "wnn"));
    canna = GTK_TOGGLE_BUTTON(lookup_widget(widget, "canna"));
#if !defined(USE_WNN)
    gtk_widget_set_sensitive(GTK_WIDGET(wnn), FALSE);
#endif
#if !defined(USE_CANNA)
    gtk_widget_set_sensitive(GTK_WIDGET(canna), FALSE);
#endif

    use_map = GTK_TOGGLE_BUTTON(lookup_widget(widget, "use_map"));
    map_entry = GTK_ENTRY(lookup_widget(widget, "map_entry"));
    map_choose = lookup_widget(widget, "map_choose");

    kana_ctrl = GTK_TOGGLE_BUTTON(lookup_widget(widget, "kana_ctrl"));
    kana_shift = GTK_TOGGLE_BUTTON(lookup_widget(widget, "kana_shift"));
    kana_entry = GTK_ENTRY(lookup_widget(widget, "kana_entry"));
    kana_choose = lookup_widget(widget, "kana_choose");

    shift_method[0] = GTK_TOGGLE_BUTTON(lookup_widget(widget, "normal"));
    shift_method[1] = GTK_TOGGLE_BUTTON(lookup_widget(widget, "prefix"));
    shift_method[2] = GTK_TOGGLE_BUTTON(lookup_widget(widget, "sync"));
    oya_entry[0] = GTK_ENTRY(lookup_widget(widget, "left_entry"));
    oya_choose[0] = lookup_widget(widget, "left_choose");
    oya_entry[1] = GTK_ENTRY(lookup_widget(widget, "right_entry"));
    oya_choose[1] = lookup_widget(widget, "right_choose");

    use_roma = GTK_TOGGLE_BUTTON(lookup_widget(widget, "use_roma"));
    roma_entry = GTK_ENTRY(lookup_widget(widget, "roma_entry"));
    roma_choose = lookup_widget(widget, "roma_choose");
    roma_strict = GTK_TOGGLE_BUTTON(lookup_widget(widget, "roma_strict"));

    preedit_font = GTK_ENTRY(lookup_widget(widget, "preedit_font"));
    status_font = GTK_ENTRY(lookup_widget(widget, "status_font"));

    // 変換サーバー
    if (global_prop.conv_server == 0)
        gtk_toggle_button_set_active(wnn, TRUE);
    else
        gtk_toggle_button_set_active(canna, TRUE);

    // 図形キー
    gtk_toggle_button_set_active(use_map, global_prop.use_keymap);
    gtk_entry_set_text(map_entry, global_prop.keymap_file.c_str());

    // 仮名キー
    int mod = getKeyMod(global_prop.kana_key);
    gtk_toggle_button_set_active(kana_ctrl, (mod & ControlMask) != 0);
    gtk_toggle_button_set_active(kana_shift, (mod & ShiftMask) != 0);
    gtk_entry_set_text(kana_entry, getKeyName(0, getKeyCode(global_prop.kana_key)).c_str());

    // シフトキー
    gtk_toggle_button_set_active(shift_method[global_prop.shift_method], TRUE);
    for (int i = 0; i < 2; i++)
        gtk_entry_set_text(oya_entry[i], global_prop.shift_key[i].c_str());

    // ローマ字変換
    gtk_toggle_button_set_active(use_roma, global_prop.use_roma);
    gtk_entry_set_text(roma_entry, global_prop.roma_file.c_str());
    gtk_toggle_button_set_active(roma_strict, global_prop.roma_strict);

    // 表示フォント
    gtk_entry_set_text(preedit_font, global_prop.preedit_font.c_str());
    gtk_entry_set_text(status_font, global_prop.status_font.c_str());
}


void
onButtonToggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    bool isWnn = gtk_toggle_button_get_active(wnn);

    // 図形キー
    gtk_widget_set_sensitive(GTK_WIDGET(map_entry),
            isWnn || gtk_toggle_button_get_active(use_map));
    gtk_widget_set_sensitive(map_choose,
            isWnn || gtk_toggle_button_get_active(use_map));

    // 仮名キー
    gtk_widget_set_sensitive(GTK_WIDGET(kana_ctrl), isWnn);
    gtk_widget_set_sensitive(GTK_WIDGET(kana_shift), isWnn);
    gtk_widget_set_sensitive(GTK_WIDGET(kana_entry), isWnn);
    gtk_widget_set_sensitive(GTK_WIDGET(kana_choose), isWnn);

    // シフトキー
    bool isSync = gtk_toggle_button_get_active(shift_method[2]);
    for (int i = 0; i < 2; i++) {
        gtk_widget_set_sensitive(GTK_WIDGET(oya_entry[i]), isSync);
        gtk_widget_set_sensitive(GTK_WIDGET(oya_choose[i]), isSync);
    }

    // ローマ字変換
    gtk_widget_set_sensitive(GTK_WIDGET(use_roma), isWnn);
    bool isUseRoma = gtk_toggle_button_get_active(use_roma);
    gtk_widget_set_sensitive(GTK_WIDGET(roma_entry), isWnn && isUseRoma);
    gtk_widget_set_sensitive(roma_choose, isWnn && isUseRoma);
    gtk_widget_set_sensitive(GTK_WIDGET(roma_strict), isWnn && isUseRoma);
}


void
onKanaKeyChoose                        (GtkButton       *button,
                                        gpointer         user_data)
{
    DetectDlg dlg(main_window);
    if (dlg.setVisible(true) == DetectDlg::IDOK)
        gtk_entry_set_text(kana_entry, getKeyName(0, dlg.keycode).c_str());
}


void
onMapFileChoose                        (GtkButton       *button,
                                        gpointer         user_data)
{
    FileDialog dlg(main_window, "キー配列定義ファイル",
                   gtk_entry_get_text(map_entry));
    if (dlg.setVisible(true) == FileDialog::IDOK)
        gtk_entry_set_text(map_entry, dlg.getFile().c_str());
}


void
onLeftShiftChoose                      (GtkButton       *button,
                                        gpointer         user_data)
{
    DetectDlg dlg(main_window);
    if (dlg.setVisible(true) != DetectDlg::IDOK)
        return;

    KeySym sym = XKeycodeToKeysym(GDK_DISPLAY(), dlg.keycode, 0);
    if (sym == XK_Control_L || sym == XK_Control_R
            || sym == XK_Alt_L || sym == XK_Alt_R
            || sym == XK_Shift_L || sym == XK_Shift_R) {
        MessageBox msg(main_window, "[Ctrl], [Shift], [Alt]キーは親指キーにできません");
        msg.setVisible(true);
        return;
    }

    gtk_entry_set_text(oya_entry[0], getKeyName(0, dlg.keycode).c_str());
}


void
onRightShiftChoose                     (GtkButton       *button,
                                        gpointer         user_data)
{
    DetectDlg dlg(main_window);
    if (dlg.setVisible(true) != DetectDlg::IDOK)
        return;

    KeySym sym = XKeycodeToKeysym(GDK_DISPLAY(), dlg.keycode, 0);
    if (sym == XK_Control_L || sym == XK_Control_R
            || sym == XK_Alt_L || sym == XK_Alt_R
            || sym == XK_Shift_L || sym == XK_Shift_R) {
        MessageBox msg(main_window, "[Ctrl], [Shift], [Alt]キーは親指キーにできません");
        msg.setVisible(true);
        return;
    }

    gtk_entry_set_text(oya_entry[1], getKeyName(0, dlg.keycode).c_str());
}


void
onRomaFileChoose                       (GtkButton       *button,
                                        gpointer         user_data)
{
    FileDialog dlg(main_window, "ローマ字変換定義ファイル",
                   gtk_entry_get_text(roma_entry));
    if (dlg.setVisible(true) == FileDialog::IDOK)
        gtk_entry_set_text(roma_entry, dlg.getFile().c_str());
}


void updateServer()
    // nicolatterにSIGHUPを送る
{
    FILE* pp = popen(FIND_SERVER_CMD, "r");
    if (pp) {
        char ps_line[1000];
        int pid;
        char dmy[1000], cmd[1000];
        while (fgets(ps_line, sizeof(ps_line), pp)) {
            if (sscanf(ps_line, "%d %s %s %s %s",
                       &pid, dmy, dmy, dmy, cmd) == 5) {
                if (FileName(cmd).getFile() == server_name) {
                    TRACE("server pid = %d\n", pid);
                    kill(pid, SIGHUP);
                }
            }
        }
        pclose(pp);
    }
}

void
onOK                                   (GtkButton       *button,
                                        gpointer         user_data)
{
    // 変換サーバー
    global_prop.conv_server = gtk_toggle_button_get_active(wnn) ? 0 : 1;

    // 図形キー
    global_prop.use_keymap = gtk_toggle_button_get_active(use_map);
    global_prop.keymap_file = gtk_entry_get_text(map_entry);

    // 仮名キー
    int mod = 0;
    if (gtk_toggle_button_get_active(kana_ctrl))
        mod |= ControlMask;
    if (gtk_toggle_button_get_active(kana_shift))
        mod |= ShiftMask;
    global_prop.kana_key = getKeyName2(mod,
                getKeyCode(gtk_entry_get_text(kana_entry)));

    // シフトキー
    for (int i = 0; i < 3; i++) {
        if (gtk_toggle_button_get_active(shift_method[i])) {
            global_prop.shift_method = i;
            break;
        }
    }

    if (global_prop.shift_method == 2) {
        global_prop.shift_key[0] = gtk_entry_get_text(oya_entry[0]);
        global_prop.shift_key[1] = gtk_entry_get_text(oya_entry[1]);
    }

    global_prop.use_roma = gtk_toggle_button_get_active(use_roma);
    global_prop.roma_file = gtk_entry_get_text(roma_entry);
    global_prop.roma_strict = gtk_toggle_button_get_active(roma_strict);

    // フォント
    global_prop.preedit_font = gtk_entry_get_text(preedit_font);
    global_prop.status_font = gtk_entry_get_text(status_font);
    
    global_prop.save();
    updateServer();
    
    gtk_main_quit();
}


void
onCancel                               (GtkButton       *button,
                                        gpointer         user_data)
{
    gtk_main_quit();
}

void
on_preedit_sel_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
    FontDialog dlg(main_window);
    if (dlg.setVisible(true) == FontDialog::IDOK)
        gtk_entry_set_text(preedit_font, dlg.getFontName().c_str());
}


void
on_status_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
    FontDialog dlg(main_window);
    if (dlg.setVisible(true) == FontDialog::IDOK)
        gtk_entry_set_text(status_font, dlg.getFontName().c_str());
}


gboolean
on_nicoconf_delete_event               (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    onCancel(NULL, NULL);
    return TRUE;
}


void
on_cnv_fg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_cnv_bg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_inp_fg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_inp_bg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}

