/*
 * 編集禁止! - このファイルはGladeによって生成されています.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

GtkWidget*
create_map_window ()
{
  GtkWidget *map_window;
  GtkWidget *vbox2;
  GtkWidget *menubar1;
  GtkWidget *file;
  GtkWidget *file_menu;
  GtkAccelGroup *file_menu_accels;
  GtkWidget *file_new;
  GtkWidget *file_open;
  GtkWidget *file_save;
  GtkWidget *file_saveas;
  GtkWidget *__________1;
  GtkWidget *file_close;
  GtkWidget *view;
  GtkWidget *view_menu;
  GtkAccelGroup *view_menu_accels;
  GtkWidget *view_funcs;
  GtkWidget *view_keyprop;
  GtkWidget *view_funcprop;
  GtkWidget *help;
  GtkWidget *help_menu;
  GtkAccelGroup *help_menu_accels;
  GtkWidget *help_about;
  GtkWidget *scrolled;
  GtkWidget *map_clist;
  GtkWidget *label6;
  GtkWidget *label7;
  GtkWidget *label8;
  GtkWidget *label9;

  map_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_widget_set_name (map_window, "map_window");
  gtk_object_set_data (GTK_OBJECT (map_window), "map_window", map_window);
  gtk_widget_set_usize (map_window, 380, 200);
  gtk_window_set_title (GTK_WINDOW (map_window), _("keyconf"));

  vbox2 = gtk_vbox_new (FALSE, 0);
  gtk_widget_set_name (vbox2, "vbox2");
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (map_window), vbox2);

  menubar1 = gtk_menu_bar_new ();
  gtk_widget_set_name (menubar1, "menubar1");
  gtk_widget_ref (menubar1);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "menubar1", menubar1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (menubar1);
  gtk_box_pack_start (GTK_BOX (vbox2), menubar1, FALSE, FALSE, 0);

  file = gtk_menu_item_new_with_label (_("ファイル"));
  gtk_widget_set_name (file, "file");
  gtk_widget_ref (file);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file", file,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file);
  gtk_container_add (GTK_CONTAINER (menubar1), file);

  file_menu = gtk_menu_new ();
  gtk_widget_set_name (file_menu, "file_menu");
  gtk_widget_ref (file_menu);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_menu", file_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (file), file_menu);
  file_menu_accels = gtk_menu_ensure_uline_accel_group (GTK_MENU (file_menu));

  file_new = gtk_menu_item_new_with_label (_("新規作成"));
  gtk_widget_set_name (file_new, "file_new");
  gtk_widget_ref (file_new);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_new", file_new,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file_new);
  gtk_container_add (GTK_CONTAINER (file_menu), file_new);

  file_open = gtk_menu_item_new_with_label (_("開く..."));
  gtk_widget_set_name (file_open, "file_open");
  gtk_widget_ref (file_open);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_open", file_open,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file_open);
  gtk_container_add (GTK_CONTAINER (file_menu), file_open);

  file_save = gtk_menu_item_new_with_label (_("保存"));
  gtk_widget_set_name (file_save, "file_save");
  gtk_widget_ref (file_save);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_save", file_save,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file_save);
  gtk_container_add (GTK_CONTAINER (file_menu), file_save);

  file_saveas = gtk_menu_item_new_with_label (_("名前を付けて保存..."));
  gtk_widget_set_name (file_saveas, "file_saveas");
  gtk_widget_ref (file_saveas);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_saveas", file_saveas,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file_saveas);
  gtk_container_add (GTK_CONTAINER (file_menu), file_saveas);

  __________1 = gtk_menu_item_new ();
  gtk_widget_set_name (__________1, "__________1");
  gtk_widget_ref (__________1);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "__________1", __________1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (__________1);
  gtk_container_add (GTK_CONTAINER (file_menu), __________1);
  gtk_widget_set_sensitive (__________1, FALSE);

  file_close = gtk_menu_item_new_with_label (_("閉じる"));
  gtk_widget_set_name (file_close, "file_close");
  gtk_widget_ref (file_close);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "file_close", file_close,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (file_close);
  gtk_container_add (GTK_CONTAINER (file_menu), file_close);

  view = gtk_menu_item_new_with_label (_("表示"));
  gtk_widget_set_name (view, "view");
  gtk_widget_ref (view);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "view", view,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (view);
  gtk_container_add (GTK_CONTAINER (menubar1), view);

  view_menu = gtk_menu_new ();
  gtk_widget_set_name (view_menu, "view_menu");
  gtk_widget_ref (view_menu);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "view_menu", view_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (view), view_menu);
  view_menu_accels = gtk_menu_ensure_uline_accel_group (GTK_MENU (view_menu));

  view_funcs = gtk_check_menu_item_new_with_label (_("制御機能一覧"));
  gtk_widget_set_name (view_funcs, "view_funcs");
  gtk_widget_ref (view_funcs);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "view_funcs", view_funcs,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (view_funcs);
  gtk_container_add (GTK_CONTAINER (view_menu), view_funcs);

  view_keyprop = gtk_check_menu_item_new_with_label (_("キーのプロパティ..."));
  gtk_widget_set_name (view_keyprop, "view_keyprop");
  gtk_widget_ref (view_keyprop);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "view_keyprop", view_keyprop,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (view_keyprop);
  gtk_container_add (GTK_CONTAINER (view_menu), view_keyprop);

  view_funcprop = gtk_check_menu_item_new_with_label (_("制御機能のプロパティ..."));
  gtk_widget_set_name (view_funcprop, "view_funcprop");
  gtk_widget_ref (view_funcprop);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "view_funcprop", view_funcprop,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (view_funcprop);
  gtk_container_add (GTK_CONTAINER (view_menu), view_funcprop);

  help = gtk_menu_item_new_with_label (_("ヘルプ"));
  gtk_widget_set_name (help, "help");
  gtk_widget_ref (help);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "help", help,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (help);
  gtk_container_add (GTK_CONTAINER (menubar1), help);

  help_menu = gtk_menu_new ();
  gtk_widget_set_name (help_menu, "help_menu");
  gtk_widget_ref (help_menu);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "help_menu", help_menu,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (help), help_menu);
  help_menu_accels = gtk_menu_ensure_uline_accel_group (GTK_MENU (help_menu));

  help_about = gtk_menu_item_new_with_label (_("keyconfについて..."));
  gtk_widget_set_name (help_about, "help_about");
  gtk_widget_ref (help_about);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "help_about", help_about,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (help_about);
  gtk_container_add (GTK_CONTAINER (help_menu), help_about);

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_name (scrolled, "scrolled");
  gtk_widget_ref (scrolled);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "scrolled", scrolled,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrolled);
  gtk_box_pack_start (GTK_BOX (vbox2), scrolled, TRUE, TRUE, 0);
  gtk_widget_set_usize (scrolled, -2, 200);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  map_clist = gtk_clist_new (4);
  gtk_widget_set_name (map_clist, "map_clist");
  gtk_widget_ref (map_clist);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "map_clist", map_clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (map_clist);
  gtk_container_add (GTK_CONTAINER (scrolled), map_clist);
  gtk_clist_set_column_width (GTK_CLIST (map_clist), 0, 80);
  gtk_clist_set_column_width (GTK_CLIST (map_clist), 1, 80);
  gtk_clist_set_column_width (GTK_CLIST (map_clist), 2, 80);
  gtk_clist_set_column_width (GTK_CLIST (map_clist), 3, 80);
  gtk_clist_column_titles_show (GTK_CLIST (map_clist));

  label6 = gtk_label_new (_("キー"));
  gtk_widget_set_name (label6, "label6");
  gtk_widget_ref (label6);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "label6", label6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label6);
  gtk_clist_set_column_widget (GTK_CLIST (map_clist), 0, label6);

  label7 = gtk_label_new (_("単独打鍵"));
  gtk_widget_set_name (label7, "label7");
  gtk_widget_ref (label7);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "label7", label7,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label7);
  gtk_clist_set_column_widget (GTK_CLIST (map_clist), 1, label7);

  label8 = gtk_label_new (_("(左)シフト"));
  gtk_widget_set_name (label8, "label8");
  gtk_widget_ref (label8);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "label8", label8,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label8);
  gtk_clist_set_column_widget (GTK_CLIST (map_clist), 2, label8);

  label9 = gtk_label_new (_("右シフト"));
  gtk_widget_set_name (label9, "label9");
  gtk_widget_ref (label9);
  gtk_object_set_data_full (GTK_OBJECT (map_window), "label9", label9,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label9);
  gtk_clist_set_column_widget (GTK_CLIST (map_clist), 3, label9);

  gtk_signal_connect (GTK_OBJECT (map_window), "realize",
                      GTK_SIGNAL_FUNC (on_map_window_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (map_window), "delete_event",
                      GTK_SIGNAL_FUNC (on_map_window_delete_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (map_window), "destroy",
                      GTK_SIGNAL_FUNC (on_map_window_destroy),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (file_new), "activate",
                      GTK_SIGNAL_FUNC (on_file_new_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (file_open), "activate",
                      GTK_SIGNAL_FUNC (on_file_open_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (file_save), "activate",
                      GTK_SIGNAL_FUNC (on_file_save_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (file_saveas), "activate",
                      GTK_SIGNAL_FUNC (on_file_saveas_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (file_close), "activate",
                      GTK_SIGNAL_FUNC (on_close_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (view_funcs), "activate",
                      GTK_SIGNAL_FUNC (on_view_funcs_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (view_keyprop), "activate",
                      GTK_SIGNAL_FUNC (on_view_keyprop_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (view_funcprop), "activate",
                      GTK_SIGNAL_FUNC (on_view_funcprop_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (help_about), "activate",
                      GTK_SIGNAL_FUNC (on_about_activate),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (map_clist), "select_row",
                      GTK_SIGNAL_FUNC (on_map_select_row),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (map_clist), "button_release_event",
                      GTK_SIGNAL_FUNC (on_map_clist_button_release_event),
                      NULL);

  return map_window;
}

GtkWidget*
create_funcs_window ()
{
  GtkWidget *funcs_window;
  GtkWidget *scrolled;
  GtkWidget *funcs_clist;
  GtkWidget *label1;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *label5;

  funcs_window = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_widget_set_name (funcs_window, "funcs_window");
  gtk_object_set_data (GTK_OBJECT (funcs_window), "funcs_window", funcs_window);
  gtk_widget_set_usize (funcs_window, 460, 200);
  gtk_window_set_title (GTK_WINDOW (funcs_window), _("Control functions"));

  scrolled = gtk_scrolled_window_new (NULL, NULL);
  gtk_widget_set_name (scrolled, "scrolled");
  gtk_widget_ref (scrolled);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "scrolled", scrolled,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (scrolled);
  gtk_container_add (GTK_CONTAINER (funcs_window), scrolled);
  gtk_widget_set_usize (scrolled, -2, 200);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);

  funcs_clist = gtk_clist_new (5);
  gtk_widget_set_name (funcs_clist, "funcs_clist");
  gtk_widget_ref (funcs_clist);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "funcs_clist", funcs_clist,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (funcs_clist);
  gtk_container_add (GTK_CONTAINER (scrolled), funcs_clist);
  gtk_clist_set_column_width (GTK_CLIST (funcs_clist), 0, 80);
  gtk_clist_set_column_width (GTK_CLIST (funcs_clist), 1, 80);
  gtk_clist_set_column_width (GTK_CLIST (funcs_clist), 2, 80);
  gtk_clist_set_column_width (GTK_CLIST (funcs_clist), 3, 80);
  gtk_clist_set_column_width (GTK_CLIST (funcs_clist), 4, 80);
  gtk_clist_column_titles_show (GTK_CLIST (funcs_clist));

  label1 = gtk_label_new (_("機能名"));
  gtk_widget_set_name (label1, "label1");
  gtk_widget_ref (label1);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "label1", label1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label1);
  gtk_clist_set_column_widget (GTK_CLIST (funcs_clist), 0, label1);

  label2 = gtk_label_new (_("未入力"));
  gtk_widget_set_name (label2, "label2");
  gtk_widget_ref (label2);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "label2", label2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label2);
  gtk_clist_set_column_widget (GTK_CLIST (funcs_clist), 1, label2);

  label3 = gtk_label_new (_("入力中"));
  gtk_widget_set_name (label3, "label3");
  gtk_widget_ref (label3);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "label3", label3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label3);
  gtk_clist_set_column_widget (GTK_CLIST (funcs_clist), 2, label3);

  label4 = gtk_label_new (_("変換中"));
  gtk_widget_set_name (label4, "label4");
  gtk_widget_ref (label4);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "label4", label4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label4);
  gtk_clist_set_column_widget (GTK_CLIST (funcs_clist), 3, label4);

  label5 = gtk_label_new (_("候補一覧"));
  gtk_widget_set_name (label5, "label5");
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (funcs_window), "label5", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_clist_set_column_widget (GTK_CLIST (funcs_clist), 4, label5);

  gtk_signal_connect (GTK_OBJECT (funcs_window), "realize",
                      GTK_SIGNAL_FUNC (on_funcs_window_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (funcs_window), "delete_event",
                      GTK_SIGNAL_FUNC (on_funcs_window_delete_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (funcs_clist), "select_row",
                      GTK_SIGNAL_FUNC (on_funcs_select_row),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (funcs_clist), "button_release_event",
                      GTK_SIGNAL_FUNC (on_funcs_clist_button_release_event),
                      NULL);

  return funcs_window;
}

GtkWidget*
create_key_prop ()
{
  GtkWidget *key_prop;
  GtkWidget *table1;
  GtkWidget *label10;
  GtkWidget *label11;
  GtkWidget *hbox1;
  GtkWidget *key_ctrl;
  GtkWidget *key_entry;
  GtkWidget *key_choose;
  GtkWidget *hbox2;
  GSList *key_type_group = NULL;
  GtkWidget *type_graph;
  GtkWidget *type_func;
  GtkWidget *func_frame;
  GtkWidget *hbox3;
  GtkWidget *label15;
  GtkWidget *func_sel;
  GtkWidget *entry1;
  GtkWidget *graph_frame;
  GtkWidget *table2;
  GtkWidget *single_entry;
  GtkWidget *left_entry;
  GtkWidget *right_entry;
  GtkWidget *label13;
  GtkWidget *label14;
  GtkWidget *label12;
  GtkWidget *ok_button;
  GtkWidget *hseparator1;

  key_prop = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_widget_set_name (key_prop, "key_prop");
  gtk_object_set_data (GTK_OBJECT (key_prop), "key_prop", key_prop);
  gtk_window_set_title (GTK_WINDOW (key_prop), _("key property"));
  gtk_window_set_policy (GTK_WINDOW (key_prop), FALSE, TRUE, TRUE);

  table1 = gtk_table_new (6, 2, FALSE);
  gtk_widget_set_name (table1, "table1");
  gtk_widget_ref (table1);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "table1", table1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (key_prop), table1);
  gtk_container_set_border_width (GTK_CONTAINER (table1), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 5);

  label10 = gtk_label_new (_("キー："));
  gtk_widget_set_name (label10, "label10");
  gtk_widget_ref (label10);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label10", label10,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label10);
  gtk_table_attach (GTK_TABLE (table1), label10, 0, 1, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  label11 = gtk_label_new (_("種類："));
  gtk_widget_set_name (label11, "label11");
  gtk_widget_ref (label11);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label11", label11,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label11);
  gtk_table_attach (GTK_TABLE (table1), label11, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  hbox1 = gtk_hbox_new (FALSE, 5);
  gtk_widget_set_name (hbox1, "hbox1");
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_table_attach (GTK_TABLE (table1), hbox1, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  key_ctrl = gtk_check_button_new_with_label (_("[Ctrl]"));
  gtk_widget_set_name (key_ctrl, "key_ctrl");
  gtk_widget_ref (key_ctrl);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "key_ctrl", key_ctrl,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (key_ctrl);
  gtk_box_pack_start (GTK_BOX (hbox1), key_ctrl, FALSE, FALSE, 0);

  key_entry = gtk_entry_new ();
  gtk_widget_set_name (key_entry, "key_entry");
  gtk_widget_ref (key_entry);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "key_entry", key_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (key_entry);
  gtk_box_pack_start (GTK_BOX (hbox1), key_entry, TRUE, TRUE, 0);
  gtk_widget_set_usize (key_entry, 80, -2);
  gtk_entry_set_editable (GTK_ENTRY (key_entry), FALSE);

  key_choose = gtk_button_new_with_label (_("検出..."));
  gtk_widget_set_name (key_choose, "key_choose");
  gtk_widget_ref (key_choose);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "key_choose", key_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (key_choose);
  gtk_box_pack_start (GTK_BOX (hbox1), key_choose, FALSE, FALSE, 0);

  hbox2 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox2, "hbox2");
  gtk_widget_ref (hbox2);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "hbox2", hbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox2);
  gtk_table_attach (GTK_TABLE (table1), hbox2, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  type_graph = gtk_radio_button_new_with_label (key_type_group, _("図形キー"));
  key_type_group = gtk_radio_button_group (GTK_RADIO_BUTTON (type_graph));
  gtk_widget_set_name (type_graph, "type_graph");
  gtk_widget_ref (type_graph);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "type_graph", type_graph,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (type_graph);
  gtk_box_pack_start (GTK_BOX (hbox2), type_graph, FALSE, FALSE, 0);

  type_func = gtk_radio_button_new_with_label (key_type_group, _("制御キー"));
  key_type_group = gtk_radio_button_group (GTK_RADIO_BUTTON (type_func));
  gtk_widget_set_name (type_func, "type_func");
  gtk_widget_ref (type_func);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "type_func", type_func,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (type_func);
  gtk_box_pack_start (GTK_BOX (hbox2), type_func, FALSE, FALSE, 0);

  func_frame = gtk_frame_new (_("制御機能"));
  gtk_widget_set_name (func_frame, "func_frame");
  gtk_widget_ref (func_frame);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "func_frame", func_frame,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (func_frame);
  gtk_table_attach (GTK_TABLE (table1), func_frame, 0, 2, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

  hbox3 = gtk_hbox_new (FALSE, 5);
  gtk_widget_set_name (hbox3, "hbox3");
  gtk_widget_ref (hbox3);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "hbox3", hbox3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox3);
  gtk_container_add (GTK_CONTAINER (func_frame), hbox3);
  gtk_container_set_border_width (GTK_CONTAINER (hbox3), 5);

  label15 = gtk_label_new (_("機能名："));
  gtk_widget_set_name (label15, "label15");
  gtk_widget_ref (label15);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label15", label15,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label15);
  gtk_box_pack_start (GTK_BOX (hbox3), label15, FALSE, FALSE, 0);

  func_sel = gtk_combo_new ();
  gtk_widget_set_name (func_sel, "func_sel");
  gtk_widget_ref (func_sel);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "func_sel", func_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (func_sel);
  gtk_box_pack_start (GTK_BOX (hbox3), func_sel, TRUE, TRUE, 0);

  entry1 = GTK_COMBO (func_sel)->entry;
  gtk_widget_set_name (entry1, "entry1");
  gtk_widget_ref (entry1);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "entry1", entry1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entry1);

  graph_frame = gtk_frame_new (_("図形文字"));
  gtk_widget_set_name (graph_frame, "graph_frame");
  gtk_widget_ref (graph_frame);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "graph_frame", graph_frame,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (graph_frame);
  gtk_table_attach (GTK_TABLE (table1), graph_frame, 0, 2, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL), 0, 0);

  table2 = gtk_table_new (2, 3, FALSE);
  gtk_widget_set_name (table2, "table2");
  gtk_widget_ref (table2);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "table2", table2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table2);
  gtk_container_add (GTK_CONTAINER (graph_frame), table2);
  gtk_container_set_border_width (GTK_CONTAINER (table2), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table2), 5);

  single_entry = gtk_entry_new ();
  gtk_widget_set_name (single_entry, "single_entry");
  gtk_widget_ref (single_entry);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "single_entry", single_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (single_entry);
  gtk_table_attach (GTK_TABLE (table2), single_entry, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (single_entry, 50, -2);

  left_entry = gtk_entry_new ();
  gtk_widget_set_name (left_entry, "left_entry");
  gtk_widget_ref (left_entry);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "left_entry", left_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (left_entry);
  gtk_table_attach (GTK_TABLE (table2), left_entry, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (left_entry, 50, -2);

  right_entry = gtk_entry_new ();
  gtk_widget_set_name (right_entry, "right_entry");
  gtk_widget_ref (right_entry);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "right_entry", right_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (right_entry);
  gtk_table_attach (GTK_TABLE (table2), right_entry, 2, 3, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (right_entry, 50, -2);

  label13 = gtk_label_new (_("(左)シフト："));
  gtk_widget_set_name (label13, "label13");
  gtk_widget_ref (label13);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label13", label13,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label13);
  gtk_table_attach (GTK_TABLE (table2), label13, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label13), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label13), 0, 0.5);

  label14 = gtk_label_new (_("右シフト："));
  gtk_widget_set_name (label14, "label14");
  gtk_widget_ref (label14);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label14", label14,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label14);
  gtk_table_attach (GTK_TABLE (table2), label14, 2, 3, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label14), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label14), 0, 0.5);

  label12 = gtk_label_new (_("単独打鍵："));
  gtk_widget_set_name (label12, "label12");
  gtk_widget_ref (label12);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "label12", label12,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label12);
  gtk_table_attach (GTK_TABLE (table2), label12, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label12), GTK_JUSTIFY_LEFT);
  gtk_misc_set_alignment (GTK_MISC (label12), 0, 0.5);

  ok_button = gtk_button_new_with_label (_("適用"));
  gtk_widget_set_name (ok_button, "ok_button");
  gtk_widget_ref (ok_button);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "ok_button", ok_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (ok_button);
  gtk_table_attach (GTK_TABLE (table1), ok_button, 0, 2, 5, 6,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (ok_button, 70, -2);

  hseparator1 = gtk_hseparator_new ();
  gtk_widget_set_name (hseparator1, "hseparator1");
  gtk_widget_ref (hseparator1);
  gtk_object_set_data_full (GTK_OBJECT (key_prop), "hseparator1", hseparator1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hseparator1);
  gtk_table_attach (GTK_TABLE (table1), hseparator1, 0, 2, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  gtk_signal_connect (GTK_OBJECT (key_prop), "realize",
                      GTK_SIGNAL_FUNC (on_key_prop_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (key_prop), "delete_event",
                      GTK_SIGNAL_FUNC (on_key_prop_delete_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (key_choose), "clicked",
                      GTK_SIGNAL_FUNC (on_keyprop_keychoose_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (type_graph), "toggled",
                      GTK_SIGNAL_FUNC (on_keyprop_type_toggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (type_func), "toggled",
                      GTK_SIGNAL_FUNC (on_keyprop_type_toggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (ok_button), "clicked",
                      GTK_SIGNAL_FUNC (on_keyprop_ok),
                      NULL);

  return key_prop;
}

GtkWidget*
create_func_prop ()
{
  GtkWidget *func_prop;
  GtkWidget *vbox1;
  GtkWidget *hbox4;
  GtkWidget *label20;
  GtkWidget *name_entry;
  GtkWidget *frame1;
  GtkWidget *table3;
  GtkWidget *psel_combo1;
  GtkWidget *entry2;
  GtkWidget *psel_combo2;
  GtkWidget *entry3;
  GtkWidget *psel_combo3;
  GtkWidget *entry4;
  GtkWidget *psel_combo4;
  GtkWidget *entry5;
  GtkWidget *label24;
  GtkWidget *label23;
  GtkWidget *label22;
  GtkWidget *label21;
  GtkWidget *hseparator2;
  GtkWidget *ok_button;

  func_prop = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_widget_set_name (func_prop, "func_prop");
  gtk_object_set_data (GTK_OBJECT (func_prop), "func_prop", func_prop);
  gtk_window_set_title (GTK_WINDOW (func_prop), _("control function property"));

  vbox1 = gtk_vbox_new (FALSE, 5);
  gtk_widget_set_name (vbox1, "vbox1");
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (func_prop), vbox1);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 5);

  hbox4 = gtk_hbox_new (FALSE, 0);
  gtk_widget_set_name (hbox4, "hbox4");
  gtk_widget_ref (hbox4);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "hbox4", hbox4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox4);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox4, TRUE, TRUE, 0);

  label20 = gtk_label_new (_("機能名："));
  gtk_widget_set_name (label20, "label20");
  gtk_widget_ref (label20);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "label20", label20,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label20);
  gtk_box_pack_start (GTK_BOX (hbox4), label20, FALSE, FALSE, 0);

  name_entry = gtk_entry_new ();
  gtk_widget_set_name (name_entry, "name_entry");
  gtk_widget_ref (name_entry);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "name_entry", name_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (name_entry);
  gtk_box_pack_start (GTK_BOX (hbox4), name_entry, TRUE, TRUE, 0);

  frame1 = gtk_frame_new (_("機能片"));
  gtk_widget_set_name (frame1, "frame1");
  gtk_widget_ref (frame1);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "frame1", frame1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame1);
  gtk_box_pack_start (GTK_BOX (vbox1), frame1, TRUE, TRUE, 0);

  table3 = gtk_table_new (4, 2, FALSE);
  gtk_widget_set_name (table3, "table3");
  gtk_widget_ref (table3);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "table3", table3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table3);
  gtk_container_add (GTK_CONTAINER (frame1), table3);
  gtk_container_set_border_width (GTK_CONTAINER (table3), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table3), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table3), 5);

  psel_combo1 = gtk_combo_new ();
  gtk_widget_set_name (psel_combo1, "psel_combo1");
  gtk_widget_ref (psel_combo1);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "psel_combo1", psel_combo1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (psel_combo1);
  gtk_table_attach (GTK_TABLE (table3), psel_combo1, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  entry2 = GTK_COMBO (psel_combo1)->entry;
  gtk_widget_set_name (entry2, "entry2");
  gtk_widget_ref (entry2);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "entry2", entry2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entry2);

  psel_combo2 = gtk_combo_new ();
  gtk_widget_set_name (psel_combo2, "psel_combo2");
  gtk_widget_ref (psel_combo2);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "psel_combo2", psel_combo2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (psel_combo2);
  gtk_table_attach (GTK_TABLE (table3), psel_combo2, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  entry3 = GTK_COMBO (psel_combo2)->entry;
  gtk_widget_set_name (entry3, "entry3");
  gtk_widget_ref (entry3);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "entry3", entry3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entry3);

  psel_combo3 = gtk_combo_new ();
  gtk_widget_set_name (psel_combo3, "psel_combo3");
  gtk_widget_ref (psel_combo3);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "psel_combo3", psel_combo3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (psel_combo3);
  gtk_table_attach (GTK_TABLE (table3), psel_combo3, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  entry4 = GTK_COMBO (psel_combo3)->entry;
  gtk_widget_set_name (entry4, "entry4");
  gtk_widget_ref (entry4);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "entry4", entry4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entry4);

  psel_combo4 = gtk_combo_new ();
  gtk_widget_set_name (psel_combo4, "psel_combo4");
  gtk_widget_ref (psel_combo4);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "psel_combo4", psel_combo4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (psel_combo4);
  gtk_table_attach (GTK_TABLE (table3), psel_combo4, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  entry5 = GTK_COMBO (psel_combo4)->entry;
  gtk_widget_set_name (entry5, "entry5");
  gtk_widget_ref (entry5);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "entry5", entry5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (entry5);

  label24 = gtk_label_new (_("候補一覧："));
  gtk_widget_set_name (label24, "label24");
  gtk_widget_ref (label24);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "label24", label24,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label24);
  gtk_table_attach (GTK_TABLE (table3), label24, 0, 1, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label24), 1, 0.5);

  label23 = gtk_label_new (_("変換中："));
  gtk_widget_set_name (label23, "label23");
  gtk_widget_ref (label23);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "label23", label23,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label23);
  gtk_table_attach (GTK_TABLE (table3), label23, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label23), 1, 0.5);

  label22 = gtk_label_new (_("入力中："));
  gtk_widget_set_name (label22, "label22");
  gtk_widget_ref (label22);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "label22", label22,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label22);
  gtk_table_attach (GTK_TABLE (table3), label22, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label22), 1, 0.5);

  label21 = gtk_label_new (_("未入力："));
  gtk_widget_set_name (label21, "label21");
  gtk_widget_ref (label21);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "label21", label21,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label21);
  gtk_table_attach (GTK_TABLE (table3), label21, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label21), 1, 0.5);

  hseparator2 = gtk_hseparator_new ();
  gtk_widget_set_name (hseparator2, "hseparator2");
  gtk_widget_ref (hseparator2);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "hseparator2", hseparator2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hseparator2);
  gtk_box_pack_start (GTK_BOX (vbox1), hseparator2, FALSE, FALSE, 0);

  ok_button = gtk_button_new_with_label (_("適用"));
  gtk_widget_set_name (ok_button, "ok_button");
  gtk_widget_ref (ok_button);
  gtk_object_set_data_full (GTK_OBJECT (func_prop), "ok_button", ok_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (ok_button);
  gtk_box_pack_start (GTK_BOX (vbox1), ok_button, FALSE, FALSE, 0);

  gtk_signal_connect (GTK_OBJECT (func_prop), "realize",
                      GTK_SIGNAL_FUNC (on_func_prop_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (func_prop), "delete_event",
                      GTK_SIGNAL_FUNC (on_func_prop_delete_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (ok_button), "clicked",
                      GTK_SIGNAL_FUNC (on_funcprop_ok),
                      NULL);

  return func_prop;
}

GtkWidget*
create_k_popup ()
{
  GtkWidget *k_popup;
  GtkAccelGroup *k_popup_accels;
  GtkWidget *popup_delete;

  k_popup = gtk_menu_new ();
  gtk_widget_set_name (k_popup, "k_popup");
  gtk_object_set_data (GTK_OBJECT (k_popup), "k_popup", k_popup);
  k_popup_accels = gtk_menu_ensure_uline_accel_group (GTK_MENU (k_popup));

  popup_delete = gtk_menu_item_new_with_label (_("削除"));
  gtk_widget_set_name (popup_delete, "popup_delete");
  gtk_widget_ref (popup_delete);
  gtk_object_set_data_full (GTK_OBJECT (k_popup), "popup_delete", popup_delete,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (popup_delete);
  gtk_container_add (GTK_CONTAINER (k_popup), popup_delete);

  gtk_signal_connect (GTK_OBJECT (popup_delete), "activate",
                      GTK_SIGNAL_FUNC (on_popup_delete_activate),
                      NULL);

  return k_popup;
}

GtkWidget*
create_f_popup ()
{
  GtkWidget *f_popup;
  GtkAccelGroup *f_popup_accels;
  GtkWidget *f_popup_delete;

  f_popup = gtk_menu_new ();
  gtk_widget_set_name (f_popup, "f_popup");
  gtk_object_set_data (GTK_OBJECT (f_popup), "f_popup", f_popup);
  f_popup_accels = gtk_menu_ensure_uline_accel_group (GTK_MENU (f_popup));

  f_popup_delete = gtk_menu_item_new_with_label (_("削除..."));
  gtk_widget_set_name (f_popup_delete, "f_popup_delete");
  gtk_widget_ref (f_popup_delete);
  gtk_object_set_data_full (GTK_OBJECT (f_popup), "f_popup_delete", f_popup_delete,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (f_popup_delete);
  gtk_container_add (GTK_CONTAINER (f_popup), f_popup_delete);

  gtk_signal_connect (GTK_OBJECT (f_popup_delete), "activate",
                      GTK_SIGNAL_FUNC (on_f_popup_delete_activate),
                      NULL);

  return f_popup;
}

