// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"

#include <assert.h>

#include "client.h"
#include "Connection.h"
#include "iiimtrans.h"
#include "iiimdisp.h"

static bool iiimpUnexpected(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return false;
}

static bool iiimpNotImplemented(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return false;
}

static bool iiimpConnect(Connection* conn, IIIMPReader* input)
{
    input->card8(); // byte order; IIIMPReaderでよきにやってくれる
    int proto_ver = input->card8();
    if (proto_ver != 2) {
        error("IIIMP protocol version error: %d\n", proto_ver);
        assert(0);
    }

    InputMethod* im = conn->createIM();
    assert(im);

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_CONNECT_REPLY);
    imstr->card16(im->getId());

    // list of language names
    imstr->reserve16();
    imstr->uniString("ja");
    imstr->store_back(imstr->len_from_reserved());
    conn->send_queue.push_back(imstr);

    return true;
}

static bool iiimpDisconnect(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    conn->remove_im(imid);
    
    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_DISCONNECT_REPLY);
    imstr->card16(imid);
    imstr->pad();
    conn->send_queue.push_back(imstr);
    return true;
}

#ifdef DEBUG
#include <stdio.h>
#endif

static int setIMValues(InputMethod* im, IIIMPReader* input)
{
    int attr_id = input->card16();
    input->card16(); // pad
    int vlen = input->card32();
#ifdef DEBUG
    printf("attr_id: %x: ", attr_id);
    for (int i = 0; i < vlen; i++) {
        unsigned char v = input->card8();
        printf("%02x ", v);
    }
    printf("\n");
#endif
    switch (attr_id)
    {
    case 0x1011:
        break;
    default:
        assert(0);
    }

    return 2 + 2 + 4 + vlen + input->pad();
}

static bool iiimpSetIMValues(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    InputMethod* im = conn->getIM(imid);
    assert(im);
    input->card16(); // pad
    int len = input->card32();

    while (len > 0)
        len -= setIMValues(im, input);

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_SETIMVALUES_REPLY);
    imstr->card16(imid);
    imstr->pad();
    conn->send_queue.push_back(imstr);
    return true;
}

static int setICValues(InputContext* ic, IIIMPReader* input)
{
    int attr_id = input->card16();
    int vlen = input->card16();
#ifdef DEBUG
    printf("attr_id: %x: ", attr_id);
    for (int i = 0; i < vlen; i++) {
        unsigned char v = input->card8();
        printf("%02x ", v);
    }
    printf("\n");
#endif
    switch (attr_id)
    {
    case 1:
        break;
    default:
        assert(0);
    }
    
    return 2 + 2 + vlen + input->pad();
}
        
static bool iiimpCreateIC(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    InputMethod* im = conn->getIM(imid);
    assert(im);
    InputContext* ic = im->createIC();
    assert(im);
    
    int len = input->card16();
    while (len > 0)
        len -= setICValues(ic, input);

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_CREATEIC_REPLY);
    imstr->card16(imid);
    imstr->card16(ic->getId());
    conn->send_queue.push_back(imstr);
    return true;
}

static bool iiimpResetIC(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    int icid = input->card16();

    // TODO: ICの初期化

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_RESETIC_REPLY);
    imstr->card16(imid);
    imstr->card16(icid);
    conn->send_queue.push_back(imstr);
    return true;
}

static bool iiimpGetIMValues(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpDestroyIC(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpSetICValues(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpSetICFocus(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpUnsetICFocus(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpGetICValues(Connection* conn, IIIMPReader* input)
{
    assert(0);
    return true;
}

static bool iiimpTriggerNotify(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    int icid = input->card16();

    // TODO:

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_TRIGGER_NOTIFY_REPLY);
    imstr->card16(imid);
    imstr->card16(icid);
    conn->send_queue.push_back(imstr);
    return true;
}

static bool iiimpForwardEvent(Connection* conn, IIIMPReader* input)
{
    int imid = input->card16();
    InputMethod* im = conn->getIM(imid);
    int icid = input->card16();
    InputContext* ic = im->getIC(icid);

    int type = input->card32();
    switch (type)
    {
    case 0: // STRING
        assert(0);
    case 1: // TEXT
        assert(0);
    case 2: // KEYEVENT
        {
            int len = input->card32();
            while (len > 0) {
                KeyEvent ke = input->keyevent();
                len -= 16;
                ic->processEvent(ke);
            }
        }
        break;
    default:
        assert(0);
    }
    ic->updateView();

    IIIMPWriter* imstr = conn->createIIIMPWriter(IM_FORWARD_EVENT_REPLY);
    imstr->card16(imid);
    imstr->card16(icid);
    conn->send_queue.push_back(imstr);
    return true;
}

struct DispatchTable
{
    int msg;
    bool (*func)(Connection* conn, IIIMPReader* input);
};

static const DispatchTable dispatcher[] = {
    { IM_CONNECT, iiimpConnect },
    { IM_CONNECT_REPLY, iiimpUnexpected },
    { IM_DISCONNECT, iiimpDisconnect },
    { IM_DISCONNECT_REPLY, iiimpUnexpected },
    { IM_REGISTER_TRIGGER_KEYS, iiimpUnexpected },
    { IM_TRIGGER_NOTIFY, iiimpTriggerNotify },
    { IM_TRIGGER_NOTIFY_REPLY, iiimpNotImplemented },
    
    { IM_SETIMVALUES, iiimpSetIMValues },
    { IM_SETIMVALUES_REPLY, iiimpNotImplemented },
    { IM_GETIMVALUES, iiimpGetIMValues },
    { IM_GETIMVALUES_REPLY, iiimpUnexpected },
    { IM_FORWARD_EVENT, iiimpForwardEvent },
    { IM_FORWARD_EVENT_REPLY, iiimpNotImplemented },
    { IM_COMMIT_STRING, iiimpUnexpected },

    { IM_CREATEIC, iiimpCreateIC },
    { IM_CREATEIC_REPLY, iiimpUnexpected },
    { IM_DESTROYIC, iiimpDestroyIC },
    { IM_DESTROYIC_REPLY, iiimpUnexpected },
    { IM_SETICVALUES, iiimpSetICValues },
    { IM_SETICVALUES_REPLY, iiimpUnexpected },
    { IM_GETICVALUES, iiimpGetICValues },
    { IM_GETICVALUES_REPLY, iiimpUnexpected },

    { IM_SETICFOCUS, iiimpSetICFocus },
    { IM_SETICFOCUS_REPLY, iiimpUnexpected },
    { IM_UNSETICFOCUS, iiimpUnsetICFocus },
    { IM_UNSETICFOCUS_REPLY, iiimpUnexpected },
    
    { IM_RESETIC, iiimpResetIC },
    { IM_RESETIC_REPLY, iiimpUnexpected },
    { 0, NULL }
};

void dispatchIIIMPMessage(Connection* cli, IIIMPReader* input)
{
    assert(cli);
    assert(input);

    for (int i = 0; dispatcher[i].msg; i++) {
        if (dispatcher[i].msg == input->opcode()) {
            assert(dispatcher[i].func);
            dispatcher[i].func(cli, input);
            return;
        }
    }
    error("unknown opcode: %d\n", input->opcode());
    assert(0);
}
