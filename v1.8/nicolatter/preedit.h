﻿// -*- mode:c++; coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 編集ウィンドウ

#ifndef NICOL_PREEDIT_H
#define NICOL_PREEDIT_H

#ifdef USE_GTK
    #include <gtk/gtk.h>
#else
    #include <X11/Intrinsic.h>
#endif

#include <window.h>

//////////////////////////////////////////////////////////////////////
// Caret

class Caret
{
    struct Killer {
        Caret* p;
        Killer(): p(0) { }
        ~Killer() { delete p; p = NULL; }
        void set(Caret* p_) { p = p_; }
    };
    friend struct Killer;

    static Killer killer;
    static Caret* this_;

#ifdef USE_GTK
    GtkWidget* focused;
    int timeout_id;
#else
    Widget focused;
    XtIntervalId timeout_id;
#endif
    CRect rect;
    bool isBlack;

public:
    static Caret* instance();

#ifdef USE_GTK
    void setFocus(GtkWidget* widget);
#else
    void setFocus(Widget widget);
#endif
    void setPosition(const CRect& rect);
    virtual int setVisible(bool v);
private:
    Caret();
    virtual ~Caret();
#ifdef USE_GTK
    static gint onTimeout(void* data);
#else
    static void onTimeout(void* , XtIntervalId* );
#endif
    Caret(const Caret& );               // not implement
    Caret& operator = (const Caret& );  // not implement
};

//////////////////////////////////////////////////////////////////////
// PreeditWindow

class InputContext;
class PreeditWindow: public PopupWindow
{
    typedef PopupWindow super;

public:
    CRect area;
    CRect areaNeeded;
    CPoint spot;    // spot-location
    int colormap;
    int stdColormap;
    int foreground;
    int background;
    int backgroundPixmap;
    string fontName;
    int lineSpace;
    int cursor;
    CRect size;     // preedit, statusの両方がrootスタイルのとき
                    // statusの位置調整に使う
    static const int PREEDIT_BORDER;

protected:
#ifdef USE_GTK
    GtkWidget* canvas;
#else
    Widget canvas;
#endif
    CFont font;
    static int white, blue, red, black;

private:
    InputContext* ic;

public:
    virtual ~PreeditWindow();
    virtual void update() = 0;
    void updateFont();
    virtual int setVisible(bool v);
    bool isStyleRoot() const;

    PreeditWindow& operator = (const PreeditWindow& );

protected:
    PreeditWindow(InputContext* ic);
    void adjustLocation(const CRect& logical);

private:
    void create();
    void createColors();
    virtual void draw(const CRect& ) = 0;

#ifdef USE_GTK
    static gint onExposed(GtkWidget* drawPane, GdkEventExpose* event,
                    PreeditWindow* this_);
    static void onRealized(GtkWidget* drawPane, PreeditWindow* this_);
    static void onHide(GtkWidget* w, PreeditWindow* this_);
#else
    static void onExposed(Widget w, void* closure, XEvent* event, Boolean* cont);
#endif
    PreeditWindow(const PreeditWindow& );   // not impl
};

#endif // NICOL_PREEDIT_H
