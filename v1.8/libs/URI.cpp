﻿// -*- coding:utf-8-with-signature -*-
// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifdef WIN32
  #pragma warning(disable:4786)
  #include <windows.h>
#endif

#include <vector>
#include <list>
#include <deque>
#include <assert.h>
#include <stdio.h>
#include <ctype.h>
#include "URI.h"
#include <string.h>
using namespace std;

#ifndef WIN32
inline const char* CharNext(const char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
inline char* CharNext(char* p)
{
    return p + mblen(p, MB_CUR_MAX);
}
#define stricmp strcasecmp
#endif

//////////////////////////////////////////////////////////////////////
// FileName

FileName::FileName()
{
}

FileName::FileName(const FileName& a): host(a.host), dir(a.dir), file(a.file)
{
}

FileName::FileName(const char* a)
{
    parse(a);
}

FileName::FileName(const string& a)
{
    parse(a.c_str());
}

FileName::~FileName()
{
}

FileName& FileName::operator = (const FileName& a)
{
    if (this != &a) {
        host = a.host;
        dir = a.dir;
        file = a.file;
    }
    return *this;
}

int FileName::parse(const char* str)
    // 戻り値
    //      ファイル名として有効なバイト数
{
    const char* p = str;
    host = dir = file = "";

    // ホスト名
    if ((p[0] == '/' || p[0] == '\\') && (p[1] == '/' || p[1] == '\\')) {
        p += 2;
        while (*p && *p != '/' && *p != '\\') {
#ifdef WIN32
            if (::IsDBCSLeadByte(*p))
                host += *p++;
            host += *p++;
#else
            for (int ct = mblen(p, MB_CUR_MAX); ct > 0; ct--)
                host += *p++;
#endif
        }
    }

#ifdef WIN32
    // 1999.09.08 URI("file:///c:/hoge").getPath() = "/c:/hoge"
    // これを救う
    if (*p == '/' && isalpha(p[1]) && p[2] == ':')
        p++;
#endif

    // ディレクトリ
    const char* top = p;
    const char* sep = 0;
    while (*p) {
        if (*p == '/' || *p == '\\')
            sep = p;
        p = ::CharNext(p);
    }

    if (sep) {
        dir.assign(top, sep - top + 1);
        file = sep + 1;
    }
    else {
        dir = "";
        file = top;
    }
    if (file == ".." || file == ".") {
        dir += file + "/";
        file = "";
    }

    for (unsigned i = 0; i < dir.length(); i++) {
        if (dir[i] == '\\')
            dir[i] = '/';
    }

    return strlen(str);
        // TODO: 不適切な文字のチェック
}

string FileName::getHost() const
{
    return host;
}

string FileName::getPath() const
{
    return dir;
}

string FileName::getFile() const
{
    return file;
}

string FileName::getBody() const
{
    const char* top = file.c_str();
    const char* p = top;
    const char* sep = 0;
    while (*p) {
        if (*p == '.')
            sep = p;
        p = ::CharNext(p);
    }

    if (!sep || sep == p)
        return file;
    else
        return string(file, 0, sep - top);
}

string FileName::getExt(bool normalize) const
    // 戻り値
    //      '.'を含まない
{
    const char* p = file.c_str();
    const char* sep = 0;
    while (*p) {
        if (*p == '.')
            sep = p;
        p = ::CharNext(p);
    }
    if (!sep || sep == p)
        return "";
    else {
        if (!normalize)
            return sep + 1;
        else {
            string r;
            for (const char* q = sep + 1; *q; q++)
                r += tolower(*q);
            return r;
        }
    }
}

string FileName::toString() const
{
    string r;
    if (host != "")
        r = string("//") + host;
    return r + dir + file;
}

FileName FileName::resolve(const FileName& ref) const
    // 絶対パスを返す
{
    if (ref.host != "")
        return ref;
    if (ref.host == "" && ref.dir == "" && ref.file == "")
        return *this;

    string retval;
    if (host != "")
        retval = "//" + host;

    if (ref.dir[0] == '/')
        return retval + ref.dir + ref.file;

    // 相対パス
    vector<int> depth;
    for (unsigned i = 0; i < dir.length(); i++) {
        if (dir[i] == '/')
            depth.push_back(retval.length() + i);
    }

    retval += dir;

    const char* p = ref.dir.c_str();
    while (*p) {
        if (*p == '.' && p[1] == '.' && (p[2] == '/' || p[2] == 0)) {
            if (depth.size() > 1) {
                depth.pop_back();
                retval.erase(retval.begin() + depth.back() + 1,
                                retval.end());
            }
            else {
                // ルートでの"../"は無視したいが，RFC準拠から付ける
                retval += "../";
                // depth.push_back(retval.length() - 1);
            }
            p += 2 + (p[2] != 0);
        }
        else if (*p == '.' && (p[1] == '/' || p[1] == '.'))
            p += 1 + (p[1] != 0);
        else {
            while (*p) {
#ifdef WIN32
                if (::IsDBCSLeadByte(*p))
                    retval += *p++;
                retval += *p++;
#else
                for (int ct = mblen(p, MB_CUR_MAX); ct > 0; ct--)
                    retval += *p++;
#endif
                if (p[-1] == '/') {
                    depth.push_back(retval.length() - 1);
                    break;
                }
            }
        }
    }
    return FileName(retval + ref.file);
}

string FileName::getRelative(const FileName& base) const
{
    if (host != base.host)
        return toString();
    if (dir[0] != '/'
#ifdef WIN32
        && !(isalpha(dir[0]) && dir[1] == ':' && dir[2] == '/')
#endif
            ) {
        return toString();
    }

    if (base.dir == dir && base.file == file)
        return "";

    int i;
    list<int> depth;
    for (i = 0; i < base.dir.length(); i++) {
        if (base.dir[i] == '/')
            depth.push_back(i);
    }

    const char* b = base.dir.c_str();
    const char* r = dir.c_str();
    for (i = 0; *r && *b; i++) {
        if (base.dir[i] != dir[i])
            break;
        if (dir[i] == '/') {
            if (!depth.size())
                break;
            depth.pop_front();
        }
        b++;
        r++;
    }

    string retval;
    for (i = 0; i < depth.size(); i++)
        retval += "../";
    retval += r + file;
    if (file == "" && retval == "" && base.file != "")
        retval = "./";

    return retval;
}

bool FileName::operator == (const FileName& a) const
{
    return host == a.host && dir == a.dir && file == a.file;
}

string FileName::getPath2() const
{
    if (file != "")
        return dir;
    string p;
    p.assign(dir, 0, dir.length() - 1);
    return FileName(p).getPath();
}

string FileName::getFile2() const
{
    if (file != "")
        return file;
    string p;
    p.assign(dir, 0, dir.length() - 1);
    return FileName(p).getFile();
}

string FileName::escapeFile(const char* p)
{
    static const char* unuse = "\"%*/:<>?\\|";
        // VFATで使用不可な文字，エスケープ用'%'

    string retval;
    char buf[4];

    while (*p) {
#ifdef WIN32
        if (::IsDBCSLeadByte(*p)) {
            retval.append(p, 2);
            p += 2;
            continue;
        }
#endif
        if (((unsigned char) *p) < 0x20 || *p == 0x7f || strchr(unuse, *p) != NULL) {
                sprintf(buf, "%%%02x", *p);
                retval += buf;
                p++;
                continue;
        }
        else
            retval += *(p++);
    }
    return retval;
}

//////////////////////////////////////////////////////////////////////
// URI

URI::URI()
{
}

URI::URI(const URI& a): scheme(a.scheme), authority(a.authority),
                    path(a.path), query(a.query), fragment(a.fragment)
{
}

URI::URI(const string& a)
{
    parse(a.c_str());
}

URI::URI(const char* a)
{
    parse(a);
}

URI::~URI()
{
}

URI& URI::operator = (const URI& a)
{
    if (this != &a) {
        scheme = a.scheme;
        authority = a.authority;
        path = a.path;
        query = a.query;
        fragment = a.fragment;
    }
    return *this;
}

string URI::getScheme() const
{
    return scheme;
}

string URI::getUserInfo() const
{
    const char* p = authority.c_str();
    const char* h = strchr(p, '@');
    if (h)
        return string(p, h - p);
    else
        return "";
}

string URI::getHost() const
    // authority = [ userinfo "@" ] host [ ":" port ]
{
    const char* p = authority.c_str();
    const char* h = strchr(p, '@');
    h = h != 0 ? h + 1 : p;
    const char* t = strchr(h, ':');
    if (t)
        return string(h, t - h);
    else
        return string(h);
}

int URI::getPort() const
{
    const char* p = authority.c_str();
    const char* h = strchr(p, '@');
    h = h != 0 ? h + 1 : p;
    const char* t = strchr(h, ':');
    if (t)
        return atoi(t + 1);
    else
        return -1;
}

string URI::getPath() const
{
    return path;
}

string URI::getQuery() const
{
    return query;
}

string URI::getFragment() const
{
    return fragment;
}

URI URI::resolve(const URI& ref) const
    // *thisを基点にしてrefへの絶対URIを返す
    // Windows 95でのInternetCombineUrl() APIは使い物にならない
    // RFC 2396 C. Examples of Resolving Relative URI References
{
    URI retval(ref);

    if (ref.scheme != "")
        return ref;

    retval.scheme = scheme;

    if (ref.authority != "")
        return retval;

    retval.authority = authority;

    if (ref.path != "") {
        FileName p = path == "" ? "/" : path;
        retval.path = p.resolve(ref.path).toString();
        return retval;
    }

    if (ref.query != "") {
        // ref = "?..."のとき，現在の文書ではなく，
        // ディレクトリに'?'を付ける
        retval.path = FileName(path).getPath();
        return retval;
    }

    retval.path = path;
    retval.query = query;

    if (ref.fragment != "") {
        // 現在の文書に"#..."を付ける。
        retval.fragment = ref.fragment;
        return retval;
    }

    // refが空の時，現在の文書を返す
    return *this;
}

string URI::getRelative(const URI& base) const
    // baseを基点にした相対URI文字列を返す
{
    if (base.scheme != scheme)
        return toString();

    string retval;

    if (authority != "" && base.authority != authority) {
        retval = "//" + authority;
        retval += path;
        if (query != "")
            retval += "?" + query;
        if (fragment != "")
            retval += "#" + fragment;
        return retval;
    }

    retval = FileName(path).getRelative(base.path);
    if (query != "" && query != base.query)
        retval += "?" + query;
    if (fragment != "" && fragment != base.fragment)
        retval += "#" + fragment;

    return retval;
}

bool isUric(const char* p)
{
    const unsigned char u = *((unsigned char*) p);
    return u >= 0x21 && u != 0x7f && u != 0x3c && u != 0x3e && u != 0x22;
}

int URI::parse(const char* str_uri)
    // 戻り値
    //      URIとして有効なバイト数
{
    const char* s= str_uri;
    const char* p = s;
    scheme = authority = path = query = fragment = "";

    // scheme
    while (isUric(p) && *p != ':' && *p != '/' && *p != '?' && *p != '#') p++;
    if (*p == ':') {
        scheme.assign(s, p - s); p++;
    }
    else
        p = s;

    // authority
    if (*p == '/' && *(p + 1) == '/') {
        s = (p += 2);
        while (isUric(p) && *p != '/' && *p != '?' && *p != '#') p++;
        authority.assign(s, p - s);
    }

#ifdef WIN32
    if (scheme == "file" && *p == '/' && isalpha(p[1]) && p[2] == ':')
        p++;
#endif

    // path
    s = p;
    while (isUric(p) && *p != '?' && *p != '#') p++;
    if (p != s)
        path.assign(s, p - s);
    else {
        if (scheme == "http")
            path = '/';
    }

    // query
    if (*p == '?') {
        s = (++p);
        while (isUric(p) && *p != '?') p++;
        query.assign(s, p - s);
    }

    // fragment
    if (*p == '#') {
        s = (++p);
        while (isUric(p)) p++;
        fragment.assign(s, p - s);
    }

    return p - str_uri;
}

string URI::toString() const
{
    string retval;
    if (scheme != "")
        retval = scheme + ":";
    if (authority != "")
        retval += "//" + authority;
    retval += path;
    if (query != "")
        retval += "?" + query;
    if (fragment != "")
        retval += "#" + fragment;
    return retval;
}

bool URI::isDescendantOf(const URI& parent) const
{
    if (scheme != parent.scheme || authority != parent.authority)
        return false;
    string check_path = path[0] == '/' ? path : parent.resolve(*this).getPath();

    string p = FileName(parent.path).getPath();
    string c = FileName(check_path).getPath();
    if (p.size() > c.size())
        return false;
    bool f = true;
    for (unsigned i = 0; i < p.size(); i++) {
        if (c[i] != p[i]) {
            f = false;
            break;
        }
    }
    return f;
}

static inline int sign(int x)
{
    return x > 0 ? +1 : (x < 0 ? -1 : 0);
}

static inline unsigned char hex2char(const unsigned char* h)
{
    return (*h >= '0' && *h <= '9' ? *h - '0' :
            (*h >= 'A' && *h <= 'F' ? *h - 'A' + 10 : *h - 'a' + 10)) * 16
        + (h[1] >= '0' && h[1] <= '9' ? h[1] - '0' :
           (h[1] >= 'A' && h[1] <= 'F' ? h[1] - 'A' + 10 : h[1] - 'a' + 10));
}

int URI::compare(const URI& uri1, const URI& uri2)
    // 機能
    //      URLによる比較
    //      判断がつかないときはm_sUrlを単に文字列として比較する
    // TODO: rfc2068によれば，次の3つのURIを等しいと判定できないといけない。
    //      http://abc.com:80/~smith/home.html
    //      http://ABC.com/%7Esmith/home.html
    //      http://ABC.com:/%7esmith/home.html
{
    // ホスト -> スキーム -> パス -> クエリー

    deque<string> domain[2];
    string h[2];
    h[0] = uri1.getHost();
    h[1] = uri2.getHost();

    int i;
    for (i = 0; i < 2; i++) {
        const char* p = h[i].c_str();
        while (*p) {
            string a;
            while (*p && *p != '.')
                a += *p++;
            domain[i].push_front(a);
            if (*p == '.')
                p++;
        }
    }

    for (i = 0; i < domain[0].size() && i < domain[1].size(); i++) {
        int r = stricmp(domain[0].operator [](i).c_str(),
                        domain[1].operator [](i).c_str());
        if (r)
            return r;
    }

    if (uri1.scheme != uri2.scheme)
        return uri1.scheme.compare(uri2.scheme);

    int r = strcmp(uri1.path.c_str(), uri2.path.c_str());
    if (r)
        return r;

    r = strcmp(uri1.query.c_str(), uri2.query.c_str());
    if (r)
        return r;

    r = strcmp(uri1.fragment.c_str(), uri2.fragment.c_str());
    if (r)
        return r;

    return uri1.toString().compare(uri2.toString());
}

string URI::escape(const char* uri)
    // URIとして使えない文字を%xxに変換する。すでに%xxに変換されている文字には触らない。
    //      変換対象はcontrol, space, delims, unwise, 0x80以上の文字
    //      '#'は変換しない。
    //      　多バイト文字は考慮しない。特にシフトJISだと酷いことになるが，
    //      現時点でURIにUS-ASCII以外指定するな，と。
{
    const char* e = "<>%\"{}|\\^[]`";
    char* dest = new char[strlen(uri) * 3 + 1];
    char* d = dest;
    const unsigned char* p = (unsigned char*) uri;

    while (*p) {
        if (*p < 0x21 || *p > 0x7e || strchr(e, *p)) {
            if (*p == '%' && isxdigit(*(p + 1)) && isxdigit(*(p + 2)))
                *(d++) = *p;
            else
                d += sprintf(d, "%%%02x", *p);
        }
        else
            *(d++) = *p;
        p++;
    }
    *d = 0;

    string retval = dest;
    delete [] dest;
    return retval;
}

string URI::unescape(const char* escaped_uri)
    // 機能
    //      %xxを復元する。ただし次の文字と0x80以上の文字は復元しない。
    // 解説
    //      http://www.ics.uci.edu/pub/ietf/uri/draft-fielding-uri-syntax-03.txt:
    //          reserved = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
    //                     "$" | ","
    //          control  = <US-ASCII coded characters 00-1F and 7F hexadecimal>
    //          space    = <US-ASCII coded character 20 hexadecimal>
    //          delims   = "<" | ">" | "#" | "%" | <">
    //          unwise   = "{" | "}" | "|" | "\" | "^" | "[" | "]" | "`"
{
    const char* ununescape = ";/?:@&=+$,<>#%\"{}|\\^[]`";
    string r;
    const char* p = escaped_uri;
    unsigned char c;

    while (*p) {
        if (*p == '%' && isxdigit(*(p + 1)) && isxdigit(*(p + 2))) {
            c = hex2char((unsigned char*) p + 1);
#ifdef WIN32
            if (c >= 0x20 && c <= 0x7e && !strchr(ununescape, c))
                // fileスキームで空白文字を許容する。
#else
            if (c >= 0x21 && c <= 0x7e && !strchr(ununescape, c))
#endif  // WIN32
                r += c;
            else
                r.append(p, 3);
            p += 3;
        }
        else
            r += *(p++);
    }

    return r;
}

bool URI::operator == (const URI& a) const
{
    return compare(*this, a) == 0;
}

bool URI::operator != (const URI& a) const
{
    return compare(*this, a) != 0;
}

bool URI::operator < (const URI& a) const
{
    return compare(*this, a) < 0;
}

string URI::toLocal() const
    // URI->filename
{
    static const char* unuse = "\"$%*/:<>?\\|";
        // VFATで使用不可な文字，schemeのエスケープ用'$'，通常のエスケープ用'%'

    string retval = "";
    char buf[4];

    retval = scheme + "$\\" + getHost();
    if (getPort() != -1) {
        char dmy[100];
        sprintf(dmy, "$3a%d", getPort());
        retval += dmy;
    }

    const char* p = path == "" ? "/" : path.c_str();
    while (*p) {
#ifdef WIN32
        if (::IsDBCSLeadByte(*p)) {
            retval.append(p, 2);
            p += 2;
            continue;
        }
#else
        int clen = mblen(p, MB_CUR_MAX);
        if (clen > 1) {
            retval.append(p, clen);
            p += clen;
            continue;
        }
#endif
        else if (*p == '/' || *p == '\\') {
            retval += '\\';
            p++;
            continue;
        }
        else if (((unsigned char) *p) < 0x20 || *p == 0x7f
                    || strchr(unuse, *p) != NULL) {
            sprintf(buf, "%%%02x", *p);
            retval += buf;
            p++;
            continue;
        }
        else
            retval += *(p++);
    }

    if (query != "") {
        retval += "%3f";
        p = query.c_str();
        while (*p) {
#ifdef WIN32
            if (::IsDBCSLeadByte(*p)) {
                retval.append(p, 2);
                p += 2;
                continue;
            }
#else
            int clen = mblen(p, MB_CUR_MAX);
            if (clen > 1) {
                retval.append(p, clen);
                p += clen;
                continue;
            }
#endif
            else if (((unsigned char) *p) < 0x20 || strchr(unuse, *p) != NULL) {
                sprintf(buf, "%%%02x", *p);
                retval += buf;
                p++;
                continue;
            }
            else
                retval += *(p++);
        }
    }
    if (retval.length() > 0 && retval[retval.length() - 1] == '\\')
        retval += "index.html";

    return retval;
}

string URI::getAuthority() const
{
    return authority;
}

void URI::setFragment(const char* s)
{
    fragment = s;
}
