// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef NICOLATTER_CONFIG_H
#define NICOLATTER_CONFIG_H

// Define if your system has the <langinfo.h> header and nl_langinfo() function.
#define HAVE_LANGINFO_H

// Define if your system defines the socklen_t type in <sys/socket.h> header.
#define HAVE_SOCKLEN_T

/* Define if the declaration of iconv() needs const. */
#undef ICONV_CONST 

// 変換サーバーの選択
// 以下は、configureスクリプトによって自動的に設定される

// Define if your system has a Canna conversion server
#define USE_CANNA

// Define if your system has a Canna 3.7 or later
#define CANNA37

// Define if your system has a Wnn4 compatible conversion server
#define USE_WNN

// Define if enable NLS
#define ENABLE_NLS 1

////////////////////////////////////////////////////////////////////////

#ifdef _POSIX_C_SOURCE
#error '_POSIX_C_SOURCE' redefine
#else
#define _XOPEN_SOURCE 600
#define _POSIX_C_SOURCE 200112L
#endif

#ifndef HAVE_LANGINFO_H
  #define CURRENT_CODESET "EUC-JP"
#endif

#ifdef USE_CANNA
#ifdef CANNA37
  #define CANNA_NEW_WCHAR_AWARE
  #define CANNA_WCHAR16
#else
  #define CANNA_WCHAR
  #define WCHAR16      
  #define _WCHAR_T
#endif
#endif

#ifdef USE_WNN
  #ifndef WNN_ENVRCFILE
#define WNN_ENVRCFILE "/etc/FreeWnn/ja/wnnenvrc"
  #endif
#endif  // USE_WNN

#define FIND_SERVER_CMD "ps x"
/*
    nicolatterのpidを調べるために用いるpsコマンド
  
    これはVine Linux 1.1用だが，Vine Linux 1.1のpsコマンドはUNIX仕
    様に適合していない。

    Vine Linux以外の場合は，psコマンドを呼んだプロセスのユーザーが
    起動したプロセス一覧を，次の形式で表示するように引数を定める

~$ ps x
  PID TTY STAT TIME COMMAND
 6810  p1 S    0:00 ./nicolatter 

    UNIX仕様に準拠したOSなら，次のようになるか
        #define FIND_SERVER_CMD "ps -o pid,tty,nice,time,comm"
*/

// 日本（日本語EUC）ロケール名
#define JA_EUCJP_LOCALE_NAME "ja_JP.eucJP"

#define server_name "nicolatter"
#define VERSION "1.7.8"

#define SYS_RC_DIR "/etc/X11/nicolatter"

#endif
