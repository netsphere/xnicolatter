// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_XIMTRANS_H
#define QSNICOLA_XIMTRANS_H

#include <X11/Xlib.h>
#include "transbase.h"
#include "LevelSelector.h"

////////////////////////////////////////////////////////////////////////
// XimWriter

class XimWriter: public SendBuffer
{
public:
    XimWriter(bool bigEndian, int opcode); // Connection#createSendBuffer()
    virtual ~XimWriter();

    void card8(unsigned char val);
    void str8(const char* s);   // 長さ1バイト+文字列
    void str16(const char* s);  // 長さ2バイト+文字列
    void xkeyevent(const XKeyEvent& e);
    virtual int getData(unsigned char* out);
};

////////////////////////////////////////////////////////////////////////
// XimReader

class XimReader: public RecvBuffer
{
public:
    XimReader(bool isBigEndian);
    XimReader(bool isBigEndian, const char* b); // XIM X transport
    virtual ~XimReader();

    virtual bool input(int fd); // XIM TCP transport
    int opcode() const;
    string str8();      // 長さ1バイト
    string str16();     // 長さ2バイト
    virtual KeyEvent keyevent();
};

#endif // QSNICOLA_TRANSPORT_H
