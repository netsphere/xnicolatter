// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// Canna用の編集ウィンドウ，ステータスウィンドウ，候補ウィンドウ

#include "../config.h"
#ifdef USE_CANNA

#include "preedit.h"
#include "status.h"

//////////////////////////////////////////////////////////////////////
// CannaPreedit

class CannaConv;
class CannaPreedit: public PreeditWindow
{
    typedef PreeditWindow super;
    CannaConv* conv;
public:
    CannaPreedit(CannaConv* conv, InputContext* ic);
    virtual ~CannaPreedit();
    virtual void update();
private:
    virtual void draw(const CRect& );
};

//////////////////////////////////////////////////////////////////////
// CannaStatus

class CannaConv;
class CannaStatus: public StatusWindow
{
    typedef StatusWindow super;
    CannaConv* conv;

public:
    CannaStatus(CannaConv* conv, InputContext* ic);
    virtual ~CannaStatus();
    virtual void update();
};

//////////////////////////////////////////////////////////////////////
// CannaCandidate

class CannaConv;
class CannaCandidate: public CandidateWindow
{
    typedef CandidateWindow super;

#ifdef USE_GTK
    GtkWidget* canvas;
#else
    Widget canvas;
#endif
    XFontSet font;
    CannaConv* conv;
    PreeditWindow* preeditWindow;
public:
    CannaCandidate(CannaConv* conv, class PreeditWindow* pre);
    virtual ~CannaCandidate();
    void update();
private:
#ifdef USE_GTK
    static gint onExposed(GtkWidget* w, GdkEventExpose* event,
                    CannaCandidate* this_);
#else
    static void onExposed(Widget w, void* closure, XEvent* event, Boolean* cont);
#endif
};

#endif  // USE_CANNA
