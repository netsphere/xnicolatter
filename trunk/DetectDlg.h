// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include <gtk/gtk.h>
#include "dialog.h"

//////////////////////////////////////////////////////////////////////
// DetectDlg

class DetectDlg: public Dialog
    // キーの検出を行う
{
public:
    int keycode;

    DetectDlg(GtkWindow* parent);
    virtual ~DetectDlg();
    virtual int setVisible(bool );
private:
    static void onCancel(GtkButton* button, DetectDlg* this_);
    static void onKeyPressed(GtkWidget* widget, GdkEventKey* event, DetectDlg* this_);
};

