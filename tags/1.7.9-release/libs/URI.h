// Q's C++ Library
// Copyright (c) 1996-1999 Hisashi HORIKAWA. All rights reserved.

#ifndef QSLIB_URI
#define QSLIB_URI

#include <string>
using namespace std;

//////////////////////////////////////////////////////////////////////
// URI

class URI
    // java.net.URL
    // RFC 2396 "Uniform Resource Identifiers (URI): Generic Syntax"
    //      URI参照 = ( 絶対URI | 相対URI ) [ "#" fragment ]
    //      generic URI = [ scheme ":" ] [ "//" authority ] path [ "?" query ]
    //      authority = [ userinfo "@" ] host [ ":" port ]
{
    string scheme, authority, path, query, fragment;
public:
    URI();
    URI(const URI& a);
    URI(const string& a);
    URI(const char* a);
    virtual ~URI();

    URI& operator = (const URI& a);

    string getScheme() const;
    string getAuthority() const;
    string getUserInfo() const;
    string getHost() const;
    int getPort() const;
    string getPath() const;
    string getQuery() const;
    string getFragment() const;
    void setFragment(const char* );

    URI resolve(const URI& ref) const;
        // 相対URIの解決
    string getRelative(const URI& base) const;
        // baseを基点とした相対URIに変換
        // URIを返さないことに注意
    bool isDescendantOf(const URI& ) const;

    static string escape(const char* uri);
    static string unescape(const char* escaped_uri);

    string toLocal() const;
        // VFAT FSに保存できるようにURI文字をエスケープする。
        // "://" -> "$\\"
    string toString() const;

    static int compare(const URI& x, const URI& y);
    bool operator == (const URI& a) const;
    bool operator != (const URI& a) const;
    bool operator < (const URI& a) const;
private:
    int parse(const char* str);
};

//////////////////////////////////////////////////////////////////////
// FileName

class FileName
    // java.io.File
    //      filename = [ '//' host ] path file
    //      dir = ... '/'
    //      file = body [ '.' ext ]
    //      '.'で始まるファイル名は，全体をbodyとして扱う。
{
    string host, dir, file;
public:
    FileName();
    FileName(const FileName& a);
    FileName(const string& a);
    FileName(const char* a);
    // FileName(const string& h, const string& p, const string& f);
    virtual ~FileName();

    FileName& operator = (const FileName& a);

    string getHost() const;
    string getPath() const; // 必ず'/'で終わる。ドライブ名を含む
        // URI::getPath()はファイル名を含むが，こちらは含まないことに注意
    string getFile() const;

    string getPath2() const;
    string getFile2() const;
        // "/hogehoge/"のとき
        //      getPath() = "/hogehoge/", getFile() = ""
        //      getPath2() = "/", getFile2() = "hogehoge"   末尾に'/'は付かない

    string getBody() const;
    string getExt(bool normalize) const;
        // normalize = true -> 拡張子を小文字で返す。

    FileName resolve(const FileName& ref) const;
    string getRelative(const FileName& base) const;
        // *thisとbaseが同じファイルの時""を返す
    bool isDescendantOf(const FileName& ) const;

    string toString() const;

    static string escapeFile(const char* str);
        // 入力文字列中の'/', ':'等をエスケープして，ファイル名として使えるようにする。
    static string escapePath(const char* str);
        // 入力文字中の':'等をエスケープする。'/'はエスケープせず，
        // この関数の戻り値を使って深い階層のファイルを作成できる。

    bool operator == (const FileName& a) const;
    bool operator < (const FileName& a) const;

private:
    int parse(const char* str);
};

#ifdef WIN32
extern off_t qGetFileSize(const char* filename);
extern bool isExistFile(const char* filename);
extern bool isDirectory(const char* filename);
#endif

#endif
