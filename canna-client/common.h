
size_t cannawcs_to_utf8(char* dest, const cannawc* wcs, size_t n);

size_t euc_to_utf8(char* dest, const unsigned char* euc, size_t n);

const char* dump(const unsigned char* s, int bytes);

