// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.
//   email: <mailto:vzw00011@nifty.ne.jp>
//   website: <http://www2.airnet.ne.jp/pak04955/>

#ifndef QSNICOLA_IMEXT
#define QSNICOLA_IMEXT

#include <X11/Xlib.h>

enum IMExtCmd {
  IMEXT_NIHONGO = 1,    // ??
  IMEXT_ON = 2,         // 日本語モードにする。ユーザーによるモード変更は禁止しない
  IMEXT_OFF = 3,        // 半角英数モードにする。ユーザーによるモード変更は禁止しない

  IMEXT_HIDE = 4,       // IMを一時的に無効化
  IMEXT_RECOVER = 5,    // HIDEする前の状態に戻す

  IMEXT_ROMA = 6,       // ローマ字入力に変更  nicolatterは実装しない
  IMEXT_KANA = 7,       // かな入力に変更      nicolatterは実装しない

  IMEXT_HIRAGANA = 8,   // ひらがな固定 ユーザーによるモード変更を禁止する？
  IMEXT_KATAKANA = 9,   // カタカナ固定
  IMEXT_ALNUM = 10      // 全角(？)英数固定
};

#ifdef USE_GTK
extern GdkFilterReturn filterXEvent(GdkXEvent* xev_, GdkEvent* ev, void* data);
#else
extern bool filterXEvent(const XEvent& ev);
#endif

extern Atom XA_XIM_PROTOCOL;

#endif // QSNICOLA_IMEXT
