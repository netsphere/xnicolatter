// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

// キー配列設定プログラム

#include "../config.h"

#include "support.h"
#include "callbacks.h"
#include "interface.h"
#include <URI.h>
#include <gdk/gdkx.h>
#include <unistd.h>

GtkWidget *map_window;
GtkWidget *funcs_window;
extern KeyProp keyProp;
extern FuncProp funcProp;

int
main (int argc, char *argv[])
{
/*
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);
*/
    setenv("LC_CTYPE", JA_EUCJP_LOCALE_NAME, 1);
    gtk_set_locale ();
    gtk_init (&argc, &argv);
    top_display = GDK_DISPLAY();
/*
  add_pixmap_directory (PACKAGE_DATA_DIR "/pixmaps");
  add_pixmap_directory (PACKAGE_SOURCE_DIR "/pixmaps");
*/

  map_window = create_map_window ();
  gtk_widget_show (map_window);
  funcs_window = create_funcs_window ();
  gtk_widget_show (funcs_window);
  keyProp.window = create_key_prop ();
    gtk_widget_show(keyProp.window);
  funcProp.window = create_func_prop ();
    gtk_widget_show(funcProp.window);

    if (argc >= 2) {
        char buf[1000];
        getcwd(buf, sizeof(buf));
        string path = buf;
        if (buf[strlen(buf) - 1] != '/')
            path += '/';
        string file = FileName(path).resolve(argv[1]).toString();
        loadFile(file);
    }

  gtk_main ();
  return 0;
}

