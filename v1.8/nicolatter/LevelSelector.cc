// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 同時押下，プレフィックスシフト動作

#include "../config.h"

#include <X11/keysym.h>
#include <X11/Xlib.h>

#include "LevelSelector.h"
#include "client.h"
#include "start.h"

using namespace std;

//////////////////////////////////////////////////////////////////////

LevelSelector* keyChar = NULL;

KeyEvent::KeyEvent(): xk(NULL)
{
}

KeyEvent::KeyEvent(const KeyEvent& x): type(x.type), keycode(x.keycode),
    keychar(x.keychar), modifier(x.modifier), time(x.time)
{
    if (x.xk) {
        xk = (XKeyEvent*) malloc(sizeof(XKeyEvent));
        memcpy(xk, x.xk, sizeof(XKeyEvent));
    }
    else
        xk = NULL;
}

KeyEvent::KeyEvent(const XKeyEvent& x): type(x.type), keycode(x.keycode),
  modifier(x.state), time(x.time)
{
    keychar = 0; // TODO: impl
    xk = (XKeyEvent*) malloc(sizeof(XKeyEvent));
    memcpy(xk, &x, sizeof(XKeyEvent));
}

KeyEvent::~KeyEvent()
{
    if (xk) {
        free(xk);
        xk = NULL;
    }
}

KeyEvent& KeyEvent::operator = (const KeyEvent& x)
{
    if (this != &x) {
        type = x.type;
        keycode = x.keycode;
        keychar = x.keychar;
        modifier = x.modifier;
        time = x.time;
        if (x.xk) {
            free(xk);
            xk = (XKeyEvent*) malloc(sizeof(XKeyEvent));
            memcpy(xk, x.xk, sizeof(XKeyEvent));
        }
    }
    return *this;
}

//////////////////////////////////////////////////////////////////////
// NormalSelector

NormalSelector::NormalSelector()
{
}

NormalSelector::~NormalSelector()
{
}

void NormalSelector::input(InputContext* ic, const KeyEvent& event)
{
    if (event.type != KeyPress)
        return;
    KeySym sym = XKeycodeToKeysym(top_display, event.keycode, 0);
    if (sym == XK_Control_L || sym == XK_Control_R
            || sym == XK_Alt_L || sym == XK_Alt_R
            || sym == XK_Shift_L || sym == XK_Shift_R) {
        return;
    }
    ic->input(event, (event.modifier & ShiftMask) != 0 ? 1 : 0);
}

string NormalSelector::getStatus() const
{
    return "";
}

//////////////////////////////////////////////////////////////////////
// PrefixSelector

PrefixSelector::PrefixSelector(): is_shift(false)
{
}

PrefixSelector::~PrefixSelector()
{
}

void PrefixSelector::input(InputContext* ic, const KeyEvent& event)
{
    if (event.type != KeyPress)
        return;
    KeySym sym = XKeycodeToKeysym(top_display, event.keycode, 0);
    if (sym == XK_Control_L || sym == XK_Control_R
            || sym == XK_Alt_L || sym == XK_Alt_R)
        return;
    if (sym == XK_Shift_L || sym == XK_Shift_R) {
        is_shift = true;
        return;
    }
    ic->input(event, is_shift);
    is_shift = false;
}

string PrefixSelector::getStatus() const
{
    return is_shift ? "S" : " ";
}
