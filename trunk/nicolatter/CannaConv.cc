﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// Canna変換サーバーとの通信

#include "../config.h"
#ifdef USE_CANNA

#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <X11/keysym.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "CannaConv.h"    // WCHAR16などを定義してからincludeすること。
#include <canna/jrkanji.h>

#include "CannaWindow.h"
#include "client.h"
#include "../global.h"
#include "LevelSelector.h"
#include <misc.h>
#include <qsdebug.h>

//////////////////////////////////////////////////////////////////////

#if defined(CANNA_WCHAR) || defined(CANNA_WCHAR16)
int c16_mbtowc(CannaWChar* wc, const char* p, size_t n)
{
    assert(wc);
    assert(p);

    unsigned char* s = (unsigned char*) p;
    int l;
    int clen;
    clen = l = mblen(p, n);

    if (l < 0) {
        error("error: c16_mbtowc(): guess libcanna is non-wc version.\n");
#ifdef DEBUG
        error("    MB_CUR_MAX = %d, s = ", MB_CUR_MAX);
        for (int ii = 0; ii < MB_CUR_MAX && p[ii]; ii++)
            error("%02x ", (unsigned char) p[ii]);
        error("\n");
#endif
    }
    else {
        *wc = 0;
        while (--l >= 0)
            *wc = (*wc << 8) + *s++;
    }
    return clen;
}

size_t c16_wcstombs(char* s, const CannaWChar* wcs, size_t n)
{
    int count = 0;
    if (wcs) {
        while (n > 0 && *wcs) {
            if (*wcs > 0xff) {
                *s++ = (*wcs >> 8) & 0xff;
                count++;
                n--;
            }
            *s++ = *wcs++ & 0xff;
            count++;
            n--;
        }
    }
    if (n > 0)
        *s = '\0';
    return count;
}

int c16_wctomb(char* s, CannaWChar wc)
{
    int l = 1;
    if (wc >= 256) {
        *s++ = (wc >> 8) & 0xff;
        l = 2;
    }
    *s++ = wc & 0xff;
    return l;
}
#endif  // CANNA_WCHAR

//////////////////////////////////////////////////////////////////////
// EchoLine

EchoLine::EchoLine(): length(0), revPos(0), revLen(0)
{
    line[0] = 0;
}

//////////////////////////////////////////////////////////////////////
// CannaConv

CannaConv::CannaConv(InputContext* ic): super(ic), candWindow(NULL)
{
    static bool initialized = false;
    context = rand();

    if (!initialized) {
        char** warning = NULL;
        if (wcKanjiControl(0, KC_INITIALIZE, (char*) &warning) == -1) {
            QsTRACE(1, "Cannaの初期化に失敗\n");
            assert(0);
        }
        if (warning) {
            for (char** p = warning; *p; p++)
                QsTRACE(1, "警告：%s\n", *p);
        }
        initialized = true;
    }

    setKanaMode(MODE_OFF);
}

CannaConv::~CannaConv()
{
    delete candWindow;
}

struct CannaKeyMap {
    KeySym sym;
    int k, s, c;
};

const CannaKeyMap cmap[] = {
    // XK, 単独, shift, ctrl
    { XK_Zenkaku_Hankaku, CANNA_KEY_HANKAKUZENKAKU, 0, 0 },
    { XK_Muhenkan, CANNA_KEY_Nfer, CANNA_KEY_Shift_Nfer, CANNA_KEY_Cntrl_Nfer },
    { XK_Kanji, CANNA_KEY_Xfer, CANNA_KEY_Shift_Xfer, CANNA_KEY_Cntrl_Xfer },
    { XK_Henkan_Mode, CANNA_KEY_Xfer, CANNA_KEY_Shift_Xfer, CANNA_KEY_Cntrl_Xfer },
    { XK_Hiragana_Katakana, CANNA_KEY_HIRAGANA, CANNA_KEY_KATAKANA, 0 },
    { XK_Up,        CANNA_KEY_Up,       CANNA_KEY_Shift_Up,     CANNA_KEY_Cntrl_Up },
    { XK_Left,      CANNA_KEY_Left,     CANNA_KEY_Shift_Left,   CANNA_KEY_Cntrl_Left },
    { XK_Right,     CANNA_KEY_Right,    CANNA_KEY_Shift_Right,  CANNA_KEY_Cntrl_Right },
    { XK_Down,      CANNA_KEY_Down,     CANNA_KEY_Shift_Down,   CANNA_KEY_Cntrl_Down },
    { XK_Insert,    CANNA_KEY_Insert,   0, 0 },
    { XK_Next,      CANNA_KEY_Rollup,   0, 0 }, // PageDown
    { XK_Prior,     CANNA_KEY_Rolldown, 0, 0 }, // PageUp
    { XK_Home,      CANNA_KEY_Home,     0, 0 },
// CANNA_KEY_Help
// CANNA_KEY_KP_Key
    { XK_End,       CANNA_KEY_End,      0, 0 },
    { XK_space,     ' ',                CANNA_KEY_Shift_Space, 0 },
    { XK_F1,        CANNA_KEY_F1,       0, 0 },
    { XK_F2,        CANNA_KEY_F2,       0, 0 },
    { XK_F3,        CANNA_KEY_F3,       0, 0 },
    { XK_F4,        CANNA_KEY_F4,       0, 0 },
    { XK_F5,        CANNA_KEY_F5,       0, 0 },
    { XK_F6,        CANNA_KEY_F6,       0, 0 },
    { XK_F7,        CANNA_KEY_F7,       0, 0 },
    { XK_F8,        CANNA_KEY_F8,       0, 0 },
    { XK_F9,        CANNA_KEY_F9,       0, 0 },
    { XK_F10,       CANNA_KEY_F10,      0, 0 },
    { XK_Delete,    0x7f, 0, 0 },
    { 0, 0, 0, 0 }
};

KanaKanjiStatus CannaConv::input(const KeyEvent& event, int level)
    // canna版
/*
    制御キー    元の配列を使う
    図形キー    仮名モードのとき配列を変更，それ以外のとき元の配列
    未定義キー  元の配列を使う
*/
{
    if ((event.modifier & Mod1Mask) != 0) { // [Alt]
        if (echo.length == 0)
            return THROUGH;
        else
            return NONE;
    }

    assert(level >= 0 && level <= 2);
    wcKanjiStatus status;
    memset(&status, 0, sizeof(status));
    CannaWChar ch = 0;
    CannaWChar wc_buf[1000];
    char mb_buf[1000];
    int result = 0;

    const Key* key = keyMap->find_key(getKeyName(event.modifier, event.keycode));
#if DEBUG > 1
    QsTRACE(1, "use = %d, key = %04x\n", global_prop.use_keymap, key);
    if (key)
        QsTRACE(1, "key-type = %d\n", key->getType());
#endif
    // TRACE("mode = %d\n", getKanaMode());

    if (key && key->getType() == GRAPHIC_KEY
                && global_prop.use_keymap && getKanaMode() == MODE_KANA) {
        const char* p = dynamic_cast<const GraphicKey*>(key)
                                            ->getGraphChar(level).c_str();
        result = 0;
        while (*p) {
            int clen = c16_mbtowc(&ch, p, MB_CUR_MAX);
            if (clen > 0) {
                result += wcKanjiString(context, (int) ch,
                                        wc_buf + result,
                                        sizeof(wc_buf) / sizeof(CannaWChar),
                                        &status);
                p += clen;
            }
        }
    }
    else {
        // 制御キー or 英数モード or ローマ字入力
        XKeyEvent tmp = *event.xk;
        if (level)
            tmp.state |= ShiftMask;
        else
            tmp.state &= ~ShiftMask;
        KeySym sym;
        int r = XLookupString(&tmp, mb_buf, sizeof(mb_buf), &sym, NULL);
        if (r >= 1) {
            ch = '\0';
            c16_mbtowc(&ch, mb_buf, MB_CUR_MAX);
        }
        else {
            // カーソルキーなど
            for (int i = 0; cmap[i].sym; i++) {
                if (sym == cmap[i].sym) {
                    if ((tmp.state & ControlMask) != 0)
                        ch = cmap[i].c;
                    else if ((tmp.state & ShiftMask) != 0)
                        ch = cmap[i].s;
                    else
                        ch = cmap[i].k;
                    break;
                }
            }
        }
        if (!ch)
            return THROUGH;

        result = wcKanjiString(context, (int) ch,
                               wc_buf, sizeof(wc_buf) / sizeof(CannaWChar),
                               &status);
    }

    if (status.length >= 0) {
        memcpy(echo.line, status.echoStr, status.length * sizeof(CannaWChar));
        echo.line[status.length] = 0;
        echo.length = status.length;
        echo.revPos = status.revPos;
        echo.revLen = status.revLen;
    }

    if (status.info & KanjiGLineInfo) {
        memcpy(cand.line, status.gline.line, status.gline.length * sizeof(CannaWChar));
        cand.line[status.gline.length] = 0;
        cand.length = status.gline.length;
        cand.revPos = status.gline.revPos;
        cand.revLen = status.gline.revLen;

        if (cand.line[0] != 0) {
            if (!candWindow)
                candWindow = new CannaCandidate(this, ic->preeditWindow);
        }
    }

#if DEBUG > 1
    if (status.info & KanjiModeInfo) {
        c16_wcstombs(mb_buf, (CannaWChar*) status.mode, sizeof(mb_buf));
        QsTRACE(1, "mode = %s\n", mb_buf);
    }
#endif

    if (result > 0) {
        // 確定文字列あり
        wc_buf[result] = 0;
        c16_wcstombs(mb_buf, wc_buf, sizeof(mb_buf));
        // TRACE("det = %02x %02x (%s)\n", wc_buf[0], wc_buf[1], mb_buf);
        unsigned char c = mb_buf[0];
        if (c >= 0x20 && c <= 0x7e || c >= 0xa1 && c <= 0xfe) {
            determined = mb_buf;
            return GRAPHIC_CHAR;
        }
        else
            return THROUGH;
    }
    return NONE;
}

void CannaConv::all_determine()
{
    // TODO:
}

void CannaConv::clear()
{
    // TODO:
}

struct CannaModeMap {
    int cmode;
    KanaMode nmode;
};

const CannaModeMap cmodemap[] = {
    { CANNA_MODE_AlphaMode, MODE_OFF },
    { CANNA_MODE_EmptyMode, MODE_KANA },
    { CANNA_MODE_KigoMode,  MODE_ALPHA },
    { CANNA_MODE_YomiMode,  MODE_KANA },
    { -1, MODE_OFF }
};

KanaMode CannaConv::getKanaMode() const
{
    char cur_mode[10];

    wcKanjiControl(context, KC_SETMODEINFOSTYLE, (char*) 1); // ModeInfoStyleIsNumeric);
    wcKanjiControl(context, KC_QUERYMODE, cur_mode);
    wcKanjiControl(context, KC_SETMODEINFOSTYLE, (char*) 0); // ModeInfoStyleIsString);

    // TRACE("mode = %d\n", cur_mode[0] - '@');
    for (int i = 0; cmodemap[i].cmode != -1; i++) {
        if (cur_mode[0] - '@' == cmodemap[i].cmode) {
            // TRACE("sel_mode = %d\n", cmodemap[i].nmode);
            return cmodemap[i].nmode;
        }
    }
    return MODE_ALPHA;  // TODO:
}

void CannaConv::setKanaMode(KanaMode mode)
{
    wcKanjiStatusWithValue ksv;
    memset(&ksv, 0, sizeof(ksv));
    wcKanjiStatus ks;
    memset(&ks, 0, sizeof(ks));
    CannaWChar buf[1000];
    buf[0] = 0;

    ksv.ks = &ks;
    ksv.buffer = buf;
    ksv.n_buffer = sizeof(buf) / sizeof(CannaWChar);

    if (mode == MODE_OFF)
        ksv.val = CANNA_MODE_AlphaMode;
    else if (mode == MODE_KANA)
        ksv.val = CANNA_MODE_HenkanMode;
    else if (mode == MODE_ALPHA)
        ksv.val = CANNA_MODE_HanAlphaKakuteiMode;
    else {
        assert(0);
    }

    int r = wcKanjiControl(context, KC_CHANGEMODE, (char*) &ksv);
    if (r > 0) {
        QsTRACE(1, "r > 0\n");
        // TODO: エラーチェック
    }
}

PreeditWindow* CannaConv::createPreeditWindow()
{
    return new CannaPreedit(this, ic);
}

StatusWindow* CannaConv::createStatusWindow()
{
    return new CannaStatus(this, ic);
}

string CannaConv::getModeLine() const
{
    CannaWChar cur_mode[100];
    char mb_buf[100];
    wcKanjiControl(context, KC_QUERYMODE, (char*) cur_mode);
    c16_wcstombs(mb_buf, cur_mode, sizeof(mb_buf));
    return mb_buf;
}

void CannaConv::setCandidateVisible(bool v)
{
    if (candWindow)
        candWindow->setVisible(v);
}

void CannaConv::updateCandidate()
{
    if (candWindow)
        candWindow->update();
}

#endif  // USE_CANNA
