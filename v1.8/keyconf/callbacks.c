// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include "../config.h"

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "../keymap.h"
#include "../DetectDlg.h"
#include <gtk/gtk.h>
#include <misc-gtk.h>

#define APP_NAME    "keyconf"
#define APP_VERSION VERSION

MapWindow mapWindow;
FuncsWindow funcsWindow;
KeyProp keyProp;
FuncProp funcProp;
MapEditorDoc doc;

////////////////////////////////////////////////////////////////////////
// MapEditorDoc

MapEditorDoc::MapEditorDoc()
{
}

MapEditorDoc::~MapEditorDoc()
{
    map_data.clear();
}

void MapEditorDoc::setKey(Key* key)
{
    Key* old_key = const_cast<Key*>(map_data.find_key(key->name));
    if (old_key) {
            // TODO: MapWindowへ移動
        int row = gtk_clist_find_row_from_data(
                    GTK_CLIST(mapWindow.clist), old_key);
        gtk_clist_set_row_data(GTK_CLIST(mapWindow.clist), row, key);
        map_data.keyList.erase(old_key);
    }
    map_data.keyList.insert(key);
    mapWindow.update(key);

    setModified(true);
    updateTitle();
}

void MapEditorDoc::setCtrlFunc(CtrlFunc* func)
{
    assert(func);
    KeyMap::FuncList::iterator old_func = map_data.funcList.find(func);
    if (old_func != map_data.funcList.end()) {
        for (int i = 0; i < COUNT_IME_STAT; i++)
            (*old_func)->piece[i] = func->piece[i];
        funcsWindow.update(*old_func);
        delete func;
    }
    else {
        map_data.funcList.insert(func);
        funcsWindow.update(func);
    }
    
    keyProp.updateFuncs();

    setModified(true);
    updateTitle();
}

bool MapEditorDoc::save()
{
    assert(filename != "");
    bool r = map_data.save(filename.c_str());
    if (r) {
        setModified(false);
        updateTitle();
    }
    return r;
}

////////////////////////////////////////////////////////////////////////
// MapWindow

const int MapWindow::COLUMN_COUNT = 4;

MapWindow::MapWindow()
{
}

MapWindow::~MapWindow()
{
}

void MapWindow::updateTitle(const string& title)
{
    gtk_window_set_title(GTK_WINDOW(window), title.c_str());
}

void
on_map_window_realize                  (GtkWidget       *widget,
                                        gpointer         user_data)
{
    mapWindow.window = widget;
    mapWindow.clist = lookup_widget(widget, "map_clist");

    GtkWidget* view_menu = lookup_widget(widget, "view_menu");
    gtk_signal_connect(GTK_OBJECT(view_menu), "map",
                GTK_SIGNAL_FUNC(MapWindow::onViewMenuMap), &mapWindow);

    doc.attachController(&mapWindow);
    doc.updateTitle();
}

void
on_map_select_row                      (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    Key* key = static_cast<Key*>(gtk_clist_get_row_data(clist, row));
    if (key) {
        gtk_widget_show(keyProp.window);
        keyProp.update(getKeyMod(key->name), getKeyCode(key->name));
    }
}

void MapWindow::drawLine(int row, const Key* key)
{
    if (row < 0) {
        char* s[COLUMN_COUNT];
        for (int i = 0; i < COLUMN_COUNT; i++)
            s[i] = "";
        row = gtk_clist_append(GTK_CLIST(clist), s);
        gtk_clist_set_row_data(GTK_CLIST(clist), row, (void*) key);
    }

    gtk_clist_set_text(GTK_CLIST(clist), row, 0, key->name.c_str());
    if (key->getType() == GRAPHIC_KEY) {
        const GraphicKey* g = dynamic_cast<const GraphicKey*>(key);
        for (int i = 0; i < 3; i++) {
            gtk_clist_set_text(GTK_CLIST(clist), row, i + 1,
                    (string("\"") + g->getGraphChar(i) + "\"").c_str());
        }
    }
    else {
        const FunctionKey* c = dynamic_cast<const FunctionKey*>(key);
        for (int i = 0; i < 3; i++) {
            if (c->func[i]) {
                gtk_clist_set_text(GTK_CLIST(clist), row, i + 1,
                                        c->func[i]->name.c_str());
            }
            else
                gtk_clist_set_text(GTK_CLIST(clist), row, i + 1, "");
        }
    }
}

void MapWindow::update(const Key* key)
{
    gtk_clist_freeze(GTK_CLIST(clist));
    if (!key) {
        // 表示を全て更新
        gtk_clist_clear(GTK_CLIST(clist));
        KeyMap::KeyList::const_iterator i;
        for (i = doc.map_data.keyList.begin(); i != doc.map_data.keyList.end(); i++)
            drawLine(-1, *i);
    }
    else {
        // 表示を一部更新
        int row = gtk_clist_find_row_from_data(GTK_CLIST(clist), (void*) key);
        drawLine(row, key);
    }
    gtk_clist_thaw(GTK_CLIST(clist));
}

gboolean
on_map_window_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    on_close_activate(NULL, NULL);
    return TRUE;
}


void
on_map_window_destroy                  (GtkObject       *object,
                                        gpointer         user_data)
{
    gtk_main_quit();
}

void MapWindow::onViewMenuMap(GtkWidget* widget, MapWindow* this_)
{
    GtkWidget* funcs = lookup_widget(this_->window, "view_funcs");
    GtkWidget* keyprop = lookup_widget(this_->window, "view_keyprop");
    GtkWidget* funcprop = lookup_widget(this_->window, "view_funcprop");

    GTK_CHECK_MENU_ITEM(funcs)->active = GTK_WIDGET_VISIBLE(funcsWindow.window) != 0;
    GTK_CHECK_MENU_ITEM(keyprop)->active = GTK_WIDGET_VISIBLE(keyProp.window) != 0;
    GTK_CHECK_MENU_ITEM(funcprop)->active = GTK_WIDGET_VISIBLE(funcProp.window) != 0;
}

void
on_popup_delete_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
    // キー配列のポップアップ
{
    GtkCList* clist = GTK_CLIST(mapWindow.clist);

    int row = (int) clist->selection->data;
    Key* key = (Key*) gtk_clist_get_row_data(clist, row);
    doc.map_data.keyList.erase(key);
    doc.setModified(true);
    doc.updateTitle();

    mapWindow.update(NULL);
    keyProp.update(0, 0);
}

gboolean
on_map_clist_button_release_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
    if (event->button == MENU_BUTTON) {
        int row, col;
        gtk_clist_get_selection_info(GTK_CLIST(widget),
                        (int) event->x, (int) event->y, &row, &col);
        TRACE("map: row = %d, col = %d\n", row, col);
        gtk_clist_select_row(GTK_CLIST(widget), row, col);

        GtkWidget* menu = create_k_popup();
        gtk_menu_popup(GTK_MENU(menu), 0, 0, 0, 0, 0, 0);
        return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////

void
on_file_new_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    if (!mapWindow.close())
        return;

    doc.map_data.clear();
    doc.setModified(false);
    doc.updateTitle();

    mapWindow.update(NULL);
    funcsWindow.update(NULL);
    keyProp.update(0, 0);
    keyProp.updateFuncs();
    funcProp.update(NULL);
}

bool loadFile(const string& filename)
{
    if (!doc.map_data.load(filename))
        return false;

    doc.filename = filename;
    doc.setModified(false);
    doc.updateTitle();

    mapWindow.update(NULL);
    funcsWindow.update(NULL);
    keyProp.update(0, 0);
    keyProp.updateFuncs();
    funcProp.update(NULL);

    return true;
}

void
on_file_open_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    if (!mapWindow.close())
        return;

    FileDialog dlg(GTK_WINDOW(mapWindow.window), "開く",
                   doc.filename.c_str());
    if (dlg.setVisible(true) == FileDialog::IDOK) {
        doc.map_data.clear();
        loadFile(dlg.getFile());
    }
}

void
on_file_save_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    mapWindow.save();
}

void
on_file_saveas_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    mapWindow.saveAs();
}


void
on_close_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    if (mapWindow.close())
        gtk_widget_destroy(mapWindow.window);
}

void
on_view_funcs_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    gtk_widget_show(funcsWindow.window);
    gdk_window_raise(funcsWindow.window->window);
}


void
on_view_funcprop_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    gtk_widget_show(funcProp.window);
    gdk_window_raise(funcProp.window->window);
}

void
on_view_keyprop_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    gtk_widget_show(keyProp.window);
    gdk_window_raise(keyProp.window->window);
}

void
on_about_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
    MessageBox dlg(GTK_WINDOW(mapWindow.window), APP_NAME " " APP_VERSION);
    dlg.setVisible(true);
}

////////////////////////////////////////////////////////////////////////
// FuncsWindow

const int FuncsWindow::COLUMN_COUNT = 5;

void
on_funcs_window_realize                (GtkWidget       *widget,
                                        gpointer         user_data)
{
    funcsWindow.window = widget;
    funcsWindow.clist = lookup_widget(widget, "funcs_clist");
}

gboolean
on_funcs_window_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    gtk_widget_hide(funcsWindow.window);
    return TRUE;
}


void
on_funcs_select_row                    (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    CtrlFunc* func = static_cast<CtrlFunc*>(gtk_clist_get_row_data(clist, row));
    if (func) {
        gtk_widget_show(funcProp.window);
        funcProp.update(func);
    }
}

void FuncsWindow::drawLine(int row, const CtrlFunc* func)
{
    if (row < 0) {
        const char* s[COLUMN_COUNT];
        s[0] = func->name.c_str();
        for (int i = 0; i < COUNT_IME_STAT; i++)
            s[i + 1] = find_piece_name(func->piece[i]);
        row = gtk_clist_append(GTK_CLIST(clist), const_cast<char**>(s));
    }
    else {
        gtk_clist_set_text(GTK_CLIST(clist), row, 0, func->name.c_str());
        for (int i = 0; i < COUNT_IME_STAT; i++) {
            gtk_clist_set_text(GTK_CLIST(clist), row, i + 1,
                                find_piece_name(func->piece[i]));
        }
    }
    gtk_clist_set_row_data(GTK_CLIST(clist), row, (void*) func);
}

void FuncsWindow::update(const CtrlFunc* func)
{
    gtk_clist_freeze(GTK_CLIST(clist));
    if (!func) {
        // すべて更新
        gtk_clist_clear(GTK_CLIST(clist));
        KeyMap::FuncList::const_iterator i;
        for (i = doc.map_data.funcList.begin(); i != doc.map_data.funcList.end(); i++)
            drawLine(-1, *i);
    }
    else {
        int row = gtk_clist_find_row_from_data(GTK_CLIST(clist), (void*) func);
        drawLine(row, func);
    }
    gtk_clist_thaw(GTK_CLIST(clist));
}

void
on_f_popup_delete_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
    // 制御機能のポップアップ
{
    GtkCList* clist = GTK_CLIST(funcsWindow.clist);

    int row = (int) clist->selection->data;
    CtrlFunc* func = (CtrlFunc*) gtk_clist_get_row_data(clist, row);

    MessageBox msg(GTK_WINDOW(funcsWindow.window),
            "制御機能「%s」を削除しますか？　関連するキーも削除されます。",
            func->name.c_str());
    msg.setType(MessageBox::MB_YES_NO);
    if (msg.setVisible(true) == MessageBox::IDYES) {
        TRACE("del func: row = %d\n", row);

    redo:
        KeyMap::KeyList::iterator i = doc.map_data.keyList.begin();
        while (i != doc.map_data.keyList.end()) {
            if ((*i)->getType() == FUNCTION_KEY) {
                FunctionKey* fkey = dynamic_cast<FunctionKey*>(*i);
                if (fkey->func[0] == func || fkey->func[1] == func
                        || fkey->func[2] == func) {
                    doc.map_data.keyList.erase(i);
                    goto redo;
                }
            }
            i++;
        }

        doc.map_data.funcList.erase(func);
        doc.setModified(true);
        doc.updateTitle();

        funcsWindow.update(NULL);
        funcProp.update(NULL);
        mapWindow.update(NULL);
        keyProp.update(0, 0);
        keyProp.updateFuncs();
    }
}

gboolean
on_funcs_clist_button_release_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
    if (event->button == MENU_BUTTON) {
        int row, col;
        gtk_clist_get_selection_info(GTK_CLIST(widget),
                        (int) event->x, (int) event->y, &row, &col);
        TRACE("funcs: row = %d, col = %d\n", row, col);
        gtk_clist_select_row(GTK_CLIST(widget), row, col);

        GtkWidget* menu = create_f_popup();
        gtk_menu_popup(GTK_MENU(menu), 0, 0, 0, 0, 0, 0);
        return TRUE;
    }
    return FALSE;
}

////////////////////////////////////////////////////////////////////////
// KeyProp

void
on_key_prop_realize                    (GtkWidget       *widget,
                                        gpointer         user_data)
{
    keyProp.window = widget;

    // キー
    keyProp.key_ctrl = lookup_widget(widget, "key_ctrl");
    keyProp.key_entry = lookup_widget(widget, "key_entry");
    assert(keyProp.key_entry);

    // 種類
    keyProp.type_graph = lookup_widget(widget, "type_graph");
    keyProp.type_func = lookup_widget(widget, "type_func");

    // フレーム
    keyProp.graph_frame = lookup_widget(widget, "graph_frame");
    keyProp.func_frame = lookup_widget(widget, "func_frame");
    gtk_widget_hide(keyProp.func_frame);

    // 図形文字
    keyProp.graph_entry[0] = lookup_widget(widget, "single_entry");
    keyProp.graph_entry[1] = lookup_widget(widget, "left_entry");
    keyProp.graph_entry[2] = lookup_widget(widget, "right_entry");

    // 制御機能
    keyProp.func_sel = lookup_widget(widget, "func_sel");
    assert(keyProp.func_sel);
    keyProp.updateFuncs();
}

void
on_keyprop_keychoose_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{
    DetectDlg dlg(GTK_WINDOW(keyProp.window));
    if (dlg.setVisible(true) == DetectDlg::IDOK) {
        gtk_entry_set_text(GTK_ENTRY(keyProp.key_entry),
                                getKeyName(0, dlg.keycode).c_str());
    }
/*
    if (dlg.setVisible(true) == DetectDlg::IDOK)
        keyProp.update(0, dlg.keycode);
*/
}

gboolean
on_key_prop_delete_event               (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    gtk_widget_hide(keyProp.window);
    return TRUE;
}


void
on_keyprop_type_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(keyProp.type_graph))) {
        gtk_widget_hide(keyProp.func_frame);
        gtk_widget_show(keyProp.graph_frame);
    }
    else {
        gtk_widget_hide(keyProp.graph_frame);
        gtk_widget_show(keyProp.func_frame);
    }
}

void
on_keyprop_ok                          (GtkButton       *button,
                                        gpointer         user_data)
{
    string key_name = gtk_entry_get_text(GTK_ENTRY(keyProp.key_entry));
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(keyProp.key_ctrl)))
        key_name = string("ctrl-") + key_name;

    string s[3];
    Key* key;
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(keyProp.type_graph))) {
        for (int i = 0; i < 3; i++)
            s[i] = gtk_entry_get_text(GTK_ENTRY(keyProp.graph_entry[i]));
        key = new GraphicKey(key_name, s);
    }
    else {
        s[0] = gtk_entry_get_text(
                        GTK_ENTRY(GTK_COMBO(keyProp.func_sel)->entry));
        s[1] = "";  // TODO: 制御機能も[Shift]併用できるようにする
        s[2] = "";
        key = doc.map_data.createFuncKey(key_name, s);
    }
    doc.setKey(key);
}

void KeyProp::update(int mod, int keycode)
{
    cur_code = keycode;
    if (keycode == 0) {
        gtk_entry_set_text(GTK_ENTRY(key_entry), "");
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(type_graph), TRUE);
        for (int i = 0; i < 3; i++)
            gtk_entry_set_text(GTK_ENTRY(graph_entry[i]), "");
        return;
    }

    gtk_entry_set_text(GTK_ENTRY(key_entry),
                                getKeyName(0, keycode).c_str());
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(key_ctrl), mod != 0);

    const Key* key = doc.map_data.find_key(getKeyName(mod, keycode));
    if (key) {
        if (key->getType() == GRAPHIC_KEY) {
            // TRACE("keytype == GRAPHIC_KEY\n");
            const GraphicKey* g = dynamic_cast<const GraphicKey*>(key);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(type_graph), TRUE);
            for (int i = 0; i < 3; i++) {
                gtk_entry_set_text(GTK_ENTRY(graph_entry[i]),
                                        g->getGraphChar(i).c_str());
            }
        }
        else {
            // TRACE("keytype == FUNC_KEY\n");
            const FunctionKey* c = dynamic_cast<const FunctionKey*>(key);
            gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(type_func), TRUE);
            const CtrlFunc* func = c->func[0];
            gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(func_sel)->entry),
                               func ? func->name.c_str() : "");
        }
    }
    else {
        // detectで未定義のキーを選んだ
        gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(type_graph), TRUE);
        for (int i = 0; i < 3; i++)
            gtk_entry_set_text(GTK_ENTRY(graph_entry[i]), "");
    }
}

void KeyProp::updateFuncs()
{
    GList* glist = NULL;
    KeyMap::FuncList::const_iterator i;
    for (i = doc.map_data.funcList.begin(); i != doc.map_data.funcList.end(); i++)
        glist = g_list_append(glist, (void*) (*i)->name.c_str());

    gtk_combo_set_popdown_strings(GTK_COMBO(func_sel), glist) ;
}

////////////////////////////////////////////////////////////////////////
// FuncProp

void FuncProp::update(const CtrlFunc* func)
{
    if (!func) {
        gtk_entry_set_text(GTK_ENTRY(name_entry), "");
        for (int i = 0; i < COUNT_IME_STAT; i++)
            gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(pie_sel[i])->entry), "");
    }
    else {
        gtk_entry_set_text(GTK_ENTRY(name_entry), func->name.c_str());
        for (int i = 0; i < COUNT_IME_STAT; i++) {
            gtk_entry_set_text(GTK_ENTRY(GTK_COMBO(pie_sel[i])->entry),
                        find_piece_name(func->piece[i]));
        }
    }
}

void
on_func_prop_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
    funcProp.window = widget;
    funcProp.name_entry = lookup_widget(widget, "name_entry");
    funcProp.pie_sel[0] = lookup_widget(widget, "psel_combo1");
    funcProp.pie_sel[1] = lookup_widget(widget, "psel_combo2");
    funcProp.pie_sel[2] = lookup_widget(widget, "psel_combo3");
    funcProp.pie_sel[3] = lookup_widget(widget, "psel_combo4");

    for (int i = 0; i < COUNT_IME_STAT; i++) {
        GList* glist = NULL;
        for (int j = 0; funcNameId[j].name; j++) {
            if (funcNameId[j].canUse[i])
                glist = g_list_append(glist, (void*) funcNameId[j].name);
        }
        gtk_combo_set_popdown_strings(GTK_COMBO(funcProp.pie_sel[i]), glist);
    }
}

void
on_funcprop_ok                         (GtkButton       *button,
                                        gpointer         user_data)
{
    CtrlFunc* func = new CtrlFunc();
    func->name = gtk_entry_get_text(GTK_ENTRY(funcProp.name_entry));
    for (int i = 0; i < COUNT_IME_STAT; i++) {
        func->piece[i] = find_piece_id(
            gtk_entry_get_text(GTK_ENTRY(GTK_COMBO(funcProp.pie_sel[i])->entry)), i);
    }
    doc.setCtrlFunc(func);
}

gboolean
on_func_prop_delete_event              (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    gtk_widget_hide(funcProp.window);
    return TRUE;
}

////////////////////////////////////////////////////////////////////////

