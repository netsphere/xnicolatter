// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 入力状態などの保持
// クライアントのプロトコルには依存しない

#include "../config.h"

#include "client.h"
#include "Connection.h"
#include "preedit.h"
#include "status.h"
#include "WnnConv.h"
#include "CannaConv.h"
#include "../global.h"

////////////////////////////////////////////////////////////////////////
// InputContext

InputContext* InputContext::focused = NULL;
int InputContext::next_icid = 1;

InputContext::InputContext(InputMethod* im): id(0), inputStyle(0),
    clientWindow(0), focusWindow(0), ext_event_mask(false),
    kanaKanji(NULL), preeditWindow(NULL), statusWindow(NULL),
    disabled(false),
    im_(im)
{
    id = next_icid++;
    initialize();
}

InputContext::~InputContext()
{
    preeditWindow->destroy();
    statusWindow->destroy();

    delete preeditWindow;
    delete statusWindow;
    delete kanaKanji;

    if (this == focused)
        focused = NULL;
}

void InputContext::initialize()
    // for restart
{
    delete kanaKanji;

#if defined(USE_WNN) && defined(USE_CANNA)
    if (global_prop.conv_server == 0)
        kanaKanji = new WnnConv(this);
    else
        kanaKanji = new CannaConv(this);
#elif defined(USE_WNN)
    kanaKanji = new WnnConv(this);
#elif defined(USE_CANNA)
    kanaKanji = new CannaConv(this);
#else
  #error must define USE_WNN or USE_CANNA
#endif  // USE_WNN && USE_CANNA

    PreeditWindow* p = kanaKanji->createPreeditWindow();
    if (preeditWindow) {
        *p = *preeditWindow;
        delete preeditWindow;
    }
    preeditWindow = p;

    StatusWindow* s = kanaKanji->createStatusWindow();
    if (statusWindow) {
        *s = *statusWindow;
        delete statusWindow;
    }
    statusWindow = s;
}

int InputContext::getId() const
{
    return id;
}

InputMethod* InputContext::getIM() const
{
    return im_;
}

void InputContext::updateFocusPosition()
{
    // TRACE("focus = %x, client = %x\n", focusWindow, clientWindow);

    Window root = XRootWindow(top_display, 0);
    Window c;
    int x, y;
    ::XTranslateCoordinates(top_display, focusWindow ? focusWindow : clientWindow,
                            root, 0, 0, &x, &y, &c);
    position.x = x;
    position.y = y;
}

void InputContext::input(const KeyEvent& event, int level)
    // 入力: キー/Levelの組み合わせ
{
    Connection* conn = getIM()->getConnection();
    KanaKanjiStatus status = kanaKanji->input(event, level);

    // 確定文字列があって，さらにスルーしたいので，確定文字列は別扱いする

    bool commit = false;
    if (kanaKanji->getDetermined() != "") {
        // 確定文字列あり
        conn->appendCommitString(this, kanaKanji->getDetermined());
        kanaKanji->clearDetermined();
        commit = true;
    }

    switch (status)
    {
    case THROUGH:
        conn->appendThroughEvent(this, event, commit);
        break;
    case NONE:
        break;
    case GRAPHIC_CHAR:
        /* empty */
        break;
    case CONTROL_FUNC:
        // 仮名 -> 英字
        kanaKanji->setKanaMode(MODE_OFF);
        break;
    default:
        assert(0);
    }
}

void InputContext::processEvent(const KeyEvent& event)
    // 入力: キーsequence
{
    Connection* conn = getIM()->getConnection();
    setFocus(true);
    if (disabled)
        conn->appendThroughEvent(this, event, false);

    switch (kanaKanji->getKanaMode())
    {
    case MODE_OFF:
    case MODE_ALPHA: // 無変換固定
        if (event.type == KeyPress)
            input(event, (event.modifier & ShiftMask) != 0 ? 1 : 0);
        break;
    case MODE_KANA:
        keyChar->input(this, event);
        break;
    default:
        assert(0);
    }
}

void InputContext::updateView()
{
    if (disabled || !isFocused()) {
        preeditWindow->setVisible(false);
        statusWindow->setVisible(false);
        kanaKanji->setCandidateVisible(false);
        return;
    }

    updateFocusPosition();

    preeditWindow->update();
    statusWindow->update();
    kanaKanji->updateCandidate();
}

void InputContext::setFocus(bool f)
{
    if (f) {
        if (focused && focused != this) {
            InputContext* old = focused;
            focused = NULL;
            old->updateView();
        }

        focused = this;
        updateView();
    }
    else {
        if (this == focused) {
            focused = NULL;
            updateView();
        }
    }
}

bool InputContext::isFocused() const
{
    return this == focused;
}

////////////////////////////////////////////////////////////////////////
// InputMethod

int InputMethod::next_imid = 1;

InputMethod::InputMethod(Connection* conn): client(conn)
{
    id = next_imid++;
}

InputMethod::~InputMethod()
{
}

InputContext* InputMethod::createIC()
{
    InputContext* r = new InputContext(this);
    contexts.push_back(r);
    return r;
}

int InputMethod::getId() const
{
    return id;
}

InputContext* InputMethod::getIC(int id) const
{
    InputContexts::const_iterator i;
    for (i = contexts.begin(); i != contexts.end(); i++) {
        if (id == (*i)->getId())
            return *i;
    }
    return NULL;
}

Connection* InputMethod::getConnection() const
{
    return client;
}

void InputMethod::remove_ic(int id)
{
    InputContexts::iterator i;
    for (i = contexts.begin(); i != contexts.end(); i++) {
        if (id == (*i)->getId()) {
            // TRACE("remove_ic(): %d\n", id);
            contexts.erase(i);
            return;
        }
    }
}

