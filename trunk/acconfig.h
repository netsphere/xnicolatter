#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY
#undef HAVE_LIBSM
#undef PACKAGE_LOCALE_DIR
#undef PACKAGE_DATA_DIR
#undef PACKAGE_SOURCE_DIR

// Define if you have Wnn 4.2 or FreeWnn.
#undef WNN4

// Define if you have Wnn6.
#undef WNN6

#undef USE_WNN

// Define if you have Canna.
#undef USE_CANNA

// Define if you want nicolatter-gtk.
#undef USE_GTK
