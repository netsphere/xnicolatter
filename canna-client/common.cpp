
#define CANNA_NEW_WCHAR_AWARE

#include <stdio.h>
#include <assert.h>
#include <iconv.h>
#include <string.h>
#include <canna/jrkanji.h>

// Canna は EUC-JPでやり取りする。UTF-8 に変換する.
//   G1 0x0000 ASCII
//   G2 0x8080 漢字
//   G3 0x0080 半角カナ   0x8e
//   G4 0x8000 補助漢字   0x8f
// @param n destの大きさ.
// @return 生成したマルチバイト列のバイト数。終端のナル文字は含まない。
size_t cannawcs_to_utf8(char* dest, const cannawc* wcs, size_t n)
{
    assert(wcs);
  
    char* outp = dest;
    char euc[4];

    iconv_t ic = iconv_open("UTF-8", "EUCJP-MS");
    assert( ic != (iconv_t) -1 );

    while (n > 0 && *wcs) {
      char* q = euc;
      switch (*wcs & 0x8080)
      {
      case 0: // G1
        *q++ = *wcs++ & 0x7f;
        break;
      case 0x8080: // G2
        *q++ = (*wcs >> 8) & 0xff;
        *q++ = *wcs++ & 0xff;
        break;
      case 0x0080: // G3
        *q++ = 0x8e;
        *q++ = *wcs++ & 0xff;
        break;
      case 0x8000: // G4
        *q++ = 0x8f;
        *q++ = (*wcs >> 8) & 0xff;
        *q++ = 0x80 | (*wcs++ & 0xff);
        break;
      }
      *q = '\0';

      size_t inbytesleft = q - euc;
      q = euc;
      iconv(ic, &q, &inbytesleft, &outp, &n);
    }
    if (n > 0)
      *outp = '\0';

    iconv_close(ic);
    return outp - dest;
}


size_t euc_to_utf8(char* dest, const unsigned char* euc, size_t n)
{
    assert( euc );

    iconv_t ic = iconv_open("UTF-8", "EUCJP-MS");
    assert( ic != (iconv_t) -1 );

    size_t inbytesleft = strlen((char*) euc);
    char* outp = dest;
    iconv(ic, (char**) &euc, &inbytesleft, &outp, &n);

    if (n > 0)
      *outp = '\0';

    iconv_close(ic);
    return outp - dest;
}


static char xbuf[1000];
const char* dump(const unsigned char* s, int bytes)
{
  xbuf[0] = '\0';
  for (int i = 0; i < bytes; i++)
    sprintf(xbuf + strlen(xbuf), " %x", s[i]);

  return xbuf;
}

           
