// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// Wnn用単語登録プログラム

#include "../config.h"
#ifdef USE_WNN

#include <gtk/gtk.h>

#include "../jllib.h"

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include <misc.h>
#include "../qWnn.h"

////////////////////////////////////////////////////////////////////////
// Hinsi

struct Hinsi
{
    int no;
    string name;
};

typedef ptr_vector<Hinsi*> HinsiList;
HinsiList hinsiList;

int findHinsi(const string& str)
{
    HinsiList::const_iterator i;
    for (i = hinsiList.begin(); i != hinsiList.end(); i++) {
        if ((*i)->name == str)
            return (*i)->no;
    }
    return -1;
}

////////////////////////////////////////////////////////////////////////
// AddWord

class AddWord
{
public:
    GtkWidget* window;
    GtkWidget* read_entry;
    GtkWidget* kanji_entry;
    GtkWidget* comment_entry;
    GtkWidget* hinsi_combo;
    GtkWidget* hinsi_combo_entry;
    GtkWidget* dic_combo;
};

AddWord addWord;

struct wnn_buf* wnn = NULL;

void
on_addword_window_realize              (GtkWidget       *widget,
                                        gpointer         user_data)
{
    addWord.window = widget;
    addWord.read_entry = lookup_widget(widget, "read_entry");
    addWord.kanji_entry = lookup_widget(widget, "kanji_entry");
    addWord.comment_entry = lookup_widget(widget, "comment_entry");
    addWord.hinsi_combo = lookup_widget(widget, "hinsi_combo");
    addWord.hinsi_combo_entry = lookup_widget(widget, "hinsi_combo_entry");
    addWord.dic_combo = lookup_widget(widget, "dic_combo");

    // TODO: impl
    gtk_widget_set_sensitive(GTK_COMBO(addWord.dic_combo)->entry, FALSE);
    
    wnn = wnn_connect();
    assert(wnn);

    char buf[1000];

    // 辞書一覧
    WNN_DIC_INFO* dicinfo = NULL;
    int dic_count = jl_dic_list(wnn, &dicinfo);
    for (int i = 0; i < dic_count; i++) {
        wnn_wcstombs(buf, dicinfo[i].comment, sizeof(buf));
        TRACE("comment = %s\n", buf);
        TRACE("dic_file = %s\n", dicinfo[i].fname);
    }

    // 品詞一覧
    for (int i = 0; i < 100; i++) {
        w_char* name = jl_hinsi_name(wnn, i);
        if (name) {
            wnn_wcstombs(buf, name, sizeof(buf));
                    // nameの領域はjl_hinsi_dicts()で上書きされる

            // 登録可能な辞書があるか
            int* area;
            int num = jl_hinsi_dicts(wnn, i, &area);
            if (num) {
                TRACE("%d: '%s'\n", i, buf);
                Hinsi* h = new Hinsi();
                h->no = i;
                h->name = buf;
                hinsiList.push_back(h);
            }
            else {
                TRACE("%d: '%s' -- 登録できない\n", i, buf);
            }
        }
    }

    HinsiList::const_iterator i;
    GList* glist = NULL;
    for (i = hinsiList.begin(); i != hinsiList.end(); i++)
        glist = g_list_append(glist, (void*) (*i)->name.c_str());
    gtk_combo_set_popdown_strings(GTK_COMBO(addWord.hinsi_combo), glist);
}

void
on_ok_button_clicked                   (GtkButton       *button,
                                        gpointer         user_data)
{
    string read = gtk_entry_get_text(GTK_ENTRY(addWord.read_entry));
    string kanji = gtk_entry_get_text(GTK_ENTRY(addWord.kanji_entry));
    string comment = gtk_entry_get_text(GTK_ENTRY(addWord.comment_entry));
    string hinsi_str = gtk_entry_get_text(GTK_ENTRY(addWord.hinsi_combo_entry));

    if (read != "" && kanji != "") {
        int hinsi = findHinsi(hinsi_str);
        assert(hinsi != -1);
        int* area;
        jl_hinsi_dicts(wnn, hinsi, &area);
        int dic_no = area[0];   // TODO: ユーザーが選択
        TRACE("read = %s, kanji = %s, comment = %s, hinsi = %d, dic = %d\n",
                read.c_str(), kanji.c_str(), comment.c_str(), hinsi, dic_no);
        w_char read_w[1000];
        w_char kanji_w[1000];
        w_char comment_w[1000];
        wnn_mbstowcs(read_w, read.c_str(), sizeof(read_w) / sizeof(w_char));
        wnn_mbstowcs(kanji_w, kanji.c_str(), sizeof(kanji_w) / sizeof(w_char));
        wnn_mbstowcs(comment_w, comment.c_str(), sizeof(comment_w) / sizeof(w_char));
        int r = jl_word_add(wnn, dic_no, read_w, kanji_w, comment_w, hinsi, 0);
        if (r < 0) {
            error("登録に失敗\n");
        }
    }
}


void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
    if (wnn) {
        jl_dic_save_all(wnn);
        jl_close(wnn);
        wnn = NULL;
    }
    gtk_main_quit();
}

#endif  // USE_WNN
