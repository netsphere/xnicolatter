// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 大域設定

#include "config.h"

#include <cstdio>
#include <cassert>
#include <strings.h> // strcasecmp()
#include "global.h"
#include "URI.h"
#include "misc.h"
using namespace std;

//////////////////////////////////////////////////////////////////////
// GlobalProperty

GlobalProperty global_prop;

static const char* nico_dir = "/.nicolatter";

const string GlobalProperty::RC_FILE = string(nico_dir) + "/global";
const string GlobalProperty::SYS_RC_FILE = SYS_RC_DIR "/global";

GlobalProperty::GlobalProperty()
{
    initialize();
}

GlobalProperty::~GlobalProperty()
{
}

void GlobalProperty::initialize()
    // for restart
{
    string home_dir = getHomeDir();
    
#if defined(USE_WNN)
    conv_server = 0;    // wnn
#elif defined(USE_CANNA)
    conv_server = 1;
#else
    #error must define USE_WNN or USE_CANNA
#endif

    use_keymap = true;
    keymap_file = home_dir + nico_dir + "/nicola.keymap";

    kana_key = "ctrl-space";

    shift_method = 2;   // sync
    shift_key[0] = "BackSpace";
    shift_key[1] = "space";

    use_roma = false;
    roma_file = home_dir + nico_dir + "/normal.roma";
    roma_strict = true;

    preedit_font = status_font =
        "-*-helvetica-medium-r-normal--14-*-*-*-*-*-iso8859-1,"
        "-*-fixed-medium-r-normal--14-*-*-*-*-*-jisx0208.1983-0";

    xim = true;
    iiimp = false;
}

const char* GlobalProperty::shift_methods[] = {
    "normal",
    "prefix",
    "synchronous",
    NULL
};

const char* GlobalProperty::conv_servs[] = {
    "wnn",
    "canna",
    NULL
};

static const char* bool_name[] = { "no", "yes" };

static const int PROP_NAME_COUNT = 12;
static const char* prop_name[] = {
    "conversion-server",   // 0
    "use-keymap",
    "keymap-file",
    "kana-key",
    "shift-method",
    "left-shift-key",       // 5
    "right-shift-key",
    "use-roma",
    "roma-file",
    "roma-case-sensitive",
    "preedit-font",         // 10
    "status-font",
    "alnum-keymap",
    NULL
};

void GlobalProperty::save_sub(FILE* fp) const
/*
    RFC 822
     field       =  field-name ":" [ field-body ] CRLF
     field-name  =  1*<any CHAR, excluding CTLs, SPACE, and ":">
     field-body  =  field-body-contents
                    [CRLF LWSP-char field-body]
*/
{
    // 操作
    fprintf(fp, "%s: %s\n", prop_name[0], conv_servs[conv_server]);
    fprintf(fp, "%s: %s\n", prop_name[1], bool_name[use_keymap]);
    fprintf(fp, "%s: %s\n", prop_name[2], keymap_file.c_str());
    fprintf(fp, "%s: %s\n", prop_name[12], alnum_keymap.c_str());
    fprintf(fp, "%s: %s\n", prop_name[3], kana_key.c_str());
    fprintf(fp, "%s: %s\n", prop_name[4], shift_methods[shift_method]);
    fprintf(fp, "%s: %s\n", prop_name[5], shift_key[0].c_str());
    fprintf(fp, "%s: %s\n", prop_name[6], shift_key[1].c_str());
    fprintf(fp, "%s: %s\n", prop_name[7], bool_name[use_roma]);
    fprintf(fp, "%s: %s\n", prop_name[8], roma_file.c_str());
    fprintf(fp, "%s: %s\n", prop_name[9], bool_name[roma_strict]);
    // 表示
    fprintf(fp, "%s: %s\n", prop_name[10], preedit_font.c_str());
    fprintf(fp, "%s: %s\n", prop_name[11], status_font.c_str());
}

bool GlobalProperty::save() const
{
    string file = getHomeDir() + RC_FILE;
    if (!createFolder(FileName(file).getPath().c_str()))
        return false;

    FILE* fp = fopen(file.c_str(), "w");
    if (!fp)
        return false;
    save_sub(fp);
    fclose(fp);
    return true;
}

bool GlobalProperty::load()
{
    string file = getHomeDir() + RC_FILE;
    FILE* fp = fopen(file.c_str(), "r");
    if (!fp) {
        file = SYS_RC_FILE;
        fp = fopen(file.c_str(), "r");
    }
    if (!fp)
        return false;
    
    char buf[1000];
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        const unsigned char* p = (unsigned char*) buf;
        string name, body;
        p = skip_space(p);
        while (*p >= 0x21 && *p <= 0x7e && *p != ':')
            name += *p++;
        p = skip_space(p);
        if (*p == ':')
            p++;
        p = skip_space(p);
        while (*p >= 0x20)
            body += *p++;

        int name_id = -1;
        for (int i = 0; i < PROP_NAME_COUNT; i++) {
            if (!strcasecmp(name.c_str(), prop_name[i])) {
                name_id = i;
                break;
            }
        }
        if (name_id < 0)
            continue;

        switch (name_id)
        {
#if defined(USE_WNN) && defined(USE_CANNA)
        case 0:
            for (int i = 0; conv_servs[i]; i++) {
                if (body == conv_servs[i]) {
                    conv_server = i;
                    break;
                }
            }
            break;
#else
        case 0:
            break;
#endif
        case 1:
            use_keymap = isYesString(body.c_str());
            break;
        case 2:
            keymap_file = body;
            break;
        case 3:
            kana_key = body;
            break;
        case 4:
            for (int i = 0; shift_methods[i]; i++) {
                if (body == shift_methods[i]) {
                    shift_method = i;
                    break;
                }
            }
            break;
        case 5:
            shift_key[0] = body;
            break;
        case 6:
            shift_key[1] = body;
            break;
        case 7:
            use_roma = isYesString(body.c_str());
            break;
        case 8:
            roma_file = body;
            break;
        case 9:
            roma_strict = isYesString(body.c_str());
            break;
        case 10:
            preedit_font = body;
            break;
        case 11:
            status_font = body;
            break;
        case 12:
            alnum_keymap = body;
            break;
        default:
            assert(0);
            break;
        }
    }
    fclose(fp);

#if DEBUG > 1
    save_sub(stdout);
#endif
    
    return true;
}

//////////////////////////////////////////////////////////////////////

