// Q's C++ Library
// Copyright (c) 1996-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// RFC 2279: UTF-8, a transformation format of ISO 10646

#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <inttypes.h>

size_t ucstoutf8(char* dest, const uint32_t* src, size_t n)
    // 機能
    //      ワイド文字列をUTF-8文字列に変換する。
    // 解説
    //      ucs2utf()関数は，srcが指すワイド文字列をUTF-8に変換して，
    //      destが指す配列に格納する。destの大きさはnで与えられる。
    //      sがナル文字に達するか，書き込んだバイト数がnバイトに達す
    //      ると終了する。destは，srcがナル文字に達したときのみナル終端す
    //      る。
    //      TODO: destがNULLポインタのとき，必要なバイト数を返す。
    //      UNIXのwcstombs()と同じインターフェイス。
    // 戻り値
    //      destに何バイト書き込んだか。終端のナル文字は数えない。
    //      戻り値がnのとき，destはナル終端していない。
    //      無効なワイド文字があったときは，(size_t) -1を返す。
{
    size_t count = 0;

    while (1) {
        if (*src <= 0x7f) {
            if (count >= n)
                break;
            *(dest++) = (char) *(src++);
            if (*(src - 1) == 0)
                break;
            count++;
        }
        else if (*src <= 0x7ff) {
            if (count >= n - 1)
                break;
            *(dest++) = (*src >> 6) | 0xc0;
            *(dest++) = (*(src++) & 0x3f) | 0x80;
            count += 2;
        }
        else if (*src <= 0xffff) {
            if (count >= n - 2)
                break;
            *(dest++) = (*src >> 12) | 0xe0;
            *(dest++) = ((*src >> 6) & 0x3f) | 0x80;
            *(dest++) = (*(src++) & 0x3f) | 0x80;
            count += 3;
        }
        else if (*src <= 0x1fffff) {
            if (count >= n - 3)
                break;
            *(dest++) = (*src >> 18) | 0xf0;
            *(dest++) = ((*src >> 12) & 0x3f) | 0x80;
            *(dest++) = ((*src >>  6) & 0x3f) | 0x80;
            *(dest++) = (*(src++) & 0x3f) | 0x80;
            count += 4;
        }
        else if (*src <= 0x3ffffff) {
            if (count >= n - 4)
                break;
            *(dest++) = (*src >> 24) | 0xf8;
            *(dest++) = ((*src >> 18) & 0x3f) | 0x80;
            *(dest++) = ((*src >> 12) & 0x3f) | 0x80;
            *(dest++) = ((*src >>  6) & 0x3f) | 0x80;
            *(dest++) = (*(src++) & 0x3f) | 0x80;
            count += 5;
        }
        else if (*src <= 0x7fffffff) {
            if (count >= n - 5)
                break;
            *(dest++) = (*src >> 30) | 0xfc;
            *(dest++) = ((*src >> 24) & 0x3f) | 0x80;
            *(dest++) = ((*src >> 18) & 0x3f) | 0x80;
            *(dest++) = ((*src >> 12) & 0x3f) | 0x80;
            *(dest++) = ((*src >>  6) & 0x3f) | 0x80;
            *(dest++) = (*(src++) & 0x3f) | 0x80;
            count += 6;
        }
        else {
            errno = EILSEQ;
            return (size_t) -1;
        }
    }

    return count;
}

size_t utf8toucs(uint32_t* pwcs, const char* src_, size_t n)
    // 機能
    //     UFT-8文字列をワイド文字列に変換する。
    //     入力がUTF-8, 出力がUCS-4であることを除き，mbstowcs()と同じ
    //     要素nに達するか，0終端まで変換する。（バイト'0'は変換する）
    // 解説
    //      nは，要素数（文字数）。
    // 戻り値
    //      文字が誤り：(size_t) -1
    //      0終端を除く変換した要素（文字）数
{
    size_t count = 0;
    const unsigned char* s = (unsigned char*) src_;

    for (size_t count = 0; count < n; count++) {
        if (*s == '\0') {
            if (pwcs)
                *pwcs++ = *s++;
            // カウントアップしない
            break;
        }
        else if (*s <= 0x7f) {
            if (pwcs)
                *pwcs++ = *s++;
        }
        else if (*s < 0xc0) {
            errno = EILSEQ;
            return (size_t) -1;
        }
        else {
            uint32_t c;
            int b;
            if (*s < 0xe0) { c = (*s++) & 0x1f; b = 2; }
            else if (*s < 0xf0) { c = (*s++) & 0x0f; b = 3; }
            else if (*s < 0xf8) { c = (*s++) & 0x07; b = 4; }
            else if (*s < 0xfc) { c = (*s++) & 0x03; b = 5; }
            else if (*s < 0xfe) { c = (*s++) & 0x01; b = 6; }
            else {
                errno = EILSEQ;
                return (size_t) -1;
            }
            while (--b) {
                if ((*s & 0xc0) != 0x80) {
                    errno = EILSEQ;
                    return (size_t) -1;
                }
                c = (c << 6) + ((*s++) & 0x3f);
            }
            if (pwcs)
                *pwcs++ = c;
        }
    }
    return count;
}

int utf8len(const char* p_, size_t n)
    // mblen()のUTF-8版
{
    if (!p_)
        return 0; // UTF-8 is not state-dependent

    const unsigned char* s = (unsigned char*) p_;
    if (*s == '\0')
        return 0;
    else if (*s <= 0x7f)
        return 1;
    else if (*s < 0xc0) {
        errno = EILSEQ;
        return -1;
    }
    else {
        int b;
        if (*s < 0xe0) { b = 2; }
        else if (*s < 0xf0) { b = 3; }
        else if (*s < 0xf8) { b = 4; }
        else if (*s < 0xfc) { b = 5; }
        else if (*s < 0xfe) { b = 6; }
        else {
            errno = EILSEQ;
            return -1;
        }
        int cnt = b;
        while (--b) {
            if ((*++s & 0xc0) != 0x80) {
                errno = EILSEQ;
                return -1;
            }
        }
        return cnt;
    }
}

/*
class UtfInputFilter: protected wistream
    // UTF-8で保存されたストリームから読みとる。
{
public:
    UtfInputFilter(istream* is = NULL): is_(is) { }
    virtual ~UtfInputFilter() { }
    void attach(istream* is) { is_ = is; }

    UtfInputFilter& read(wchar_t* buf, int count)
    {
        assert(is_);
        is_->read();
        return *this;
    }

private:
    istream* is_;   // 多バイト版
};

class UtfOutputFilter: protected wostream
{
public:
    UtfOutputFilter(ostream* os = NULL): os_(os) { }
    virtual ~UtfOutputFilter() { }
    void attach(ostream* os) { os_ = os; }

private:
    ostream* os_;
};

class KanjiInputFilter: protected istream
{
public:
    KanjiInputFilter(istream* is = NULL): is_(is) { }
    virtual ~KanjiInputFilter() { }

    void attach(istream* is) { is_ = is; }

    KanjiInputFilter& read(char* buf, int count)
    {
        assert(is_);
        is_->read(buf, count);
        return *this;
    }

private:
    istream* is_;
};

class KanjiOutputFilter: protected ostream
{
    KanjiOutputFilter(ostream* os = NULL): os_(os) { }
    virtual ~KanjiOutputFilter() { }

    void attach(ostream* os) { os_ = os; }
    KanjiOutputFilter& write(const char* buf, int count)
    {
        assert(os_);
        os_->write(buf, count);
        return *this;
    }

private:
    ostream* os_;
};
*/
