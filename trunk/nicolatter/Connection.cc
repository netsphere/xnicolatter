﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// クライアントとの接続

#include "../config.h"

#include <cerrno>
#include <cassert>
#include <unistd.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

#include "Connection.h"
#include "client.h"
#include "iiimtrans.h"
#include "ximtrans.h"
#include "iiimdisp.h"
#include "ximdisp.h"
#include "start.h"
#include "XimProto.h"
#include "imext.h"
#include <utf8.h>
#include <EncString.h>
#include <qsdebug.h>


////////////////////////////////////////////////////////////////////////

char* mbstoct(char* ct, const char* euc)
    // EUC-JP -> コンパウンド・テキスト
{
    const char* list[1];
    XTextProperty textprop;

    list[0] = euc;
    textprop.value = NULL;
    XmbTextListToTextProperty(top_display, const_cast<char**>(list), 1,
                                    XCompoundTextStyle, &textprop);
    // TRACE("text_prop.val = %x\n", textprop.value);
    strcpy(ct, (char*) textprop.value);
    XFree(textprop.value); // 2000.01.02 利用者側で解放する
    return ct;
}

////////////////////////////////////////////////////////////////////////
// Connection

Connection::Connection(Connection::TransportType t, int fd)
    : type(t), sock_fd(fd), selfWindow(0), clientWindow(0), rbuf(NULL)
    // XIM TCP transport / IIIMP
{
    assert(type == XIM_TCP_TRANSPORT || type == IIIMP);
#ifdef USE_GTK
    io_tag = gdk_input_add(fd, GDK_INPUT_READ, onTcpReceived, NULL);
#else
    io_tag = XtAppAddInput(app, fd, (void*) XtInputReadMask, onTcpReceived, NULL);
#endif
}

Connection::Connection(Connection::TransportType t, Window self, Window client)
    : type(t), sock_fd(-1), io_tag(0), selfWindow(self), clientWindow(client),
      rbuf(NULL)
    // XIM X transport
{
    assert(type == XIM_X_TRANSPORT || type == UNICODE_INPUT);

    if (type == UNICODE_INPUT) {
        // Unicode InputはInputMethod, InputContextを区別しない
        InputMethod* im = createIM();
        assert(im);
        InputContext* ic = im->createIC();
        assert(ic);
        ic->clientWindow = clientWindow;
        ic->inputStyle = XIMPreeditNothing;
    }
}

Connection::~Connection()
{
    close();
}

#ifdef USE_GTK
// XIM TCP transport / IIIMP
// xwnmo/ximdispt.c:XimRequestDispatch()参照
void Connection::onTcpReceived(void* data, int source, GdkInputCondition )
{
    int sfd = source;
#else
void Connection::onTcpReceived(void* closure, int* source, XtInputId* id)
{
    int sfd = *source;
#endif // USE_GTK
    QsTRACE(1, "onTcpReceived()\n");

    Connection* cli = find_client_tcp(sfd);
    if (!cli) {
        QsTRACE(1, "tcp: fd not found: %d\n", sfd);
        return;
    }

    if (!cli->rbuf) {
        if (cli->getType() == XIM_TCP_TRANSPORT)
            cli->rbuf = new XimReader(cli->bigEndian);
        else
            cli->rbuf = new IIIMPReader(cli->bigEndian);
    }

    if (!cli->rbuf->input(sfd)) {
        QsTRACE(1, "connection closed: %d\n", sfd);
        cli->close();
        clients.erase(cli);
        delete cli->rbuf;
        cli->rbuf = NULL;
        return;
    }
    cli->bigEndian = cli->rbuf->isBigEndian();

    if (cli->rbuf->isFilled()) {
        if (cli->getType() == XIM_TCP_TRANSPORT)
            dispatchXimMessage(cli, dynamic_cast<XimReader*>(cli->rbuf));
        else
            dispatchIIIMPMessage(cli, dynamic_cast<IIIMPReader*>(cli->rbuf));
        cli->sendEvents();

        delete cli->rbuf;
        cli->rbuf = NULL;
    }
}

void Connection::onXInput(const XClientMessageEvent& ev)
    // XIM X transport: キー入力
{
    assert(ev.message_type == XA_XIM_PROTOCOL);
    assert(rbuf == NULL);

    if (ev.format == 8) {
        // only-CM
        rbuf = new XimReader(bigEndian, ev.data.b);
        bigEndian = rbuf->isBigEndian();
    }
    else if (ev.format == 32) {
        // Property-with-CM
        int len = ev.data.l[0];
        Atom prop_atom = ev.data.l[1];
        Atom type_ret = 0;
        int format_ret = 0;
        unsigned long nitems_ret = 0;
        unsigned long bytes_after_ret = 0;
        unsigned char* prop_ret = NULL;
        int r = XGetWindowProperty(
            top_display, ev.window, prop_atom,
            0, (len + 3) / 4, True, XA_STRING,
            &type_ret, &format_ret, &nitems_ret, &bytes_after_ret,
            &prop_ret);
        assert(r == Success);

        rbuf = new XimReader(bigEndian, (char*) prop_ret);
        bigEndian = rbuf->isBigEndian();
        XFree(prop_ret);
    }
    else {
        error("read error: ev.format = %d\n", ev.format);
        return;
    }

    dispatchXimMessage(this, dynamic_cast<XimReader*>(rbuf));
    sendEvents();
    delete rbuf;
    rbuf = NULL;
}

void Connection::close()
{
    if (io_tag) {
#ifdef USE_GTK
        gdk_input_remove(io_tag);
#else
        XtRemoveInput(io_tag);
#endif
        io_tag = 0;
    }
    if (sock_fd != -1) {
        ::close(sock_fd);
        sock_fd = -1;
    }
    if (selfWindow) {
        XDestroyWindow(top_display, selfWindow);
        selfWindow = 0;
    }
    if (rbuf) {
        delete rbuf;
        rbuf = NULL;
    }
}

InputMethod* Connection::createIM()
{
    InputMethod* im = new InputMethod(this);
    methods.push_back(im);
    return im;
}

InputMethod* Connection::getIM(int id) const
{
    InputMethods::const_iterator i;
    for (i = methods.begin(); i != methods.end(); i++) {
        if (id == (*i)->getId())
            return *i;
    }
    QsTRACE(1, "imid = %d: not found\n", id);
    return NULL;
}

void Connection::remove_im(int imid)
{
    InputMethods::iterator i;
    for (i = methods.begin(); i != methods.end(); i++) {
        if ((*i)->getId() == imid) {
            methods.erase(i);
            return;
        }
    }
}

void Connection::appendCommitString(InputContext* ic, const string& s)
    // 入力
    //     s: EUC-JP文字列
{
    QsTRACE(1, "Connection::appendCommitString(): '%s'\n", s.c_str());
    
    if (type == XIM_X_TRANSPORT || type == XIM_TCP_TRANSPORT) {
        char ct[s.length() * 10];
        XimWriter* imstr = new XimWriter(bigEndian, XIM_COMMIT);
        imstr->card16(ic->getIM()->getId());
        imstr->card16(ic->getId());
        imstr->card16(XimLookupChars | XimSYNCHRONUS);
        mbstoct(ct, s.c_str());
        imstr->str16(ct);
        send_queue.push_back(imstr);
    }
    else {
        // Unicode Input
        EncString es = EncString(s.c_str(), s.length(), "EUC-JP");
        es.convert("UTF-8");
        uint32_t ucs[es.length() + 1];
        utf8toucs(ucs, (char*) es.data(), es.length());
        QsTRACE(1, "UCS: ");
        for (const uint32_t* p = ucs; *p; p++) {
            XClientMessageEvent e;
            memset(&e, 0, sizeof(e));
            e.type = ClientMessage;
            e.display = top_display;
            e.window = clientWindow;
            e.message_type = XInternAtom(top_display, "UnicodeInput", False);
            e.format = 32;
            e.data.l[0] = *p;
            QsTRACE(1, "%02x ", e.data.l[0]);
            XSendEvent(top_display, clientWindow, False, NoEventMask,
                       (XEvent*) &e);
        }
        QsTRACE(1, "\n");
    }
}

void Connection::appendThroughEvent(InputContext* ic, const KeyEvent& ev,
                                    bool after_commit)
{
    switch (type)
    {
    case XIM_X_TRANSPORT:
    case XIM_TCP_TRANSPORT:
        {
            XimWriter* imstr = createXimWriter(XIM_FORWARD_EVENT);
            imstr->card16(ic->getIM()->getId());
            imstr->card16(ic->getId());
            imstr->card16(XimSYNCHRONUS);    // flag
            imstr->xkeyevent(*ev.xk);
            if (after_commit) {
                pendings.push_back(imstr);
                // XIM_FORWARDをCOMMITに続けて返送するとなぜかFORWARDが先に入る
                // ので，COMMITに対するクライアントのSYNC_REPLYでFORWARDする。
            }
            else 
                send_queue.push_back(imstr);
        }
        break;
    case IIIMP:
        assert(0);
        break;
    case UNICODE_INPUT:
        if (ev.type == KeyPress) {
            char mb_buf[10];
            KeySym sym;
            int r = XLookupString(ev.xk, mb_buf, sizeof(mb_buf), &sym, NULL);
            for (int i = 0; i < r; i++) {
                XClientMessageEvent e;
                memset(&e, 0, sizeof(e));
                e.type = ClientMessage;
                e.display = top_display;
                e.window = clientWindow;
                e.message_type = XInternAtom(top_display, "UnicodeInput", False);
                e.format = 32;
                e.data.l[0] = mb_buf[i];
                XSendEvent(top_display, clientWindow, False, NoEventMask,
                           (XEvent*) &e);
            }
        }
        break;
    default:
        assert(0);
    }
}


void Connection::sendEvents()
{
    while (send_queue.size()) {
        SendBuffer* s = send_queue.front();
        send_queue.pop_front();

        int len = s->getData(NULL);
        
        if (type == XIM_TCP_TRANSPORT || type == IIIMP) {
            unsigned char* buf = new unsigned char[len];
            s->getData(buf);
            if (write(sock_fd, buf, len) < len) {
                QsTRACE(1, "flush(): write error\n");
                assert(0);
            }
            delete [] buf;
        }
        else {
            XClientMessageEvent ev;
            memset(&ev, 0, sizeof(ev));
            ev.type = ClientMessage;
            ev.display = top_display;
            ev.window = getClientWindow();
            ev.message_type = XA_XIM_PROTOCOL;
            
            if (len <= 20) {
                ev.format = 8;
                s->getData((unsigned char*) ev.data.b);
            }
            else {
                static Atom prop_atom = 0;
                if (prop_atom == 0) {
                    prop_atom = XInternAtom(top_display,
                                            "NICO_PROP", False);
                }
                
                unsigned char* buf = new unsigned char[len];
                s->getData(buf);
                XChangeProperty(top_display, getClientWindow(),
                                prop_atom, XA_STRING, 8, PropModeAppend,
                                buf, len);
                delete [] buf;

                ev.format = 32;
                ev.data.l[0] = len;
                // アトムの長さではなく，書き込むデータの長さ。
                // imTrX.c:_XimXGetReadData()
                ev.data.l[1] = prop_atom;
            }
            XSendEvent(top_display, getClientWindow(), False,
                       NoEventMask, (XEvent*) &ev);
        }
        delete s;
    }
}

Window Connection::getClientWindow() const
{
    assert(type == XIM_X_TRANSPORT || type == UNICODE_INPUT);
    return clientWindow;
}

Connection::TransportType Connection::getType() const
{
    return type;
}

XimWriter* Connection::createXimWriter(int opcode) const
    // XIM
{
    assert(type == XIM_X_TRANSPORT || type == XIM_TCP_TRANSPORT);
    return new XimWriter(bigEndian, opcode);
}

IIIMPWriter* Connection::createIIIMPWriter(int opcode) const
    // IIIMP
{
    assert(type == IIIMP);
    return new IIIMPWriter(bigEndian, opcode);
}

//////////////////////////////////////////////////////////////////////

Connections clients;

Connection* Connection::find_client_tcp(int fd)
    // XIM TCP transport / IIIMP
{
    Connections::const_iterator i;
    for (i = clients.begin(); i != clients.end(); i++) {
        if (((*i)->type == Connection::XIM_TCP_TRANSPORT
                     || (*i)->type == Connection::IIIMP)
                && (*i)->sock_fd == fd)
            return *i;
    }
    return NULL;
}

Connection* find_client_x_self(Window self)
    // XIM X transport
{
    Connections::const_iterator i;
    for (i = clients.begin(); i != clients.end(); i++) {
        if ((*i)->type == Connection::XIM_X_TRANSPORT
                && (*i)->selfWindow == self)
            return *i;
    }
    return NULL;
}

Connection* find_client_x_client(Window cli)
    // XIM X transport / Unicode Input
{
    Connections::const_iterator i;
    for (i = clients.begin(); i != clients.end(); i++) {
        if (((*i)->type == Connection::XIM_X_TRANSPORT
                 || (*i)->type == Connection::UNICODE_INPUT)
                && (*i)->clientWindow == cli)
            return *i;
    }
    return NULL;
}
