// Q's C++ Library
// Copyright (c) 1999-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QLIB_SOCKET__
#define QLIB_SOCKET__

#include <exception>

using namespace std;

class CSocket
{
public:
    int sock_fd;
    int port;

    CSocket();
    virtual ~CSocket();
};

class SystemCallError: public exception
{
    int e;
public:
    SystemCallError(int e_)
    {
        e = e_;
    }
    
    virtual ~SystemCallError() throw()
    {
    }
    
    int getErrno() const
    {
        return e;
    }
};

extern CSocket setupServer(int port) throw(SystemCallError);

#endif // QLIB_SOCKET__
