// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/
// -*- mode: C++ -*-

#include "../config.h"

#include <cstdio>
#include <cassert>
#include <iconv.h>
#ifdef HAVE_LANGINFO_H
#include <langinfo.h>
#endif
#include <X11/keysym.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "uniblock.h"

#include <FontDialog.h>
#include <misc.h>
#include <misc-gtk.h>

///////////////////////////////////////////////////////////////////////

PaletteWindow paletteWindow;
SymbolPage symbolPage;
TablePage tablePage;
UnicodePage uniPage;

///////////////////////////////////////////////////////////////////////
// CharTableBase

const int CharTableBase::TABLE_BORDER = 3;
const int CharTableBase::CHAR_BORDER = 2;
const char* CharTableBase::LABEL_FONT = "-*-fixed-medium-r-normal--14-*-*-*-*-*-*-*";

CharTableBase::CharTableBase()
    : selector(NULL), drawing(NULL), vscrollbar(NULL),
      labelFont(NULL), row_label(NULL), col_label(NULL),
      width(0), height(0), cur_ccs(0)
{
}

void CharTableBase::updateAdjustment()
{
    if (vscrollbar) {
        GtkAdjustment* adj = gtk_range_get_adjustment(
                                      GTK_RANGE(vscrollbar));
        adj->lower = 0;
        adj->upper = ccs[cur_ccs].getSize();
        adj->value = ccs[cur_ccs].start;
        adj->step_increment = width;
        adj->page_increment = width * height;
        adj->page_size = width * height;

        gtk_adjustment_changed(adj);
    }
}

void CharTableBase::on_adj_value_changed(GtkAdjustment* adjustment,
                                         CharTableBase* this_)
{
    this_->ccs[this_->cur_ccs].start = (int) adjustment->value;
    gtk_widget_queue_draw(this_->drawing);
    gtk_widget_queue_draw(this_->col_label);
    gtk_widget_queue_draw(this_->row_label);

    int code = this_->ccs[this_->cur_ccs].start
                        + this_->ccs[this_->cur_ccs].pos;
    paletteWindow.setCode(this_->getText(code));
    this_->updateSelector();
}

void CharTableBase::on_drawing_realize()
{
    gdk_window_set_background(
        drawing->window,
        &drawing->style->base[GTK_WIDGET_STATE(drawing)]);

    updateAdjustment();
    GtkAdjustment* adj = gtk_range_get_adjustment(GTK_RANGE(vscrollbar));

    gtk_signal_connect(GTK_OBJECT(adj), "value_changed",
                       GTK_SIGNAL_FUNC(on_adj_value_changed), this);
}

void CharTableBase::on_col_label_expose_event(GtkWidget* widget)
{
    char buf[10];
    for (int i = 0; i < width; i++) {
        sprintf(buf, "+%d", i);
        gdk_draw_string(
            widget->window,
            labelFont,
            widget->style->black_gc,
            TABLE_BORDER + i * chsiz.width - tablePage.chsiz.x + CHAR_BORDER,
            15,
            buf);
    }
}

void CharTableBase::on_row_label_expose_event(GtkWidget* widget)
{
    gdk_window_clear(widget->window);

    int code;
    char buf[10];
    for (int i = 0; i < height; i++) {
        code = ccs[cur_ccs].start + i * width;
        const CharSet& cs = ccs[cur_ccs];
        if (!cs.isDBCS()) {
            // IRV or 半角カナ
            sprintf(buf, "%02x", code + cs.siz[1].l);
        }
        else {
            // JIS X 0208
            sprintf(buf, "%02x",
                    code / cs.siz[1].getSize() + cs.siz[0].l);
            sprintf(buf + 2, "%02x",
                    code % cs.siz[1].getSize() + cs.siz[1].l);
        }

        gdk_draw_string(
            widget->window, labelFont,
            widget->style->black_gc,
            -chsiz.x,
            TABLE_BORDER + i * chsiz.height - chsiz.y + CHAR_BORDER,
            buf);
    }
}

void CharTableBase::setCursorVisible(bool f)
{
    assert(width != 0);
    int x = (ccs[cur_ccs].pos % width) * chsiz.width + TABLE_BORDER;
    int y = (ccs[cur_ccs].pos / width) * chsiz.height + TABLE_BORDER;
    gdk_draw_rectangle(drawing->window,
            f ? drawing->style->black_gc : drawing->style->white_gc,
            FALSE, x, y, chsiz.width, chsiz.height);
}

void CharTableBase::on_drawing_draw_focus()
{
    assert(drawing);

    if (GTK_WIDGET_HAS_FOCUS(drawing)) {
        gtk_paint_shadow(drawing->style, drawing->window,
            GTK_STATE_NORMAL,
            GTK_SHADOW_IN,
            0, drawing, "palette.drawing",
            0, 0, drawing->allocation.width, drawing->allocation.height);
    }
    else {
        gdk_draw_rectangle(drawing->window, drawing->style->white_gc,
                           FALSE, 1, 1,
                           drawing->allocation.width - 3,
                           drawing->allocation.height - 3);
    }
}

void CharTableBase::on_drawing_size_allocate(GtkAllocation* allocation)
{
    // TRACE("on_drawing_size_allocate\n");
    createFont();
    assert(chsiz.width != 0 && chsiz.height != 0);
    
    width = (allocation->width - TABLE_BORDER * 2)
                                / chsiz.width;
    height = (allocation->height - TABLE_BORDER * 2)
                                / chsiz.height;
    updateAdjustment();

    if (drawing) {
        gtk_widget_queue_draw(drawing);
        gtk_widget_queue_draw(row_label);
        gtk_widget_queue_draw(col_label);
    }
}

void CharTableBase::on_drawing_button_key_sub()
{
    bool to_update = false;

    while (ccs[cur_ccs].pos < 0) {
        to_update = true;
        ccs[cur_ccs].start -= width;
        ccs[cur_ccs].pos += width;

        if (ccs[cur_ccs].start < 0) {
            ccs[cur_ccs].start = ccs[cur_ccs].pos = 0;
            break;
        }
    }

    while (ccs[cur_ccs].pos >= width * height) {
        to_update = true;
        ccs[cur_ccs].start += width;
        ccs[cur_ccs].pos -= width;
    }

    if (to_update) {
        GtkAdjustment* adj = gtk_range_get_adjustment(GTK_RANGE(vscrollbar));
        gtk_adjustment_set_value(adj, ccs[cur_ccs].start);
    }

    int code = ccs[cur_ccs].start + ccs[cur_ccs].pos;
    paletteWindow.setCode(getText(code));
    updateSelector();
    setCursorVisible(true);
}

gboolean CharTableBase::on_drawing_key_press_event(GtkWidget* widget,
                                                   GdkEventKey* event)
{
    setCursorVisible(false);

    switch (event->keyval)
    {
    case XK_Left:
        ccs[cur_ccs].pos--;
        break;
    case XK_Up:
        ccs[cur_ccs].pos -= width;
        break;
    case XK_Right:
        ccs[cur_ccs].pos++;
        break;
    case XK_Down:
        ccs[cur_ccs].pos += width;
        break;
    case XK_Page_Up:
        ccs[cur_ccs].pos -= height * width;
        break;
    case XK_Page_Down:
        ccs[cur_ccs].pos += height * width;
        break;
    case XK_Return:
        on_sel_button_clicked(NULL, NULL);
        setCursorVisible(true);
        goto l1;
    default:
        setCursorVisible(true);
        return FALSE;   // デフォルト処理を続行
    }

    if (ccs[cur_ccs].start + ccs[cur_ccs].pos >= ccs[cur_ccs].getSize())
        ccs[cur_ccs].pos = ccs[cur_ccs].getSize() - ccs[cur_ccs].start - 1;

    on_drawing_button_key_sub();
l1:
    gtk_signal_emit_stop_by_name(GTK_OBJECT(widget), "key_press_event");
    return TRUE;    // デフォルト処理を禁止
}

void CharTableBase::on_drawing_button_press_event(GtkWidget* widget,
                                                  GdkEventButton* event)
{
    int x = (int) (event->x - TABLE_BORDER) / chsiz.width;
    int y = (int) (event->y - TABLE_BORDER) / chsiz.height;
    if (x >= width || y > height)
        return;

    int pos = x + y * width;
    if (ccs[cur_ccs].start + pos >= ccs[cur_ccs].getSize()) {
        return;
    }

    gtk_widget_grab_focus(drawing);

    setCursorVisible(false);
    ccs[cur_ccs].pos = pos;
    on_drawing_button_key_sub();
}

void CharTableBase::on_drawing_focus_out_event(GtkWidget* widget)
{
    GTK_WIDGET_UNSET_FLAGS(widget, GTK_HAS_FOCUS);
    gtk_widget_draw_focus(widget);
}

///////////////////////////////////////////////////////////////////////
// CharGroups

struct CharNamePair
{
    string c;
    string name;
};

struct CharGroup
{
    typedef ptr_vector<CharNamePair*> CharList;
    string name;
    CharList charList;
};

typedef ptr_vector<CharGroup*> CharGroups;
CharGroups charGroups;

bool load_symdef(const string& filename)
{
    FILE* fp = fopen(filename.c_str(), "r");
    if (!fp)
        return false;

    CharGroup* block = 0;
    char buf[1000];
    int line = 0;
    while (fgets(buf, sizeof(buf), fp) != 0) {
        line++;

        const char* p = buf;
        p = skip_space(p);
        if (!*p || *p == '#') {
            continue;   // コメント
        }
        else if (!strncasecmp(p, "block", 5)) {
            p = skip_space(p + 5);
            if (!*p || *p == '#') {
                error("%s(%d): ブロック名がありません\n",
                      filename.c_str(), line);
                delete block; block = 0;
                continue;
            }
            const char* q = p;
            while (*q && *q != '#')
                q++;
            while (q > p && isspace(q[-1]))
                q--;

            block = new CharGroup();
            block->name.assign(p, q - p);
            charGroups.push_back(block);
        }
        else {
            string ch, name;
            while (*p && !isspace(*p)) {
                ch += *p;
                // p += mblen(p, MB_CUR_MAX);
                p++;
            }
            p = skip_space(p);

            const char* q = p;
            while (*q && *q != '#')
                q++;
            while (q > p && isspace(q[-1]))
                q--;
            if (p == q)
                name = "";  // 文字のみ
            else
                name.assign(p, q - p);

            if (block != 0) {
                CharNamePair* pair = new CharNamePair();
                pair->c = ch;
                pair->name = name;
                block->charList.push_back(pair);
            }
        }
    }
    fclose(fp);
    return true;
}

///////////////////////////////////////////////////////////////////////
// SymbolPage

void
on_symbol_page_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    symbolPage.window = widget;
    VERIFY(symbolPage.kind_clist = lookup_widget(widget,
                                                 "symbol_kind_clist"));
    VERIFY(symbolPage.char_clist = lookup_widget(widget,
                                                 "symbol_char_clist"));
}

void
on_symbol_kind_clist_realize           (GtkWidget       *widget,
                                        gpointer         user_data)
{
    CharGroups::const_iterator i;
    for (i = charGroups.begin(); i != charGroups.end(); i++) {
        const char* buf[1];
        buf[0] = (*i)->name.c_str();
        int row = gtk_clist_append(GTK_CLIST(widget), const_cast<char**>(buf));
        gtk_clist_set_row_data(GTK_CLIST(widget), row, *i);
    }
}

void
on_symbol_kind_clist_select_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    gtk_clist_freeze(GTK_CLIST(symbolPage.char_clist));
    gtk_clist_clear(GTK_CLIST(symbolPage.char_clist));

    CharGroup* g = (CharGroup*) gtk_clist_get_row_data(clist, row);
    CharGroup::CharList::const_iterator i;
    for (i = g->charList.begin(); i != g->charList.end(); i++) {
        const char* buf[3];
        char code[5];
        sprintf(code, "%02x", (*i)->c[0] & 0x7f);
        sprintf(code + 2, "%02x", (*i)->c[1] & 0x7f);

        buf[0] = (*i)->c.c_str();
        buf[1] = code;
        buf[2] = (*i)->name.c_str();
        int row = gtk_clist_append(GTK_CLIST(symbolPage.char_clist),
                            const_cast<char**>(buf));
        gtk_clist_set_row_data(GTK_CLIST(symbolPage.char_clist),
                            row, *i);
    }

    gtk_clist_thaw(GTK_CLIST(symbolPage.char_clist));
}

void
on_symbol_char_clist_select_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
    CharNamePair* c = (CharNamePair*) gtk_clist_get_row_data(clist, row);
    gtk_entry_set_text(GTK_ENTRY(paletteWindow.code_entry), "");

    // 記号表は短文（文字列）への拡張を考え，直接テキストに追加する。
    gtk_text_insert(GTK_TEXT(paletteWindow.text), NULL,
                    &paletteWindow.text->style->black, NULL,
                    c->c.c_str(), c->c.length());
/*
    int pos = gtk_editable_get_position(GTK_EDITABLE(paletteWindow.text));
    gtk_editable_insert_text(GTK_EDITABLE(paletteWindow.text),
                             c->c.c_str(), c->c.length(), &pos);
    gtk_editable_set_position(GTK_EDITABLE(paletteWindow.text), pos);
*/
}

///////////////////////////////////////////////////////////////////////
// UnicodePage

static const char* UNICODE_FONT_NAME
           = "-misc-fixed-medium-r-normal-ja-18-*-*-*-*-*-iso10646-1";

UnicodePage::UnicodePage(): font(NULL)
{
}

UnicodePage::~UnicodePage()
{
}

void
on_uni_page_realize                    (GtkWidget       *widget,
                                        gpointer         user_data)
{
    TRACE("on_uni_page_realize()\n");
    UnicodePage* this_ = &uniPage;
    
    VERIFY(this_->selector = lookup_widget(widget, "block_sel"));
    VERIFY(this_->drawing = lookup_widget(widget, "uni_drawing"));
    VERIFY(this_->row_label = lookup_widget(widget, "uni_row"));
    VERIFY(this_->col_label = lookup_widget(widget, "uni_col"));
    VERIFY(this_->vscrollbar = lookup_widget(widget, "uni_vscrollbar"));

    // 文字集合の初期化
    CharSet cs;
    cs.name = "iso10646-1";
    cs.siz[0] = Range(0, 0xff);
    cs.siz[1] = Range(0, 0xff);
    this_->ccs.push_back(cs);
    
    // ブロック・セレクターの初期化
    GtkWidget* menu = gtk_option_menu_get_menu(
                               GTK_OPTION_MENU(this_->selector));
    gtk_container_foreach(GTK_CONTAINER(menu),
                          (GtkCallback) gtk_widget_destroy, NULL);

    for (unsigned int i = 0; uniBlock[i].name; i++) {
        GtkWidget* item = gtk_menu_item_new_with_label(uniBlock[i].name);
        gtk_menu_append(GTK_MENU(menu), item);
    }
    gtk_widget_show_all(menu);

    gtk_signal_connect(
            GTK_OBJECT(menu), "selection-done",
            GTK_SIGNAL_FUNC(UnicodePage::on_selection_done), &uniPage);

    this_->createFont();
}

string UnicodePage::getText(unsigned int code) const
{
    assert(cur_ccs == 0);

    char inbuf[10];
//    inbuf[0] = 0xfe;
//    inbuf[1] = 0xff;
    inbuf[0] = (code >> 8) & 0xff;
    inbuf[1] = code & 0xff;
    inbuf[2] = '\0';
    inbuf[3] = '\0';
#ifdef HAVE_LANGINFO_H
    iconv_t cd = iconv_open(nl_langinfo(CODESET), "UTF-16");
#else
    iconv_t cd = iconv_open(CURRENT_CODESET, "UTF-16");
#endif
    assert(cd != (iconv_t) -1);
    const char* pin = inbuf;
    size_t left = 4;
    char outbuf[20];
    outbuf[0] = '\0';
    char* p = outbuf;
    size_t bufleft = sizeof(outbuf);
#ifdef ICONV_CONST
    size_t r = iconv(cd, &pin, &left, &p, &bufleft);
#else
    size_t r = iconv(cd, const_cast<char**>(&pin), &left, &p, &bufleft);
#endif
    if (r == (size_t) -1) {
        iconv_close(cd);
        return "";
    }
    *p = '\0';
    iconv_close(cd);

    return outbuf;
}

void UnicodePage::on_selection_done(GtkMenuShell* menu_shell,
                                    UnicodePage* this_)
    // GtkMenuShell::selection_done
{
    int idx = getSelectedIndex(GTK_OPTION_MENU(this_->selector));
    if (idx >= 0) {
        this_->ccs[this_->cur_ccs].start = uniBlock[idx].start;
        this_->ccs[this_->cur_ccs].pos = 0;
        if (this_->drawing) {
            gtk_widget_queue_draw(this_->drawing);
            gtk_widget_queue_draw(this_->row_label);
            gtk_widget_queue_draw(this_->col_label);
        }

        this_->updateAdjustment();
        paletteWindow.setCode(this_->getText(uniBlock[idx].start));
    }
}

gboolean
on_uni_row_label_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    uniPage.on_row_label_expose_event(widget);
    return TRUE;
}


gboolean
on_uni_col_label_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    uniPage.on_col_label_expose_event(widget);
    return TRUE;
}

gboolean
on_uni_drawing_expose_event            (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data)
{
    UnicodePage* this_ = &uniPage;

    gdk_window_clear(widget->window);
    this_->on_drawing_draw_focus();

    if (!this_->font)
        return TRUE;
    
    Display* disp = GDK_DISPLAY();
    Window w = GDK_WINDOW_XWINDOW(widget->window);
    GC gc = XCreateGC(disp, w, 0, NULL);
    XSetFont(disp, gc, uniPage.font->fid);

    for (int y = 0; y <= this_->height; y++) {
        for (int x = 0; x < this_->width; x++) {
            int code = this_->ccs[this_->cur_ccs].start
                                  + x + y * this_->width;
            if (code < this_->ccs[this_->cur_ccs].getSize()) {
                XChar2b ch;
                memset(&ch, 0, sizeof(ch));
                ch.byte1 = (code >> 8) & 0xff;
                ch.byte2 = code & 0xff;

                XDrawString16(disp, w, gc,
                              UnicodePage::TABLE_BORDER + x * uniPage.chsiz.width
                                      - uniPage.chsiz.x + UnicodePage::CHAR_BORDER,
                              UnicodePage::TABLE_BORDER + y * uniPage.chsiz.height
                                      - uniPage.chsiz.y + UnicodePage::CHAR_BORDER,
                              &ch, 1);
            }
        }
    }

    XFreeGC(disp, gc);
    uniPage.setCursorVisible(true);
    return TRUE;
}


gboolean
on_uni_drawing_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data)
{
    uniPage.on_drawing_button_press_event(widget, event);
    return TRUE;
}


gboolean
on_uni_drawing_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{
    uniPage.on_drawing_key_press_event(widget, event);
    return TRUE;
}


gboolean
on_uni_drawing_focus_in_event          (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
    GTK_WIDGET_SET_FLAGS(uniPage.drawing, GTK_HAS_FOCUS);
    gtk_widget_draw_focus(uniPage.drawing);
    return TRUE;
}


gboolean
on_uni_drawing_focus_out_event         (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{
    uniPage.on_drawing_focus_out_event(widget);
    return TRUE;
}

void
on_uni_drawing_realize                 (GtkWidget       *widget,
                                        gpointer         user_data)
{
    TRACE("on_uni_drawing_realize()\n");
    uniPage.on_drawing_realize();
}

void UnicodePage::createFont()
{
    if (!font) {
        font = XLoadQueryFont(GDK_DISPLAY(), UNICODE_FONT_NAME);
        labelFont = gdk_fontset_load(LABEL_FONT);
        if (font) {
            chsiz = CRect(0, -font->ascent,
                          font->max_bounds.rbearing,
                          font->ascent + font->descent);
            chsiz.width += CHAR_BORDER * 2;
            chsiz.height += CHAR_BORDER * 2;
            
            TRACE("uni: x = %d, y = %d, w = %d, h = %d\n",
                  chsiz.x, chsiz.y, chsiz.width, chsiz.height);
        }
        else {
            error("cannot load unicode font: '%s'\n", UNICODE_FONT_NAME);
            chsiz = CRect(0, -22, 28, 28);
        }
    }
}

void
on_uni_drawing_size_allocate           (GtkWidget       *widget,
                                        GtkAllocation   *allocation,
                                        gpointer         user_data)
{
    uniPage.on_drawing_size_allocate(allocation);
}

void
on_uni_drawing_draw_focus              (GtkWidget       *widget,
                                        gpointer         user_data)
{
    uniPage.on_drawing_draw_focus();
}

void UnicodePage::updateSelector()
{
    // TRACE("updateSelector\n");
    unsigned int i;
    for (i = 0; uniBlock[i].name; i++) {
        if (uniBlock[i].start > ccs[cur_ccs].start + ccs[cur_ccs].pos)
            break;
    }
    gtk_option_menu_set_history(GTK_OPTION_MENU(selector), i - 1);
}

///////////////////////////////////////////////////////////////////////
// PaletteWindow

PaletteWindow::PaletteWindow()
{
}

PaletteWindow::~PaletteWindow()
{
}
     
void
on_palette_window_realize              (GtkWidget       *widget,
                                        gpointer         user_data)
{
    paletteWindow.code_entry = lookup_widget(widget, "code_entry");
    assert(paletteWindow.code_entry);
    paletteWindow.text = lookup_widget(widget, "adopt_text");
    assert(paletteWindow.text);
}

void
on_font_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
    FontDialog dlg(GTK_WINDOW(paletteWindow.window));
    if (dlg.setVisible(true) == FontDialog::IDOK) {
        // TODO: implement
    }
}


void
on_copy_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
    GtkEditable* e = GTK_EDITABLE(paletteWindow.text);
    int pos = gtk_editable_get_position(e);
    e->selection_start_pos = 0;
    e->selection_end_pos = gtk_text_get_length(GTK_TEXT(e));
    gtk_editable_copy_clipboard(e);
    gtk_editable_set_position(e, pos);
}


void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
    gtk_widget_destroy(paletteWindow.window);
}


void
on_palette_window_destroy              (GtkObject       *object,
                                        gpointer         user_data)
{
    gtk_main_quit();
}


void
on_sel_button_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
    const char* s = gtk_entry_get_text(
                              GTK_ENTRY(paletteWindow.code_entry));
    char buf[3];
    char* p = buf;
    
    while (*s) {
        unsigned int n;
        if (sscanf(s, "%02x", &n) < 1)
            break;
        *p++ = n & 0xff;
        s += 2;
    }
    *p = '\0';

    if (buf[0]) {
        gtk_text_insert(GTK_TEXT(paletteWindow.text), NULL,
                        &paletteWindow.text->style->black, NULL,
                        buf, strlen(buf));
/*
        int pos = gtk_editable_get_position(GTK_EDITABLE(paletteWindow.text));
        gtk_editable_insert_text(GTK_EDITABLE(paletteWindow.text),
                                 buf, strlen(buf), &pos);
        gtk_editable_set_position(GTK_EDITABLE(paletteWindow.text), pos);
*/
    }
}

void PaletteWindow::setCode(const string& s)
{
    char code_str[100];
    for (unsigned int i = 0; i < s.length(); i++)
        sprintf(code_str + i * 2, "%02x", s[i] & 0xff);

    gtk_entry_set_text(GTK_ENTRY(code_entry), code_str);
}

///////////////////////////////////////////////////////////////////////

