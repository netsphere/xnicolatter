// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// ステータスとプロパティボタンの表示

#include "../config.h"

#ifdef USE_GTK
#include <gtk/gtk.h>
#else
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Label.h>
#endif  // USE_GTK

#include "status.h"
#include "client.h"
#include "preedit.h"
#include "start.h"
#include "../global.h"

//////////////////////////////////////////////////////////////////////
// StatusWindow

extern const char* DEFAULT_FONT;

StatusWindow::StatusWindow(InputContext* ic_): label(0), ic(ic_)
{
    create();
}

StatusWindow::~StatusWindow()
{
    destroy();
}

void StatusWindow::create()
{
    size.width = 60;    // TODO: フォントから幅を計算
    size.height = 20;
#ifdef USE_GTK
    window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_POPUP));
    label = gtk_label_new("英数");
    gtk_widget_show(label);
    gtk_container_add(GTK_CONTAINER(window), label);

    GtkStyle* style = gtk_style_copy(gtk_widget_get_style(label));
    style->font = gdk_fontset_load(
        const_cast<char*>(global_prop.status_font.c_str()));
    gtk_widget_set_style(label, style);
#else
    window = XtCreatePopupShell("status-shell",
                    overrideShellWidgetClass, top_widget, NULL, 0);
    label = XtVaCreateManagedWidget("canvas", labelWidgetClass, window,
        XtNinternational, True,
        XtVaTypedArg, XtNfontSet, XtRString,
            global_prop.status_font.c_str(),
            global_prop.status_font.length() + 1,
        XtNwidth, size.width,
        NULL);
#endif
}

StatusWindow& StatusWindow::operator = (const StatusWindow& a)
{
    area             = a.area;
    areaNeeded          = a.areaNeeded;
    colormap            = a.colormap;
    stdColormap         = a.stdColormap;
    foreground          = a.foreground;
    background          = a.background;
    backgroundPixmap    = a.backgroundPixmap;
    fontName            = a.fontName;
    lineSpace           = a.lineSpace;
    cursor              = a.cursor;
    size = a.size;
    return *this;
}

bool StatusWindow::isStyleArea() const
{
    return (ic->inputStyle & XIMStatusArea) != 0
                && (area.x || area.y || area.width || area.height);
}

void StatusWindow::adjustLocation()
{
    XPoint loc;
    Window root = XRootWindow(top_display, 0);

/*
    1999.07.20
        Netscape NavigatorはXIMStatusAreaスタイルを指定するにも関わらず，
        XNAreaを設定しない。そこで，そのような場合はrootスタイルと同様に扱う
*/

    if (isStyleArea()) {
        // off-the-spot
        // areaは，クライアントウィンドウからの相対位置
        Window c;
        int x, y;
        ::XTranslateCoordinates(top_display, ic->clientWindow, root,
                                                0, 0, &x, &y, &c);
        loc.x = x + area.x;
        loc.y = y + area.y;
        // TRACE("status-area: x = %d, y = %d\n", loc.x, loc.y);
    }
    else {
        // root style
        Window clientTop = getToplevelWindow(top_display, ic->clientWindow);

        Window c;
        int x, y;
        ::XTranslateCoordinates(top_display, clientTop, root,
                                                0, 0, &x, &y, &c);
        int geo_x, geo_y;
        unsigned int w, h, b, d;
        XGetGeometry(top_display, clientTop, &root,
                                &geo_x, &geo_y, &w, &h, &b, &d);
        loc.x = x;
        loc.y = y + h;

        // preeditがroot-windowスタイルだとpreeditと表示が重なる
        if (ic->preeditWindow->isStyleRoot()
            && (ic->focusWindow ? ic->focusWindow : ic->clientWindow)
            == clientTop) {
            loc.y += ic->preeditWindow->size.height;
        }

        // 画面外に出たときは画面右下に表示。
        // 実際にウィンドウが大きいときのほか，クライアントウィンドウの大きさ
        // の値がおかしいとき
        int root_x, root_y;
        unsigned int root_w, root_h, root_b, root_d;
        XGetGeometry(top_display, root, &root,
                    &root_x, &root_y, &root_w, &root_h, &root_b, &root_d);
        if (loc.x > root_w - size.width)
            loc.x = root_w - size.width;
        
        if (loc.y > root_h - size.height) {
            // 下に食み出たときは，画面右下
            loc.x = root_w - size.width;
            loc.y = root_h - size.height;
        }
    }

    // TRACE("status: x = %d, y = %d\n", loc.x, loc.y);
    setLocation(CPoint(loc.x, loc.y));
}
