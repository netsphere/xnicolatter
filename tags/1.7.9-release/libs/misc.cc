// Q's C++ Library
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <cstdio>
#include <cerrno>
#include <cstdarg>
#include <cassert>
#include <sys/stat.h>   // mkdir()
#include <pwd.h>        // getpwuid()
#include <unistd.h>     // geteuid()
#include <strings.h>    // strcasecmp()
#include <X11/Xlib.h>

#include "misc.h"
using namespace std;

//////////////////////////////////////////////////////////////////////
// TRACE

#ifdef DEBUG
static bool trace_use_file = false;
static string trace_filename;

void q_trace(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    // g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, format, args);
            // g_log()は末尾に改行が入る
    if (!trace_use_file)
        vprintf(format, args);
    else {
        FILE* fp = fopen(trace_filename.c_str(), "a");
        if (fp) {
            vfprintf(fp, format, args);
            fclose(fp);
        }
    }
    va_end(args);
}

void q_trace_set_file(const char* fname)
{
    assert(fname && *fname);
    trace_filename = fname;
    trace_use_file = true;
}
#endif

//////////////////////////////////////////////////////////////////////
// X11用

int getColor(Display* disp, const char* name)
{
    assert(disp != 0 && name != 0);
    Colormap cmap;
    XColor c0, c1;
    cmap = DefaultColormap(disp, 0);
    XAllocNamedColor(disp, cmap, name, &c1, &c0);
    return c1.pixel;
}

XFontSet createFont(
    Display* disp,
    const char* name,
    const char* fallback_font)
{
    XFontSet fs = 0;
    char** miss = NULL;
    int miss_count;
    char* def_string;

    fs = XCreateFontSet(disp, name, &miss, &miss_count, &def_string);
    if (!fs) {
        error("error: cannot create fontset: '%s'\n", name);
        XFreeStringList(miss);
        miss = NULL;
        fs = XCreateFontSet(disp, fallback_font, &miss, &miss_count, &def_string);
        if (!fs) {
            error("error: cannot create fontset with fallback-font-name.\n");
            XFreeStringList(miss);
            return 0;
        }
    }

    if (miss_count > 0) {
        error("warning: cannot load font: ");
        for (int i = 0; i < miss_count; i++)
            error("%s ", miss[i]);
        error("\n");
        XFreeStringList(miss);

        TRACE("fs locale = '%s'\n", XLocaleOfFontSet(fs));
        TRACE("def_string = '%s'\n", def_string);

        if (def_string[0] == '\0') {
            miss = NULL;
            fs = XCreateFontSet(disp, fallback_font, &miss, &miss_count, &def_string);
            XFreeStringList(miss);
        }
    }

    return fs;
}

bool isToplevelWindow(Display* disp, Window w)
{
    assert(disp);
    assert(w);
    
    Window root = 0;
    Window parent = 0;
    Window* children = NULL;
    unsigned int count;
    XQueryTree(disp, w, &root, &parent, &children, &count);
    XFree(children);
    return root == parent;
}

Window getToplevelWindow(Display* disp, Window w)
{
    assert(disp);
    assert(w);

    Window root, parent;
    Window* children;
    unsigned int count;
    while (true) {
        root = parent = 0;
        children = NULL;
        XQueryTree(disp, w, &root, &parent, &children, &count);
        XFree(children);
        if (root == parent)
            break;
        w = parent;
    }
    return w;
}

//////////////////////////////////////////////////////////////////////

string getHomeDir()
{
/*
    // あまり環境変数は使わない
    const char* home = getenv("HOME");
    if (home)
        return home;
*/
    struct passwd* p = getpwuid(geteuid());
    if(p)
        return p->pw_dir;
    else
        return "";
}

void error(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    // g_logv(G_LOG_DOMAIN, G_LOG_LEVEL_MESSAGE, format, args);
            // g_log()は末尾に改行が入る
    vfprintf(stderr, format, args);
    va_end(args);
}

int char_length(const char* b, const char* e)
    // [b, e)の文字数を返す
{
    int count = 0;
    while (*b && b < e) {
        b += mblen(b, MB_CUR_MAX);
        count++;
    }
    return count;
}

int byte_length(const char* s, int clen)
{
    const char* p = s;
    while (*p && --clen >= 0)
        p += mblen(p, MB_CUR_MAX);
    return p - s;
}

bool createFolder(const char* path)
    // 機能
    //      フォルダを（階層が深くても）作成する。
    // 入力
    //      pathの末尾に'\'を付けても付けなくても良い
    // 戻り値
    //      TRUE: フォルダを作成したか既に存在する。
    //      FALSE: 失敗
{
    assert(path);

    string cur;
    const char* p = path;
    if (path[0] == '/' || path[1] == '\\')
        p++;

    while (*p) {
        cur += '/';
        while (*p && *p != '/' && *p != '\\') {
#ifdef WIN32
            if (::IsDBCSLeadByte(*p))
                cur += *p++;
#endif
            cur += *p++;
        }

        struct stat st;
        if (stat(cur.c_str(), &st) < 0) {
            if (errno == ENOENT) {
                if (mkdir(cur.c_str(), 0755) != 0 && errno != EEXIST) {
                    perror("mkdir");
                    return false;
                }
            }
            else {
                perror("stat");
                return false;
            }
        }

        if (*p)
            p++;
    }
    return true;
}

bool isYesString(const char* s)
    // "yes" or "true"のときtrue
{
    return !strcasecmp(s, "yes") || !strcasecmp(s, "true");
}

//////////////////////////////////////////////////////////////////////
// CPoint

CPoint::CPoint()
{
    x = 0; y = 0;
}

CPoint::CPoint(int x_, int y_)
{
    x = x_; y = y_;
}

CPoint::CPoint(const XPoint& xp)
{
    x = xp.x; y = xp.y;
}

CPoint::~CPoint()
{
}

CPoint CPoint::operator + (const XPoint& a) const
{
    return CPoint(x + a.x, y + a.y);
}

CPoint CPoint::operator - (const XPoint& a) const
{
    return CPoint(x - a.x, y - a.y);
}

CPoint& CPoint::operator = (const XPoint& a)
{
    x = a.x; y = a.y;
    return *this;
}

//////////////////////////////////////////////////////////////////////
// CRect

CRect::CRect()
{
    x = y = width = height = 0;
}

CRect::CRect(int x_, int y_, int w, int h)
{
    x = x_; y = y_; width = w; height = h;
}

CRect::CRect(const XRectangle& a)
{
    x = a.x; y = a.y; width = a.width; height = a.height;
}

CRect::~CRect()
{
}

CRect& CRect::operator = (const XRectangle& a)
{
    x = a.x; y = a.y; width = a.width; height = a.height;
    return *this;
}

///////////////////////////////////////////////////////////////////////
// CFont

CFont::FontMap CFont::fm;

CFont::CFont(): disp(0)
{
};

CFont::~CFont()
{
    destroy();
}

bool CFont::create(Display* d, const char* name)
{
    assert(d);
    assert(name && name[0]);

    if (cur_name != "")
        destroy();
    
    disp = d;
    ref* p = fm[name];
    if (p) {
        p->count++;
        cur_name = name;
        return true;
    }
    
    p = new ref();
    p->font = createFont(disp, name);
    if (!p->font) {
        delete p;
        return false;
    }
    p->count = 1;
    fm[name] = p;
    cur_name = name;
    return true;
}

void CFont::destroy()
{
    if (!disp || cur_name == "")
        return;

    ref* p = fm[cur_name];
    if (p) {
        p->count--;
        if (p->count <= 0) {
            XFreeFontSet(disp, p->font);
            fm.erase(cur_name);
            delete p;
        }
    }
    cur_name = "";
}

XFontSet CFont::xfont() const
{
    if (cur_name == "")
        return 0;
    ref* p = fm[cur_name];
    return p ? p->font : 0;
}
