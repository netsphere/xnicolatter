// Q's Nicolatter for X
// Copyright (c) 1998-2003 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"

#include <X11/keysym.h>
#ifdef USE_GTK
#include <gtk/gtk.h>
#else
#include <X11/Intrinsic.h>
#endif  // USE_GTK

#include "syncshift.h"
#include "conv.h"
#include "client.h"
#include "../global.h"

//////////////////////////////////////////////////////////////////////

#ifndef USE_GTK
extern XtAppContext app;
#endif

//////////////////////////////////////////////////////////////////////
// SyncSelector

enum key_type_t {
    char_key = 1,
    shift_key,
    undef_key
};

int shift_code[2];   // 0 = 左，1 = 右
const int SAME_TIME = 150;

SyncSelector::SyncSelector(): state(1), timeout_id(0)
{
    memset(&gr, 0, sizeof(gr));
    memset(&sr, 0, sizeof(sr));
}

SyncSelector::~SyncSelector()
{
}

void SyncSelector::setShiftKey(int left, int right)
{
    shift_code[0] = left;
    shift_code[1] = right;
}

static key_type_t getKeyType(const KeyEvent& key)
{
    if ((key.modifier & ControlMask) != 0) {
        // TRACE("[ctrl]- \n");
        return undef_key;
    }
    else if (key.keycode == shift_code[0] || key.keycode == shift_code[1])
        return shift_key;
    else
        return char_key;
}

static int sk(const KeyEvent& key)
{
    return key.keycode == shift_code[0] ? 1 : 2;
}

InputContext* ic = NULL;

#ifdef USE_GTK
gint SyncSelector::onTimeout(void* data)
{
    SyncSelector* this_ = static_cast<SyncSelector*>(data);
    gtk_timeout_remove(this_->timeout_id);
        // Xtでは自動的に削除される
#else
void SyncSelector::onTimeout(void* closure, XtIntervalId* id)
{
    SyncSelector* this_ = static_cast<SyncSelector*>(closure);
#endif
    this_->timeout_id = 0;

    // TRACE("onTimeout()\n");
    switch (this_->state)
    {
    case 1:
        break;
    case 2:
        ic->input(this_->gr, 0);
        break;
    case 3:
        ic->input(this_->sr, 0);
        break;
    case 4:
        ic->input(this_->gr, sk(this_->sr));
        break;
    case 5:
        ic->input(this_->gr, sk(this_->sr));
        break;
    default:
        assert(0);
    }
    this_->state = 1;

    // 1999.05.07 XIM_SYNCを送っても，次のキー入力があるまでクライアン
    // トは反応しない。
/*
    // ClientMessageでは反応がなかった。
    Display* disp = GDK_DISPLAY();
    XClientMessageEvent ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = ClientMessage;
    ev.display = disp;
    ev.window = ic->focusWindow;
    ev.message_type = XInternAtom(disp, "nicolatter", False);
    ev.format = 8;
    XSendEvent(GDK_DISPLAY(), ic->focusWindow, False, NoEventMask, (XEvent*) &ev);
*/
    // クライアントウィンドウにKeyPressを送り，コールバックしてもらう
    XKeyEvent ev;
    memset(&ev, 0, sizeof(ev));
    ev.type = KeyPress;
    ev.display = top_display;
    ev.window = ic->focusWindow;
    XSendEvent(top_display, ic->focusWindow, False, KeyPressMask, (XEvent*) &ev);
#ifdef USE_GTK
    return TRUE;
#endif
}

void SyncSelector::input(InputContext* ic, const KeyEvent& event)
{
#ifdef USE_GTK
    if (timeout_id) {
        gtk_timeout_remove(timeout_id);
        timeout_id = 0;
    }
#else
    if (timeout_id) {
        XtRemoveTimeOut(timeout_id);
        timeout_id = 0;
    }
#endif  // USE_GTK

    ::ic = ic;
    KeySym sym = XKeycodeToKeysym(top_display, event.keycode, 0);
    if (sym == XK_Control_L || sym == XK_Control_R
            || sym == XK_Alt_L || sym == XK_Alt_R
            || sym == XK_Shift_L || sym == XK_Shift_R) {
        return;
    }

    key_type_t type = getKeyType(event);

    int action;
    if (event.type == KeyPress)
        action = (type - 1) * 2;
    else {
        if (event.keycode == gr.keycode)
            action = 1; // 図形キーOFF
        else if (event.keycode == sr.keycode)
            action = 3; // シフトキーOFF
        else
            return; // gr, srに入っていないキーのOFFか未定義キー
    }

// A1 = 図形キーON, A2 = 図形キーOFF, A3 = 親指シフトキーON, A4 = 親指シフトキーOFF
// A5 = 未定義キーON, A6 = 未定義キーOFF
// A7 = [Shift] ON, A8 = [Shift] OFF（未実装）

    switch ((state - 1) * 6 + action)
    {
    case 0: /* S1/A1 */
        // TRACE("s1/a1 ");
        gr = event;   state = 2;  break;
    case 1: /* S1/A2 */
        // TRACE("s1/a2 ");
        break;
    case 2: /* S1/A3 */
        // TRACE("s1/a3 ");
        sr = event;   state = 3;  break;
    case 3: /* S1/A4 */
        // TRACE("s1/a4 ");
        break;
    case 4: /* S1/A5 */
        // TRACE("s1/a5 ");
        ic->input(event, 0); // スルーすると図形キーを追い抜いてしまう。
        break;
    // case 5:  /* S1/A6 */ break;
    case 6: /* S2/A1 */
        // TRACE("s2/a1 ");    // 図形キーのキーリピート
        ic->input(gr, 0);
        gr = event;   break;
    case 7: /* S2/A2 */
        // TRACE("s2/a2 ");    // 図形キー単独打鍵（押す−＞離す）
        ic->input(gr, 0);
        state = 1;  break;
    case 8: // {S2/A3}
        // TRACE("s2/a3 ");
        if (event.time - gr.time <= SAME_TIME) {
            sr = event; state = 4;    // 3キー判定へ
        }
        else {
            ic->input(gr, 0);
            sr = event; state = 3;
        }
        break;
    case  9: /* S2/A4 */
        // TRACE("s2/a4 ");
        break;
    case 10: /* S2/A5 */
        // TRACE("s2/a5 ");
        ic->input(gr, 0);
        ic->input(event, 0);
        state = 1; break;
    // case 11: /* S2/A6 */ break;
    case 12: /* S3/A1 */
        // TRACE("s3/a1 ");
        if (event.time - sr.time <= SAME_TIME) {
            gr = event;           state = 5;  // 3キー判定へ
        }
        else {
            ic->input(sr, 0);
            gr = event;   state = 2;
        }
        break;
    case 13: /* S3/A2 */
        // TRACE("s3/a2 ");
        break;
    case 14: /* S3/A3 */
        // TRACE("s3/a3 ");    // シフトキーのキーリピート
        ic->input(sr, 0);
        sr = event; break;
        // TODO: 両親指ポン
    case 15: /* S3/A4 */
        // TRACE("s3/a4 ");        // シフトキーの単独打鍵
        ic->input(sr, 0);
        state = 1; break;
    case 16: /* S3/A5 */
        // TRACE("s3/a5 ");
        ic->input(sr, 0);
        ic->input(event, 0);
        state = 1; break;
    // case 17: /* S3/A6 */ break;
    case 18: /* S4/A1 */
        // TRACE("s4/a1 ");
        if (event.time - sr.time < sr.time - gr.time) {
            // 後ろの図形キーと同時
            ic->input(gr, 0);
            gr = event;       state = 5;  // 逐次との判定が必要
        }
        else {
            // 前の図形キーと同時
            ic->input(gr, sk(sr));
            gr = event;       state = 2;
        }
        break;
    case 19:    // {S4/A2}
        // TRACE("s4/a2 ");
        // m↓→s↓→m↑
        if (sr.time - gr.time > (event.time - sr.time) * 4) {
            // 単独打鍵が重なった
            ic->input(gr, 0);
            state = 3;
        }
        else if (sr.time - gr.time <= event.time - sr.time) {
            // 前の文字キーとの同時打鍵が確実になった
            ic->input(gr, sk(sr));
            state = 1;
        }
        // 別の図形キーとの3キー判定まで待つ
        break;
    case 20: /* S4/A3 */
        // TRACE("s4/a3 ");
        ic->input(gr, sk(sr));
        sr = event;   state = 3;  break;  // m↓→s↓→s↓
    case 21: /* S4/A4 */
        // TRACE("s4/a4 ");
        ic->input(gr, sk(sr));
        state = 1;  break;
    case 22: /* S4/A5 */
        // TRACE("s4/a5 ");
        ic->input(gr, sk(sr));
        ic->input(event, 0);
        state = 1; break;
    // case 23: /* S4/A6 */ break;
    case 24: /* S5/A1 */
        // TRACE("s5/a1 ");    // s↓→m↓→m↓
        ic->input(gr, sk(sr));
        gr = event;   state = 2; break;
    case 25: /* S5/A2 */
        // TRACE("s5/a2 ");
        ic->input(gr, sk(sr));
        state = 1; break;   // s↓→m↓→m↑
    case 26: /* S5/A3 */
        // TRACE("s5/a3 ");
        // s↓→m↓→s↓
        if (event.time - gr.time < gr.time - sr.time) {
            // 後ろのシフトキーと同時
            ic->input(sr, 0);
            sr = event;       state = 4;
        }
        else {
            // 前のキーと同時
            ic->input(gr, sk(sr));
            sr = event;       state = 3;
        }
        break;
    case 27:    // {S5/A4}
        // TRACE("s5/a4 ");
        // s↓→m↓→s↑
        if (gr.time - sr.time > (event.time - gr.time) * 4) {
            // 単独打鍵が重なった
            ic->input(sr, 0);
            state = 2;
        }
        else if (gr.time - sr.time <= event.time - gr.time) {
            ic->input(gr, sk(sr));
            state = 1;
        }
        break;
    case 28: /* S5/A5 */
        // TRACE("s5/a5 ");
        ic->input(gr, sk(sr));
        ic->input(event, 0);
        state = 1; break;
    // case 29: /* S5/A6 */ break;
    default:
        assert(0);
    }

    // タイムアウトの設定
#ifdef USE_GTK
    switch (state)
    {
    case 1:
        break;  // 必要なし
    case 2:
    case 3:
        timeout_id = gtk_timeout_add(SAME_TIME, onTimeout, this);
        break;
    case 4:
        timeout_id = gtk_timeout_add(sr.time - gr.time, onTimeout, this);
            // 3キー判定を考慮すると，前2キーの時間差だけ待てばよい
        break;
    case 5:
        timeout_id = gtk_timeout_add(gr.time - sr.time, onTimeout, this);
        break;
    default:
        assert(0);
    }
#else
    switch (state)
    {
    case 1:
        break;  // 必要なし
    case 2:
    case 3:
        timeout_id = XtAppAddTimeOut(app, SAME_TIME, onTimeout, this);
        break;
    case 4:
        timeout_id = XtAppAddTimeOut(app, sr.time - gr.time, onTimeout, this);
            // 3キー判定を考慮すると，前2キーの時間差だけ待てばよい
        break;
    case 5:
        timeout_id = XtAppAddTimeOut(app, gr.time - sr.time, onTimeout, this);
        break;
    default:
        assert(0);
    }
#endif  // USE_GTK
}

string SyncSelector::getStatus() const
{
    return "";
}
