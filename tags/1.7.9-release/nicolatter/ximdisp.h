// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// XIMプロトコル；トランスポート層は含まない

#ifndef NICOL_XIMDISP_H
#define NICOL_XIMDISP_H

#include "ximtrans.h"

typedef ptr_list<SendBuffer*> Pendings;
extern Pendings pendings;

class Connection;
extern void dispatchXimMessage(Connection* cli, XimReader* input);

#endif // XIM_CONN_H
