// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

// 漢字検索

#include <string>
#include <gtk/gtk.h>

#include <misc.h>

using namespace std;

///////////////////////////////////////////////////////////////////////
// CharGroups

extern bool load_symdef(const string& filename);

struct Range
{
    int l;
    int h;

    Range(int l_ = 0, int h_ = 0): l(l_), h(h_) { }
    
    int getSize() const {
        return h - l + 1;
    }
};

///////////////////////////////////////////////////////////////////////
// SymbolPage

class SymbolPage
    // 記号表
{
public:
    GtkWidget* window;
    GtkWidget* kind_clist;
    GtkWidget* char_clist;
};

void
on_symbol_page_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_symbol_kind_clist_realize           (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_symbol_kind_clist_select_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);


void
on_symbol_char_clist_select_row        (GtkCList        *clist,
                                        gint             row,
                                        gint             column,
                                        GdkEvent        *event,
                                        gpointer         user_data);

///////////////////////////////////////////////////////////////////////
// CharTableBase

struct CharSet
{
    string name;

    int start;  // 画面左上のコード 0〜(94 * 94 - 1)
    int pos;    // 画面左上からの位置
    Range siz[2]; // 文字集合の大きさ

    CharSet(): start(0), pos(0) { }
    virtual ~CharSet() { }

    bool isDBCS() const {
        return siz[0].l || siz[0].h;
    }

    int getSize() const {
        if (!isDBCS())
            return siz[1].getSize();
        else
            return siz[0].getSize() * siz[1].getSize();
    }
};

class CharTableBase
{
public:
    GtkWidget* selector;
    GtkWidget* drawing;
    GtkWidget* vscrollbar;
    GdkFont* labelFont;
    GtkWidget* row_label;
    GtkWidget* col_label;

    int width;  // 1行に表示できる文字数
    int height; // 1画面に表示できる行数
    CRect chsiz;   // 1文字のドット数

    int cur_ccs;
    vector<CharSet> ccs;
    
    static const int TABLE_BORDER;
    static const int CHAR_BORDER;
    static const char* LABEL_FONT;

    CharTableBase();
    virtual ~CharTableBase() { }

    void updateAdjustment();
    void setCursorVisible(bool );
    virtual string getText(unsigned int ) const = 0;
    virtual void updateSelector() = 0;
    virtual void createFont() = 0;

    void on_drawing_size_allocate(GtkAllocation* allocation);
    void on_col_label_expose_event(GtkWidget* widget);
    void on_row_label_expose_event(GtkWidget* widget);
    void on_drawing_draw_focus();
    void on_drawing_focus_out_event(GtkWidget* widget);
    void on_drawing_button_press_event(GtkWidget* widget,
                                       GdkEventButton* event);
    gboolean on_drawing_key_press_event(GtkWidget* widget,
                                        GdkEventKey* event);
    void on_drawing_realize();
private:
    void on_drawing_button_key_sub();
    static void on_adj_value_changed(GtkAdjustment* adjustment,
                                     CharTableBase* );
};

///////////////////////////////////////////////////////////////////////
// TablePage

class TablePage: public CharTableBase
    // 文字コード表
{
public:
    GdkFont* font;

    TablePage();
    virtual ~TablePage();

    virtual void createFont();
    virtual string getText(unsigned int code) const;
    virtual void updateSelector();
    void updateCharsets();
    static void on_selection_done(GtkMenuShell* menu_shell, TablePage* );
};

extern TablePage tablePage;

void
on_table_page_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_table_drawing_size_allocate         (GtkWidget       *widget,
                                        GtkAllocation   *allocation,
                                        gpointer         user_data);


void
on_table_drawing_draw_focus            (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_table_drawing_realize               (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_table_col_label_expose_event        (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_table_row_label_expose_event        (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_table_drawing_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);


gboolean
on_table_drawing_button_press_event    (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_table_drawing_key_press_event       (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_table_drawing_focus_in_event        (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_table_drawing_focus_out_event       (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

///////////////////////////////////////////////////////////////////////
// UnicodePage

class UnicodePage: public CharTableBase
{
public:
    XFontStruct* font;

    UnicodePage();
    virtual ~UnicodePage();

    virtual void createFont();
    virtual string getText(unsigned int code) const;
    virtual void updateSelector();
    static void on_selection_done(GtkMenuShell* menu_shell, UnicodePage* );
};

void
on_uni_page_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_uni_row_label_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_uni_col_label_expose_event          (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_uni_drawing_expose_event            (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_uni_drawing_button_press_event      (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_uni_drawing_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

gboolean
on_uni_drawing_focus_in_event          (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

gboolean
on_uni_drawing_focus_out_event         (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data);

void
on_uni_drawing_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_uni_drawing_size_allocate           (GtkWidget       *widget,
                                        GtkAllocation   *allocation,
                                        gpointer         user_data);

void
on_uni_drawing_draw_focus              (GtkWidget       *widget,
                                        gpointer         user_data);

///////////////////////////////////////////////////////////////////////
// PaletteWindow

class PaletteWindow
{
public:
    GtkWidget* window;
    GtkWidget* code_entry;
    GtkWidget* text;

    PaletteWindow();
    virtual ~PaletteWindow();

    void setCode(const string& s);
};

extern PaletteWindow paletteWindow;

void
on_palette_window_realize              (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_font_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_copy_button_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_cancel_button_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_palette_window_destroy              (GtkObject       *object,
                                        gpointer         user_data);

void
on_sel_button_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

///////////////////////////////////////////////////////////////////////


void
on_notebook_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);
