/*
 * 編集禁止! - このファイルはGladeによって生成されています.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

GtkWidget*
create_nicoconf (void)
{
  GtkWidget *nicoconf;
  GtkWidget *vbox3;
  GtkWidget *notebook1;
  GtkWidget *vbox1;
  GtkWidget *hbox2;
  GtkWidget *label2;
  GSList *conv_server_group = NULL;
  GtkWidget *wnn;
  GtkWidget *canna;
  GtkWidget *frame1;
  GtkWidget *table1;
  GtkWidget *hbox3;
  GtkWidget *kana_ctrl;
  GtkWidget *kana_shift;
  GtkWidget *kana_entry;
  GtkWidget *kana_choose;
  GtkWidget *hbox5;
  GSList *shift_method_group = NULL;
  GtkWidget *normal;
  GtkWidget *prefix;
  GtkWidget *sync;
  GtkWidget *hbox6;
  GtkWidget *label7;
  GtkWidget *left_entry;
  GtkWidget *left_choose;
  GtkWidget *label8;
  GtkWidget *right_entry;
  GtkWidget *right_choose;
  GtkWidget *label6;
  GtkWidget *label4;
  GtkWidget *label3;
  GtkWidget *label5;
  GtkWidget *hbox4;
  GtkWidget *map_entry;
  GtkWidget *map_choose;
  GtkWidget *use_map;
  GtkWidget *frame5;
  GtkWidget *table4;
  GtkWidget *label18;
  GtkWidget *hbox8;
  GtkWidget *alnum_map_entry;
  GtkWidget *alnum_map_choose;
  GtkWidget *use_alnum_map;
  GtkWidget *label16;
  GtkWidget *vbox2;
  GtkWidget *use_roma;
  GtkWidget *hbox1;
  GtkWidget *label1;
  GtkWidget *roma_entry;
  GtkWidget *roma_choose;
  GtkWidget *roma_strict;
  GtkWidget *label15;
  GtkWidget *vbox4;
  GtkWidget *frame3;
  GtkWidget *table2;
  GtkWidget *preedit_font;
  GtkWidget *status_font;
  GtkWidget *preedit_sel;
  GtkWidget *status_sel;
  GtkWidget *label9;
  GtkWidget *label10;
  GtkWidget *frame4;
  GtkWidget *table3;
  GtkWidget *label12;
  GtkWidget *cnv_fg_sel;
  GtkWidget *cnv_bg_sel;
  GtkWidget *label11;
  GtkWidget *inp_fg_sel;
  GtkWidget *inp_bg_sel;
  GtkWidget *inp_bg_col;
  GtkWidget *cnv_bg_col;
  GtkWidget *inp_fg_col;
  GtkWidget *cnv_fg_col;
  GtkWidget *label13;
  GtkWidget *label14;
  GtkWidget *label17;
  GtkWidget *hbox7;
  GtkWidget *ok_button;
  GtkWidget *cancel_button;

  nicoconf = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_object_set_data (GTK_OBJECT (nicoconf), "nicoconf", nicoconf);
  gtk_container_set_border_width (GTK_CONTAINER (nicoconf), 5);
  gtk_window_set_title (GTK_WINDOW (nicoconf), _("Q's Nicolatter property"));
  gtk_window_set_policy (GTK_WINDOW (nicoconf), FALSE, TRUE, TRUE);

  vbox3 = gtk_vbox_new (FALSE, 10);
  gtk_widget_ref (vbox3);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "vbox3", vbox3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox3);
  gtk_container_add (GTK_CONTAINER (nicoconf), vbox3);

  notebook1 = gtk_notebook_new ();
  gtk_widget_ref (notebook1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "notebook1", notebook1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (notebook1);
  gtk_box_pack_start (GTK_BOX (vbox3), notebook1, TRUE, TRUE, 0);
  gtk_notebook_set_tab_hborder (GTK_NOTEBOOK (notebook1), 10);

  vbox1 = gtk_vbox_new (FALSE, 10);
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox1);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 10);

  hbox2 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox2);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox2", hbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox2);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox2, FALSE, TRUE, 0);

  label2 = gtk_label_new (_("変換サーバー："));
  gtk_widget_ref (label2);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label2", label2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label2);
  gtk_box_pack_start (GTK_BOX (hbox2), label2, FALSE, FALSE, 0);

  wnn = gtk_radio_button_new_with_label (conv_server_group, _("Wnn"));
  conv_server_group = gtk_radio_button_group (GTK_RADIO_BUTTON (wnn));
  gtk_widget_ref (wnn);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "wnn", wnn,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (wnn);
  gtk_box_pack_start (GTK_BOX (hbox2), wnn, FALSE, FALSE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wnn), TRUE);

  canna = gtk_radio_button_new_with_label (conv_server_group, _("Canna"));
  conv_server_group = gtk_radio_button_group (GTK_RADIO_BUTTON (canna));
  gtk_widget_ref (canna);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "canna", canna,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (canna);
  gtk_box_pack_start (GTK_BOX (hbox2), canna, FALSE, FALSE, 0);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (canna), TRUE);

  frame1 = gtk_frame_new (_("キー配列（日本語）"));
  gtk_widget_ref (frame1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "frame1", frame1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame1);
  gtk_box_pack_start (GTK_BOX (vbox1), frame1, TRUE, TRUE, 0);

  table1 = gtk_table_new (5, 2, FALSE);
  gtk_widget_ref (table1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "table1", table1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table1);
  gtk_container_add (GTK_CONTAINER (frame1), table1);
  gtk_container_set_border_width (GTK_CONTAINER (table1), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 5);

  hbox3 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox3);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox3", hbox3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox3);
  gtk_table_attach (GTK_TABLE (table1), hbox3, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  kana_ctrl = gtk_check_button_new_with_label (_("Ctrl"));
  gtk_widget_ref (kana_ctrl);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "kana_ctrl", kana_ctrl,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (kana_ctrl);
  gtk_box_pack_start (GTK_BOX (hbox3), kana_ctrl, FALSE, FALSE, 0);

  kana_shift = gtk_check_button_new_with_label (_("Shift"));
  gtk_widget_ref (kana_shift);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "kana_shift", kana_shift,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (kana_shift);
  gtk_box_pack_start (GTK_BOX (hbox3), kana_shift, FALSE, FALSE, 0);

  kana_entry = gtk_entry_new ();
  gtk_widget_ref (kana_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "kana_entry", kana_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (kana_entry);
  gtk_box_pack_start (GTK_BOX (hbox3), kana_entry, TRUE, TRUE, 0);
  gtk_entry_set_editable (GTK_ENTRY (kana_entry), FALSE);

  kana_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (kana_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "kana_choose", kana_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (kana_choose);
  gtk_box_pack_start (GTK_BOX (hbox3), kana_choose, FALSE, FALSE, 0);

  hbox5 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox5);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox5", hbox5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox5);
  gtk_table_attach (GTK_TABLE (table1), hbox5, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  normal = gtk_radio_button_new_with_label (shift_method_group, _("同時押下"));
  shift_method_group = gtk_radio_button_group (GTK_RADIO_BUTTON (normal));
  gtk_widget_ref (normal);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "normal", normal,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (normal);
  gtk_box_pack_start (GTK_BOX (hbox5), normal, FALSE, FALSE, 0);

  prefix = gtk_radio_button_new_with_label (shift_method_group, _("プレフィックス"));
  shift_method_group = gtk_radio_button_group (GTK_RADIO_BUTTON (prefix));
  gtk_widget_ref (prefix);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "prefix", prefix,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (prefix);
  gtk_box_pack_start (GTK_BOX (hbox5), prefix, FALSE, FALSE, 0);

  sync = gtk_radio_button_new_with_label (shift_method_group, _("同時打鍵"));
  shift_method_group = gtk_radio_button_group (GTK_RADIO_BUTTON (sync));
  gtk_widget_ref (sync);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "sync", sync,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (sync);
  gtk_box_pack_start (GTK_BOX (hbox5), sync, FALSE, FALSE, 0);

  hbox6 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox6);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox6", hbox6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox6);
  gtk_table_attach (GTK_TABLE (table1), hbox6, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  label7 = gtk_label_new (_("左："));
  gtk_widget_ref (label7);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label7", label7,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label7);
  gtk_box_pack_start (GTK_BOX (hbox6), label7, FALSE, FALSE, 0);

  left_entry = gtk_entry_new ();
  gtk_widget_ref (left_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "left_entry", left_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (left_entry);
  gtk_box_pack_start (GTK_BOX (hbox6), left_entry, TRUE, TRUE, 0);
  gtk_widget_set_usize (left_entry, 100, -2);
  gtk_entry_set_editable (GTK_ENTRY (left_entry), FALSE);

  left_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (left_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "left_choose", left_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (left_choose);
  gtk_box_pack_start (GTK_BOX (hbox6), left_choose, FALSE, FALSE, 0);

  label8 = gtk_label_new (_("右："));
  gtk_widget_ref (label8);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label8", label8,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label8);
  gtk_box_pack_start (GTK_BOX (hbox6), label8, FALSE, FALSE, 0);

  right_entry = gtk_entry_new ();
  gtk_widget_ref (right_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "right_entry", right_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (right_entry);
  gtk_box_pack_start (GTK_BOX (hbox6), right_entry, TRUE, TRUE, 0);
  gtk_widget_set_usize (right_entry, 100, -2);
  gtk_entry_set_editable (GTK_ENTRY (right_entry), FALSE);

  right_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (right_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "right_choose", right_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (right_choose);
  gtk_box_pack_start (GTK_BOX (hbox6), right_choose, FALSE, FALSE, 0);

  label6 = gtk_label_new (_("親指キー："));
  gtk_widget_ref (label6);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label6", label6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label6), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label6), 1, 0.5);

  label4 = gtk_label_new (_("仮名キー："));
  gtk_widget_ref (label4);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label4", label4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label4), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label4), 1, 0.5);

  label3 = gtk_label_new (_("定義ファイル："));
  gtk_widget_ref (label3);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label3", label3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label3), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);

  label5 = gtk_label_new (_("シフト方式："));
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label5", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 0, 1, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label5), 1, 0.5);

  hbox4 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox4);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox4", hbox4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox4);
  gtk_table_attach (GTK_TABLE (table1), hbox4, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  map_entry = gtk_entry_new ();
  gtk_widget_ref (map_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "map_entry", map_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (map_entry);
  gtk_box_pack_start (GTK_BOX (hbox4), map_entry, TRUE, TRUE, 0);

  map_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (map_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "map_choose", map_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (map_choose);
  gtk_box_pack_start (GTK_BOX (hbox4), map_choose, FALSE, FALSE, 0);

  use_map = gtk_check_button_new_with_label (_("図形キーを置き換える"));
  gtk_widget_ref (use_map);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "use_map", use_map,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (use_map);
  gtk_table_attach (GTK_TABLE (table1), use_map, 0, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  frame5 = gtk_frame_new (_("キー配列（無変換固定）"));
  gtk_widget_ref (frame5);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "frame5", frame5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame5);
  gtk_box_pack_start (GTK_BOX (vbox1), frame5, TRUE, TRUE, 0);

  table4 = gtk_table_new (2, 2, FALSE);
  gtk_widget_ref (table4);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "table4", table4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table4);
  gtk_container_add (GTK_CONTAINER (frame5), table4);
  gtk_container_set_border_width (GTK_CONTAINER (table4), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table4), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table4), 5);

  label18 = gtk_label_new (_("定義ファイル："));
  gtk_widget_ref (label18);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label18", label18,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label18);
  gtk_table_attach (GTK_TABLE (table4), label18, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  hbox8 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox8);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox8", hbox8,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox8);
  gtk_table_attach (GTK_TABLE (table4), hbox8, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  alnum_map_entry = gtk_entry_new ();
  gtk_widget_ref (alnum_map_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "alnum_map_entry", alnum_map_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (alnum_map_entry);
  gtk_box_pack_start (GTK_BOX (hbox8), alnum_map_entry, TRUE, TRUE, 0);

  alnum_map_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (alnum_map_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "alnum_map_choose", alnum_map_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (alnum_map_choose);
  gtk_box_pack_start (GTK_BOX (hbox8), alnum_map_choose, FALSE, FALSE, 0);

  use_alnum_map = gtk_check_button_new_with_label (_("図形キーを置き換える"));
  gtk_widget_ref (use_alnum_map);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "use_alnum_map", use_alnum_map,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (use_alnum_map);
  gtk_table_attach (GTK_TABLE (table4), use_alnum_map, 0, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  label16 = gtk_label_new (_("キー配列"));
  gtk_widget_ref (label16);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label16", label16,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label16);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 0), label16);

  vbox2 = gtk_vbox_new (FALSE, 5);
  gtk_widget_ref (vbox2);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "vbox2", vbox2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox2);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox2);
  gtk_container_set_border_width (GTK_CONTAINER (vbox2), 5);

  use_roma = gtk_check_button_new_with_label (_("ローマ字変換する"));
  gtk_widget_ref (use_roma);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "use_roma", use_roma,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (use_roma);
  gtk_box_pack_start (GTK_BOX (vbox2), use_roma, FALSE, FALSE, 0);

  hbox1 = gtk_hbox_new (FALSE, 5);
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (vbox2), hbox1, FALSE, TRUE, 0);

  label1 = gtk_label_new (_("定義ファイル："));
  gtk_widget_ref (label1);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label1", label1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label1);
  gtk_box_pack_start (GTK_BOX (hbox1), label1, FALSE, FALSE, 0);

  roma_entry = gtk_entry_new ();
  gtk_widget_ref (roma_entry);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "roma_entry", roma_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (roma_entry);
  gtk_box_pack_start (GTK_BOX (hbox1), roma_entry, TRUE, TRUE, 0);

  roma_choose = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (roma_choose);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "roma_choose", roma_choose,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (roma_choose);
  gtk_box_pack_start (GTK_BOX (hbox1), roma_choose, FALSE, FALSE, 0);

  roma_strict = gtk_check_button_new_with_label (_("大文字・小文字を区別する"));
  gtk_widget_ref (roma_strict);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "roma_strict", roma_strict,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (roma_strict);
  gtk_box_pack_start (GTK_BOX (vbox2), roma_strict, FALSE, FALSE, 0);

  label15 = gtk_label_new (_("ローマ字変換"));
  gtk_widget_ref (label15);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label15", label15,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label15);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 1), label15);

  vbox4 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox4);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "vbox4", vbox4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox4);
  gtk_container_add (GTK_CONTAINER (notebook1), vbox4);
  gtk_container_set_border_width (GTK_CONTAINER (vbox4), 10);

  frame3 = gtk_frame_new (_("フォント"));
  gtk_widget_ref (frame3);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "frame3", frame3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame3);
  gtk_box_pack_start (GTK_BOX (vbox4), frame3, FALSE, TRUE, 0);

  table2 = gtk_table_new (2, 3, FALSE);
  gtk_widget_ref (table2);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "table2", table2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table2);
  gtk_container_add (GTK_CONTAINER (frame3), table2);
  gtk_container_set_border_width (GTK_CONTAINER (table2), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table2), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table2), 5);

  preedit_font = gtk_entry_new ();
  gtk_widget_ref (preedit_font);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "preedit_font", preedit_font,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (preedit_font);
  gtk_table_attach (GTK_TABLE (table2), preedit_font, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  status_font = gtk_entry_new ();
  gtk_widget_ref (status_font);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "status_font", status_font,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (status_font);
  gtk_table_attach (GTK_TABLE (table2), status_font, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  preedit_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (preedit_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "preedit_sel", preedit_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (preedit_sel);
  gtk_table_attach (GTK_TABLE (table2), preedit_sel, 2, 3, 0, 1,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (preedit_sel, 50, -2);

  status_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (status_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "status_sel", status_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (status_sel);
  gtk_table_attach (GTK_TABLE (table2), status_sel, 2, 3, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (status_sel, 50, -2);

  label9 = gtk_label_new (_("編集行："));
  gtk_widget_ref (label9);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label9", label9,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label9);
  gtk_table_attach (GTK_TABLE (table2), label9, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label9), 1, 0.5);

  label10 = gtk_label_new (_("ステータス行："));
  gtk_widget_ref (label10);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label10", label10,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label10);
  gtk_table_attach (GTK_TABLE (table2), label10, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label10), 1, 0.5);

  frame4 = gtk_frame_new (_("色"));
  gtk_widget_ref (frame4);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "frame4", frame4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (frame4);
  gtk_box_pack_start (GTK_BOX (vbox4), frame4, FALSE, TRUE, 0);

  table3 = gtk_table_new (3, 5, FALSE);
  gtk_widget_ref (table3);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "table3", table3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table3);
  gtk_container_add (GTK_CONTAINER (frame4), table3);
  gtk_container_set_border_width (GTK_CONTAINER (table3), 5);
  gtk_table_set_row_spacings (GTK_TABLE (table3), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table3), 5);

  label12 = gtk_label_new (_("変換中："));
  gtk_widget_ref (label12);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label12", label12,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label12);
  gtk_table_attach (GTK_TABLE (table3), label12, 0, 1, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  cnv_fg_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (cnv_fg_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "cnv_fg_sel", cnv_fg_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cnv_fg_sel);
  gtk_table_attach (GTK_TABLE (table3), cnv_fg_sel, 2, 3, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (cnv_fg_sel, 50, -2);

  cnv_bg_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (cnv_bg_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "cnv_bg_sel", cnv_bg_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cnv_bg_sel);
  gtk_table_attach (GTK_TABLE (table3), cnv_bg_sel, 4, 5, 2, 3,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (cnv_bg_sel, 50, -2);

  label11 = gtk_label_new (_("入力中："));
  gtk_widget_ref (label11);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label11", label11,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label11);
  gtk_table_attach (GTK_TABLE (table3), label11, 0, 1, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);

  inp_fg_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (inp_fg_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "inp_fg_sel", inp_fg_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (inp_fg_sel);
  gtk_table_attach (GTK_TABLE (table3), inp_fg_sel, 2, 3, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (inp_fg_sel, 50, -2);

  inp_bg_sel = gtk_button_new_with_label (_("選択..."));
  gtk_widget_ref (inp_bg_sel);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "inp_bg_sel", inp_bg_sel,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (inp_bg_sel);
  gtk_table_attach (GTK_TABLE (table3), inp_bg_sel, 4, 5, 1, 2,
                    (GtkAttachOptions) (0),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_widget_set_usize (inp_bg_sel, 50, -2);

  inp_bg_col = gtk_drawing_area_new ();
  gtk_widget_ref (inp_bg_col);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "inp_bg_col", inp_bg_col,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (inp_bg_col);
  gtk_table_attach (GTK_TABLE (table3), inp_bg_col, 3, 4, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  cnv_bg_col = gtk_drawing_area_new ();
  gtk_widget_ref (cnv_bg_col);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "cnv_bg_col", cnv_bg_col,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cnv_bg_col);
  gtk_table_attach (GTK_TABLE (table3), cnv_bg_col, 3, 4, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  inp_fg_col = gtk_drawing_area_new ();
  gtk_widget_ref (inp_fg_col);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "inp_fg_col", inp_fg_col,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (inp_fg_col);
  gtk_table_attach (GTK_TABLE (table3), inp_fg_col, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  cnv_fg_col = gtk_drawing_area_new ();
  gtk_widget_ref (cnv_fg_col);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "cnv_fg_col", cnv_fg_col,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cnv_fg_col);
  gtk_table_attach (GTK_TABLE (table3), cnv_fg_col, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (GTK_FILL), 0, 0);

  label13 = gtk_label_new (_("文字："));
  gtk_widget_ref (label13);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label13", label13,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label13);
  gtk_table_attach (GTK_TABLE (table3), label13, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label13), 0, 0.5);

  label14 = gtk_label_new (_("背景："));
  gtk_widget_ref (label14);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label14", label14,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label14);
  gtk_table_attach (GTK_TABLE (table3), label14, 3, 4, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label14), 0, 0.5);

  label17 = gtk_label_new (_("表示"));
  gtk_widget_ref (label17);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "label17", label17,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label17);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook1), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook1), 2), label17);

  hbox7 = gtk_hbox_new (TRUE, 0);
  gtk_widget_ref (hbox7);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "hbox7", hbox7,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox7);
  gtk_box_pack_start (GTK_BOX (vbox3), hbox7, FALSE, TRUE, 0);

  ok_button = gtk_button_new_with_label (_("OK"));
  gtk_widget_ref (ok_button);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "ok_button", ok_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (ok_button);
  gtk_box_pack_start (GTK_BOX (hbox7), ok_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (ok_button, 110, -2);

  cancel_button = gtk_button_new_with_label (_("キャンセル"));
  gtk_widget_ref (cancel_button);
  gtk_object_set_data_full (GTK_OBJECT (nicoconf), "cancel_button", cancel_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cancel_button);
  gtk_box_pack_start (GTK_BOX (hbox7), cancel_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (cancel_button, 110, -2);

  gtk_signal_connect (GTK_OBJECT (nicoconf), "realize",
                      GTK_SIGNAL_FUNC (onRealized),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (nicoconf), "delete_event",
                      GTK_SIGNAL_FUNC (on_nicoconf_delete_event),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (wnn), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (canna), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (kana_choose), "clicked",
                      GTK_SIGNAL_FUNC (onKanaKeyChoose),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (normal), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (prefix), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (sync), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (left_choose), "clicked",
                      GTK_SIGNAL_FUNC (onLeftShiftChoose),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (right_choose), "clicked",
                      GTK_SIGNAL_FUNC (onRightShiftChoose),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (map_choose), "clicked",
                      GTK_SIGNAL_FUNC (onMapFileChoose),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (use_map), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (use_roma), "toggled",
                      GTK_SIGNAL_FUNC (onButtonToggled),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (roma_choose), "clicked",
                      GTK_SIGNAL_FUNC (onRomaFileChoose),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (preedit_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_preedit_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (status_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_status_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (cnv_fg_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_cnv_fg_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (cnv_bg_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_cnv_bg_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (inp_fg_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_inp_fg_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (inp_bg_sel), "clicked",
                      GTK_SIGNAL_FUNC (on_inp_bg_sel_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (ok_button), "clicked",
                      GTK_SIGNAL_FUNC (onOK),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (cancel_button), "clicked",
                      GTK_SIGNAL_FUNC (onCancel),
                      NULL);

  return nicoconf;
}

