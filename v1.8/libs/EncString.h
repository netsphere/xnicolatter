﻿// -*- coding:utf-8-with-signature; mode:c++ -*-

// エンコーディングを保持する文字列クラス
// ナル文字を含むことができる (UTF-16対策)

#include <string>

class EncString
{
    unsigned char* str;

    // バイト単位. 終端ナル文字は含まない.
    int len;
 
    std::string encoding;

public:
    EncString();
    virtual ~EncString();

    EncString(const EncString& );
    EncString(const char* str, int len, const std::string& enc);
    EncString& operator = (const EncString& );

    int convert(const std::string& new_enc);
    void regard(const std::string& new_enc);
    
    const unsigned char* data() const;
    int length() const;
};
