// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef NICOLATTER_CONN_H
#define NICOLATTER_CONN_H

#include <string>
#ifdef USE_GTK
  #include <gdk/gdktypes.h>
#else
  #include <X11/Intrinsic.h>
#endif

#include <misc.h>

using namespace std;

//////////////////////////////////////////////////////////////////////
// Connection

class SendBuffer;
class RecvBuffer;
class XimWriter;
class IIIMPWriter;
class InputMethod;
class InputContext;
class KeyEvent;
class Connection
{
public:
    typedef ptr_vector<InputMethod*> InputMethods;
    typedef ptr_list<SendBuffer*> SendQueue;
    enum TransportType {
        XIM_X_TRANSPORT = 1,
        XIM_TCP_TRANSPORT,
        UNICODE_INPUT,
        IIIMP
    };

    InputMethods methods;
    SendQueue send_queue; // 送信待ちイベント

private:
    TransportType type;
    int sock_fd; // XIM TCP transport
    int io_tag;
    Window clientWindow; // XIM X transport / Unicode Input
    Window selfWindow; // XIM X transport
    bool bigEndian;
    static int next_imid;
    RecvBuffer* rbuf;

public:
    Connection(TransportType type, int fd); // XIM TCP transport / IIIMP
    Connection(TransportType type, Window self, Window client);
                                   // XIM X transport / Unicode Input
    virtual ~Connection();

    InputMethod* createIM();
    void remove_im(int imid);
    InputMethod* getIM(int id) const;
    void appendCommitString(InputContext* ic, const string& s);
    void appendThroughEvent(InputContext* ic, const KeyEvent& ev,
                            bool after_commit);
    void close();
    Window getClientWindow() const;
    TransportType getType() const;
    XimWriter* createXimWriter(int opcode) const; // XIM
    IIIMPWriter* createIIIMPWriter(int opcode) const; // IIIMP
    void onXInput(const XClientMessageEvent& ev);

private:
    void sendEvents();

#ifdef USE_GTK
    static void onTcpReceived(void* data, int source, GdkInputCondition );
#else
    static void onTcpReceived(void* closure, int* source, XtInputId* id);
#endif
    static Connection* find_client_tcp(int fd);

    friend Connection* find_client_x_self(Window self);
    friend Connection* find_client_x_client(Window client);
};

typedef ptr_set<Connection*, less<Connection*> > Connections;
extern Connections clients;

#endif // NICOLATTER_CONN_H
