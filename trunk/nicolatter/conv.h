// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_CONV_H
#define QSNICOLA_CONV_H

#include "../keymap.h"
#include "LevelSelector.h"

//////////////////////////////////////////////////////////////////////
// CharStatusList

enum CharStatus
{
    CHAR_ROMA,
    CHAR_READ   // 英数固定の場合は直接この状態になる
};

class CharStatusList: public vector<CharStatus>
{
public:
    void replace(int pos, int n, CharStatus val)
    {
        erase(begin() + pos, begin() + pos + n - 1);
        operator [](pos) = val;
    }
};

//////////////////////////////////////////////////////////////////////
// KanaKanjiConv

enum KanaKanjiStatus {
    NONE,
    THROUGH,
    GRAPHIC_CHAR,
    CONTROL_FUNC    // モード遷移
};

enum KanaMode {
    MODE_OFF,   // 直接入力
    MODE_KANA,  // 仮名
    MODE_ALPHA  // 無変換固定
};

class InputContext;
class PreeditWindow;
class StatusWindow;
class Connection;
class KanaKanjiConv
{
protected:
    InputContext* ic;
    string determined;
public:
    virtual ~KanaKanjiConv();

    virtual KanaKanjiStatus input(const KeyEvent& event, int level) = 0;

    virtual void all_determine() = 0;
    virtual void clear() = 0;
    string getDetermined() const;
    void clearDetermined();
    virtual KanaMode getKanaMode() const = 0;
    virtual void setKanaMode(KanaMode mode) = 0;
    virtual PreeditWindow* createPreeditWindow() = 0;
    virtual StatusWindow* createStatusWindow() = 0;
    virtual void setCandidateVisible(bool ) = 0;
    virtual void updateCandidate() = 0;
protected:
    KanaKanjiConv(InputContext* ic);
};

#endif  // QSNICOLA_CONV_H

