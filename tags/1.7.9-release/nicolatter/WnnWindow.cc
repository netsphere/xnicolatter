// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// Wnn用の編集ウィンドウ，ステータスウィンドウ，候補ウィンドウ

#include "../config.h"

#include <algorithm>
#include <X11/Xlib.h>
#ifdef USE_GTK
#include <gdk/gdkx.h>
#else
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/List.h>
#endif  // USE_GTK

#include "WnnWindow.h"
#include "WnnConv.h"
#include "client.h"
#include "LevelSelector.h"
#include "start.h"
#include "../global.h"

#ifdef USE_GTK
#include <misc-gtk.h>
#endif  // USE_GTK

using namespace std;

//////////////////////////////////////////////////////////////////////
// WnnPreedit

WnnPreedit::WnnPreedit(WnnConv* conv_, InputContext* ic):
                super(ic), conv(conv_)
{
}

WnnPreedit::~WnnPreedit()
{
}

void WnnPreedit::draw(const CRect& )
{
    if (!conv || conv->im_state == MI_NYURYOKU)
        return;

#ifdef USE_GTK
    Window w = GDK_WINDOW_XWINDOW(canvas->window);
    GC gc = XCreateGC(top_display, GDK_WINDOW_XWINDOW(canvas->window),
                      0, 0);
#else
    Window w = XtWindow(canvas);
    GC gc = XtGetGC(canvas, 0, NULL);
#endif
    XClearWindow(top_display, w);
    conv->setCandidateVisible(conv->im_state == KOUHO_ICHIRAN);

    XRectangle logical;
    int x, y;
    int cur_xpos = -1;

    switch (conv->im_state)
    {
    case HENKAN_MAE:
        {
            XmbTextExtents(font.xfont(), conv->preconv.c_str(),
                           conv->preconv.length(), NULL, &logical);
            x = -logical.x + PREEDIT_BORDER;
            y = -logical.y + PREEDIT_BORDER;
            XSetForeground(top_display, gc, blue);
            XSetBackground(top_display, gc, white);

            const char* p = conv->preconv.c_str();
            int i;
            for (i = 0; i < conv->charSizes.size(); i++) {
                if (conv->caret == i)
                    cur_xpos = x;
/*
                XSetForeground(disp, this_->gc1,
                        conv->statusList[i] == CHAR_ROMA ? this_->blue : this_->red);
        // TODO: ローマ字変換後の仮名文字と無変換文字列を区別する
*/
                XmbDrawImageString(top_display, w, font.xfont(), gc,
                                   x, y, p, conv->charSizes[i]);
                XmbTextExtents(font.xfont(), p, conv->charSizes[i], NULL,
                               &logical);
                x += logical.width;
                p += conv->charSizes[i];
            }
            if (conv->caret == i)
                cur_xpos = x;
            Caret::instance()->setPosition(CRect(cur_xpos,
                                                 PREEDIT_BORDER, 1,
                                                 logical.height));
            Caret::instance()->setVisible(true);
        }
        break;
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        {
            Caret::instance()->setVisible(false);

            XmbTextExtents(font.xfont(),
                           conv->converted.c_str(),
                           conv->converted.length(),
                           NULL, &logical);
            x = -logical.x + PREEDIT_BORDER;
            y = -logical.y + PREEDIT_BORDER;
            XSetForeground(top_display, gc, blue);
            XSetBackground(top_display, gc, white);

            const char* p = conv->converted.c_str();
            for (int i = 0; i < conv->conv_i.size() - 1; i++) {
                if (conv->cur_clause == i) {
                    XSetForeground(top_display, gc, white);
                    XSetBackground(top_display, gc, blue);
                }

                int clen = conv->conv_i[i + 1] - conv->conv_i[i];
                XmbDrawImageString(top_display, w, font.xfont(), gc,
                    x, y, p, clen);

                if (conv->cur_clause == i) {
                    XSetForeground(top_display, gc, blue);
                    XSetBackground(top_display, gc, white);
                }

                XmbTextExtents(font.xfont(), p, clen, NULL, &logical);
                x += logical.width;
                p += clen;
            }
        }
        break;
    default:
        assert(0);
    }
#ifdef USE_GTK
    XFreeGC(top_display, gc);
#else
    XtReleaseGC(canvas, gc);
#endif
}

void WnnPreedit::update()
{
    if (conv->im_state == MI_NYURYOKU) {
        Caret::instance()->setVisible(false);
        setVisible(false);
        return;
    }

    if (!font.xfont()) {
        updateFont();
        assert(font.xfont());
    }
    
    XRectangle logical;
    switch (conv->im_state)
    {
    case HENKAN_MAE:
        XmbTextExtents(font.xfont(), conv->preconv.c_str(),
                       conv->preconv.length(), NULL, &logical);
        break;
    case ZEN_HENKAN:
    case KOUHO_ICHIRAN:
        XmbTextExtents(font.xfont(), conv->converted.c_str(),
                       conv->converted.length(), NULL, &logical);
        break;
    default:
        assert(0);
    }
#if DEBUG > 1
    TRACE("extents: x = %d, y = %d, w = %d, h = %d\n",
                logical.x, logical.y, logical.width, logical.height);
#endif

    adjustLocation(logical);
    setVisible(true);

#ifdef USE_GTK
    gtk_widget_draw(GTK_WIDGET(window), 0);
#else
    Window w = XtWindow(canvas);
    if (w)
        XClearArea(top_display, w, 0, 0, 0, 0, True);
#endif

#if 0
        // TODO: callbacksスタイル
    SendBuffer* imstr = new SendBuffer();
    imstr->head(XIM_PREEDIT_DRAW, 0);
    imstr->card16(imid);
    imstr->card16(icid);
    ...
    imstr->str16(conv->preconv.c_str());
    imstr->pad();
    card16();
    card16(0);  // unused
    feedback
    conn->send_queue.push_back(imstr);
#endif
}

//////////////////////////////////////////////////////////////////////
// WnnStatus

const char* WnnStatus::modes[] = { "[英数]", "[ひら]", "[無固]" };

WnnStatus::WnnStatus(WnnConv* conv_, InputContext* ic):
            super(ic), conv(conv_)
{
}

WnnStatus::~WnnStatus()
{
}

void WnnStatus::update()
{
    int kana_mode = conv->getKanaMode();
/*
    // IME OFFでも現在のモードを知るためにステータスウィンドウを表示した方が便宜なので，
    // このコードをコメントアウトした。

    // IME OFFでステータスウィンドウを消す
    if (kana_mode == 0) {
        setVisible(false);
        return;
    }
*/
    adjustLocation();
    setVisible(true);

    string ss = conv->wnn->is_open() ?  modes[kana_mode] : "変換サーバーに接続できません";
    string ks = keyChar->getStatus();
    if (ks != "")
        ss += string(" ") + ks;
    
#ifdef USE_GTK
    gtk_label_set_text(GTK_LABEL(label), ss.c_str());
#else
    XtVaSetValues(label, XtNlabel, ss.c_str(), NULL);
#endif
}

//////////////////////////////////////////////////////////////////////
// WnnCandidate

int WnnCandidate::CAND_SIZE = 10;
    // TODO: nicoconfで変更可能にする

const int WnnCandidate::BORDER = 3;

WnnCandidate::WnnCandidate(WnnConv* conv_, PreeditWindow* pre)
                                : conv(conv_), preeditWindow(pre)
{
#ifndef USE_GTK
    font = 0;
#endif
    create();
}

WnnCandidate::~WnnCandidate()
{
#ifndef USE_GTK
    if (font) {
        XFreeFontSet(top_display, font);
        font = 0;
    }
#endif
    destroy();
}

void WnnCandidate::create()
{
#ifdef USE_GTK
    window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_POPUP));
    // gtk_window_set_title(GTK_WINDOW(window), "候補");
    gtk_window_set_policy(GTK_WINDOW(window), TRUE, TRUE, TRUE);

    GtkWidget* box = gtk_vbox_new(FALSE, 0);
    gtk_widget_show(box);
    gtk_container_add(GTK_CONTAINER(window), box);

    scrolled = gtk_scrolled_window_new(0, 0);
    gtk_widget_show(scrolled);
    gtk_widget_set_usize(scrolled, 120, 20);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled),
                                   GTK_POLICY_NEVER, GTK_POLICY_ALWAYS);
    gtk_box_pack_start(GTK_BOX(box), scrolled, TRUE, TRUE, 0);

    list_widget = gtk_clist_new(1);
    gtk_widget_show(list_widget);

    // .gtkrcでfontsetを指定していないときも表示できるようにする
    GtkStyle* style = gtk_style_copy(gtk_widget_get_style(list_widget));
    style->font = gdk_fontset_load(
        const_cast<char*>(global_prop.preedit_font.c_str()));
    gtk_widget_set_style(list_widget, style);
    
    // gtk_clist_set_column_width(GTK_CLIST(list), 0, 100);
    gtk_clist_set_column_auto_resize(GTK_CLIST(list_widget), 0, TRUE);
    gtk_container_add(GTK_CONTAINER(scrolled), list_widget);
    adjustment = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrolled));

    label = gtk_label_new("");
    gtk_widget_show(label);
    gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 0);

    gtk_signal_connect(GTK_OBJECT(list_widget), "select_row",
                                GTK_SIGNAL_FUNC(onSelected), this);
#else
    window = XtCreatePopupShell("cand-shell", overrideShellWidgetClass,
                top_widget,
                NULL, 0);
    canvas = window;
/*
    canvas = XtVaCreateManagedWidget("canvas", formWidgetClass,
                window,
                XtNwidth, 250, XtNheight, 20,
                NULL);
*/
    XtAddEventHandler(canvas, ExposureMask, False, onExposed, this);
    font = createFont(top_display, global_prop.preedit_font.c_str());
            // TODO: preeditと同じフォントを使う
    assert(font);
#endif
}

void WnnCandidate::setCandidates(const CandidateList& list)
{
    cand_list = list;
    sel_top = 0;
#ifdef USE_GTK
    gtk_clist_freeze(GTK_CLIST(list_widget));
    gtk_clist_clear(GTK_CLIST(list_widget));

    const char* data[1];
    CandidateList::const_iterator i;
    for (i = list.begin(); i != list.end(); i++) {
#if DEBUG >= 2
        char buf[100];
        sprintf(buf, "%d ", i - list.begin());
        data[0] = (string(buf) + *i).c_str();
#else
        data[0] = i->c_str();
#endif
        gtk_clist_append(GTK_CLIST(list_widget), const_cast<char**>(data));
    }
    gtk_clist_thaw(GTK_CLIST(list_widget));
    resizeWindow();
#else
    cur_sel = 0;
    resizeWindow();
    // draw();
#endif
}

void WnnCandidate::update()
{
    if (!cand_list.size()) {
        setVisible(false);
        return;
    }

    XPoint pre_loc = preeditWindow->getLocation();
    pre_loc.y += preeditWindow->size.height;
    setLocation(pre_loc);

    setVisible(true);
#ifndef USE_GTK
    draw();
#endif
}

#ifdef USE_GTK
void WnnCandidate::onSelected(GtkCList* clist, gint row, gint column,
                            GdkEvent* event, WnnCandidate* this_)
    // GtkCList::select_row
    // マウスクリックで候補が選択されたとき

    // 1999.08.24 gtk_clist_select_row()でも呼ばれるようなので，ここで
    //      確定すると不味そう。
{
    // TRACE("onSelected()\n");
    // conv->onCandidateSelected(row);
}
#endif

void WnnCandidate::select(int index)
{
    assert(index >= 0);

#ifdef USE_GTK
    int old_sel_top = sel_top;
#endif
    if (index < sel_top || index >= sel_top + CAND_SIZE) {
        sel_top = 0;
        while (sel_top + CAND_SIZE <= index)
            sel_top += CAND_SIZE;
    }
#ifdef USE_GTK
    gtk_clist_select_row(GTK_CLIST(list_widget), index, 0);

    if (sel_top != old_sel_top)
        resizeWindow();

    gtk_adjustment_set_value(adjustment,
        adjustment->lower
        + (adjustment->upper - adjustment->lower)
                * ((double) sel_top / (double) cand_list.size()));

    char buf[100];
    sprintf(buf, "%d/%d", index + 1, cand_list.size());
    gtk_label_set_text(GTK_LABEL(label), buf);
#else
    cur_sel = index;

    resizeWindow();
    Window w = XtWindow(canvas);
    if (w)
        XClearArea(top_display, w, 0, 0, 0, 0, True);
#endif
}

void WnnCandidate::clear()
{
    cand_list.clear();
#ifdef USE_GTK
    gtk_clist_clear(GTK_CLIST(list_widget));
#else
    Window w = XtWindow(canvas);
    if (w)
        XClearArea(top_display, w, 0, 0, 0, 0, True);
#endif
}

#ifdef USE_GTK
void WnnCandidate::resizeWindow()
{
    static const int CELL_SPACING = 1;
            // gtk+ 1.2.3: gtkclist.cで決め打ち
    static const int HOFFSET = 30; // スクロールバーの分
    static const int VOFFSET = 4;
    
    GtkCList* clist = GTK_CLIST(list_widget);
    int row_height = clist->row_height + CELL_SPACING;

    // 候補ウィンドウの幅を決定する
    int width = -1;
    XRectangle rect;
    XFontSet xfont = GDK_FONT_XFONTSET(list_widget->style->font);
    for (int i = 0; i < min(int(cand_list.size() - sel_top), CAND_SIZE); i++) {
        XmbTextExtents(xfont, cand_list[sel_top + i].c_str(), cand_list[sel_top + i].length(), NULL, &rect);
        width = max(int(rect.width), width);
    }
    // TRACE("width = %d\n", width);
    
    int height;
    height = row_height * min(int(cand_list.size() - sel_top), CAND_SIZE);
/*
    if (GTK_WIDGET(scrolled)->window)
        gdk_window_resize(GTK_WIDGET(scrolled)->window, width, height);
    else
*/
    gtk_widget_set_usize(GTK_WIDGET(scrolled),
                         width + HOFFSET, height + VOFFSET);
        // tips: ウィンドウを自動縮小 (auto_shrink) にすると上手く
        //       サイズ変更できる
}
#else
void WnnCandidate::resizeWindow()
{
    string line;
    char buf[100];
    int i;
    for (i = sel_top; i < sel_top + CAND_SIZE && i < cand_list.size(); i++)
        line += cand_list[i] + "　";
    sprintf(buf, "%d/%d", cur_sel + 1, cand_list.size());
    line += buf;

    XRectangle logical;
    XmbTextExtents(font, line.c_str(), line.length(), NULL, &logical);
    setSize(logical.width + BORDER * 2, logical.height + BORDER * 2);
}

void WnnCandidate::draw()
{
    static int blue = 0;
    static int white = 0;
    if (!blue && !white) {
        blue = getColor(top_display, "blue");
        white = getColor(top_display, "white");
    }

    Window w = XtWindow(canvas);
    assert(w);
    GC gc = XtGetGC(canvas, 0, NULL);

    XClearWindow(top_display, w);

    XSetForeground(top_display, gc, blue);
    XSetBackground(top_display, gc, white);

    XRectangle logical;
    int x = BORDER;
    for (int i = sel_top; i < sel_top + CAND_SIZE && i < cand_list.size();
                i++) {
        if (i == cur_sel) {
            XSetForeground(top_display, gc, white);
            XSetBackground(top_display, gc, blue);
        }

        XmbTextExtents(font,
                cand_list[i].c_str(), cand_list[i].length(),
                NULL, &logical);
        XmbDrawImageString(top_display, w, font, gc,
                x, -logical.y + BORDER,
                cand_list[i].c_str(), cand_list[i].length());
        x += logical.width;

        if (i == cur_sel) {
            XSetForeground(top_display, gc, blue);
            XSetBackground(top_display, gc, white);
        }

        static const char* sp = "　";
        XmbTextExtents(font, sp, strlen(sp), NULL, &logical);
        XmbDrawImageString(top_display, w, font, gc,
                x, -logical.y + BORDER, sp, strlen(sp));
        x += logical.width;
    }

    char buf[100];
    sprintf(buf, "%d/%d", cur_sel + 1, cand_list.size());
    XmbDrawImageString(top_display, w, font, gc,
                    x, -logical.y + BORDER, buf, strlen(buf));
    XtReleaseGC(canvas, gc);
}

void WnnCandidate::onExposed(Widget , void* closure, XEvent* event, Boolean* cont)
{
    WnnCandidate* this_ = (WnnCandidate*) closure;
    this_->draw();
}
#endif  // USE_GTK
