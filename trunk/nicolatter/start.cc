// -*- coding:utf-8 -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"
#include "../define.h"

#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <locale.h>
#include <sys/utsname.h> // uname()
#include <sys/socket.h>
#include <sys/stat.h>    // umask()
#include <netinet/in.h>

#include <X11/Xatom.h>
#include <X11/StringDefs.h>

#ifdef USE_GTK
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#else
#include <X11/Intrinsic.h>
#include <X11/Xaw/Form.h>
#include <X11/Shell.h>
#endif  // USE_GTK

#include <network/qsocket.h>
#include <qsdebug.h>

#include "XimProto.h"
#include "client.h"
#include "Connection.h"
#include "conv.h"
#include "WnnConv.h"
#include "../global.h"
#include "LevelSelector.h"
#include "syncshift.h"
#include "imext.h"

///////////////////////////////////////////////////////////////////////

KeyMap* keyMap = NULL;
KeyMap* alnumKeymap = NULL;

static int xim_port = 0;
static int iiimp_fd = -1;
static const char* SUPPORTED_LOCALES
        = "ja_JP.ujis,ja_JP.eucJP,ja_JP.EUC,ja_JP.EUC-JP,ja_JP,ja";

#ifdef USE_GTK
GtkWidget* top_widget = NULL;
#else
XtAppContext app;
Widget top_widget = 0;
static Atom XA_TARGETS = 0;
#endif

static Atom XA_XIM_SERVERS = 0;
static Atom XA_LOCALES = 0;
static Atom XA_TRANSPORT = 0;
static Atom server_atom = 0;

///////////////////////////////////////////////////////////////////////

#ifdef USE_GTK
// XIM TCP transport / IIIMP
void sock_proc(void* data, int source, GdkInputCondition )
{
    int sfd = source;
#else
void sock_proc(XtPointer closure, int* source, XtInputId* id)
{
    int sfd = *source;
#endif  // USE_GTK

    int fd = accept(sfd, NULL, NULL);
    if (fd == -1) {
        perror("accept");
        exit(1);
    }
    int flags = fcntl(fd, F_GETFL, 0);
    assert(flags != -1);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);

    Connection* cli;
    if (iiimp_fd == sfd)
        cli = new Connection(Connection::IIIMP, fd);
    else
        cli = new Connection(Connection::XIM_TCP_TRANSPORT, fd);
    clients.insert(cli);
}

#ifdef USE_GTK
void onSelectionRequired(GtkWidget* widget,
                GtkSelectionData* selection, guint info, guint time)
    // XIM
    // GtkWidget::selection_get
{
    // TRACE("onSelectionRequired()\n");

    char buf[200];
    if (selection->target == XA_LOCALES) {
        sprintf(buf, "%s%s", XIM_LOCALE_CATEGORY, SUPPORTED_LOCALES);
        // TRACE("XA_LOCALES = '%s'\n", buf);
        gtk_selection_data_set(selection, XA_LOCALES, 8,
                                (unsigned char*) buf, strlen(buf));
    }
    else if (selection->target == XA_TRANSPORT) {
        utsname name;
        uname(&name);
        sprintf(buf, "%sX/,tcp/%s:%d", XIM_TRANSPORT_CATEGORY,
                name.nodename, xim_port);
        // TRACE("XA_TRANSPORT = '%s'\n", buf);
        gtk_selection_data_set(selection, XA_TRANSPORT, 8,
                               (unsigned char*) buf, strlen(buf));
    }
    else {
        error("unknown target atom: %d\n", selection->target);
        assert(0);
    }
}
#else // USE_GTK
static void lose_proc(Widget widget, Atom* selection)
{
    QsTRACE(1, "lose_proc()\n");
        // TODO:
}

Boolean onSelectionRequired(Widget widget, Atom* selection, Atom* target,
                    Atom* type_ret, XtPointer* val_ret,
                    unsigned long* len_ret, int* fmt_ret)
    // XIM
    // Xlibからのロケールや通信経路の問い合わせに答える。
{
    // TRACE("onSelectionRequired()\n");
    char buf[1000];

    if (*target == XA_TARGETS) {
        QsTRACE(1, "XA_TARGETS\n");
        Atom* r = (Atom*) XtMalloc(2 * sizeof(Atom));
        r[0] = XA_LOCALES;
        r[1] = XA_TRANSPORT;
        *type_ret = XA_ATOM;
        *val_ret = r;
        *fmt_ret = 32;  // sizeof(Atom) * 8
        *len_ret = 2;
        return True;
    }
    else if (*target == XA_LOCALES) {
        // *type_ret = XA_STRING;
        *type_ret = *target; // xc/lib/X11/imDefIm.c:_XimPreConnectionIM()
        sprintf(buf, "%s%s", XIM_LOCALE_CATEGORY, SUPPORTED_LOCALES);
        QsTRACE(1, "XA_LOCALES = '%s'\n", buf);
        *val_ret = XtNewString(buf);
        *fmt_ret = 8;
        *len_ret = strlen(buf);
        return True;
    }
    else if (*target == XA_TRANSPORT) {
        utsname name;
        uname(&name);
#if 1
        sprintf(buf, "%sX/,tcp/%s:%d", XIM_TRANSPORT_CATEGORY,
                name.nodename, xim_port);
#else
        // TCP/IP TEST
        sprintf(buf, "%stcp/%s:%d", XIM_TRANSPORT_CATEGORY,
                name.nodename, xim_port);
#endif
        QsTRACE(1, "XA_TRANSPORT = '%s'\n", buf);
        // *type_ret = XA_STRING;
        *type_ret = *target;
        *val_ret = XtNewString(buf);
        *fmt_ret = 8;
        *len_ret = strlen(buf);
        return True;
    }
    else {
        error("unknown target atom: %d\n", *target);
        assert(0);
    }
    return False;
}
#endif  // USE_GTK

 
static void createConnectionPort(Connection::TransportType type,
                                 int port)
    // XIM / IIIMP
    // Xlibとの通信経路を作る
{
    CSocket* sock;
    try {
        sock = CSocket::setupServer(nullptr, port);
    }
    catch (SystemCallError& e) {
        exit(1);
    }

    if (type == Connection::XIM_TCP_TRANSPORT)
        xim_port = sock->port();
    else if (type == Connection::IIIMP)
        iiimp_fd = sock->sock_fd;
    
#ifdef USE_GTK
    int input_id = gdk_input_add(sock.sock_fd, GDK_INPUT_READ, sock_proc, NULL);
#else
    int input_id = XtAppAddInput(app, sock->sock_fd, (void*) XtInputReadMask,
                                 sock_proc, NULL);
#endif
    QsTRACE(1, "input_id = %d\n", input_id);
}

static void setRootProperty(Display* disp)
    // XIM
    // ルートウィンドウのプロパティを変更する。
    // PropertyNotifyメッセージが発生するので，セレクションを所有した
    // 後で変更すること。
{
    Window root = XRootWindow(disp, 0);
    Atom type = 0;
    int format = 0;
    unsigned long nitems = 0;
    unsigned long bytes_after;
    unsigned char* value = NULL;

    XGrabServer(disp);

    // IMサーバーを登録できるか？
    if (XGetWindowProperty(disp, root, XA_XIM_SERVERS, 0, 1000, False,
                    AnyPropertyType, &type, &format, &nitems,
                    &bytes_after, &value) != Success) {
        error("XGetWindowProperty() failed.\n");
        XUngrabServer(disp);
        assert(0);
    }

    Atom* rep_atoms = new Atom[nitems + 1];
    rep_atoms[0] = server_atom;
    int rep_count = 1;

    // 他のIMサーバーがあるか？
    if (type == XA_ATOM && format == 32 && nitems > 0) {
        Atom* atoms = (Atom*) value;
        for (unsigned int i = 0; i < nitems; i++) {
#ifdef DEBUG
            char* const s = XGetAtomName(disp, atoms[i]);
            QsTRACE(1, "im server: '%s'\n", s);
            XFree(s);
#endif
            if (atoms[i] != server_atom)
                rep_atoms[rep_count++] = atoms[i];
        }
    }
    XFree(value);

    XChangeProperty(disp, root, XA_XIM_SERVERS, XA_ATOM, 32,
                PropModeReplace, (unsigned char*) rep_atoms, rep_count);
        // PropertyNotifyイベントが発生するので，内容に変更がない場合
        // も更新する。
    XUngrabServer(disp);
    delete [] rep_atoms;
}

static int my_handler(Display* disp, XErrorEvent* ev)
    // 誤ったウィンドウIDを指定されても落ちないようにする
{
    char buf[1000];
    XGetErrorText(disp, ev->error_code, buf, sizeof(buf));
    error("X error: %s\n", buf);
    char n[10];
    sprintf(n, "%d", ev->request_code);
    XGetErrorDatabaseText(disp, "XRequest", n, "(unknown request)",
                          buf, sizeof(buf));
    error("  request opcode: %s, %d\n", buf, ev->minor_code);
    error("  resource id: %d\n", ev->resourceid);
        // BadWindowのとき，（不正な）ウィンドウID
        // TODO: このウィンドウIDと関連するICを無効にする？
    return 0; // ignore error
}

#ifdef USE_GTK
static void registerImServer(GtkWidget* top)
#else
static void registerImServer(Widget top)
#endif
    // Q's NicolatterをIMサーバーとして登録する。
{
    if (global_prop.xim) {
        createConnectionPort(Connection::XIM_TCP_TRANSPORT, 0);

        if (!XA_XIM_SERVERS) {
            XA_XIM_SERVERS = XInternAtom(top_display, XIM_SERVERS, False);
            XA_LOCALES = XInternAtom(top_display, XIM_LOCALES, False);
            XA_TRANSPORT = XInternAtom(top_display, XIM_TRANSPORT, False);
#ifndef USE_GTK
            XA_TARGETS = XInternAtom(top_display, "TARGETS", False);
#endif
            // XA_LOCALESは，XIM_SERVERSを変更する前にアトムを生成しておく
            // こと。
            // xc/lib/X11/imDefIm.c参照。

            char buf[1000];
            sprintf(buf, "%s%s", XIM_SERVER_CATEGORY, server_name);
            server_atom = XInternAtom(top_display, buf, False);

            XFlush(top_display);
        }

        // 呼び出し口を作る
#ifdef USE_GTK
        if (gtk_selection_owner_set(top, server_atom, CurrentTime)) {
            gtk_selection_add_target(top, server_atom, XA_LOCALES, 0);
            gtk_selection_add_target(top, server_atom, XA_TRANSPORT, 0);
        }
        else {
            error("セレクションを所有できません。\n");
            exit(1);
        }
#else
        if (!XtOwnSelection(top, server_atom, CurrentTime,
                            onSelectionRequired, lose_proc, NULL)) {
            error("セレクションを所有できません。\n");
            exit(1);
        }
#endif

        setRootProperty(top_display);
    }
    else if (global_prop.iiimp) {
        createConnectionPort(Connection::IIIMP, 9010); // default port
    }
}

static bool init_prop()
{
    assert(top_display);

    if (!global_prop.load()) {
        error("環境ファイルの読み込みに失敗\n");
        global_prop.initialize();
        global_prop.save();
    }

    delete keyMap;
    keyMap = new KeyMap();
    if (!keyMap->load(global_prop.keymap_file)) {
        error("'%s'の読み込みに失敗\n", global_prop.keymap_file.c_str());
        delete keyMap;
        keyMap = NULL;
        return false;
    }

    delete alnumKeymap;
    alnumKeymap = NULL;
    if (global_prop.alnum_keymap != "") {
        alnumKeymap = new KeyMap();
        if (!alnumKeymap->load(global_prop.alnum_keymap)) {
            error("'%s'の読み込みに失敗\n", global_prop.alnum_keymap.c_str());
            delete alnumKeymap;
            alnumKeymap = NULL;
            return false;
        }
    }
    
#ifdef USE_WNN
    if (global_prop.conv_server == 0 && global_prop.use_roma) {
        if (!WnnConv::loadRomaDef(global_prop.roma_file.c_str())) {
            error("cannot load romaji file: '%s'\n", global_prop.roma_file.c_str());
            delete keyMap;
            return false;
        }
    }
#endif

    delete keyChar;
    switch (global_prop.shift_method)
    {
    case 0:
        keyChar = new NormalSelector();
        break;
    case 1:
        keyChar = new PrefixSelector();
        break;
    case 2:
        {
            int left = XKeysymToKeycode(top_display,
                    XStringToKeysym(global_prop.shift_key[0].c_str()));
            int right = XKeysymToKeycode(top_display,
                    XStringToKeysym(global_prop.shift_key[1].c_str()));

            SyncSelector* sync = new SyncSelector();
            sync->setShiftKey(left, right);
            keyChar = sync;
        }
        break;
    default:
        assert(0);
    }

    return true;
}

#ifdef USE_GTK
static gint onTopClose(GtkWidget* top, GdkEventAny* event, void* )
    // GtkWidget::delete_event
{
    QsTRACE(1, "onTopClose()\n");
    return FALSE;   // 処理を続行
        // TODO: ルートウィンドウに登録したプロパティを削除する
}

static void onTopRealize(GtkWidget* widget, void* )
    // GtkWidget::realize
{
    gdk_window_add_filter(top_widget->window, filterXEvent, NULL);
    registerImServer(widget);
    XSetErrorHandler(my_handler);
}
#endif  // USE_GTK

static void onSIGHUP(int sig)
{
    QsTRACE(1, "onSIGHUP(): sig = %d\n", sig);

    init_prop();

    Connections::iterator i;
    for (i = clients.begin(); i != clients.end(); i++) {
        Connection::InputMethods::iterator j;
        for (j = (*i)->methods.begin(); j != (*i)->methods.end(); j++) {
            InputMethod::InputContexts::iterator k;
            for (k = (*j)->contexts.begin(); k != (*j)->contexts.end(); k++)
                (*k)->initialize();
        }
    }
}

static void do_exit()
{
    clients.clear();
    delete keyMap;
    delete keyChar;
    exit(0);
}

static void onSIGTERM(int sig)
{
    QsTRACE(1, "onSIGTERM(): sig = %d\n", sig);

    Connections::iterator i;
    for (i = clients.begin(); i != clients.end(); i++)
        (*i)->close();
    
#ifdef USE_GTK
    gtk_main_quit();
#else
    // XtAppSetExitFlag(app);
    // XtAppSetExitFlag()は，フラグセット後，さらにイベントを受け取った時点で
    // ループを抜ける
    do_exit();
#endif
}

static void daemonMode()
{
    pid_t pid = fork();
    if (pid == -1) {
        error("fork failed.\n");
        return;
    }

    if (pid) {
        // parent process
        exit(0);
    }
    else {
        // child process
        setsid();
        chdir("/");
        umask(0);

        VERIFY(freopen("/dev/null", "r", stdin));
        VERIFY(freopen("/dev/null", "a", stdout));
        VERIFY(freopen("/dev/null", "a", stderr));
    }
}

int main(int argc, char* argv[])
{
    // TRACE_SET_FILE((string(getenv("HOME")) + "/ni.log").c_str());

    bool isDaemon = false;
    global_prop.xim = true;
    global_prop.iiimp = false;

    int c;
    while ((c = getopt(argc, argv, ":Dx:i:")) != -1) {
        switch (c)
        {
        case 'D':
            isDaemon = true;
            break;
        case 'x':
            global_prop.xim = isYesString(optarg);
            break;
        case 'i':
            global_prop.iiimp = isYesString(optarg);
            break;
        case ':':
            error("Option -%c requires an operand\n", optopt);
            break;
        default:
            error("Unrecognized option: -%c\n", optopt);
            break;
        }
    }
    QsTRACE(1, "daemon mode: %d\n", (int) isDaemon);
    QsTRACE(1, "XIM support: %d\n", (int) global_prop.xim);
    QsTRACE(1, "IIIMP support: %d\n", (int) global_prop.iiimp);

    if (!global_prop.xim && !global_prop.iiimp) {
        error("error: not define XIM support nor IIIMP support.\n");
        return 1;
    }
    
    if (isDaemon) 
        daemonMode();

    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = onSIGHUP;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGHUP, &sa, NULL);

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = onSIGTERM;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGTERM, &sa, NULL);

    setenv("LC_CTYPE", JA_EUCJP_LOCALE_NAME, 1);

#ifdef USE_GTK
    gtk_init(&argc, &argv);
    gtk_set_locale();       // 必ずgtk_init()の後ろ。再帰を防ぐ

    top_display = GDK_DISPLAY();

    if (!init_prop())
        return 1;

    top_widget = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(top_widget), server_name);
        // ウィンドウ・タイトル。VJE拡張はウィンドウ・タイトルでIMサー
        // バーを探す

    gtk_signal_connect(GTK_OBJECT(top_widget), "delete_event",
                       GTK_SIGNAL_FUNC(onTopClose), NULL);
    gtk_signal_connect(GTK_OBJECT(top_widget), "destroy",
                       GTK_SIGNAL_FUNC(gtk_main_quit), NULL);
    gtk_signal_connect(GTK_OBJECT(top_widget), "realize",
                       GTK_SIGNAL_FUNC(onTopRealize), NULL);
    gtk_signal_connect(GTK_OBJECT(top_widget), "selection_get",
                       GTK_SIGNAL_FUNC(onSelectionRequired), NULL);

    gtk_widget_set_usize(GTK_WIDGET(top_widget), 100, 10);
    gtk_widget_realize(top_widget);

#if 0
    // 2000.10.23 システムの負荷が異様に高くなる
    while (true) {
        if (XPending(top_display)) {
            XEvent xev;
            XPeekEvent(top_display, &xev);
            if (filterXEvent((GdkXEvent*) &xev, NULL, NULL)
                                            == GDK_FILTER_REMOVE) {
                XNextEvent(top_display, &xev);
                continue;
            }
            else 
                gtk_main_iteration();
        }
        else {
            if (gtk_events_pending())
                gtk_main_iteration();
        }
        sleep(0);
    }
#else
    gtk_main();
#endif

#else
    XtSetLanguageProc(NULL, NULL, NULL);

    Arg args[1];
    XtSetArg(args[0], XtNtitle, server_name);
        // ウィンドウ・タイトル。VJE拡張はウィンドウ・タイトルでIMサー
        // バーを探す
    top_widget = XtAppInitialize(&app, server_name, NULL, 0,
                            &argc, argv, NULL, args, 1);

    top_display = XtDisplay(top_widget);

    if (!init_prop())
        return 1;

    XtVaCreateManagedWidget("form", formWidgetClass,
                            top_widget,
                            XtNwidth, 100, XtNheight, 50, NULL);
/*
    XtAddEventHandler(top_widget, NoEventMask, True,
                      onClientEvent, NULL);
*/
    XtSetMappedWhenManaged(top_widget, False);
    XtRealizeWidget(top_widget);

    registerImServer(top_widget);
    XSetErrorHandler(my_handler);

    // Event.c:XtAppMainLoop()
    while (true) {
        XEvent event;
        XtAppNextEvent(app, &event);
        if (filterXEvent(event))
            continue;
        XtDispatchEvent(&event);
    }
#endif  // USE_GTK
    do_exit();
    return 0;
}
