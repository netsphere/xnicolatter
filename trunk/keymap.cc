﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2003 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "config.h"
#include "define.h"

#include "keymap.h"
#include <file_name.h>
#include <string.h>
#include <qsdebug.h>

//////////////////////////////////////////////////////////////////////

#define APP_NAME "Q's Nicolatter for X"
Display* top_display = NULL;

//////////////////////////////////////////////////////////////////////
// Key

Key::Key(const string& name_): name(name_)
{
}

string getKeyName(int mod, int keycode)
{
    assert(top_display);

    string retval;
    if ((mod & ControlMask) != 0)
        retval = "ctrl-";
    KeySym sym = XKeycodeToKeysym(top_display, keycode, 0);
    const char* s = XKeysymToString(sym);
    if (s)
        retval += s;
    return retval;
}

string getKeyName2(int mod, int keycode)
    // [Shift]を考慮する
{
    assert(top_display);

    string retval;
    if ((mod & ControlMask) != 0)
        retval = "ctrl-";
    if ((mod & ShiftMask) != 0)
        retval += "shift-";
    KeySym sym = XKeycodeToKeysym(top_display, keycode, 0);
    const char* s = XKeysymToString(sym);
    if (s)
        retval += s;
    return retval;
}

int getKeyCode(const string& name)
{
    assert(top_display);

    const char* p = name.c_str();
    KeySym sym;
    if (!strncmp(p, "ctrl-", 5))
        return getKeyCode(string(name, 5));
    else if (!strncmp(p, "shift-", 6))
        return getKeyCode(string(name, 6));
    else {
        sym = XStringToKeysym(p);
        return XKeysymToKeycode(top_display, sym);
    }
}

int getKeyMod(const string& name)
{
    if (!strncmp(name.c_str(), "ctrl-", 5))
        return getKeyMod(string(name, 5)) | ControlMask;
    else if (!strncmp(name.c_str(), "shift-", 6))
        return getKeyMod(string(name, 6)) | ShiftMask;
    return 0;
}

//////////////////////////////////////////////////////////////////////
// GraphicKey

GraphicKey::GraphicKey(const string& name, const string* s): Key(name)
{
    for (int i = 0; i < 3; i++)
        chars[i] = s[i];
}

GraphicKey::~GraphicKey()
{
}

KeyType GraphicKey::getType() const
{
    return GRAPHIC_KEY;
}

string GraphicKey::getGraphChar(int level) const
{
    assert(level >= 0 && level < 3);
    return chars[level];
}

//////////////////////////////////////////////////////////////////////
// FunctionKey

FunctionKey::FunctionKey(const string& name): Key(name)
{
}

FunctionKey::~FunctionKey()
{
}

KeyType FunctionKey::getType() const
{
    return FUNCTION_KEY;
}

//////////////////////////////////////////////////////////////////////
// KeyMap

KeyMap::KeyMap()
{
}

KeyMap::~KeyMap()
{
    clear();
}

FunctionKey* KeyMap::createFuncKey(const string& name, const string s[])
{
    FunctionKey* key = new FunctionKey(name);
    assert(key);
    for (int i = 0; i < 3; i++) {
        if (s[i] != "") {
            key->func[i] = find_func(s[i]);
#ifdef DEBUG
            if (!key->func[i])
                QsTRACE(1, "機能名が違う: %s\n", s[i].c_str());
#endif
        }
        else
            key->func[i] = 0;
    }
    return key;
}

const CtrlFunc* KeyMap::find_func(const string& func_name) const
{
    FuncList::const_iterator i;
    for (i = funcList.begin(); i != funcList.end(); i++) {
        if ((*i)->name == func_name)
            return *i;
    }
    return NULL;
}

void KeyMap::clear()
{
    QsTRACE(1, "KeyMap::clear()\n");
    keyList.clear();
    funcList.clear();
}

void KeyMap::loadFunc(FILE* fp, int& line)
{
    char buf[1000];
    while (fgets(buf, sizeof(buf), fp) != 0) {
        line++;

        const char* p = buf;
        p = skip_space(p);
        if (!*p || *p == '#')
            continue;
        else if (!strncasecmp(p, "end-define", 10))
            return;

        string name;
        string piece[4];
        while (*p && !isspace(*p))
            name += *p++;

        p = skip_space(p);
        for (int i = 0; i < 4; i++) {
            if (!*p || *p == '#')
                break;

            while (*p && !isspace(*p))
                piece[i] += *p++;
            p = skip_space(p);
        }

        CtrlFunc* func = new CtrlFunc();
        func->name = name;
        for (int i = 0; i < 4; i++) {
            if (piece[i] != "") {
                func->piece[i] = find_piece_id(piece[i], i);
                if (func->piece[i] == CF_ERROR) {
                    QsTRACE(1, "(%d): %s %s 機能片の名前が違うか，そのIME状態では使えない",
                        line, name.c_str(), piece[i].c_str());
                }
            }
            else
                func->piece[i] = CF_ERROR;
        }
        FuncList::iterator i = funcList.find(func);
        if (i != funcList.end())
            funcList.erase(i);
        funcList.insert(func);
    }
}

void KeyMap::loadKey(FILE* fp, int& line)
{
    char buf[1000];
    while (fgets(buf, sizeof(buf), fp) != 0) {
        line++;

        const char* p = buf;
        string name, lv[3];
        KeyType type = UNDEF_KEY;
        Key* key;

        p = skip_space(p);
        if (!*p || *p == '#') {
            // TRACE("(%d): comment: %s\n", line, buf);
            continue;
        }
        else if (!strncasecmp(p, "end-define", 10))
            return;

        // key
        while (*p && !isspace(*p))
            name += *p++;

        p = skip_space(p);

        // char
        for (int i = 0; i < 3; i++) {
            if (!*p || *p == '#')
                break;
            else if (*p == '\"') {
                if (type == UNDEF_KEY || type == GRAPHIC_KEY)
                    type = GRAPHIC_KEY;
                else {
                    QsTRACE(1, "(%d): type conflict", line);
                    goto cont;
                }
                p++;
                while (*p && *p != '\"')
                    lv[i] += *p++;
                if (!*p)
                    break;
                p = skip_space(p + 1);
            }
            else {
                if (type == UNDEF_KEY || type == FUNCTION_KEY)
                    type = FUNCTION_KEY;
                else {
                    QsTRACE(1, "(%d): type conflict", line);
                    goto cont;
                }
                while (*p && !isspace(*p))
                    lv[i] += *p++;
                p = skip_space(p);
            }
        }
#if DEBUG >= 2
        QsTRACE(1, "line = %s %s %s %s\n", name.c_str(),
                    lv[0].c_str(), lv[1].c_str(), lv[2].c_str());
#endif
        key = NULL;
        if (type == GRAPHIC_KEY)
            key = new GraphicKey(name, lv);
        else if (type == FUNCTION_KEY)
            key = createFuncKey(name, lv);
        if (key) {
            // v1.0.1 キー配列定義は，共通部分を別ファイルにしてimportできるようにした。
            // 重複が生じたときは，overrideして最後に現れた定義を使うようにするのがよい。
            KeyList::iterator i = keyList.find(key);
            if (i != keyList.end())
                keyList.erase(i);
            keyList.insert(key);
        }

    cont:
        ;
    }
}

bool KeyMap::load(const string& filename)
{
    static const char* BEGIN_FUNC = "begin-function";
    static const char* BEGIN_KEY = "begin-key";
    static const char* IMPORT = "import";

    FILE* fp = fopen(filename.c_str(), "r");
    if (!fp)
        return false;

    int line = 0;
    char buf[1000];
    while (fgets(buf, sizeof(buf), fp) != 0) {
        line++;
        const char* p = buf;
        p = skip_space(p);
        if (!strncasecmp(p, BEGIN_FUNC, strlen(BEGIN_FUNC)))
            loadFunc(fp, line);
        else if (!strncasecmp(p, BEGIN_KEY, strlen(BEGIN_KEY)))
            loadKey(fp, line);
        else if (!strncasecmp(p, IMPORT, strlen(IMPORT))) {
            p = skip_space(p + strlen(IMPORT));
            string file;
            while (!isspace(*p))
                file += *p++;
            file = FileName(filename).resolve(FileName(file)).toString();
            if (!load(file)) {
                error("'%s'の読み込みに失敗\n", file.c_str());
                return false;
            }
        }
    }

    fclose(fp);
    return true;
}

void KeyMap::saveKeyList(FILE* fp) const
{
    fprintf(fp, "\nbegin-key\n");

    KeyList::const_iterator i;
    for (i = keyList.begin(); i != keyList.end(); i++) {
        switch ((*i)->getType())
        {
        case GRAPHIC_KEY:
            {
                GraphicKey* p = dynamic_cast<GraphicKey*>(*i);
                fprintf(fp, "\t%s   \"%s\"   \"%s\"   \"%s\"\n",
                            p->name.c_str(),
                            p->chars[0].c_str(),
                            p->chars[1].c_str(),
                            p->chars[2].c_str());
            }
            break;
        case FUNCTION_KEY:
            {
                FunctionKey* p = dynamic_cast<FunctionKey*>(*i);
                string line = string("\t") + p->name;
                for (int i = 0; i < 3; i++) {
                    if (p->func[i])
                        line += string("\t") + p->func[i]->name.c_str();
                    // TODO: 左シフトが未定義かつ右シフトが定義されているとき，保存するとずれる
                }
                fprintf(fp, "%s\n", line.c_str());
            }
            break;
        default:
            assert(0);
        }
    }

    fprintf(fp, "end-define\n");
}

void KeyMap::saveFuncList(FILE* fp) const
{
    fprintf(fp, "begin-function\n");

    FuncList::iterator i;
    for (i = funcList.begin(); i != funcList.end(); i++) {
        string line = string("\t") + (*i)->name;
        for (int s = 0; s < COUNT_IME_STAT; s++)
            line += string("\t") + find_piece_name((*i)->piece[s]);
        fprintf(fp, "%s\n", line.c_str());
    }

    fprintf(fp, "end-define\n");
}

bool KeyMap::save(const char* filename) const
{
    FILE* fp = fopen(filename, "w");
    if (!fp) {
        perror("keymap.save()");
        return false;
    }
    fprintf(fp, "# " APP_NAME " 配列定義ファイル\n\n");

    saveFuncList(fp);
    saveKeyList(fp);

    fclose(fp);
    return true;
}

const Key* KeyMap::find_key(const string& name) const
{
    KeyList::const_iterator i;
    for (i = keyList.begin(); i != keyList.end(); i++) {
        if ((*i)->name == name)
            return *i;
    }
    return NULL;
}

const FuncNameIdPair funcNameId[] =
                                    // 未入力，入力中，全変換，候補一覧，
{
    { "through",        CF_THROUGH,     { true, true, true, true } },
    { "na",             CF_NA,          { true, true, true, true } },
    { "kanji",          CF_KANJI,       { true, true, true, true } },
    { "space",          CF_SPACE,       { true, false, false, false } },
    { "convert",        CF_CONVERT,     { false, true, false, false } },
    { "next-cand",      CF_NEXT_CAND,   { false, false, true, true } },
    { "prev-cand",      CF_PREV_CAND,   { false, false, true, true } },
    { "caret-left",     CF_CARET_LEFT,  { false, true, false, false } },
    { "shrink",         CF_SHRINK,      { false, false, true, true } },
    { "caret-right",    CF_CARET_RIGHT, { false, true, false, false } },
    { "expand",         CF_EXPAND,      { false, false, true, true } },
    { "clause-left",    CF_CLAUSE_LEFT, { false, false, true, true } },
    { "clause-right",   CF_CLAUSE_RIGHT, { false, false, true, true } },
    { "all-determine",  CF_ALL_DETERMINE, { false, true, true, true } },
    { "clear",          CF_CLEAR,       { false, true, true, true } },
    { "left-erase",     CF_LEFT_ERASE,  { false, true, false, false } },
    { "right-erase",    CF_RIGHT_ERASE, { false, true, false, false } },
    { "revert",         CF_REVERT,      { false, false, true, true } },
    { "head",           CF_HEAD,        { false, true, true, true } },
    { "tail",           CF_TAIL,        { false, true, true, true } },
    { "clause-determine", CF_CLAUSE_DET, { false, false, true, true } },
    { "fixed-alnum",    CF_FIXED_ALNUM, { true, true, false, false } },
    { 0, CF_ERROR }
};

FuncPieceId find_piece_id(const string& piece_name, int ime)
{
    for (int i = 0; funcNameId[i].name; i++) {
        if (piece_name == funcNameId[i].name) {
            if (funcNameId[i].canUse[ime])
                return funcNameId[i].id;
            else
                return CF_ERROR;
        }
    }
    return CF_ERROR;
}

const char* find_piece_name(FuncPieceId id)
{
    for (int i = 0; funcNameId[i].name; i++) {
        if (id == funcNameId[i].id)
            return funcNameId[i].name;
    }
    return "";
}

void KeyMap::remove_key(const string& key_name)
{
    KeyList::iterator i;
    for (i = keyList.begin(); i != keyList.end(); i++) {
        if ((*i)->name == key_name) {
            keyList.erase(i);
            return;
        }
    }
}
