// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef NICOLATTER_CLIENT_H
#define NICOLATTER_CLIENT_H

#ifdef USE_GTK
  #include <gdk/gdk.h> // GdkInputCondition
#else
  #include <X11/Intrinsic.h>
#endif

#include <misc.h>

//////////////////////////////////////////////////////////////////////
// InputContext

class KanaKanjiConv;
class KeyEvent;
class InputMethod;
class InputContext
{
public:
    XIMStyle inputStyle;
    Window clientWindow;
    Window focusWindow;

    bool ext_event_mask;
    XPoint position;    // フォーカスウィンドウの左上のスクリーン座標

    KanaKanjiConv* kanaKanji;
    class PreeditWindow* preeditWindow; // TODO: WnnConv/CannaConvへ移動
    class StatusWindow* statusWindow; // TODO: WnnConv/CannaConvへ移動
    bool disabled;

private:
    int id;
    class InputMethod* im_;
    static InputContext* focused;
    static int next_icid;

public:
    InputContext(InputMethod* im);
    virtual ~InputContext();
    void initialize();

    void processEvent(const KeyEvent& event);
    // 入力: キーのsequence
    void input(const KeyEvent& event, int level);
    // 入力: LevelSelectorによってキー/Levelに分離されたデータ

    int getId() const;
    InputMethod* getIM() const;
    
    void updateView();
    void updateFocusPosition();
    void setFocus(bool f);
    bool isFocused() const;

private:
    InputContext(const InputContext& ); // not impl.
    InputContext& operator = (const InputContext& ); // not impl.
};

//////////////////////////////////////////////////////////////////////
// InputMethod

class Connection;
class InputMethod
{
public:
    typedef ptr_vector<InputContext*> InputContexts;
    InputContexts contexts;
private:
    int id;
    Connection* client;
    static int next_imid;
    
public:
    InputMethod(Connection* conn);
    virtual ~InputMethod();
    InputContext* createIC();
    InputContext* getIC(int id) const;
    Connection* getConnection() const;
    void remove_ic(int icid);
    int getId() const;

private:
    InputMethod(const InputMethod& ); // not impl.
    InputMethod& operator = (const InputMethod& ); // not impl.
};

#endif // NICOLATTER_CLIENT_H
