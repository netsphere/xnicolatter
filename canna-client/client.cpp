
#define CANNA_NEW_WCHAR_AWARE

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <canna/jrkanji.h>
#include "common.h"

int main()
{
    unsigned char** warning = NULL;
    char utf_buf[1000];

    // 初期化。Context は 0 固定.
    int r;
    if ( (r = jrKanjiControl(0, KC_INITIALIZE, (char*) &warning)) == -1) {
        printf("Cannaライブラリの初期化に失敗: %s\n", jrKanjiError);
        return 1;
    }
    printf("ret = %d\n", r);
    if (warning) {
      for (unsigned char** p = warning; *p; p++) {
        euc_to_utf8(utf_buf, *p, 1000);  // メッセージはEUC-JP!
        printf("Warning: %s\n", utf_buf);
      }
      return 1;
    }

    int context = rand();

    // モード設定
    jrKanjiStatus status;
    constexpr int BUF_SIZE = 1000;
    unsigned char work_buf[BUF_SIZE];

    jrKanjiStatusWithValue ksv;
    ksv.ks = &status;
    ksv.buffer = work_buf;
    ksv.bytes_buffer = BUF_SIZE;
    ksv.val = CANNA_MODE_HenkanMode;
    jrKanjiControl(context, KC_CHANGEMODE, (char*) &ksv);

    // 入力を渡していく
    int ch ;
    while ( (ch = getchar()) != EOF) {
        int result = jrKanjiString(context, ch,
                                   (char*) work_buf,
                               BUF_SIZE, // バイト数ではなく要素数
                               &status);
        if (result > 0) {
            work_buf[result] = '\0';
            //cannawcs_to_utf8(utf_buf, work_buf, 1000);
            euc_to_utf8(utf_buf, work_buf, 1000);
            printf("%s", utf_buf);
        }
    }
  
    jrKanjiControl(0, KC_FINALIZE, 0);
    return 0;
}
