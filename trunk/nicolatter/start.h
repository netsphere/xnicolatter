// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.
//   email: <mailto:vzw00011@nifty.ne.jp>
//   website: <http://www2.airnet.ne.jp/pak04955/>

#ifndef NICOLATTER_START_H
#define NICOLATTER_START_H

#ifdef USE_GTK
#include <gtk/gtkwidget.h>
extern GtkWidget* top_widget;
#else
extern XtAppContext app;
extern Widget top_widget;
#endif

extern Display* top_display;
  // keymap.cc�����

#endif // NICOLATTER_START_H
