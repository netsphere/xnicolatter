// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// XIMプロトコル；トランスボート層は含まない

#include "../config.h"

#include <cassert>
#include <string>
#include <vector>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>

#include "XimProto.h"
#include "ximtrans.h"
#include "ximdisp.h"
#include "preedit.h"
#include "client.h"
#include "Connection.h"
#include "status.h"
#include "conv.h"
#include "../global.h"
#include <misc.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// IM/IC属性値

struct ValueTypePair
{
    int id;
    const char* name;
    int type;           // 型番号
};

enum {
    IM_QUERY_INPUT_STYLE = 1,

    IC_INPUT_STYLE,
    IC_CLIENT_WINDOW,
    IC_FOCUS_WINDOW,
    IC_FILTER_EVENTS,
    IC_STRING_CONVERSION,
    IC_HOT_KEY,
    IC_HOT_KEY_STATE,
    IC_PREEDIT_ATTRIBUTES,
    IC_STATUS_ATTRIBUTES,
    IC_AREA,
    IC_AREA_NEEDED,
    IC_SPOT_LOCATION,
    IC_COLORMAP,
    IC_STD_COLORMAP,
    IC_FOREGROUND,
    IC_BACKGROUND,
    IC_BACKGROUND_PIXMAP,
    IC_FONT_SET,
    IC_LINE_SPACE,
    IC_CURSOR,
    IC_SEPARATOR_OF_NESTED_LIST
};

const ValueTypePair im_attrs[] = {
    { IM_QUERY_INPUT_STYLE, XNQueryInputStyle, XimType_XIMStyles },
    { 0, NULL, 0 }
};

const ValueTypePair ic_attrs[] = {
    { IC_INPUT_STYLE,           XNInputStyle,         XimType_CARD32 },
    { IC_CLIENT_WINDOW,         XNClientWindow,       XimType_Window },
    { IC_FOCUS_WINDOW,          XNFocusWindow,        XimType_Window },
    { IC_FILTER_EVENTS,         XNFilterEvents,       XimType_CARD32 },
/*
    1999.06.17 やっぱりgtk+アプリのみ起動できない

    1999.03.02
        XNFilterEventsを使うとgtk+アプリが起動できないが，原因が全く分からないので，
        使わないようにした。
        XIM_EXT_SET_EVENT_MASKがあるので，特に支障はないが，
            XGetICValues(ic, XNFilterEvents, &im_event_mask, 0);
            XSelectInput(disp, window, im_event_mask);
        のように，XNFilterEventsの結果だけをmaskに使うアプリで入力ができなくなるだろう。
*/

/*
    { IC_STRING_CONVERSION,     XNStringConversion,
                XimType_XIMStringConversion },  // kinput2, xwnmo don't have
    { IC_HOT_KEY,               XNHotKey,
                XimType_XIMHotKeyTriggers },    // kinput2, xwnmo don't have
    { IC_HOT_KEY_STATE,         XNHotKeyState,
                XimType_XIMHotKeyState },   // kinput2, xwnmo don't have
*/
    { IC_PREEDIT_ATTRIBUTES,    XNPreeditAttributes,  XimType_NEST },
    { IC_STATUS_ATTRIBUTES,     XNStatusAttributes,   XimType_NEST },
    { IC_AREA,                  XNArea,               XimType_XRectangle },
    { IC_AREA_NEEDED,           XNAreaNeeded,         XimType_XRectangle },
    { IC_SPOT_LOCATION,         XNSpotLocation,       XimType_XPoint },
    { IC_COLORMAP,              XNColormap,           XimType_CARD32 },
    { IC_STD_COLORMAP,          XNStdColormap,      XimType_CARD32 },
    { IC_FOREGROUND,            XNForeground,         XimType_CARD32 },
    { IC_BACKGROUND,            XNBackground,         XimType_CARD32 },
    { IC_BACKGROUND_PIXMAP,     XNBackgroundPixmap,   XimType_CARD32 },
    { IC_FONT_SET,              XNFontSet,            XimType_XFontSet },
    { IC_LINE_SPACE,            XNLineSpace,          XimType_CARD32 }, // xc/lib/X11/imRm.c, xwnmo
    // { IC_LINE_SPACE,            XNLineSpace,          XimType_CARD16 }, // kinput2
    { IC_CURSOR,                XNCursor,             XimType_CARD32 },
    { IC_SEPARATOR_OF_NESTED_LIST,
                XNSeparatorofNestedList, XimType_SeparatorOfNestedList },
    { 0, NULL, 0 }
};

const int supported_preedit[] = {
    XIMPreeditArea, // XIMPreeditCallbacks,
    XIMPreeditPosition, XIMPreeditNothing, 0 };
const int supported_status[] = {
    XIMStatusArea, /* XIMStatusCallbacks, */ XIMStatusNothing, 0 };

//////////////////////////////////////////////////////////////////////
// XIM拡張

enum { XIM_EXT_SET_EVENT_MASK = 129 };

struct CodeNamePair
{
    int code;
    const char* name;
};

const CodeNamePair extensions[] = {
    { XIM_EXT_SET_EVENT_MASK, "XIM_EXT_SET_EVENT_MASK" },
    { -1, 0 }
};

//////////////////////////////////////////////////////////////////////
// エラー名

const CodeNamePair errors[] = {
    { XIM_BadAlloc,         "BadAlloc" },
    { XIM_BadStyle,         "BadStyle" },
    { XIM_BadClientWindow,  "BadClientWindow" },
    { XIM_BadFocusWindow,   "BadFocusWindow" },
    { XIM_BadArea,          "BadArea" },
    { XIM_BadSpotLocation,  "BadSpotLocation" },
    { XIM_BadColormap,      "BadColormap" },
    { XIM_BadAtom,          "BadAtom" },
    { XIM_BadPixel,         "BadPixel" },
    { XIM_BadPixmap,        "BadPixmap" },
    { XIM_BadName,          "BadName" },
    { XIM_BadCursor,        "BadCursor" },
    { XIM_BadProtocol,      "BadProtocol" },
    { XIM_BadForeground,    "BadForeground" },
    { XIM_BadBackground,    "BadBackground" },
    { XIM_LocaleNotSupported,   "LocaleNotSupported" },
    { XIM_BadSomething,     "BadSomething" },
    { -1, 0 }
};

//////////////////////////////////////////////////////////////////////

void sendError(Connection* conn, XIMID im, XICID ic,
               int err_code, const char* str)
{
    error("client error: im = %d, ic = %d, error code = %d\n",
          im, ic, err_code);

    XimWriter* imstr = conn->createXimWriter(XIM_ERROR);
    imstr->card16(im);
    imstr->card16(ic);
    int flag = 0;
    if (im)
        flag |= XIM_IMID_VALID;
    if (ic)
        flag |= XIM_ICID_VALID;
    imstr->card16(flag);
    imstr->card16(err_code);
    
    imstr->card16(str ? strlen(str) : 0);
    imstr->card16(0);  // reserved
    if (str) {
        while (*str) {
            imstr->card8(*str);
            str++;
        }
    }
    conn->send_queue.push_back(imstr);
}

static bool ximUnexpected(Connection* conn, XimReader*)
{
    sendError(conn, 0, 0, XIM_BadProtocol, "Unexpected opcode");
    return true;
}

static bool ximNotImplemented(Connection* conn, XimReader*)
{
    sendError(conn, 0, 0, XIM_BadProtocol, "Not Implemented");
    return true;
}

static bool ximConnect(Connection* conn, XimReader*)
    // 認証とか使わないので，そのままREPLYを返す。
    // X11/imDefIm.c: _XimConnection()
    //              _XimAllRecv()
{
    XimWriter* imstr = conn->createXimWriter(XIM_CONNECT_REPLY);
    imstr->card16(PROTOCOLMAJORVERSION);
    imstr->card16(PROTOCOLMINORVERSION);
    conn->send_queue.push_back(imstr);

    return false;
}

static void storeListOfXicAttr(XimWriter& imstr, const ValueTypePair* attrs)
    // LISTofXICATTR
    // X11/imRmAttr.c: _XimGetAttributeID()
{
    // 1999.07.14 preedit, statusをネストさせたりしない
    for (int i = 0; attrs[i].name; i++) {
        imstr.card16(attrs[i].id);
        imstr.card16(attrs[i].type);
        imstr.str16(attrs[i].name);
        imstr.pad();
    }
}

static int forwardMask = KeyPressMask | KeyReleaseMask;

static bool ximOpen(Connection* conn, XimReader*)
    // "ja"が来る
    // imDefIm.c: _XimOpen()
    //          _XimOpenCheck()
{
    InputMethod* im = conn->createIM();
    assert(im);

    XimWriter* imstr = conn->createXimWriter(XIM_OPEN_REPLY);
    imstr->card16(im->getId());

    // LISTofXIMATTR
    imstr->reserve16();  // length of IM attributes
    for (int i = 0; im_attrs[i].name; i++) {
        imstr->card16(im_attrs[i].id);
        imstr->card16(im_attrs[i].type);
        imstr->str16(im_attrs[i].name);
        imstr->pad();
    }
    imstr->store_back(imstr->len_from_reserved());

    imstr->reserve16();  // length
    imstr->card16(0);    // unused
    storeListOfXicAttr(*imstr, ic_attrs);
    int at_len = imstr->len_from_reserved() - 2;
    // TRACE("at_len = %d\n", at_len);
    imstr->store_back(at_len);
    conn->send_queue.push_back(imstr);

    // フォワードしてもらうイベントを登録する。
    imstr = conn->createXimWriter(XIM_SET_EVENT_MASK);
    imstr->card16(im->getId());
    TRACE("create im: %d\n", im->getId());
    imstr->card16(0);
            // IC共通 imDefLkup.c: _XimSetEventMaskCallback()
    imstr->card32(forwardMask);
    imstr->card32(forwardMask);
    conn->send_queue.push_back(imstr);

    return false;
}

static bool ximDisconnect(Connection* conn, XimReader* input)
    // imDefIm.c: _XimDisconnect()
{
/*
    Connection* conn = find_client(fd);
    clients.erase(conn);
        1999.05.05 onReceiveConnection()の方でdeleteする
*/
    XimWriter* imstr = conn->createXimWriter(XIM_DISCONNECT_REPLY);
    conn->send_queue.push_back(imstr);

    return false;
}

static bool ximError(Connection* conn, XimReader* input)
{
    XIMID imid = input->card16();
    XICID icid = input->card16();
    int flag = input->card16();
    int code = input->card16();

    error("XIM_ERROR: ");
    if (flag & XIM_IMID_VALID)
        error("im = %d, ", imid);
    else
        error("im = (invalid), ");
    if (flag & XIM_ICID_VALID)
        error("ic = %d ", icid);
    else
        error("ic = (invalid) ");

    for (int i = 0; errors[i].name; i++) {
        if (errors[i].code == code) {
            error("%s\n", errors[i].name);
            break;
        }
    }
    int detail_len = input->card16();
    if (detail_len) {
        input->card16();        // type
        string s = input->str8();
        error("detail: %s\n", s.c_str());
    }

    return true;
}

static bool ximClose(Connection* conn, XimReader* input)
    // imDefIm.c: _XimClose()
{
    XIMID imid = input->card16();
    conn->remove_im(imid);

    XimWriter* imstr = conn->createXimWriter(XIM_CLOSE_REPLY);
    imstr->card16(imid);
    conn->send_queue.push_back(imstr);

    return false;
}

////////////////////////////////////////////////////////////////////////

static bool ximQueryExtension(Connection* conn, InputMethod* im,
                              XimReader* input)
    // imExten.c: _XimExtension()
    //          _XimQueryExtensionCheck()
{
    int size = input->card16();

    XimWriter* imstr = conn->createXimWriter(XIM_QUERY_EXTENSION_REPLY);
    imstr->card16(im->getId());
    imstr->reserve16();

    while (size > 0) {
        string name = input->str8();
        size -= name.size() + 1;
        for (int i = 0; extensions[i].name; i++) {
            if (name == extensions[i].name) {
                // TRACE("support extension: %s\n", extensions[i].name);
                imstr->card8(extensions[i].code);
                imstr->card8(0);
                imstr->str16(extensions[i].name);
                imstr->pad();
            }
        }
    }

    imstr->store_back(imstr->len_from_reserved());
    conn->send_queue.push_back(imstr);

    return false;
}

static bool ximEncodingNegotiation(Connection* conn, InputMethod* im,
                                   XimReader* input)
    // imDefIm.c: _XimEncodingNegotiation()
    //          _XimEncodingNegoCheck()
{
    typedef vector<string> Encodings;
    Encodings encodings;

    int len = input->card16();
    while (len > 0) {
        string s = input->str8();
        // TRACE("encoding: %s\n", s.c_str());
        encodings.push_back(s);
        len -= s.size() + 1;
    }

    // Xlibとの通信にはCOMPOUND_TEXTを使う。
    // eucJPも選択肢に入っているが，うまく通信できないみたい。
    int idx = 0;
    Encodings::const_iterator i;
    for (i = encodings.begin(); i != encodings.end(); i++, idx++) {
        if (*i == "COMPOUND_TEXT")
            break;
    }
    if (i == encodings.end()) {
        sendError(conn, im->getId(), 0, XIM_BadProtocol,
                  "Unsupported encoding");
    }
    else {
        XimWriter* imstr = conn->createXimWriter(
            XIM_ENCODING_NEGOTIATION_REPLY);
        imstr->card16(im->getId());
        imstr->card16(XIM_Encoding_NameCategory);
        imstr->card16(idx);
        conn->send_queue.push_back(imstr);
    }
    
    return false;
}

static bool ximGetImValues(Connection* conn, InputMethod* im, XimReader* input)
    // imDefIm.c: _XimProtoGetIMValues()
    //          _XimGetIMValuesCheck()
{
    int size = input->card16() / 2;
    vector<int> imattrs;
    int i;
    for (i = 0; i < size; i++)
        imattrs.push_back(input->card16());
    for (i = 0; i < size; i++) {
        switch (imattrs[i])
        {
        case IM_QUERY_INPUT_STYLE:
            break;
        default:
            assert(0);
        }
    }

    XimWriter* imstr = conn->createXimWriter(XIM_GET_IM_VALUES_REPLY);
    imstr->card16(im->getId());
    imstr->reserve16();      // length
    imstr->card16(IM_QUERY_INPUT_STYLE);
    imstr->reserve16();
    imstr->reserve16();
    imstr->card16(0);    // unused

    for (i = 0; supported_preedit[i]; i++) {
        for (int j = 0; supported_status[j]; j++)
            imstr->card32(supported_preedit[i] | supported_status[j]);
    }
    imstr->store_back((imstr->len_from_reserved() - 2) / 4);
    imstr->store_back(imstr->len_from_reserved());
    imstr->pad();
    imstr->store_back(imstr->len_from_reserved());
    conn->send_queue.push_back(imstr);

    return false;
}

static bool setPreeditAttributes(InputContext* ic, XimReader* input, int len)
    // 戻り値
    //     エラーが発生したとき false
{
    while (len > 0) {
        int attr = input->card16();
        // TRACE("  setPreeditAttributes(): %d\n", attr);
        int len2 = input->card16();
        switch (attr)
        {
        case IC_AREA:
            ic->preeditWindow->area.x = input->card16();
            ic->preeditWindow->area.y = input->card16();
            ic->preeditWindow->area.width = input->card16();
            ic->preeditWindow->area.height = input->card16();
#if DEBUG > 1
            TRACE("%s: x = %d, y = %d, w = %d, h = %d\n",
                    XNArea, ic->preeditWindow->area.x, ic->preeditWindow->area.y,
                    ic->preeditWindow->area.width, ic->preeditWindow->area.height);
#endif
            break;
        case IC_AREA_NEEDED:
            // TRACE("%s\n", XNAreaNeeded);
            ic->preeditWindow->areaNeeded.x = input->card16();
            ic->preeditWindow->areaNeeded.y = input->card16();
            ic->preeditWindow->areaNeeded.width = input->card16();
            ic->preeditWindow->areaNeeded.height = input->card16();
            break;
        case IC_SPOT_LOCATION:
            ic->preeditWindow->spot.x = input->card16();
            ic->preeditWindow->spot.y = input->card16();
#if DEBUG > 1
            TRACE("%s: x = %d, y = %d\n", XNSpotLocation,
                            ic->preeditWindow->spot.x, ic->preeditWindow->spot.y);
#endif
            break;
        case IC_COLORMAP:
            TRACE("%s\n", XNColormap);
            ic->preeditWindow->colormap = input->card32();
            break;
        case IC_FOREGROUND:
            // TRACE("%s\n", XNForeground);
            ic->preeditWindow->foreground = input->card32();
            break;
        case IC_BACKGROUND:
            // TRACE("%s\n", XNBackground);
            ic->preeditWindow->background = input->card32();
            break;
        case IC_BACKGROUND_PIXMAP:
            TRACE("%s\n", XNBackgroundPixmap);
            ic->preeditWindow->backgroundPixmap = input->card32();
            break;
        case IC_FONT_SET:
            ic->preeditWindow->fontName = input->str16();
            // input->pad();
            // XNFontSetはこのpad込みの長さのはずだが，XFree86では含まれていない。
            // TRACE("%s: %s\n", XNFontSet, ic->preeditWindow->fontName.c_str());
            ic->preeditWindow->updateFont();
            break;
        case IC_LINE_SPACE:
            // TRACE("%s\n", XNLineSpace);
            ic->preeditWindow->lineSpace = input->card32();
            break;
        case IC_CURSOR:
            TRACE("%s\n", XNCursor);
            ic->preeditWindow->cursor = input->card32();
            break;
        default:
            TRACE("invalid preedit ic attr: %d\n", attr);
            return false;
        }
        len -= 4 + len2;
        len -= input->pad();
    }
    return true;
}

static bool setStatusAttributes(InputContext* ic, XimReader* input, int len)
    // 戻り値
    //     エラーが発生したとき false
{
    while (len > 0) {
        int attr = input->card16();
        // TRACE("  setStatusAttributes(): %d\n", attr);
        int len2 = input->card16();
        switch (attr)
        {
        case IC_AREA:
            ic->statusWindow->area.x = input->card16();
            ic->statusWindow->area.y = input->card16();
            ic->statusWindow->area.width = input->card16();
            ic->statusWindow->area.height = input->card16();
#if DEBUG > 1
            TRACE("IC_AREA: x = %d, y = %d, w = %d, h = %d\n",
                ic->statusWindow->area.x,
                ic->statusWindow->area.y,
                ic->statusWindow->area.width,
                ic->statusWindow->area.height);
#endif
            break;
        case IC_AREA_NEEDED:
            ic->statusWindow->areaNeeded.x = input->card16();
            ic->statusWindow->areaNeeded.y = input->card16();
            ic->statusWindow->areaNeeded.width = input->card16();
            ic->statusWindow->areaNeeded.height = input->card16();
#if DEBUG > 1
            TRACE("IC_AREA_NEEDED: x = %d, y = %d, w = %d, h = %d\n",
                ic->statusWindow->areaNeeded.x,
                ic->statusWindow->areaNeeded.y,
                ic->statusWindow->areaNeeded.width,
                ic->statusWindow->areaNeeded.height);
#endif
            break;
        case IC_COLORMAP:
            ic->statusWindow->colormap = input->card32();
            break;
        case IC_FOREGROUND:
            ic->statusWindow->foreground = input->card32();
            break;
        case IC_BACKGROUND:
            ic->statusWindow->background = input->card32();
            break;
        case IC_BACKGROUND_PIXMAP:
            ic->statusWindow->backgroundPixmap = input->card32();
            break;
        case IC_FONT_SET:
            ic->statusWindow->fontName = input->str16();
            break;
        case IC_LINE_SPACE:
            ic->statusWindow->lineSpace = input->card32();
            break;
        case IC_CURSOR:
            ic->statusWindow->cursor = input->card32();
            break;
        default:
            TRACE("status attr = %d\n", attr);
            return false;
        }
        len -= 4 + len2;
        len -= input->pad();
    }
    return true;
}

#ifdef DEBUG 
void printStyle(int style)
{
    string s;
    if (style & XIMPreeditArea)
        s += "XIMPreeditArea ";
    if (style & XIMPreeditCallbacks)
        s += "XIMPreeditCallbacks ";
    if (style & XIMPreeditPosition)
        s += "XIMPreeditPosition ";
    if (style & XIMPreeditNothing)
        s += "XIMPreeditNothing ";
    if (style & XIMPreeditNone)
        s += "XIMPreeditNone ";

    if (style & XIMStatusArea)
        s += "XIMStatusArea ";
    if (style & XIMStatusCallbacks)
        s += "XIMStatusCallbacks ";
    if (style & XIMStatusNothing)
        s += "XIMStatusNothing ";
    if (style & XIMStatusNone)
        s += "XIMStatusNone ";

    TRACE("input-style: %s\n", s.c_str());
}
#endif

static bool setIcAttributes(InputContext* ic, XimReader* input, int len,
                     bool isCreate)
    // 戻り値
    //     エラーが発生したとき false
{
    assert(ic);
    assert(input);
    
    bool to_preedit_update = false;
    bool to_status_update = false;
    
    XICID icid = isCreate ? 0 : ic->getId();
    
    while (len > 0) {
        int attr = input->card16();
        // TRACE("setIcAttributes(): %d\n", attr);
        int len2 = input->card16(); // value length
        switch (attr)
        {
        case IC_INPUT_STYLE:
            {
                assert(!ic->inputStyle);
                XIMStyle style = input->card32();
                int i = 0;
                for (i = 0; supported_preedit[i]; i++) {
                    if ((style & supported_preedit[i]) != 0) {
                        ic->inputStyle = supported_preedit[i];
                        break;
                    }
                }
                if (!ic->inputStyle) {
                    error("unsupported preedit style: %d\n", style);
                    sendError(ic->getIM()->getConnection(),
                              ic->getIM()->getId(), icid,
                              XIM_BadStyle, "unsupported preedit style");
                    return false;
                }
                for (i = 0; supported_status[i]; i++) {
                    if ((style & supported_status[i]) != 0) {
                        ic->inputStyle = style;
                        break;
                    }
                }
                if (ic->inputStyle != style) {
                    error("unsupported status style: %d", style);
                    sendError(ic->getIM()->getConnection(),
                              ic->getIM()->getId(), icid,
                              XIM_BadStyle, "unsupported status style");
                    return false;
                }
#ifdef DEBUG
                printStyle(ic->inputStyle);
#endif
            }
            break;
        case IC_CLIENT_WINDOW:
            // TRACE("%s\n", XNClientWindow);
            if (ic->clientWindow) {
                // XNClientWindowは一度しか指定できない
                sendError(ic->getIM()->getConnection(),
                          ic->getIM()->getId(), icid,
                          XIM_BadClientWindow,
                          "clientWindow already set");
                return false;
            }
            ic->clientWindow = input->card32();
            break;
        case IC_FOCUS_WINDOW:
            // TRACE("%s\n", XNFocusWindow);
            ic->focusWindow = input->card32();
            to_preedit_update = true;
            to_status_update = true;
            break;
        case IC_FILTER_EVENTS:
            // TRACE("%s\n", XNFilterEvents);
            // 読み取り専用
            sendError(ic->getIM()->getConnection(),
                      ic->getIM()->getId(), icid,
                      XIM_BadProtocol, "filterEvents read only");
            return false;

        case IC_STRING_CONVERSION:
            TRACE("%s\n", XNStringConversion);
            assert(0);  // TODO: implement
            break;
        case IC_HOT_KEY:
            TRACE("%s\n", XNHotKey);
            assert(0); // TODO: implement
            break;
        case IC_HOT_KEY_STATE:
            TRACE("%s\n", XNHotKeyState);
            assert(0); // TODO: implement
            break;
        case IC_PREEDIT_ATTRIBUTES:
            // TRACE("%s\n", XNPreeditAttributes);
            if (!setPreeditAttributes(ic, input, len2)) {
                sendError(ic->getIM()->getConnection(),
                          ic->getIM()->getId(), icid,
                          XIM_BadProtocol,
                          "Unsupported preedit attribute");
                return false;
            }
            to_preedit_update = true;
            break;
        case IC_STATUS_ATTRIBUTES:
            // TRACE("%s\n", XNStatusAttributes);
            if (!setStatusAttributes(ic, input, len2)) {
                sendError(ic->getIM()->getConnection(),
                          ic->getIM()->getId(), icid,
                          XIM_BadProtocol,
                          "Unsupported status attribute");
                return false;
            }
            to_status_update = true;
            break;
        default:
            error("invalid IC attribute: %d\n", attr);
            sendError(ic->getIM()->getConnection(),
                      ic->getIM()->getId(), icid,
                      XIM_BadProtocol,
                      "invalid IC attribute");
            return false;
        }
        len -= 4 + len2;
        len -= input->pad();
    }

    if (ic->isFocused() && (to_preedit_update || to_status_update))
        ic->updateView();
    return true;
}

static bool ximCreateIc(Connection* conn, InputMethod* im, XimReader* input)
    // imDefIc.c: _XimReCreateIC()
    // imDefIc.c: _XimProtoCreateIC()
    //          _XimCreateICCheck()
{
    InputContext* ic = im->createIC();
    assert(ic);
    
    int len = input->card16();
    if (!setIcAttributes(ic, input, len, true)) {
        // 属性値がおかしい。setIcAttributes()内部でsendError()済み
        im->remove_ic(ic->getId());
        return false;
    }
    if (!ic->inputStyle) {
        error("input-styleが指定されていない\n");
        sendError(conn, im->getId(), 0, XIM_BadStyle,
                  "Unsupported input style");
        im->remove_ic(ic->getId());
        return false;
    }
    
    XimWriter* imstr = conn->createXimWriter(XIM_CREATE_IC_REPLY);
    imstr->card16(im->getId());
    imstr->card16(ic->getId());
    conn->send_queue.push_back(imstr);

    return false;
}

static bool setImAttributes(InputMethod* im, XimReader* input, int len)
    // 戻り値
    //     エラーが生じたとき false
{
    assert(im);
    assert(input);
    
    while (len > 0) {
        int attr = input->card16();
        int len2 = input->card16();
        switch (attr)
        {
        case IM_QUERY_INPUT_STYLE:
            assert(0);
            break;
        default:
            sendError(im->getConnection(), im->getId(), 0,
                      XIM_BadProtocol, "Unsupported IM attribute");
            return false;
        }
        len -= 4 + len2;
        len -= input->pad();
    }
    return true;
}

static bool ximSetImValues(Connection* conn, InputMethod* im, XimReader* input)
    // imDefIm.c: _XimProtoSetIMValues()
    // imDefIm.c: _XimSendSavedIMValues()
{
    int len = input->card16();
    if (setImAttributes(im, input, len)) {
        XimWriter* imstr = conn->createXimWriter(XIM_SET_IM_VALUES_REPLY);
        imstr->card16(im->getId());
        imstr->card16(0);   // unused
        conn->send_queue.push_back(imstr);
    }
    
    return false;
}

static bool ximDestroyIc(Connection* conn, InputMethod* im, XimReader* input)
    // imDefIc.c: _XimProtoDestroyIC()
{
    XICID icid = input->card16();
    InputContext* ic = im->getIC(icid);
    assert(ic);

    if (ic->isFocused())
        ic->setFocus(false);

    im->remove_ic(icid);

    XimWriter* imstr = conn->createXimWriter(XIM_DESTROY_IC_REPLY);
    imstr->card16(im->getId());
    imstr->card16(icid);
    conn->send_queue.push_back(imstr);

    return false;
}

static int getPreeditAttributes(InputContext* ic, XimReader* input,
                                XimWriter& imstr)
    // 戻り値
    //      消費した入力バイト数
{
    int count = 0;
    while (true) {
        int attr = input->card16();
        count += 2;
        if (attr == IC_SEPARATOR_OF_NESTED_LIST)
            break;

        imstr.card16(attr);
        imstr.reserve16();

        switch (attr)
        {
        case IC_AREA:
            imstr.card16(ic->preeditWindow->area.x);
            imstr.card16(ic->preeditWindow->area.y);
            imstr.card16(ic->preeditWindow->area.width);
            imstr.card16(ic->preeditWindow->area.height);
            break;
        case IC_AREA_NEEDED:
            imstr.card16(ic->preeditWindow->areaNeeded.x);
            imstr.card16(ic->preeditWindow->areaNeeded.y);
            imstr.card16(ic->preeditWindow->size.width);
            imstr.card16(ic->preeditWindow->size.height);
/*
            imstr.card16(ic->preeditWindow->areaNeeded.width);
            imstr.card16(ic->preeditWindow->areaNeeded.height);
*/
            break;
        case IC_SPOT_LOCATION:
            imstr.card16(ic->preeditWindow->spot.x);
            imstr.card16(ic->preeditWindow->spot.y);
            break;
        case IC_COLORMAP:
            imstr.card32(ic->preeditWindow->colormap);
            break;
        case IC_STD_COLORMAP:
            imstr.card32(ic->preeditWindow->stdColormap);
            break;
        case IC_FOREGROUND:
            imstr.card32(ic->preeditWindow->foreground);
            break;
        case IC_BACKGROUND:
            imstr.card32(ic->preeditWindow->background);
            break;
        case IC_BACKGROUND_PIXMAP:
            imstr.card32(ic->preeditWindow->backgroundPixmap);
            break;
        case IC_FONT_SET:
            imstr.str16(ic->preeditWindow->fontName.c_str());
            break;
        case IC_LINE_SPACE:
            imstr.card32(ic->preeditWindow->lineSpace);
            break;
        case IC_CURSOR:
            imstr.card32(ic->preeditWindow->cursor);
            break;
        default:
            assert(0);
        }
        imstr.store_back(imstr.len_from_reserved());
        imstr.pad();
    }
    return count;
}

static int getStatusAttributes(InputContext* ic, XimReader* input,
                               XimWriter& imstr)
    // 戻り値
    //      消費した入力バイト数
{
    int count = 0;
    while (true) {
        int attr = input->card16();
        count += 2;
        if (attr == IC_SEPARATOR_OF_NESTED_LIST)
            break;

        imstr.card16(attr);
        imstr.reserve16();

        switch (attr)
        {
        case IC_AREA:
            imstr.card16(ic->statusWindow->area.x);
            imstr.card16(ic->statusWindow->area.y);
            imstr.card16(ic->statusWindow->area.width);
            imstr.card16(ic->statusWindow->area.height);
            break;
        case IC_AREA_NEEDED:
            imstr.card16(ic->statusWindow->areaNeeded.x);
            imstr.card16(ic->statusWindow->areaNeeded.y);
            imstr.card16(ic->statusWindow->size.width);
            imstr.card16(ic->statusWindow->size.height);
/*
            imstr.card16(ic->statusWindow->areaNeeded.width);
            imstr.card16(ic->statusWindow->areaNeeded.height);
*/
            break;
        case IC_COLORMAP:
            imstr.card32(ic->statusWindow->colormap);
            break;
        case IC_STD_COLORMAP:
            imstr.card32(ic->statusWindow->stdColormap);
            break;
        case IC_FOREGROUND:
            imstr.card32(ic->statusWindow->foreground);
            break;
        case IC_BACKGROUND:
            imstr.card32(ic->statusWindow->background);
            break;
        case IC_BACKGROUND_PIXMAP:
            imstr.card32(ic->statusWindow->backgroundPixmap);
            break;
        case IC_FONT_SET:
            imstr.str16(ic->statusWindow->fontName.c_str());
            break;
        case IC_LINE_SPACE:
            imstr.card32(ic->statusWindow->lineSpace);
            break;
        case IC_CURSOR:
            imstr.card32(ic->statusWindow->cursor);
            break;
        default:
            assert(0);
        }

        imstr.store_back(imstr.len_from_reserved());
        imstr.pad();
    }
    return count;
}

static bool ximGetIcValues(Connection* conn, InputContext* ic, XimReader* input)
    // X11/imDefIc.c: _XimProtoGetICValues()
{
    XimWriter* imstr = conn->createXimWriter(XIM_GET_IC_VALUES_REPLY);
    imstr->card16(ic->getIM()->getId());
    imstr->card16(ic->getId());
    imstr->reserve16();  // length
    imstr->card16(0);    // unused

    int len = input->card16();
    while (len > 0) {
        int id = input->card16();
        // TRACE("ximGetIcValues(): %d\n", id);
        imstr->card16(id);
        imstr->reserve16();  // length
        switch (id)
        {
        case IC_INPUT_STYLE:
            imstr->card32(ic->inputStyle);
            break;
        case IC_CLIENT_WINDOW:
            imstr->card32(ic->clientWindow);
            break;
        case IC_FOCUS_WINDOW:
            imstr->card32(ic->focusWindow);
            break;
        case IC_FILTER_EVENTS:     // XNFilterEvents
            imstr->card32(forwardMask);
            ic->ext_event_mask = true;
            break;
        case IC_STRING_CONVERSION:
            assert(0);
            break;
        case IC_HOT_KEY:
            assert(0);
            break;
        case IC_HOT_KEY_STATE:
            assert(0);
            break;
        case IC_PREEDIT_ATTRIBUTES:
            len -= getPreeditAttributes(ic, input, *imstr);
            break;
        case IC_STATUS_ATTRIBUTES:
            len -= getStatusAttributes(ic, input, *imstr);
            break;
        default:
            error("invalid IC attribute: %d\n", id);
            sendError(conn, ic->getIM()->getId(), ic->getId(), XIM_BadProtocol,
                      "invalid IC attribute");
            delete imstr;
            return false;
        }

        imstr->store_back(imstr->len_from_reserved());
        imstr->pad();
        len -= 2;
    }

    imstr->store_back(imstr->len_from_reserved() - 2);
    conn->send_queue.push_back(imstr);

    /*
        1998.12.15
        ximCreateIc()でXIM_EXT_SET_EVENT_MASKを返すと，ここでクライアン
        ト・アプリが落ちる。
        XNFilterEventsを呼ばないアプリは大丈夫なんだが。
    */

    return false;
}

static bool ximSetIcFocus(Connection* conn, InputContext* ic, XimReader* input)
    // X11/imDefIc.c: _XimProtoSetFocus()
{
    ic->setFocus(true);
    return true;
}

static bool ximUnsetIcFocus(Connection* conn, InputContext* ic, XimReader* input)
    // X11/imDefIc.c: _XimProtoUnsetFocus()
{
    ic->setFocus(false);
    return true;
}

//////////////////////////////////////////////////////////////////////

Pendings pendings;

static bool ximForwardEvent(Connection* conn, InputContext* ic, XimReader* input)
/*
方針：
　(1) IME ONかつ半角英数，(2) IME OFFという，二つの状態があるのは好まない。
　半角英数モードでも常にforwardしてもらう。まぁ，英数はQWERTYでいいので，単にスルーするだけにすると思うが。

    xwnmoだとXsi/Xwnmo/xwnmo/ximdispt.cにある。
*/
    // imDefLkup.c: _XimForwardEventCore()
{
    int flag = input->card16();
    KeyEvent event = input->keyevent();

/*
    1999.07.12
        pendingしてもxemacsの状況は改善されない
        一方で，gtk+アプリが正常に動作しなくなる
        xwnmoはpendingしていない
            -> pendingすべきでない

    if (waiting) {
        TRACE("pending\n");
        pendings.push_back(forwarded);
        return true;
    }
*/

    if (!ic->ext_event_mask) {
        /*
            XIM_EXT_SET_EVENT_MASKの処理は，すべてxc/lib/X11/imExten.cで行っている。
            _XimProcExtSetEventMask()を見ると，select_event_maskにKeyPressMask |
            KeyReleaseMaskを
            指定してやれば，FilterEventを呼ばない応用ソフトでも，KeyReleaseを受け
            取れるように見える。
            1998.12.12 xc/lib/X11/imDefFlt.cも見ると，select_event_maskと
            forward_event_maskの両方に
            KeyReleaseMaskを指定すれば，バックエンドでもKeyReleaseを受け取れるよう
            に見える。
        */
        // FilterEventを呼ばない応用ソフトでもKeyReleaseをフィルタするようにする。
        ic->ext_event_mask = true;

        XimWriter* imstr = conn->createXimWriter(XIM_EXT_SET_EVENT_MASK);
        imstr->card16(ic->getIM()->getId());
        imstr->card16(ic->getId());
        imstr->card32(0);    // filter
        imstr->card32(0);    // intercept
        imstr->card32(forwardMask);     // select
        imstr->card32(forwardMask);     // forward
        imstr->card32(forwardMask);     // sync
        conn->send_queue.push_back(imstr);
    }

    if (event.type == KeyPress || event.type == KeyRelease) {
        // コールバックの時type = 0になってる
        ic->processEvent(event);
    }
    ic->updateView();

    if ((flag & XimSYNCHRONUS) != 0) {
        XimWriter* imstr = conn->createXimWriter(XIM_SYNC_REPLY);
        imstr->card16(ic->getIM()->getId());
        imstr->card16(ic->getId());
        conn->send_queue.push_back(imstr);
    }
    return true;
}

static bool ximSyncReply(Connection* conn, InputContext* ic, XimReader* input)
    // imDefLkup.c: _XimProcSyncReply()
{
    while (pendings.size() > 0) {
        SendBuffer* imstr = pendings.front();
        pendings.pop_front();
        conn->send_queue.push_back(imstr);
    }
    return true;
}

static bool ximSync(Connection* conn, InputContext* ic, XimReader* input)
    // imDefLkup.c: _XimSync()
    // フォーカスが変わるとき，フォーカスを失うIM, ICを送ってくる，らしい。
{
/*
    Pendings::iterator i;
    for (i = pendings.begin(); i != pendings.end(); i++) {
        if (i->imid == imid && i->icid == icid) {
            processEvent(conn, *i);
            pendings.erase(i);
        }
    }
*/

    XimWriter* imstr = conn->createXimWriter(XIM_SYNC_REPLY);
    imstr->card16(ic->getIM()->getId());
    imstr->card16(ic->getId());
    conn->send_queue.push_back(imstr);

    return true;
}

static bool ximSetIcValues(Connection* conn, InputContext* ic, XimReader* input)
    // X11/imDefIc.c: _XimProtoSetICValues()
{
    int len = input->card16();
    input->card16();    // unused
    if (setIcAttributes(ic, input, len, false)) {
        XimWriter* imstr = conn->createXimWriter(XIM_SET_IC_VALUES_REPLY);
        imstr->card16(ic->getIM()->getId());
        imstr->card16(ic->getId());
        conn->send_queue.push_back(imstr);
            // XIM_SET_IC_VALUESは，間に何も挟まず，
            // XIM_SET_IC_VALUES_REPLYを返さないといけない
    }
    return false;
}

static bool ximResetIc(Connection* conn, InputContext* ic, XimReader* input)
    // imDefIc.c: _XimProtoMbReset()
    // imDefIc.c: _XimProtoWcReset()
{
    ic->kanaKanji->all_determine();
    string r = ic->kanaKanji->getDetermined();
    ic->kanaKanji->clear();

    XimWriter* imstr = conn->createXimWriter(XIM_RESET_IC_REPLY);
    imstr->card16(ic->getIM()->getId());
    imstr->card16(ic->getId());
    imstr->str16(r.c_str());
    conn->send_queue.push_back(imstr);

    return true;
}

static bool ximStrConversionReply(Connection* conn, InputContext* ic,
                                  XimReader* input)
{
    assert(0); // TODO: implement
    return true;
}

static bool ximPreeditStartReply(Connection* conn, InputContext* ic,
                                 XimReader* input)
{
    assert(0); // TODO: implement
    return true;
}

static bool ximPreeditCaretReply(Connection* conn, InputContext* ic,
                                 XimReader* input)
{
    assert(0); // TODO: implement
    return true;
}

////////////////////////////////////////////////////////////////////////

struct DispatchTableConn
{
    int msg;
    bool (*func)(Connection* conn, XimReader* input);
};

static const DispatchTableConn dispatcherConn[] = {
    { XIM_ERROR, ximError },
    { XIM_CONNECT, ximConnect },
    { XIM_AUTH_REQUIRED, ximNotImplemented },
    { XIM_AUTH_REPLY, ximNotImplemented },
    { XIM_AUTH_NEXT, ximNotImplemented },
    { XIM_AUTH_SETUP, ximUnexpected },
    { XIM_AUTH_NG, ximNotImplemented },
    { XIM_CONNECT_REPLY, ximUnexpected },
    { XIM_DISCONNECT, ximDisconnect },
    { XIM_DISCONNECT_REPLY, ximUnexpected },
    { XIM_OPEN, ximOpen },
    { XIM_OPEN_REPLY, ximUnexpected },
    { XIM_CLOSE, ximClose },
    { XIM_CLOSE_REPLY, ximUnexpected },
    { XIM_SET_EVENT_MASK, ximUnexpected },
    { XIM_REGISTER_TRIGGERKEYS, ximUnexpected },
    { XIM_TRIGGER_NOTIFY, ximNotImplemented },
    { XIM_TRIGGER_NOTIFY_REPLY, ximUnexpected },
    { XIM_ENCODING_NEGOTIATION_REPLY, ximUnexpected },
    { XIM_QUERY_EXTENSION_REPLY, ximUnexpected },
    { XIM_SET_IM_VALUES_REPLY, ximUnexpected },
    { XIM_GET_IM_VALUES_REPLY, ximUnexpected },
    { XIM_CREATE_IC_REPLY, ximUnexpected },
    { XIM_DESTROY_IC_REPLY, ximUnexpected },
    { XIM_SET_IC_VALUES_REPLY, ximUnexpected },
    { XIM_GET_IC_VALUES_REPLY, ximUnexpected },
    { XIM_COMMIT, ximUnexpected },
    { XIM_RESET_IC_REPLY, ximUnexpected },
    { XIM_GEOMETRY, ximUnexpected },
    { XIM_STR_CONVERSION, ximUnexpected },
    { XIM_PREEDIT_START, ximUnexpected },
    { XIM_PREEDIT_DRAW, ximUnexpected },
    { XIM_PREEDIT_CARET, ximUnexpected },
    { XIM_PREEDIT_DONE, ximUnexpected },
    { XIM_PREEDITSTATE, ximUnexpected },
    { XIM_STATUS_START, ximUnexpected },
    { XIM_STATUS_DRAW, ximUnexpected },
    { XIM_STATUS_DONE, ximUnexpected },
    { 0, NULL }
};

struct DispatchTableIM
{
    int msg;
    bool (*func)(Connection* conn, InputMethod* im, XimReader*);
};

static const DispatchTableIM dispatcherIM[] = {
    { XIM_ENCODING_NEGOTIATION, ximEncodingNegotiation },
    { XIM_QUERY_EXTENSION, ximQueryExtension },
    { XIM_SET_IM_VALUES, ximSetImValues },
    { XIM_GET_IM_VALUES, ximGetImValues },
    { XIM_CREATE_IC, ximCreateIc },
    { XIM_DESTROY_IC, ximDestroyIc },
    { 0, NULL }
};

struct DispatchTableIC
{
    int msg;
    bool (*func)(Connection* conn, InputContext* ic, XimReader*);
};

static const DispatchTableIC dispatcherIC[] = {
    { XIM_SET_IC_VALUES, ximSetIcValues },
    { XIM_GET_IC_VALUES, ximGetIcValues },
    { XIM_SET_IC_FOCUS, ximSetIcFocus },
    { XIM_UNSET_IC_FOCUS, ximUnsetIcFocus },
    { XIM_FORWARD_EVENT, ximForwardEvent },
    { XIM_SYNC, ximSync },
    { XIM_SYNC_REPLY, ximSyncReply },
    { XIM_RESET_IC, ximResetIc },
    { XIM_STR_CONVERSION_REPLY, ximStrConversionReply },
    { XIM_PREEDIT_START_REPLY, ximPreeditStartReply },
    { XIM_PREEDIT_CARET_REPLY, ximPreeditCaretReply },
    { 0, NULL }
};

void dispatchXimMessage(Connection* cli, XimReader* input)
{
    assert(cli);
    assert(input);

    int i;
    for (i = 0; dispatcherConn[i].msg; i++) {
        if (dispatcherConn[i].msg == input->opcode()) {
            assert(dispatcherConn[i].func);
            dispatcherConn[i].func(cli, input);
            return;
        }
    }
    for (i = 0; dispatcherIM[i].msg; i++) {
        if (dispatcherIM[i].msg == input->opcode()) {
            XIMID imid = input->card16();
            InputMethod* im = cli->getIM(imid);
            assert(im);
            assert(dispatcherIM[i].func);
            dispatcherIM[i].func(cli, im, input);
            return;
        }
    }
    for (i = 0; dispatcherIC[i].msg; i++) {
        if (dispatcherIC[i].msg == input->opcode()) {
            XIMID imid = input->card16();
            InputMethod* im = cli->getIM(imid);
            assert(im);
            XICID icid = input->card16();
            InputContext* ic = im->getIC(icid);
            assert(ic);
            assert(dispatcherIC[i].func);
            dispatcherIC[i].func(cli, ic, input);
            return;
        }
    }
    assert(0); // unknown opcode
}
