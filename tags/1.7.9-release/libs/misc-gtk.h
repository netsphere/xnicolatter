// Q's C++ Library
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSLIB_MISC_GTK_H
#define QSLIB_MISC_GTK_H

#include <string>

#include <X11/Xlib.h>
#include <gtk/gtk.h>

using namespace std;

////////////////////////////////////////////////////////////////////////

enum MouseButton
    // マウスのボタンを物理位置で管理するのは
    // ユーザーインターフェイスのデザインとして望ましくない。
    // こういうのはシステムの方でグローバルに管理してほしいんだが。
{
    SELECT_BUTTON = 1,
    MENU_BUTTON = 3
};

struct PopupMenuItem
{
    const char* path;
    void (*callback)(GtkMenuItem* item, void* data);
};

inline GtkItemFactoryCallback GTK_ITEM_FACTORY_CALLBACK(
    GtkItemFactoryCallback1 func)
{
    return (GtkItemFactoryCallback) func;
}

extern bool q_rc_parse(const char* filename);
extern void q_gtk_init(int* pargc, char*** pargv);

extern XFontSet GDK_FONT_XFONTSET(GdkFont* font);

extern GtkWidget* createDropDownListBox();
extern GtkWidget* getWindowWidget(GtkWidget* widget);

/*
extern bool GtkTextLoad(GtkText* text, const string& filename);
extern bool GtkTextSave(GtkText* text, const string& filename);
*/

extern int getSelectedIndex(GtkOptionMenu* option_menu);

////////////////////////////////////////////////////////////////////////
// Date

class Date
{
    GDate* date;
public:
    Date();
    Date(int y, int m, int d);
    Date(int j);
    Date(const Date& );
    virtual ~Date();

    int getYear() const;
    int getMonth() const;
    int getDay() const;
    int getJulian() const;

    Date addYears(int y) const;
    Date addMonths(int m) const;
    Date addDays(int d) const;

    Date& operator = (const Date& );
};

#endif  // QSLIB_MISC_GTK_H

