// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// 前編集用ウィンドウを生成し，テキストを表示する。

#include "../config.h"

#include <string>
#include <X11/keysym.h>

#ifdef USE_GTK
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#else
#include <X11/Intrinsic.h>
#include <X11/Shell.h>
#include <X11/StringDefs.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Command.h>
#endif  // USE_GTK

#include "preedit.h"
#include "conv.h"
#include "client.h"
#include "conv.h"
#include "WnnConv.h"
#include "start.h"
#include "../global.h"

#ifdef USE_GTK
#include <misc-gtk.h>
#endif  // USE_GTK
#include <misc.h>

using namespace std;

//////////////////////////////////////////////////////////////////////

    // 入力中
    //      文字 = 青   背景 = 白
    // 対象文節
    //      文字 = 黒   背景 = 水色

/*
1. GCを生成     gdk_gc_new
2. 色を生成     gdk_color_parse
    フォントを生成  gdk_fontset_load()
2. 色を設定     gdk_gc_set_foreground()等
3. 描写         gdk_draw_line
*/

//////////////////////////////////////////////////////////////////////
// Caret

Caret::Killer Caret::killer;
Caret* Caret::this_ = NULL;

Caret* Caret::instance()
{
    if (!this_) {
        this_ = new Caret();
        killer.set(this_);
    }
    return this_;
}

Caret::Caret(): focused(0), timeout_id(0)
{
}

Caret::~Caret()
{
    setVisible(false);
}

#ifdef USE_GTK
void Caret::setFocus(GtkWidget* widget)
#else
void Caret::setFocus(Widget widget)
#endif
{
    setVisible(false);
    focused = widget;
}

void Caret::setPosition(const CRect& rect_)
{
    rect = rect_;
    if (timeout_id) {
        isBlack = false;
        // キャレットを移動したときは，認識性を高めるため，黒から始める
#ifdef USE_GTK
        onTimeout(this);
#else
        XtRemoveTimeOut(timeout_id);
        timeout_id = 0;
        onTimeout(this, &timeout_id);
#endif
    }
}

int Caret::setVisible(bool v)
{
    if (!focused || v == (timeout_id != 0))
        return 0;

    if (v) {
        isBlack = false;
#ifdef USE_GTK
        onTimeout(this);
#else
        if (timeout_id) {
            XtRemoveTimeOut(timeout_id);
            timeout_id = 0;
        }
        onTimeout(this, &timeout_id);
#endif
    }
    else {
        if (timeout_id) {
#ifdef USE_GTK
            gtk_timeout_remove(timeout_id);
#else
            XtRemoveTimeOut(timeout_id);
#endif
            timeout_id = 0;
        }
    }
    return 0;
}

#ifdef USE_GTK
gint Caret::onTimeout(void* closure)
{
    Caret* this_ = static_cast<Caret*>(closure);
    GdkGC* gc = this_->isBlack ? this_->focused->style->white_gc
                : this_->focused->style->black_gc;
    gdk_draw_rectangle(this_->focused->window, gc, TRUE,
                    this_->rect.x, this_->rect.y,
                    this_->rect.width, this_->rect.height);

    this_->isBlack = !this_->isBlack;

    if (this_->timeout_id)
        gtk_timeout_remove(this_->timeout_id);
    this_->timeout_id
        = gtk_timeout_add(this_->isBlack ? 400 : 200, onTimeout, this_);

    return TRUE;
}
#else
void Caret::onTimeout(void* closure, XtIntervalId* id)
{
    Caret* this_ = static_cast<Caret*>(closure);
    if (!this_->focused)
        return;

    Display* disp = XtDisplay(this_->focused);
    Window w = XtWindow(this_->focused);
    if (w) {
        GC gc = XtGetGC(this_->focused, 0, NULL);

        XSetForeground(disp, gc,
            this_->isBlack ? WhitePixel(disp, 0) : BlackPixel(disp, 0));
#if DEBUG > 1
        TRACE("disp = %x, wnd = %x, rect: x = %d, y = %d, w = %d, h = %d\n",
              disp, w,
              this_->rect.x, this_->rect.y,
              this_->rect.width, this_->rect.height);
#endif
        XFillRectangle(disp, w, gc, this_->rect.x, this_->rect.y,
                            this_->rect.width, this_->rect.height);
        XtReleaseGC(this_->focused, gc);
    }

    this_->isBlack = !this_->isBlack;
    this_->timeout_id = XtAppAddTimeOut(
            XtWidgetToApplicationContext(this_->focused),
            this_->isBlack ? 400 : 200, onTimeout, this_);
}
#endif  // USE_GTK

//////////////////////////////////////////////////////////////////////
// PreeditWindow

const int PreeditWindow::PREEDIT_BORDER = 2;

int PreeditWindow::white = 0;
int PreeditWindow::blue = 0;
int PreeditWindow::red = 0;
int PreeditWindow::black = 0;

PreeditWindow::PreeditWindow(InputContext* ic_): ic(ic_), spot(-1, -1)
{
    fontName = global_prop.preedit_font;
    create();
}

PreeditWindow::~PreeditWindow()
{
    font.destroy();
    Caret::instance()->setVisible(false);
    destroy();
}

PreeditWindow& PreeditWindow::operator = (const PreeditWindow& a)
{
    area                 = a.area;
    areaNeeded          = a.areaNeeded;
    spot                = a.spot;
    colormap           = a.colormap;
    stdColormap        = a.stdColormap;
    foreground         = a.foreground;
    background         = a.background;
    backgroundPixmap   = a.backgroundPixmap;
    fontName           = a.fontName;
    lineSpace          = a.lineSpace;
    cursor             = a.cursor;
    size               = a.size;
    updateFont();
    return *this;
}

#ifdef USE_GTK
void PreeditWindow::create()
{
    window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_POPUP));

    canvas = gtk_drawing_area_new();
    gtk_widget_show(canvas);
    gtk_container_add(GTK_CONTAINER(window), canvas);

    gtk_signal_connect(GTK_OBJECT(canvas), "realize",
                    GTK_SIGNAL_FUNC(onRealized), this);
    gtk_signal_connect(GTK_OBJECT(canvas), "expose_event",
                    GTK_SIGNAL_FUNC(onExposed), this);
    gtk_signal_connect(GTK_OBJECT(window), "hide",
                    GTK_SIGNAL_FUNC(onHide), this);

    gtk_widget_set_usize(GTK_WIDGET(window), 10, 10);
}
#else
void PreeditWindow::create()
{
    window = XtVaCreatePopupShell("preedit-shell",
                    overrideShellWidgetClass, top_widget,
                    NULL);
    canvas = window;
/*
    canvas = XtVaCreateManagedWidget("canvas", formWidgetClass,
                    window,
                    XtNwidth, 10, XtNheight, 30,
#if DEBUG >= 1
                    XtVaTypedArg, XtNbackground, XtRString,
                                    "green", strlen("green") + 1,
#endif
                    NULL);
*/
    createColors();
    XtAddEventHandler(canvas, ExposureMask, False, onExposed, this);
}
#endif  // USE_GTK

#ifdef USE_GTK
gint PreeditWindow::onExposed(GtkWidget* drawPane, GdkEventExpose* event,
                    PreeditWindow* this_)
{
    CRect rect;
    rect.x = event->area.x; rect.y = event->area.y;
    rect.width = event->area.width; rect.height = event->area.height;
    this_->draw(rect);
    return TRUE;
}
#else
void PreeditWindow::onExposed(Widget w, void* closure, XEvent* event,
                              Boolean* cont)
{
    PreeditWindow* this_ = (PreeditWindow*) closure;
    CRect rect;
    rect.x = event->xexpose.x;
    rect.y = event->xexpose.y;
    rect.width = event->xexpose.width;
    rect.height = event->xexpose.height;
#if DEBUG >= 2
    TRACE("onExposed(): x = %d, y = %d, w = %d, h = %d\n",
                        rect.x, rect.y, rect.width, rect.height);
#endif
    this_->draw(rect);
    *cont = True;
}
#endif  // USE_GTK

int PreeditWindow::setVisible(bool v)
{
    super::setVisible(v);
    
    if (v) {
        Caret::instance()->setFocus(canvas);
#if 0
        // 2000.01.10 gtk+版のみpreeditがアプリケーションのウィンドウの
        // 後ろに回ってしまうことがある
        
        // 以下のコードではうまくいかない
        // TODO: 
        Display* disp = GDK_DISPLAY();
        XSetTransientForHint(
            disp, GDK_WINDOW_XWINDOW(canvas->window),
            getToplevelWindow(disp, ic->focusWindow));
        gdk_window_raise(canvas->window);
#endif
    }
    else {
        Caret::instance()->setVisible(false);
    }
    
    return 0;
}

void PreeditWindow::createColors()
{
    updateFont();

    if (!white && !blue && !red && !black) {
        white = getColor(top_display, "white");
        blue = getColor(top_display, "blue");
        red = getColor(top_display, "red");
        black = getColor(top_display, "black");
    }
/*
    TRACE("white = %d, blue = %d, aqua = %d, black = %d\n",
        white, blue, red, black);
*/
}

void PreeditWindow::updateFont()
{
    if (fontName != "") {
        if (font.create(top_display, fontName.c_str())) {
            XFontSetExtents* fse = XExtentsOfFontSet(font.xfont());
            size = fse->max_logical_extent;
            size.height += PREEDIT_BORDER * 2;
        }
        else {
            error("updateFont failed: font name is wrong?\n");
        }
    }
}

#ifdef USE_GTK
void PreeditWindow::onHide(GtkWidget* w, PreeditWindow* this_)
{
    Caret::instance()->setVisible(false);
}

void PreeditWindow::onRealized(GtkWidget* w, PreeditWindow* this_)
    // GtkWidget::realize
{
    this_->createColors();
    TRACE("pre.wmclass_name = '%s', wmclass_class = '%s'\n",
            this_->window->wmclass_name, this_->window->wmclass_class);
}
#endif

bool PreeditWindow::isStyleRoot() const
{
    return (ic->inputStyle & XIMPreeditNothing) != 0
        || (ic->inputStyle & XIMPreeditArea) != 0
                && !area.x && !area.y && !area.width && !area.height;
}

void PreeditWindow::adjustLocation(const CRect& logical)
    // ウィンドウの位置決め
{
    XPoint loc;
    Window root = XRootWindow(top_display, 0);

    size.width = logical.width + PREEDIT_BORDER * 2;

    if ((ic->inputStyle & XIMPreeditPosition) != 0
                && (spot.x != -1 || spot.y != -1)) {
        // TRACE("spot.x = %d, spot.y = %d\n", spot.x, spot.y);
        loc.x = ic->position.x + spot.x + logical.x - PREEDIT_BORDER;
        loc.y = ic->position.y + spot.y + size.y - PREEDIT_BORDER;
    }
    else if ((ic->inputStyle & XIMPreeditArea) != 0
                && (area.x || area.y || area.width || area.height)) {
        Window c;
        int x, y;
        ::XTranslateCoordinates(top_display, ic->clientWindow, root,
                                                0, 0, &x, &y, &c);
        loc.x = x + area.x;
        loc.y = y + area.y;
        // TRACE("preedit-area: x = %d, y = %d\n", loc.x, loc.y);
    }
    else {
        // root style
        int geo_x, geo_y;
        unsigned int w, h, b, d;
        XGetGeometry(top_display,
                     ic->focusWindow ? ic->focusWindow : ic->clientWindow,
                     &root,
                     &geo_x, &geo_y, &w, &h, &b, &d);
        loc.x = ic->position.x; loc.y = ic->position.y + h;

        // 画面外？
        int root_x, root_y;
        unsigned int root_w, root_h, root_b, root_d;
        XGetGeometry(top_display, root, &root,
                    &root_x, &root_y, &root_w, &root_h, &root_b, &root_d);
        if (loc.x > root_w - size.width)
            loc.x = root_w - size.width;
        if (loc.y > root_h - size.height)
            loc.y = root_h - size.height;
    }

    setBounds(CRect(loc.x, loc.y, size.width, size.height));
/*
#ifndef USE_GTK
    Arg args[2];
    XtSetArg(args[0], XtNwidth, size.width);
    XtSetArg(args[1], XtNheight, size.height);
    XtSetValues(canvas, args, 2);
#endif
*/
}

//////////////////////////////////////////////////////////////////////

/*

string preedit_text;
int curpos = 0;
PreeditWindow* preedit = 0;

gint onKeyPressed(GtkWidget* top, GdkEventKey* event, void* )
    // GtkWidget::key_press_event
{
    TRACE("onKeyPressed(): str = %s\n", event->string);

    switch (event->keyval)
    {
    case XK_Left:
        if (curpos > 0)
            curpos--;
        break;
    case XK_Right:
        if (curpos < preedit_text.length())
            curpos++;
        break;
    case XK_BackSpace:
        if (preedit_text.length() > 0) {
            preedit_text.erase(preedit_text.end() - 1);
            if (curpos > preedit_text.length())
                curpos = preedit_text.length();
        }
        break;
    default:
        preedit_text += event->string;
        curpos++;
        break;
    }
    preedit->update(preedit_text, curpos);
    return TRUE;
}

gint onClose(GtkWidget* widget, GdkEventAny* event)
{
    TRACE("onClose()\n");
    return FALSE;
}

gint onEvent(GtkWidget* drawPane, GdkEvent* event)
{
    TRACE("event = %d\n", event->type);
    return FALSE;
}

int main(int argc, char* argv[])
{
    q_gtk_init(&argc, &argv);

    GtkWidget* top = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    preedit = new PreeditWindow();
    preedit->setVisible(true);

    gtk_widget_set_events(top, GDK_KEY_PRESS_MASK);
    gtk_signal_connect(GTK_OBJECT(top), "key_press_event",
                    GTK_SIGNAL_FUNC(onKeyPressed), 0);
    gtk_signal_connect(GTK_OBJECT(top), "delete_event",
                    GTK_SIGNAL_FUNC(onClose), 0);

    gtk_widget_show(top);
    gtk_main();

    return 0;
}

*/
