﻿// -*- coding:utf-8-with-signature -*-

// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#ifndef QSNICOLA_LEVELSELECTOR_H
#define QSNICOLA_LEVELSELECTOR_H

#include <X11/Xlib.h> // XKeyEvent
#include <string>
using namespace std;

//////////////////////////////////////////////////////////////////////
// LevelSelector

struct KeyEvent {
    int type; // KeyPress, KeyRelease
    int keycode;
    int keychar;
    int modifier; // ControlMask, ShiftMask, Mod1Mask
    int time;
    XKeyEvent* xk;

    KeyEvent();
    KeyEvent(const KeyEvent& x);
    explicit KeyEvent(const XKeyEvent& xk);
    virtual ~KeyEvent();
    KeyEvent& operator = (const KeyEvent& x);
};

class LevelSelector
    // キーON/OFFのsequenceをキー/レベルの組み合わせに変換する
    // ローマ字かな変換は，キー・文字変換の後段であって，
    // このクラス（と派生クラス）は関知しない。
{
public:
    virtual void input(class InputContext* ic, const KeyEvent& event) = 0;
    virtual string getStatus() const = 0;
    virtual ~LevelSelector() { }
};

extern LevelSelector* keyChar;

//////////////////////////////////////////////////////////////////////
// NormalSelector

class NormalSelector: public LevelSelector
    // 同時押下シフト動作
{
public:
    NormalSelector();
    virtual ~NormalSelector();
    virtual void input(InputContext* ic, const KeyEvent& event);
    virtual string getStatus() const;
};

//////////////////////////////////////////////////////////////////////
// PrefixSelector

class PrefixSelector: public LevelSelector
    // prefix style shift
{
    bool is_shift;
public:
    PrefixSelector();
    virtual ~PrefixSelector();
    virtual void input(InputContext* ic, const KeyEvent& event);
    virtual string getStatus() const;
};

#endif
