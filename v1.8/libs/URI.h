﻿// -*- coding:utf-8-with-signature -*-
// Q's C++ Library
// Copyright (c) 1996-1999 Hisashi HORIKAWA. All rights reserved.

#ifndef QSLIB_URI
#define QSLIB_URI

#include <string>

//////////////////////////////////////////////////////////////////////
// URI

class URI
    // java.net.URL
    // RFC 2396 "Uniform Resource Identifiers (URI): Generic Syntax"
    //      URI参照 = ( 絶対URI | 相対URI ) [ "#" fragment ]
    //      generic URI = [ scheme ":" ] [ "//" authority ] path [ "?" query ]
    //      authority = [ userinfo "@" ] host [ ":" port ]
{
    std::string scheme;

    std::string authority;

    std::string path, query, fragment;

public:
    URI();
    URI(const URI& a);
    URI(const std::string& a);
    explicit URI(const char* a);

    virtual ~URI();

    URI& operator = (const URI& a);

    std::string getScheme() const;
    std::string getAuthority() const;
    std::string getUserInfo() const;
    std::string getHost() const;
    int getPort() const;
    std::string getPath() const;
    std::string getQuery() const;
    std::string getFragment() const;

    void setFragment(const char* );

    // 相対URIの解決
    virtual URI resolve(const URI& ref) const;

    // baseを基点とした相対URIに変換
    // URIを返さないことに注意
    virtual std::string getRelative(const URI& base) const;

    virtual bool isDescendantOf(const URI& ) const;

    static std::string escape(const char* uri);
    static std::string unescape(const char* escaped_uri);

    // VFAT FSに保存できるようにURI文字をエスケープする。
    // "://" -> "$\\"
    std::string toLocal() const;

    std::string toString() const;

    static int compare(const URI& x, const URI& y);
    bool operator == (const URI& a) const;
    bool operator != (const URI& a) const;
    bool operator < (const URI& a) const;

private:
    int parse(const char* str);
};

//////////////////////////////////////////////////////////////////////
// FileName

class FileName
    // java.io.File
    //      filename = [ '//' host ] path file
    //      dir = ... '/'
    //      file = body [ '.' ext ]
    //      '.'で始まるファイル名は，全体をbodyとして扱う。
{
    std::string host, dir, file;
public:
    FileName();
    FileName(const FileName& a);
    FileName(const std::string& a);
    FileName(const char* a);
    // FileName(const string& h, const string& p, const string& f);
    virtual ~FileName();

    FileName& operator = (const FileName& a);

    std::string getHost() const;
    std::string getPath() const; // 必ず'/'で終わる。ドライブ名を含む
        // URI::getPath()はファイル名を含むが，こちらは含まないことに注意
    std::string getFile() const;

    std::string getPath2() const;
    std::string getFile2() const;
        // "/hogehoge/"のとき
        //      getPath() = "/hogehoge/", getFile() = ""
        //      getPath2() = "/", getFile2() = "hogehoge"   末尾に'/'は付かない

    std::string getBody() const;
    std::string getExt(bool normalize) const;
        // normalize = true -> 拡張子を小文字で返す。

    FileName resolve(const FileName& ref) const;
    std::string getRelative(const FileName& base) const;
        // *thisとbaseが同じファイルの時""を返す
    bool isDescendantOf(const FileName& ) const;

    std::string toString() const;

    static std::string escapeFile(const char* str);
        // 入力文字列中の'/', ':'等をエスケープして，ファイル名として使えるようにする。
    static std::string escapePath(const char* str);
        // 入力文字中の':'等をエスケープする。'/'はエスケープせず，
        // この関数の戻り値を使って深い階層のファイルを作成できる。

    bool operator == (const FileName& a) const;
    bool operator < (const FileName& a) const;

private:
    int parse(const char* str);
};

#ifdef WIN32
extern off_t qGetFileSize(const char* filename);
extern bool isExistFile(const char* filename);
extern bool isDirectory(const char* filename);
#endif

#endif
