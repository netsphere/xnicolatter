
# [historical] Q's Nicolatter for X

A Japanese Input Method for X11. Thumb-Shift (親指シフト; NICOLA) method.

See https://www.nslabs.jp/xnicolatter.rhtml
