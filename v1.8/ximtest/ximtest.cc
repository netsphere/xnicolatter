﻿// -*- coding:utf-8-with-signature -*-
// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <cstdio>
#include <clocale>
#include <string>
#include <cassert>
#include <X11/Xlib.h>
#include <X11/Xutil.h>  // XLookupString()
#include <X11/Xatom.h>

#include <X/InputMethod.h>
#include <misc.h>

#include "ximtest.h"

using namespace std;

////////////////////////////////////////////////////////////////////////

Display* disp = NULL;
    // Xサーバーへの接続は一つでよい。
InputMethod im;
    // IMサーバーへの接続

GC gc_normal, gc_hi, gc_rev, gc_pri, gc_sc, gc_te, gc_ul;

struct StyleText {
    int bit;
    const char* text;
};

const StyleText style_text[] = {
    { XIMPreeditArea, "XIMPreeditArea" },
    { XIMPreeditCallbacks, "XIMPreeditCallbacks" },
    { XIMPreeditPosition, "XIMPreeditPosition" },
    { XIMPreeditNothing, "XIMPreeditNothing" },
    { XIMPreeditNone, "XIMPreeditNone" },

    { XIMStatusArea, "XIMStatusArea" },
    { XIMStatusCallbacks, "XIMStatusCallbacks" },
    { XIMStatusNothing, "XIMStatusNothing" },
    { XIMStatusNone, "XIMStatusNone" },
    { 0, NULL }
};

void printInputStyle(int style)
{
    string s;
    for (int i = 0; style_text[i].text; i++) {
        if ((style & style_text[i].bit) != 0)
            s += string(" ") + style_text[i].text;
    }

    printf("%s\n", s.c_str());
}

const char* tests[] = {
    XNInputStyle, XNClientWindow, XNFocusWindow, XNFilterEvents,
    XNGeometryCallback, XNStringConversion, XNHotKey, XNHotKeyState,
    NULL
};

const char* tests_a[] = {
    XNArea, XNAreaNeeded, XNSpotLocation, XNColormap,
    XNStdColormap, XNForeground, XNBackground, XNBackgroundPixmap,
    XNFontSet, XNLineSpace, XNCursor, NULL
};

const char* tests_s[] = {
    XNArea, XNAreaNeeded, XNColormap, XNStdColormap,
    XNForeground, XNBackground, XNBackgroundPixmap,
    XNFontSet, XNLineSpace, XNLineSpace, XNCursor, NULL
};

void ic_values_test(XIC ic)
{
    if (!ic)
        return;

    const char* msg = NULL;
    char buf[1000];
    char* ptr = 0;
    int i;
    XVaNestedList list;

    for (i = 0; tests[i]; i++) {
        msg = XGetICValues(ic, tests[i], buf, 0);
        if (msg) { TRACE("XGetICValues() error: %s\n", msg); }
    }

    for (i = 0; tests_a[i]; i++) {
        list = XVaCreateNestedList(0, tests_a[i], &ptr, 0);
        msg = XGetICValues(ic, XNPreeditAttributes, list, 0);
        if (msg) { TRACE("%s, %s\n", msg, tests_a[i]); }
        XFree(list);
        // XFree(ptr);
    }

    for (i = 0; tests_s[i]; i++) {
        list = XVaCreateNestedList(0, tests_s[i], &ptr, 0);
        msg = XGetICValues(ic, XNStatusAttributes, list, 0);
        if (msg) { TRACE("%s, %s\n", msg, tests_s[i]); }
        XFree(list);
        // XFree(ptr);
    }
}

void onGeometryCallback(XIC ic, void* client_data, void* /*unused*/ )
    // xc/lib/X11/imCallbk.c: _XimGeometryCallback()
{
    TRACE("onGeometryCallback()\n");
}

// basic_string<wchar_t> preedit;
                        // egcs-1.1.2はwstringを持っていない
                        // c_str()がconst char*決め打ちになってる
string preedit;
vector<XIMFeedback> feedback;

int onPreeditStart(XIC ic, XPointer client_data, XPointer /*unused*/ )
    // xc/lib/X11/imCallbk.c: _XimPreeditStartCallback()
{
    TRACE("onPreeditStart()\n");
    preedit = "";
    feedback.clear();
    return -1;  // 表示は無制限に行える
}

void onPreeditDraw(XIC ic, MainWnd* frame, XIMPreeditDrawCallbackStruct* cbs)
{
    TRACE("onPreeditDraw()\n");

    TRACE("caret = %d, chg_first = %d, chg_length = %d\n",
                    cbs->caret, cbs->chg_first, cbs->chg_length);
            // caret = 文字単位
            // chg_first = 文字単位
            // chg_length = 文字単位    挿入のとき = 0
    /*
        ローマ字変換のとき，子音のアルファベットを消すので，
        chg_length = 1などになってる。
    */
    const char* pre = preedit.c_str();
    int mb_chg_first = byte_length(pre, cbs->chg_first);
    int mb_chg_length = byte_length(pre + mb_chg_first, cbs->chg_length);

    if (cbs->text) {
        // 挿入 or 置換
        if (cbs->text->encoding_is_wchar) {
            char buf[1000];
            cbs->text->string.wide_char[cbs->text->length] = 0;
            wcstombs(buf, cbs->text->string.wide_char, sizeof(buf));

            preedit.replace(mb_chg_first, mb_chg_length, buf);
        }
        else {
            int b = byte_length(cbs->text->string.multi_byte,
                                    cbs->text->length);
            cbs->text->string.multi_byte[b] = '\0';
            preedit.replace(mb_chg_first, mb_chg_length,
                                        cbs->text->string.multi_byte);
        }

        feedback.erase(feedback.begin() + cbs->chg_first,
                    feedback.begin() + cbs->chg_first + cbs->chg_length);
        for (int i = 0; i < cbs->text->length; i++) {
            feedback.insert(feedback.begin() + cbs->chg_first + i,
                    cbs->text->feedback[i]);
        }
    }
    else {
        // 削除
        preedit.erase(preedit.begin() + mb_chg_first,
                    preedit.begin() + mb_chg_first + mb_chg_length);
        feedback.erase(feedback.begin() + cbs->chg_first,
                    feedback.begin() + cbs->chg_first + cbs->chg_length);
    }
#ifdef DEBUG
    TRACE("preedit = %s\n", preedit.c_str());

    TRACE("feedback:");
    for (unsigned i = 0; i < feedback.size(); i++)
        TRACE(" %d", feedback[i]);
    TRACE("\n");
#endif

    frame->drawPreedit();
}

void onPreeditCaret(XIC ic, XPointer client_data,
                                XIMPreeditCaretCallbackStruct* cbs)
{
    TRACE("onPreeditCaret()\n");
}

void onPreeditDone(XIC ic, XPointer client_data, XPointer /*unused*/ )
{
    TRACE("onPreeditDone()\n");
}

void onStatusStart(XIC ic, XPointer client_data, XPointer /*unused*/ )
{
    TRACE("onStatusStart()\n");
}

void onStatusDraw(XIC ic, XPointer client_data,
                                XIMStatusDrawCallbackStruct* cbs)
{
    TRACE("onStatusDraw()\n");
}

void onStatusDone(XIC ic, XPointer client_data, XPointer /*unused*/ )
{
    TRACE("onStatusDone()\n");
}

void setImWindowGeometry(XIC ic, int which, const XConfigureEvent& ev)
{
    XRectangle area = { 0, 0, 0, 0 };
    XRectangle* r_area = NULL;
    const char* attr = !which ? XNPreeditAttributes : XNStatusAttributes;

    area.width = ev.width;
    area.height = 20;
    XVaNestedList list = XVaCreateNestedList(0, XNAreaNeeded, &area, NULL);
    XSetICValues(ic, attr, list, NULL);
    XFree(list);

    list = XVaCreateNestedList(0, XNAreaNeeded, &r_area, 0);
    const char* mes = XGetICValues(ic, attr, list, 0);
    XFree(list);
    if (mes)
        error("XGetICValues() failed: %s\n", mes);
    if (r_area) {
        area = *r_area;
        XFree(r_area);
    }
    
    area.x = 10;
    area.y = !which ? 50 : 100;
    list = XVaCreateNestedList(0, XNArea, &area, 0);
    XSetICValues(ic, attr, list, 0);
    XFree(list);
}

////////////////////////////////////////////////////////////////////////
// MainWnd

const char* DEFAULT_FONT =
    "-*-fixed-medium-r-normal--*-200-*-*-*-*-iso10646-1,"
    "-*-times-medium-r-normal--*-180-*-*-*-*-iso8859-1,"
    "-*-fixed-medium-r-normal--*-170-*-*-*-*-jisx0208.1983-0";

MainWnd::MainWnd(XIMStyle n): preedit_area(0), status_area(0),
                            need_style(n), ic(0)
{
    // ウィンドウと入力フィールドを作成
    int green = getColor(disp, "green");
    window = XCreateSimpleWindow(disp, XRootWindow(disp, 0),
                        20, 20, 400, 230, 2,
                        BlackPixel(disp, 0), green);

    fields.push_back(new Field(window, 20, 20, 170, 50));
    fields.push_back(new Field(window, 210, 20, 170, 50));

    font = createFont(disp, DEFAULT_FONT);
    assert(font);

    create_ui();
}

MainWnd::~MainWnd()
{
}

void MainWnd::create_ui()
{
    bool im_ready = im.open(disp);
    ic_style = 0;
    int im_event_mask = 0;
    if (im_ready) {
        XIMStyle ns[2];
        ns[0] = need_style;
        ns[1] = 0;
        ic_style = im.selectInputStyle(ns);

/*
                     幅 400
      ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
      ┃  (20, 20)   170                (210, 20)                     ┃
      ┃  ┏━━━━━━━━━━━┓    ┏━━━━━━━━━━━━┓  ┃
      ┃  ┃                     50     ┃                        ┃  ┃
      ┃  ┃                (190, 70)   ┃                        ┃  ┃
      ┃  ┗━━━━━━━━━━━┛    ┗━━━━━━━━━━━━┛  ┃
高さ300   ┏(20, 90)━━━━━━━━━━━━━━━━━━━━━━━┓  ┃
      ┃  ┃                     preedit                          ┃  ┃
      ┃  ┃                                              (380, 140)  ┃  ┃
      ┃  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛  ┃
      ┃  ┏(20, 160) ━━━━━━━━━━━━━━━━━━━━━━┓  ┃
      ┃  ┃                      status                          ┃  ┃
      ┃  ┃                                            (380, 210)┃  ┃
      ┃  ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛  ┃
      ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
*/

        if ((ic_style & XIMPreeditCallbacks) != 0) {
            preedit_area = XCreateSimpleWindow(disp, window,
                    20, 90, 360, 50, 2,
                    BlackPixel(disp, 0), WhitePixel(disp, 0));
            XMapWindow(disp, preedit_area);
        }
        if ((ic_style & XIMStatusCallbacks) != 0) {
            status_area = XCreateSimpleWindow(disp, window,
                    20, 160, 360, 50, 2,
                    BlackPixel(disp, 0), WhitePixel(disp, 0));
            XMapWindow(disp, status_area);
        }

        ic = createIC(im.getXIM(), ic_style, font, window);
#if 0 // TEST
        // IMサーバーがサポートしていない入力スタイル
        ic = createIC(im.getXIM(),
                      XIMPreeditCallbacks | XIMStatusCallbacks,
                      font, window);
#endif
        assert(ic);
        Fields::iterator i;
        for (i = fields.begin(); i != fields.end(); i++) {
            (*i)->attachIC(ic, ic_style);
                // 一つのICを共有してみる
        }

        // フィルタすべきイベントを問い合わせる
        const char* mes;
        if ((mes = XGetICValues(ic, XNFilterEvents, &im_event_mask, 0)) != 0)
            error("XGetICValues() failed: '%s'\n", mes);

        TRACE("XNFilterEvents = %02x\n", im_event_mask);
    }

    int mask = ExposureMask | FocusChangeMask
                | EnterWindowMask | LeaveWindowMask | KeyPressMask
                | ButtonPressMask;
    Fields::iterator i;
    for (i = fields.begin(); i != fields.end(); i++) {
        XSelectInput(disp, (*i)->window, im_event_mask | mask);
        (*i)->font = font;  // IC生成に用いたフォントで表示する
    }
}

void MainWnd::inputMethodInstantiated()
{
    create_ui();
}

void MainWnd::inputMethodDestroyed()
{
    // ICなどを全て破棄する
    ic = 0;
    Fields::iterator i;
    for (i = fields.begin(); i != fields.end(); i++)
        (*i)->detachIC();

    if (preedit_area) {
        XDestroyWindow(disp, preedit_area);
        preedit_area = 0;
    }
    if (status_area) {
        XDestroyWindow(disp, status_area);
        status_area = 0;
    }

    // 別のIMサーバーが動いてるかもしれない
    XSetLocaleModifiers("");
        // modifiersを設定し直さないと，XOpenIM()で失敗する
    create_ui();
}

void MainWnd::setVisible(bool )
{
    createGCs();

    for (unsigned int i = 0; i < fields.size(); i++)
        fields[i]->setVisible(true);

    XSelectInput(disp, window, StructureNotifyMask);
    XMapWindow(disp, window);

    while (true) {
    redo:
        XEvent e;
        XNextEvent(disp, &e);
        if (XFilterEvent(&e, None))
            continue;

        for (unsigned int i = 0; i < fields.size(); i++) {
            if (e.xany.window == fields[i]->window) {
                fields[i]->eventPerformed(e);
                goto redo;
            }
        }

        switch (e.type)
        {
        case ConfigureNotify:
            if ((ic_style & XIMPreeditArea) != 0)
                setImWindowGeometry(ic, 0, e.xconfigure);
            if ((ic_style & XIMStatusArea) != 0)
                setImWindowGeometry(ic, 1, e.xconfigure);
            break;
        default:
            break;
        }
    }
}

void MainWnd::drawPreedit()
{
    XClearWindow(disp, preedit_area);
    const char* p = preedit.c_str();
    XRectangle ext;
    XmbTextExtents(font, p, preedit.length(), NULL, &ext);
    int x = -ext.x;
    int y = -ext.y;

    for (unsigned i = 0; i < feedback.size(); i++) {
        GC gc;
        switch (feedback[i])
        {
        case 0:
            gc = gc_normal;
        case XIMReverse:
            gc = gc_rev;
            break;
        case XIMUnderline:  // 他の属性と組み合わすわけではなさそう。
            gc = gc_ul;
            break;
        case XIMHighlight:
            gc = gc_hi;
            break;
        case XIMPrimary:
            gc = gc_pri;
            break;
        case XIMSecondary:
            gc = gc_sc;
            break;
        case XIMTertiary:
            gc = gc_te;
            break;
        default:
            gc = gc_normal;     // 救っておく
            break;
        }

        int mlen = mblen(p, MB_CUR_MAX);
        XmbDrawImageString(disp, preedit_area, font, gc, x, y, p, mlen);
        XmbTextExtents(font, p, mlen, NULL, &ext);
        x += ext.width;
        p += mlen;
    }
}

void MainWnd::createGCs()
{
    XGCValues val;

    int blue = getColor(disp, "blue");
    int green = getColor(disp, "green");
    int white = getColor(disp, "white");
    int black = getColor(disp, "black");

    val.foreground = black;
    val.background = white;
    gc_normal = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = blue;
    val.background = WhitePixel(disp, 0);
    gc_hi = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = WhitePixel(disp, 0);
    val.background = blue;
    gc_rev = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = getColor(disp, "red");
    val.background = WhitePixel(disp, 0);
    gc_pri = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = WhitePixel(disp, 0);
    val.background = getColor(disp, "red");
    gc_sc = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = getColor(disp, "green");
    val.background = WhitePixel(disp, 0);
    gc_te = XCreateGC(disp, window, GCForeground | GCBackground, &val);

    val.foreground = white;
    val.background = green;
    gc_ul = XCreateGC(disp, window, GCForeground | GCBackground, &val);
}

XIC MainWnd::createIC(XIM im, XIMStyle style, XFontSet fs, Window window)
{
    assert(im);
    assert(style);
    assert(fs);
    assert(window);

    XRectangle p_area = { 0, 0, 1, 1 };
    XRectangle s_area = { 0, 0, 1, 1 };
    XPoint spot = { 0, 0 };

    int white = getColor(disp, "white");
    int blue = getColor(disp, "blue");

    // スタイルによっては使わない属性があるが，IMサーバーは無視すべき。
    XVaNestedList p_list = XVaCreateNestedList(0,
                    XNArea, &p_area,
                    XNSpotLocation, &spot,
                    XNForeground, blue,
                    XNBackground, white,
                    XNFontSet, fs,
                    NULL);

    XVaNestedList s_list = XVaCreateNestedList(0,
                    XNArea, &s_area,
                    XNForeground, white,
                    XNBackground, blue,
                    XNFontSet, fs,
                    NULL);

    XIC ic = XCreateIC(im, XNInputStyle, style,
            XNClientWindow, window,
                    // ウィンドウはmapされていないかもしれない。
                    // IMサーバーは指定のウィンドウがmapされていないことを理由に
                    // エラーにしてはならない。
            XNPreeditAttributes, p_list,
            XNStatusAttributes, s_list,
            NULL);
    XFree(p_list);
    XFree(s_list);

    if (!ic) {
        TRACE("XCreateIC() failed.\n");
        assert(0);
    }

    XIMCallback callback;
    callback.client_data = NULL;
    callback.callback = (XIMProc) onGeometryCallback;
    char* mes = XSetICValues(ic, XNGeometryCallback, &callback, NULL);
    if (mes) {
        TRACE("XSetICValues() error: %s\n", mes);
            // kinput2はサポートしていない
    }

    XIMCallback p_start = { (XPointer) this, (XIMProc) onPreeditStart };
    XIMCallback p_draw = { (XPointer) this, (XIMProc) onPreeditDraw };
    XIMCallback p_caret = { (XPointer) this, (XIMProc) onPreeditCaret };
    XIMCallback p_done = { (XPointer) this, (XIMProc) onPreeditDone };
    XIMCallback s_start = { (XPointer) this, (XIMProc) onStatusStart };
    XIMCallback s_draw = { (XPointer) this, (XIMProc) onStatusDraw };
    XIMCallback s_done = { (XPointer) this, (XIMProc) onStatusDone };

    p_list = XVaCreateNestedList(0,
                    XNPreeditStartCallback, &p_start,
                    XNPreeditDrawCallback, &p_draw,
                    XNPreeditCaretCallback, &p_caret,
                    XNPreeditDoneCallback, &p_done,
                    NULL);
    s_list = XVaCreateNestedList(0,
                    XNStatusStartCallback, &s_start,
                    XNStatusDrawCallback, &s_draw,
                    XNStatusDoneCallback, &s_done,
                    NULL);
    mes = XSetICValues(ic, XNPreeditAttributes, p_list,
                    XNStatusAttributes, s_list,
                    NULL);
    XFree(p_list);
    XFree(s_list);

    return ic;
}

////////////////////////////////////////////////////////////////////////
// Field

Field::Field(Window parent, int x, int y, int w, int h): font(0), ic(0)
{
    width = w; height = h;
    window = XCreateSimpleWindow(disp, parent, x, y, w, h, 2,
                        BlackPixel(disp, 0), WhitePixel(disp, 0));
}

Field::~Field()
{
}

void Field::attachIC(XIC ic_, XIMStyle style)
{
    ic = ic_;
    ic_style = style;
}

void Field::detachIC()
{
    ic = 0;
    ic_style = 0;
}

void Field::update(const char* adds)
{
    if (adds)
        strcat(str, adds);
    XClearArea(disp, window, 10, 10, width - 20, height - 20, False);

    XmbDrawString(disp, window, font, gc_normal, 10, 30, str, strlen(str));

    if (ic_style & XIMPreeditPosition)
        moveInputPosition();
}

void Field::eventPerformed(const XEvent& event)
{
    switch (event.type)
    {
    case KeyPress:
        onKeyPressed(event.xkey);
        break;
    case Expose:
        update(0);
        break;

#if 1
    // マウスポインタの移動でフォーカスを移動させる
    case EnterNotify:
        {
            GC gc = XCreateGC(disp, window, 0, 0);
            XSetForeground(disp, gc, getColor(disp, "black"));
            XDrawRectangle(disp, window, gc, 5, 5, width - 10, height - 10);
            if (ic) {
#if 0 // TEST
		XSetICValues(ic, XNFocusWindow, window + 100, NULL);
                TRACE("invalid id = %d\n", window + 100);
		    // 不正なウィンドウIDを渡してみる
#elif 0  // TEST
                XSetICValues((XIC) 100, XNFocusWindow, window, NULL);
                    // 不正なIC
                    // これはクライアント側だけ落ちる。XFree86の実装
                    // がヘボい。
#else
                XSetICValues(ic, XNFocusWindow, window, NULL);
#endif // TEST
                XSetICFocus(ic);
                if (ic_style & XIMPreeditPosition)
                    moveInputPosition();
            }
        }
        break;
    case LeaveNotify:
        {
            GC gc = XCreateGC(disp, window, 0, 0);
            XSetForeground(disp, gc, getColor(disp, "white"));
            XDrawRectangle(disp, window, gc, 5, 5, width - 10, height - 10);
            if (ic)
                XUnsetICFocus(ic);
        }
        break;

#ifdef DEBUG
    case ButtonPress:
        if (ic && event.xbutton.button == Button3)
            ic_values_test(ic);
        break;
#endif  // DEBUG

#else
    // クリックによるフォーカス移動
    case ButtonPress:
        XSetInputFocus(disp, window, RevertToNone, event.xbutton.time);
#ifdef DEBUG
        if (ic && event.xbutton.button == Button3)
            ic_values_test(ic);
#endif  // DEBUG
        break;
    case FocusIn:
        {
            GC gc = XCreateGC(disp, window, 0, 0);
            XSetForeground(disp, gc, getColor(disp, "black"));
            XDrawRectangle(disp, window, gc, 5, 5, width - 10, height - 10);
            XFreeGC(disp, gc);
            
            if (ic) {
                XSetICValues(ic, XNFocusWindow, window, NULL);
                XSetICFocus(ic);
                if (ic_style & XIMPreeditPosition)
                    moveInputPosition();
            }
            setIMExtValue(disp, window, IMEXT_ON);
        }
        break;
    case FocusOut:
        {
            GC gc = XCreateGC(disp, window, 0, 0);
            XSetForeground(disp, gc, getColor(disp, "white"));
            XDrawRectangle(disp, window, gc, 5, 5, width - 10, height - 10);
            XFreeGC(disp, gc);
            
            if (ic)
                XUnsetICFocus(ic);
        }
        break;
#endif

    default:
        break;
    }
}

void Field::onKeyPressed(const XKeyEvent& event)
{
    TRACE("Field::onKeyPressed()\n");

    int buflen = 100;
        // 1999.05.07 buflenを小さくしてもXBufferOverflowが返ってこない。
    char* buf = new char[buflen];

    KeySym sym;
    int len;

    if (ic) {
        Status status;
        len = XmbLookupString(ic, const_cast<XKeyEvent*>(&event),
                                buf, buflen, &sym, &status);
        // TRACE("lookup-status = %d\n", status);
        if (status == XBufferOverflow) {
            TRACE("status == XBufferOverflow\n");
            delete [] buf;
            buflen = len + 1;
            buf = new char[buflen];
            len = XmbLookupString(ic, const_cast<XKeyEvent*>(&event),
                            buf, buflen, &sym, &status);
        }
/*
        switch (status)
        {
        case XLookupNone:
            // TRACE("XLookupNone\n");
            break;
        case XLookupKeySym:
            // TRACE("XLookupKeySym\n");
            break;
        case XLookupBoth:
            // TRACE("XLookupBoth\n");
            buf[len] = 0;
            update(buf);
            break;
        case XLookupChars:
            // TRACE("XLookupChars\n");
            buf[len] = 0;
            update(buf);
            break;
        default:
            assert(0);
        }
*/
    }
    else {
        len = XLookupString(const_cast<XKeyEvent*>(&event),
                                            buf, buflen, &sym, NULL);
    }

    if (len > 0) {
        buf[len] = '\0';
        update(buf);
    }

    delete [] buf;
}

void Field::moveInputPosition()
    // 未確定ウィンドウを移動させる
    // XIMPreeditPositionのとき，文字が確定する度に入力位置を移動させる。
{
    XPoint spot;
    XVaNestedList list;

    spot.x = 10 + XmbTextEscapement(font, str, strlen(str));
    spot.y = 30;
    list = XVaCreateNestedList(0, XNSpotLocation, &spot, NULL);
    XSetICValues(ic, XNPreeditAttributes, list, NULL);
    XFree(list);
}

void Field::setVisible(bool visible)
    // イベントフィルタを設定して，ウィンドウを表示
{
    if (visible)
        XMapWindow(disp, window);
}

////////////////////////////////////////////////////////////////////////

Display* init_display()
    // ロケールの初期化とXサーバーへの接続
{
    Display* disp = NULL;

    if (setlocale(LC_ALL, "") == NULL) {
        error("error: cannot set locale\n");
        return NULL;
    }
    if ((disp = XOpenDisplay(NULL)) == NULL) {
        error("error: cannot open display\n");
        return NULL;
    }
    if (!XSupportsLocale()) {
        error("error: X does not support locale \"%s\"\n",
                                    setlocale(LC_ALL, NULL));
        return NULL;
    }
    return disp;
}

void printIMServers(Display* disp)
{
    Atom XA_XIM_SERVERS = XInternAtom(disp, "XIM_SERVERS", False);

    Window root = XRootWindow(disp, 0);
    Atom type = 0;
    int format = 0;
    unsigned long nitems = 0;
    unsigned long bytes_after;
    unsigned char* value = NULL;

    XGetWindowProperty(disp, root, XA_XIM_SERVERS, 0, 1000, False,
                       AnyPropertyType, &type, &format, &nitems,
                       &bytes_after, &value);
    if (type == XA_ATOM && format == 32 && nitems > 0) {
        Atom* atoms = (Atom*) value;
        for (unsigned int i = 0; i < nitems; i++) {
            char* const s = XGetAtomName(disp, atoms[i]);
            printf("im server: '%s'\n", s);
            XFree(s);
        }
    }
    XFree(value);
}

int main(int argc, char* argv[])
{
    disp = init_display();
    assert(disp);

    printIMServers(disp);

    if (argc != 4) {
        printf("usage: ximtest <im_server> <preedit> <status>\n");
        printf("    style: area callback position nothing\n");
        return 1;
    }

    string modif = string("@im=") + argv[1];
    const char* cur_modif = XSetLocaleModifiers(modif.c_str());
    if (cur_modif == NULL) {
        error("warning: cannot set locale modifiers\n");
        return 1;
    }
    TRACE("modif = '%s'\n", cur_modif);

    // スタイルを決める
    string preedit = argv[2];
    string status = argv[3];

    int pre_style = 0;
    int sta_style = 0;
    if (preedit == "off" || preedit == "area")
        pre_style = XIMPreeditArea;
    else if (preedit == "on" || preedit == "callback")
        pre_style = XIMPreeditCallbacks;
    else if (preedit == "over" || preedit == "position")
        pre_style = XIMPreeditPosition;
    else if (preedit == "root" || preedit == "nothing")
        pre_style = XIMPreeditNothing;

    if (status == "off" || status == "area")
        sta_style = XIMStatusArea;
    else if (status == "on" || status == "callback")
        sta_style = XIMStatusCallbacks;
    else if (status == "root" || status == "nothing")
        sta_style = XIMStatusNothing;

    assert(pre_style && sta_style);
    XIMStyle need_style = pre_style | sta_style;
    printInputStyle(need_style);

    MainWnd* top = new MainWnd(need_style);
    im.attachInputMethodListener(top);
    top->setVisible(true);

    delete top;
    return 0;
}

#if 0
    XFlush(disp);
        // ウィンドウが生成 (map?) される前にXCreateIC()すると，
        // kinput2ではICの生成に失敗する
#endif

/*
onStrConversion(XIC ic, void* client_data,
                        XIMStringConversionCallbackStruct* cbs)
{
}

onPreeditStateNotify(XIC ic, void* client_data,
                        XIMPreeditStateNotifyCallbackStruct* cbs)
{
}
*/
