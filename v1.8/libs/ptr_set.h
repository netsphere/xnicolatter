
#ifndef PTR_SET_H
#define PTR_SET_H

////////////////////////////////////////////////////////////////////////
// ptr_set

#include <set>

template <typename Ty, typename _COMP = std::less<Ty> >
class ptr_set: public std::set<Ty, _COMP>
{
    typedef std::set<Ty, _COMP> super;

public:
    typedef typename super::iterator iterator;
    typedef typename super::size_type size_type;
    typedef typename super::key_type key_type;

    ptr_set() { }
    virtual ~ptr_set() { clear(); }

    virtual void clear() {
        iterator i;
        for (i = super::begin(); i != super::end(); i++)
            delete *i;
        super::clear();
    }

    virtual size_type erase(key_type const& x) {
        Ty ptr = x;
        size_t retval = super::erase(x);
        delete ptr;
        return retval;
    }

    virtual void erase(iterator i) {
        Ty ptr = *i;
        super::erase(i);
        delete ptr;
    }
};

#endif // !PTR_SET_H
