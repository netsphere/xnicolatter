/*
 * Gladeの作成した初期main.cファイルです. 編集が必要です.
 * Gladeはこのファイルを上書きしません.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "interface.h"
#include "support.h"

int
main (int argc, char *argv[])
{
  GtkWidget *addword_window;
/*
  bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
  textdomain (PACKAGE);
*/
  gtk_set_locale ();
  gtk_init (&argc, &argv);
/*
  add_pixmap_directory (PACKAGE_DATA_DIR "/pixmaps");
  add_pixmap_directory (PACKAGE_SOURCE_DIR "/pixmaps");
*/

  /*
   * The following code was added by Glade to create one of each component
   * (except popup menus), just so that you see something after building
   * the project. Delete any components that you don't want shown initially.
   */
  addword_window = create_addword_window ();
  gtk_widget_show (addword_window);

  gtk_main ();
  return 0;
}

