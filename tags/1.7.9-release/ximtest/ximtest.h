// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

////////////////////////////////////////////////////////////////////////
// CWindow

class CWindow
{
public:
  Window window;

  CWindow(): window(0) { }
  virtual ~CWindow() { }
};

////////////////////////////////////////////////////////////////////////
// Field

class Field: public CWindow
{
public:
    XFontSet font;  // ICを共有 -> fontを揃える
private:
    XIC ic;
    XIMStyle ic_style;
    char str[1000];
    int width, height;

public:
    Field(Window parent, int x, int y, int w, int h);
    virtual ~Field();
    virtual void eventPerformed(const XEvent& event);
    virtual void setVisible(bool );
    void attachIC(XIC ic_, XIMStyle style);
    void detachIC();
private:
    virtual void onKeyPressed(const XKeyEvent& ev);
    void moveInputPosition();
    void update(const char* adds);
};

////////////////////////////////////////////////////////////////////////
// MainWnd

class MainWnd: public CWindow, public InputMethodListener
{
    typedef ptr_vector<Field*> Fields;

    Fields fields;
    Window preedit_area, status_area;
    XIMStyle need_style;    // コマンドラインの引数で与えたスタイル
    XIMStyle ic_style;      // IMサーバーとの折衝で決定したスタイル
    XIC ic;             // ICをフィールド間で共有する
    XFontSet font;

public:
    MainWnd(XIMStyle need_style);
    virtual ~MainWnd();
    virtual void setVisible(bool );

    void drawPreedit();

private:
    virtual void inputMethodInstantiated();
    virtual void inputMethodDestroyed();
    void create_ui();
    void createGCs();
    XIC createIC(XIM im, XIMStyle style, XFontSet fs, Window window);
};

