// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include <config.h>

#include <cstdio>
#include <cstring>
#include <cerrno>
#include <sys/types.h>  // workaround for FreeBSD; type 'u_char'
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#ifndef HAVE_SOCKLEN_T
  #include <inttypes.h>
typedef uint32_t socklen_t;
#endif

#include "qSocket.h"

using namespace std;

CSocket::CSocket(): sock_fd(-1), port(0)
{
}

CSocket::~CSocket()
{
    // 2000.09.16 closeの処理とか入れないように。おもしろいことになるから。
}

CSocket setupServer(int port) throw(SystemCallError)
    // 入力
    //     port == 0のとき空いてるポートを選択 
    // 戻り値
    //     エラー発生 = -1
{
    CSocket result;

    result.sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (result.sock_fd == -1) {
        int e = errno;
        perror("socket open failed");
        throw SystemCallError(e);
    }

    int reuse = 1;
    setsockopt(result.sock_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));

    sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port);
    if (bind(result.sock_fd, (sockaddr*) &addr, sizeof(addr)) == -1) {
        int e = errno;
        perror("bind failed");
        close(result.sock_fd);
        throw SystemCallError(e);
    }

    if (port)
        result.port = port;
    else {
        socklen_t socklen = sizeof(addr);
        if (getsockname(result.sock_fd, (sockaddr*) &addr, &socklen) == -1) {
            int e = errno;
            perror("get port failed");
            close(result.sock_fd);
            throw SystemCallError(e);
        }
        result.port = ntohs(addr.sin_port);
    }

    if (listen(result.sock_fd, 5) == -1) {
        int e = errno;
        perror("listen failed");
        close(result.sock_fd);
        throw SystemCallError(e);
    }

    return result;
}
