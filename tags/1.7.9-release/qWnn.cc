// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include "config.h"

#ifdef USE_WNN

#include <cstdio>
#include <cassert>
#include "qWnn.h"
#include "misc.h"

////////////////////////////////////////////////////////////////////////

#ifndef WNN_JSERVER_ENV
#define WNN_JSERVER_ENV     "JSERVER"
#endif

#ifndef WNN_J_LANG
#define WNN_J_LANG      "ja_JP"
#endif

////////////////////////////////////////////////////////////////////////

size_t wnn_mbstowcs(w_char* pcode, const char* euc, size_t )
    // EUC文字列をプロセスコードに変換する。出来合いの関数があるはずだが。
/*
Name: Extended_UNIX_Code_Fixed_Width_for_Japanese
MIBenum: 19
Source: Used in Japan.  Each character is 2 octets.
                code set 0: JIS Roman (a single 7-bit byte set)
                              1st byte = 00
                              2nd byte = 20-7E
                code set 1: JIS X0208-1990 (a double 7-bit byte set)
                            restricted  to A0-FF in both bytes
                code set 2: Half Width Katakana (a single 7-bit byte set)
                              1st byte = 00
                              2nd byte = A0-FF
                code set 3: JIS X0212-1990 (a double 7-bit byte set)
                            restricted to A0-FF in
                            the first byte
                            and 21-7E in the second byte
Alias: csEUCFixWidJapanese
*/
{
    w_char* r = pcode;
    const unsigned char* p = (unsigned char*) euc;
    while (*p) {
        if (*p == 0x8e) {
            // 半角カナ
            *(pcode++) = *(p + 1) | 0x80;
            p += 2;
        }
        if (*p & 0x80) {
            // 漢字
            *(pcode++) = (*p << 8) + *(p + 1);
            p += 2;
        }
        else {
            // ASCII
            *(pcode++) = *(p++);
        }
    }
    *pcode = 0;
    return pcode - r;
}

size_t wnn_wcstombs(char* euc, const w_char* pcode, size_t )
{
    if (!pcode)
        return 0;

    char* r = euc;
    while (*pcode) {
        char b1 = (*pcode >> 8) & 0xff;
        char b2 = *pcode & 0xff;
        if (b1) {
            // 漢字
            *(euc++) = b1; *(euc++) = b2;
        }
        else {
            if (b2 & 0x80) {
                // 半角カナ
                *(euc++) = 0x8e;
                *(euc++) = b2;
            }
            else {
                // ASCII
                *(euc++) = b2;
            }
        }
        pcode++;
    }
    *euc = 0;
    return euc - r;
}

static int confirm(const char* s)
    // ユーザーの環境がない場合に呼ばれる
{
    error("jl_open_lang(): %s\n", s);
    return 1;
}

static int err_out(const char* s)
{
    error("%s\n", s);
    return 0;
}

struct wnn_buf* wnn_connect()
{
    const char* server = getenv(WNN_JSERVER_ENV);
    if (server && !*server)
        server = NULL; 

    const char* rcfile_ = getenv("WNNENVRC");
    string rcfile;
    if (!rcfile_ || !*rcfile_) {
        // TRACE("環境変数‘WNNENVRC’が定義されていません\n");
        rcfile = WNN_ENVRCFILE;
    }
    else
        rcfile = rcfile_;

    wnn_buf* wnn = jl_open_lang(server_name, server, WNN_J_LANG,
                            rcfile.c_str(), confirm, err_out, 5);
    if (!wnn) {
        error("error: Cannot create Wnn environment.\n");
        return NULL;
    }
    else if (!jl_isconnect(wnn)) {
        error("error: Cannot connect Wnn conversion server.\n");
        return NULL;
    }

    return wnn;
}

////////////////////////////////////////////////////////////////////////
// qWnn

qWnn::qWnn()
{
    wnn = wnn_connect();
}

qWnn::~qWnn()
{
    close();
}

bool qWnn::is_open() const
{
    return wnn != NULL;
}

void qWnn::close()
{
    if (wnn) {
        jl_dic_save_all(wnn);
        jl_close(wnn);
        wnn = NULL;
    }
}

void qWnn::convert(const char* str)
    // 全変換
{
    if (!wnn)
        return;

    int bufsiz = strlen(str) + 1;
    w_char* buf = new w_char[bufsiz];
    wnn_mbstowcs(buf, str, bufsiz);
//DEBUG
    for (int i = 0; i < bufsiz; i++) {
        printf("%04x ", buf[i]);
    }
    printf("\n");
//DEBUG
    int r = jl_ren_conv(wnn, buf, 0, -1, WNN_USE_MAE);
    if (r == -1) {
        printf("wnn error: %s\n", wnn_perror());
        assert(0);
    }
    delete [] buf;
}

bool qWnn::set_clause_len(
        int clause, // 文節位置
        int len)    // 文節の文字長（バイト長ではない）
    // 文節の長さを変更して再変換する
{
    if (!wnn)
        return false;
    
    TRACE("clause = %d, len = %d\n", clause, len);

    if (clause >= jl_bun_suu(wnn))
        return false;
    int r = jl_nobi_conv(wnn, clause, len, -1, WNN_USE_MAE, WNN_SHO);
    assert(r != -1);
        // 第4引数にclause + 1  --> 文節を詰めるときに対象文節の次の文節が
        // 1文字ずつになってしまう
        // 第6引数にWNN_DAI --> 文節伸ばしで対象文節が2分される
        // （5文字の時に伸ばすと3文字になったりする）
    return true;
}

string qWnn::getComposition(int clause) const
    // 機能
    //      Wnnの内部バッファから変換結果を取り出す。
    // 入力
    //      clause  0以上の文節番号
{
    if (!wnn)
        return "";

    if (clause >= jl_bun_suu(wnn))
        return "";

    w_char buf[1000];
    char buf2[1000];
    jl_get_kanji(wnn, clause, clause + 1, buf);
    wnn_wcstombs(buf2, buf, sizeof(buf2));
    return buf2;
}

string qWnn::determineFirstClause()
{
    if (!wnn)
        return "";

    string r = getComposition(0);
    jl_update_hindo(wnn, 0, 1);
    jl_kill(wnn, 0, 1);
    return r;
}

void qWnn::updateLearning()
{
    if (!wnn)
        return;

    jl_update_hindo(wnn, 0, -1);
}

int qWnn::getReadLength(int clause) const
    // 文節の読みのバイト数を返す
{
    if (!wnn)
        return 0;

    if (clause >= jl_bun_suu(wnn))
        return 0;

    // jl_yomi_len()は文字単位で返すので，バイト単位に変換
    w_char buf[1000];
    char buf2[1000];
    jl_get_yomi(wnn, clause, clause + 1, buf);
    return wnn_wcstombs(buf2, buf, sizeof(buf2));
}

int qWnn::countClause() const
{
    if (!wnn)
        return -1;

    return jl_bun_suu(wnn);
}

int qWnn::nextCandidate(int clause, int next)
    // 戻り値
    //     候補のインデックス
{
    if (!wnn)
        return -1;

    jl_zenkouho(wnn, clause, WNN_USE_MAE, WNN_UNIQ_KNJ);
    int r = -1;
    if (next > 0) {
        while (--next >= 0)
            r = jl_next(wnn);
    }
    else if (next < 0) {
        while (++next <= 0)
            r = jl_previous(wnn);
    }
    return r;
}

CandidateList qWnn::getCandidateList(int clause) const
    // 機能
    //      候補一覧を取得する。
    // 入力
    //      clause  0以上の文節番号
    // 解説
    //      戻り値は利用者がdeleteすること
{
    CandidateList r;

    if (!wnn)
        return r;

    if (clause >= jl_bun_suu(wnn))
        return r;

    jl_zenkouho(wnn, clause, WNN_USE_MAE, WNN_UNIQ_KNJ);
    int n = jl_zenkouho_suu(wnn);
    if (!n)
        return r;

    for (int i = 0; i < n; i++) {
        w_char buf[1000];
        char buf2[1000];
        jl_get_zenkouho_kanji(wnn, i, buf);
        wnn_wcstombs(buf2, buf, sizeof(buf2));
        string s = buf2;
        r.push_back(s);
    }
    return r;
}

#endif  // USE_WNN
