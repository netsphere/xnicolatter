/*
 * 編集禁止! - このファイルはGladeによって生成されています.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

GtkWidget*
create_addword_window (void)
{
  GtkWidget *addword_window;
  GtkWidget *vbox1;
  GtkWidget *table1;
  GtkWidget *read_entry;
  GtkWidget *kanji_entry;
  GtkWidget *comment_entry;
  GtkWidget *hinsi_combo;
  GtkWidget *hinsi_combo_entry;
  GtkWidget *dic_combo;
  GtkWidget *dic_combo_entry;
  GtkWidget *label6;
  GtkWidget *label2;
  GtkWidget *label3;
  GtkWidget *label4;
  GtkWidget *label5;
  GtkWidget *hseparator2;
  GtkWidget *hbox1;
  GtkWidget *ok_button;
  GtkWidget *cancel_button;

  addword_window = gtk_window_new (GTK_WINDOW_DIALOG);
  gtk_widget_set_name (addword_window, "addword_window");
  gtk_object_set_data (GTK_OBJECT (addword_window), "addword_window", addword_window);
  gtk_window_set_title (GTK_WINDOW (addword_window), _("add word"));

  vbox1 = gtk_vbox_new (FALSE, 10);
  gtk_widget_set_name (vbox1, "vbox1");
  gtk_widget_ref (vbox1);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "vbox1", vbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox1);
  gtk_container_add (GTK_CONTAINER (addword_window), vbox1);
  gtk_container_set_border_width (GTK_CONTAINER (vbox1), 10);

  table1 = gtk_table_new (5, 2, FALSE);
  gtk_widget_set_name (table1, "table1");
  gtk_widget_ref (table1);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "table1", table1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (table1);
  gtk_box_pack_start (GTK_BOX (vbox1), table1, TRUE, TRUE, 0);
  gtk_table_set_row_spacings (GTK_TABLE (table1), 5);
  gtk_table_set_col_spacings (GTK_TABLE (table1), 5);

  read_entry = gtk_entry_new ();
  gtk_widget_set_name (read_entry, "read_entry");
  gtk_widget_ref (read_entry);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "read_entry", read_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (read_entry);
  gtk_table_attach (GTK_TABLE (table1), read_entry, 1, 2, 0, 1,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  kanji_entry = gtk_entry_new ();
  gtk_widget_set_name (kanji_entry, "kanji_entry");
  gtk_widget_ref (kanji_entry);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "kanji_entry", kanji_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (kanji_entry);
  gtk_table_attach (GTK_TABLE (table1), kanji_entry, 1, 2, 1, 2,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  comment_entry = gtk_entry_new ();
  gtk_widget_set_name (comment_entry, "comment_entry");
  gtk_widget_ref (comment_entry);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "comment_entry", comment_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (comment_entry);
  gtk_table_attach (GTK_TABLE (table1), comment_entry, 1, 2, 2, 3,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  hinsi_combo = gtk_combo_new ();
  gtk_widget_set_name (hinsi_combo, "hinsi_combo");
  gtk_widget_ref (hinsi_combo);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "hinsi_combo", hinsi_combo,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hinsi_combo);
  gtk_table_attach (GTK_TABLE (table1), hinsi_combo, 1, 2, 3, 4,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  hinsi_combo_entry = GTK_COMBO (hinsi_combo)->entry;
  gtk_widget_set_name (hinsi_combo_entry, "hinsi_combo_entry");
  gtk_widget_ref (hinsi_combo_entry);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "hinsi_combo_entry", hinsi_combo_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hinsi_combo_entry);
  gtk_entry_set_editable (GTK_ENTRY (hinsi_combo_entry), FALSE);

  dic_combo = gtk_combo_new ();
  gtk_widget_set_name (dic_combo, "dic_combo");
  gtk_widget_ref (dic_combo);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "dic_combo", dic_combo,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (dic_combo);
  gtk_table_attach (GTK_TABLE (table1), dic_combo, 1, 2, 4, 5,
                    (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);

  dic_combo_entry = GTK_COMBO (dic_combo)->entry;
  gtk_widget_set_name (dic_combo_entry, "dic_combo_entry");
  gtk_widget_ref (dic_combo_entry);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "dic_combo_entry", dic_combo_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (dic_combo_entry);
  gtk_entry_set_editable (GTK_ENTRY (dic_combo_entry), FALSE);

  label6 = gtk_label_new (_("読み："));
  gtk_widget_set_name (label6, "label6");
  gtk_widget_ref (label6);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "label6", label6,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label6);
  gtk_table_attach (GTK_TABLE (table1), label6, 0, 1, 0, 1,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label6), 1, 0.5);

  label2 = gtk_label_new (_("漢字："));
  gtk_widget_set_name (label2, "label2");
  gtk_widget_ref (label2);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "label2", label2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label2);
  gtk_table_attach (GTK_TABLE (table1), label2, 0, 1, 1, 2,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label2), 1, 0.5);

  label3 = gtk_label_new (_("コメント："));
  gtk_widget_set_name (label3, "label3");
  gtk_widget_ref (label3);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "label3", label3,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label3);
  gtk_table_attach (GTK_TABLE (table1), label3, 0, 1, 2, 3,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_misc_set_alignment (GTK_MISC (label3), 1, 0.5);

  label4 = gtk_label_new (_("品詞："));
  gtk_widget_set_name (label4, "label4");
  gtk_widget_ref (label4);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "label4", label4,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label4);
  gtk_table_attach (GTK_TABLE (table1), label4, 0, 1, 3, 4,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label4), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label4), 1, 0.5);

  label5 = gtk_label_new (_("辞書："));
  gtk_widget_set_name (label5, "label5");
  gtk_widget_ref (label5);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "label5", label5,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (label5);
  gtk_table_attach (GTK_TABLE (table1), label5, 0, 1, 4, 5,
                    (GtkAttachOptions) (GTK_FILL),
                    (GtkAttachOptions) (0), 0, 0);
  gtk_label_set_justify (GTK_LABEL (label5), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (label5), 1, 0.5);

  hseparator2 = gtk_hseparator_new ();
  gtk_widget_set_name (hseparator2, "hseparator2");
  gtk_widget_ref (hseparator2);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "hseparator2", hseparator2,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hseparator2);
  gtk_box_pack_start (GTK_BOX (vbox1), hseparator2, FALSE, TRUE, 0);

  hbox1 = gtk_hbox_new (TRUE, 0);
  gtk_widget_set_name (hbox1, "hbox1");
  gtk_widget_ref (hbox1);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "hbox1", hbox1,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (hbox1);
  gtk_box_pack_start (GTK_BOX (vbox1), hbox1, FALSE, TRUE, 0);

  ok_button = gtk_button_new_with_label (_("登録"));
  gtk_widget_set_name (ok_button, "ok_button");
  gtk_widget_ref (ok_button);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "ok_button", ok_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (ok_button);
  gtk_box_pack_start (GTK_BOX (hbox1), ok_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (ok_button, 70, -2);

  cancel_button = gtk_button_new_with_label (_("閉じる"));
  gtk_widget_set_name (cancel_button, "cancel_button");
  gtk_widget_ref (cancel_button);
  gtk_object_set_data_full (GTK_OBJECT (addword_window), "cancel_button", cancel_button,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (cancel_button);
  gtk_box_pack_start (GTK_BOX (hbox1), cancel_button, FALSE, FALSE, 0);
  gtk_widget_set_usize (cancel_button, 70, -2);

  gtk_signal_connect (GTK_OBJECT (addword_window), "realize",
                      GTK_SIGNAL_FUNC (on_addword_window_realize),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (ok_button), "clicked",
                      GTK_SIGNAL_FUNC (on_ok_button_clicked),
                      NULL);
  gtk_signal_connect (GTK_OBJECT (cancel_button), "clicked",
                      GTK_SIGNAL_FUNC (on_cancel_button_clicked),
                      NULL);

  return addword_window;
}

