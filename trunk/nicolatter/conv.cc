// Q's Nicolatter for X
// Copyright (c) 1998-2003 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// -*- encoding: euc-jp -*-

#include "../config.h"
#include "conv.h"

//////////////////////////////////////////////////////////////////////
// KanaKanjiConv

// かな漢字変換の抽象クラス

KanaKanjiConv::KanaKanjiConv(InputContext* ic_): ic(ic_)
{
}

KanaKanjiConv::~KanaKanjiConv()
{
}

string KanaKanjiConv::getDetermined() const
{
    return determined;
}

void KanaKanjiConv::clearDetermined()
{
    determined = "";
}
