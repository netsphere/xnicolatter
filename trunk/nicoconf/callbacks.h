#include <gtk/gtk.h>


void
onRealized                             (GtkWidget       *widget,
                                        gpointer         user_data);

void
onButtonToggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
onKanaKeyChoose                        (GtkButton       *button,
                                        gpointer         user_data);

void
onMapFileChoose                        (GtkButton       *button,
                                        gpointer         user_data);

void
onLeftShiftChoose                      (GtkButton       *button,
                                        gpointer         user_data);

void
onRightShiftChoose                     (GtkButton       *button,
                                        gpointer         user_data);

void
onRomaFileChoose                       (GtkButton       *button,
                                        gpointer         user_data);

void
onOK                                   (GtkButton       *button,
                                        gpointer         user_data);

void
onCancel                               (GtkButton       *button,
                                        gpointer         user_data);

void
onButtonToggled                        (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_preedit_sel_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_status_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_nicoconf_delete_event               (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_cnv_fg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_cnv_bg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_inp_fg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_inp_bg_sel_clicked                  (GtkButton       *button,
                                        gpointer         user_data);
