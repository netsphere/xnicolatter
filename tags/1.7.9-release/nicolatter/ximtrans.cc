// Q's Nicolatter for X
// Copyright (c) 1998-2001 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

// このファイル
//     XIMでの通信パケット

#include "../config.h"

#include <cstdio>
#include <string>
#include <cassert>
#include <cerrno>
#include <unistd.h>

#include "XimProto.h"
#include "preedit.h"
#include "status.h"
#include "client.h"
#include "ximtrans.h"
#include "start.h"

#include <misc.h>

using namespace std;

////////////////////////////////////////////////////////////////////////
// XimWriter

XimWriter::XimWriter(bool isBigEndian, int opcode): SendBuffer(isBigEndian)
{
    buf[0] = opcode;
    buf[1] = 0; // minor
    cur = buf + XIM_HEADER_SIZE;
}

XimWriter::~XimWriter()
{
}

void XimWriter::card8(unsigned char val)
{
    *cur++ = val;
}

void XimWriter::str16(const char* s)
{
    assert(s);
    int len = strlen(s);
    card16(len);
    memcpy(cur, s, len);
    cur += len;
}

void XimWriter::xkeyevent(const XKeyEvent& e)
{
    card16((e.serial >> 16) & 0xffff);
    *(cur++) = e.type | (e.send_event ? 0x80 : 0);
    *(cur++) = e.keycode;
    card16(e.serial & 0xffff);
    card32(e.time);
    card32(e.root);
    card32(e.window);
    card32(e.subwindow);
    card16(e.x_root);
    card16(e.y_root);
    card16(e.x);
    card16(e.y);
    card16(e.state);
    *(cur++) = e.same_screen;
}

int XimWriter::getData(unsigned char* out)
    // out == NULLの場合は必要なバイト数を返す
{
    assert(reserved.size() == 0);

    pad();
    ssize_t len = cur - buf;
    assert(!(len % 4));
    assert(sizeof(buf) >= (size_t) len);
    if (!out)
        return len;
    
    int siz = (len - XIM_HEADER_SIZE) / 4;
    // TRACE("len = %d, siz = %d\n", len, siz);
    if (bigEndian) {
        buf[2] = (siz >> 8) & 0xff;
        buf[3] = siz & 0xff;
    }
    else {
        buf[2] = siz & 0xff;
        buf[3] = (siz >> 8) & 0xff;
    }
    debug_out();

    memcpy(out, buf, len);
    cur = buf + XIM_HEADER_SIZE;
    return len;
}

////////////////////////////////////////////////////////////////////////
// XimReader

XimReader::XimReader(bool isBigEndian): RecvBuffer(isBigEndian)
{
}

bool XimReader::input(int fd)
    // XIM TCP transport
    // 戻り値
    //     ソケットが閉じられている or エラーのときfalse
{
    assert(filled == false);

    ssize_t r;
    int len; // ヘッダを含むバイト数

    if (!buf)
        buf = (unsigned char*) malloc(XIM_HEADER_SIZE);
    while (count_ < XIM_HEADER_SIZE) {
        r = read(fd, buf + count_, XIM_HEADER_SIZE - count_);
        if (r == -1) {
            if (errno == EAGAIN) {
                TRACE("not filled: count = %d\n", count_);
                debug_out();
                return true;
            }
            else if (errno == EINTR)
                continue;
            else {
                assert(0); // TODO: error handle
            }
        }
        if (r == 0)
            goto closed;
        count_ += r;
    }

    if (count_ == XIM_HEADER_SIZE && buf[0] == XIM_CONNECT) {
        buf = (unsigned char*) realloc(buf, XIM_HEADER_SIZE + 1);
        assert(buf);
        while (count_ < XIM_HEADER_SIZE + 1) {
            r = read(fd, buf + XIM_HEADER_SIZE, 1);
            if (r == -1) {
                if (errno == EAGAIN) {
                    TRACE("not filled: count = %d\n", count_);
                    debug_out();
                    return true;
                }
                else if (errno == EINTR)
                    continue;
                else {
                    assert(0); // TODO: error handle
                }
            }
            if (r == 0)
                goto closed;
            count_ += 1;
        }
        if (buf[4] == BIGENDIAN)
            bigEndian = true;
        else if (buf[4] == LITTLEENDIAN)
            bigEndian = false;
        else {
            error("XIM transport endian error.\n");
            assert(0);
        }
        TRACE("endian = %d\n", bigEndian);
    }

    if (bigEndian)
        len = XIM_HEADER_SIZE + ((buf[2] << 8) + buf[3]) * 4;
    else
        len = XIM_HEADER_SIZE + (buf[2] + (buf[3] << 8)) * 4;
    TRACE("len = %d\n", len);

    buf = (unsigned char*) realloc(buf, len);
    assert(buf);
    while (count_ < len) {
        r = read(fd, buf + count_, len - count_);
        if (r == -1) {
            if (errno == EAGAIN) {
                TRACE("not filled: count = %d\n", count_);
                debug_out();
                return true;
            }
            else if (errno == EINTR)
                continue;
            else {
                assert(0); // TODO: error handle
            }
        }
        if (r == 0)
            goto closed;
        count_ += r;
    }
    debug_out();

    cur = buf + XIM_HEADER_SIZE;
    filled = true;
    return true;

closed:
    TRACE("XIM recv: client closed\n");
    cur = buf + XIM_HEADER_SIZE;
    count_ = 0;
    filled = false;
    return false;
}

XimReader::XimReader(bool isBigEndian, const char* b): RecvBuffer(isBigEndian)
    // XIM X transport
{
    assert(b);

    buf = (unsigned char*) malloc(XIM_HEADER_SIZE);
    assert(buf);
    memcpy(buf, b, XIM_HEADER_SIZE);
    
    if (buf[0] == XIM_CONNECT) {
        if (b[4] == BIGENDIAN)
            bigEndian = true;
        else if (b[4] == LITTLEENDIAN)
            bigEndian = false;
        else {
            error("endian error.\n");
            assert(0);
        }
    }

    int len;
    if (bigEndian)
        len = ((buf[2] << 8) + buf[3]) * 4;
    else
        len = (buf[2] + (buf[3] << 8)) * 4;

    buf = (unsigned char*) realloc(buf, XIM_HEADER_SIZE + len);
    assert(buf);
    memcpy(buf + XIM_HEADER_SIZE, b + XIM_HEADER_SIZE, len);
    count_ = XIM_HEADER_SIZE + len;
    debug_out();

    cur = buf + XIM_HEADER_SIZE;
}

XimReader::~XimReader()
{
    if (buf) {
        free(buf);
        buf = NULL;
    }
}

/*
bool XimReader::next() 
{
    int siz;
    if (bigEndian)
        siz = ((start[2] << 8) + start[3]) * 4 + XIM_HEADER_SIZE;
    else
        siz = (start[2] + (start[3] << 8)) * 4 + XIM_HEADER_SIZE;
    
    start += siz;
    cur = start + XIM_HEADER_SIZE;
    return start - buf < count_;
}
*/

int XimReader::opcode() const
{
    return buf[0];
}

string XimReader::str8()
{
    string r;
    int len = *(cur++);
    r.assign((char*) cur, len);
    cur += len;
    return r;
}

string XimReader::str16()
{
    string r;
    int len = card16();
    r.assign((char*) cur, len);
    cur += len;
    return r;
}

KeyEvent XimReader::keyevent()
    // CARD16 serial hi
    // XEVENT X event
{
    XKeyEvent e;
    uint16_t serial_hi = card16();
    e.type = *cur & 0x7f;
    e.send_event = (*(cur++) & 0x80) != 0;
    e.keycode = *(cur++);
    e.serial = card16() + (serial_hi << 16);
    e.time = card32();
    e.root = card32();
    e.window = card32();
    e.subwindow = card32();
    e.x_root = card16();
    e.y_root = card16();
    e.x = card16();
    e.y = card16();
    e.state = card16();
    e.same_screen = *(cur++);
    e.display = top_display;

    return KeyEvent(e);
}

