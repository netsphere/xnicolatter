// Q's Nicolatter for X
// Copyright (c) 1998-2002 HORIKAWA Hisashi. All rights reserved.
//     mailto:vzw00011@nifty.ne.jp
//     http://www2.airnet.ne.jp/pak04955/

#include "../config.h"
#ifdef USE_CANNA

#include <inttypes.h>
#include "conv.h"

//////////////////////////////////////////////////////////////////////
// CannaConv

// ワイド文字の大きさ
#ifdef CANNA37
  #ifdef CANNA_WCHAR16
typedef uint16_t CannaWChar;
  #else
typedef uint32_t CannaWChar;
  #endif
#else
  #ifdef WCHAR16
typedef unsigned short CannaWChar;
  #else
typedef wchar_t CannaWChar;
  #endif
#endif

// 文字エンコーディング形式
#if defined(CANNA_WCHAR) || defined(CANNA_WCHAR16)
  extern int c16_mbtowc(CannaWChar* wc, const char* p, size_t );
  extern size_t c16_wcstombs(char* s, const CannaWChar* wcs, size_t n);
  extern int c16_wctomb(char* s, CannaWChar wc);
#else
  #define c16_mbtowc mbtowc
  #define c16_wcstombs wcstombs
  #define c16_wctomb wctomb
#endif  // CANNA_WCHAR

struct EchoLine
{
    CannaWChar line[1000];
    int length;
    int revPos, revLen;

    EchoLine();
};

class CannaCandidate;
class CannaConv: public KanaKanjiConv
{
public:
    typedef KanaKanjiConv super;

    EchoLine echo;
    EchoLine cand;
private:
    int context;
    CannaCandidate* candWindow;

public:
    CannaConv(InputContext* ic);
    virtual ~CannaConv();

    virtual KanaKanjiStatus input(const KeyEvent& event, int level);

    virtual void all_determine();
    virtual void clear();
    virtual KanaMode getKanaMode() const;
    virtual void setKanaMode(KanaMode mode);
    virtual PreeditWindow* createPreeditWindow();
    virtual StatusWindow* createStatusWindow();
    virtual void setCandidateVisible(bool );
    virtual void updateCandidate();
    string getModeLine() const;
};

#endif  // USE_CANNA
