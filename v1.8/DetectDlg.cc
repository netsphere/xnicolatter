// Q's Nicolatter for X
// Copyright (c) 1998-2000 Hisashi HORIKAWA. All rights reserved.

#include <X11/keysym.h>
#include <gdk/gdkx.h>
#include "DetectDlg.h"

//////////////////////////////////////////////////////////////////////
// DetectDlg

DetectDlg::DetectDlg(GtkWindow* parent): Dialog(parent, true)
{
}

DetectDlg::~DetectDlg()
{
}

void DetectDlg::onCancel(GtkButton* button, DetectDlg* this_)
    // GtkButton::clicked
{
    this_->keycode = -1;
    this_->exit(IDCANCEL);
}

void DetectDlg::onKeyPressed(GtkWidget* widget, GdkEventKey* event, DetectDlg* this_)
{
    if (event->keyval == XK_Shift_L || event->keyval == XK_Shift_R)
        return; // 1999.05.05 [Caps Lock]が入っていると[Shift]でキーを挟む。かなり謎。

    this_->keycode = XKeysymToKeycode(GDK_DISPLAY(), event->keyval);
    this_->exit(IDOK);
}

int DetectDlg::setVisible(bool visible)
{
    if (visible) {
        if (!window) {
            window = GTK_WINDOW(gtk_dialog_new());
            gtk_window_set_title(window, "キーの検出");

            GtkDialog* me = GTK_DIALOG(window);

            GtkWidget* msg = gtk_label_new("設定するキーを押して下さい");
            gtk_widget_show(msg);
            gtk_misc_set_padding(GTK_MISC(msg), 10, 10);
            gtk_box_pack_start(GTK_BOX(me->vbox), msg, TRUE, TRUE, 0);

            GtkWidget* button = gtk_button_new_with_label("キャンセル");
            gtk_widget_show(button);
            gtk_signal_connect(GTK_OBJECT(button), "clicked",
                                    GTK_SIGNAL_FUNC(onCancel), this);
            gtk_box_pack_start(GTK_BOX(me->action_area), button,
                                    TRUE, TRUE, 0);

            gtk_signal_connect(GTK_OBJECT(me), "key_press_event",
                                    GTK_SIGNAL_FUNC(onKeyPressed), this);
        }

        keycode = -1;
    }
    return Dialog::setVisible(visible);
}

