// Q's C++ Library
// Copyright (c) 1998-1999 Hisashi HORIKAWA. All rights reserved.

#ifndef QSLIB_WINDOW
#define QSLIB_WINDOW

#if !defined(__GTK_H__) && !defined(_XtIntrinsic_h)
  #error must include <gtk/gtk.h> or <X11/Intrinsic.h>
#endif

#include "misc.h"
#include <string>
using namespace std;

/*

1999.04.23 リスナーは，ある程度大きめの単位で括る。

(1)
    GdkEventButton -> ButtonListener
    GdkEventMotion -> MotionListener
(2)
    button_press, motion_notify -> MouseListener

class RealizeListener
{
public:
    virtual void onRealized() = 0;
};

class ButtonListener
{
public:
    virtual gint button_press_event_impl(GdkEventButton* event) = 0;
    virtual gint button_release_event_impl(GdkEventButton* event) = 0;
};


Gtk--は，
   gint delete_event_impl(GdkEventAny *) { // (5)
      Gtk_Main::instance()->quit();
      return 0;
   }
みたく，イベント名 + _impl()
*/

//////////////////////////////////////////////////////////////////////
// CWindow

class CWindow
{
protected:
#ifdef __GTK_H__
    GtkWindow* window;
    int to_allocate;
    CRect alloc;
        // GtkWidget::allocationはrealize中に更新されるため，自前で持つ
#else
    Widget window;
#endif

public:
    virtual ~CWindow();

#ifdef __GTK_H__
    GtkWidget* getWidget() const;
#endif

    virtual int setVisible(bool );
    bool isVisible() const;         // GTK_WIDGET_VISIBLE()
#ifdef __GTK_H__
    virtual void add(GtkWidget* child);         // gtk_container_add()
        // TODO: add(Component* )に変更。
        // ウィンドウなど，コンテナだがコンポーネントではないものがある。
#else
    virtual void add(Window child);
#endif
    
    virtual void setTitle(const string& title);  // gtk_window_set_title()
    virtual void destroy();         // gtk_object_destroy()
    virtual CPoint getLocation() const;
    virtual void setLocation(const CPoint& pt);
    virtual void setSize(int width, int height);
    virtual void setBounds(const CRect& r);

protected:
    CWindow();
    
private:
    CWindow(const CWindow& );               // not implement
    CWindow& operator = (const CWindow& );  // not implement
};

//////////////////////////////////////////////////////////////////////
// PopupWindow

class PopupWindow: public CWindow
{
    typedef CWindow super;
public:
    PopupWindow();
    virtual ~PopupWindow();

    virtual int setVisible(bool );
};

#endif  // QSLIB_WINDOW
